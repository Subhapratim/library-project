package com.example.Demo1.Exception;

import com.example.Demo1.Entity.User;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@ControllerAdvice
public class GlobalExceptionHandler {
    //StandardServletMultipartResolver
    @ExceptionHandler(MultipartException.class)
    public String handleError1(MultipartException e, RedirectAttributes redirectAttributes, HttpServletRequest request) {

//        String context = request.getRequestURI();
//        User user = (User) request.getSession().getAttribute("UserSession");
//        String sessionName = user.roleId.roleName;
//        System.out.println(context+"/"+sessionName);
        redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
        return "redirect:/error";

    }

    //CommonsMultipartResolver
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleError2(MaxUploadSizeExceededException e, RedirectAttributes redirectAttributes, HttpServletRequest request) {

        
        String context = request.getRequestURI().substring(6);
//        User user = (User) request.getSession().getAttribute("UserSession");
//        String sessionName = user.roleId.roleName;
//        System.out.println(context);
        redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
        return "redirect:"+context;

    }
}
