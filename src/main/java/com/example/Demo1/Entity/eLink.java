package com.example.Demo1.Entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "eLink")
public class eLink extends BaseDomain{
    
    @Column(name = "topic")
    private String topic;
    @Column(name = "subject")
    private String subject;
    @Column(name = "description")
    private String description;
    @Column(name = "url")
    private String url;
    @Column(name = "isActive", columnDefinition="tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "createDate")
    private Date createDate = new Date();
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "updateDate")
    private Date updateDate = new Date();

    public eLink() {
    }

    public eLink(String topic, String subject, String description, String url) {
        this.topic = topic;
        this.subject = subject;
        this.description = description;
        this.url = url;
    }

    public String getTopic() {
        return topic;
    }
    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isIsActive() {
        return isActive;
    }
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    } 
    
}
