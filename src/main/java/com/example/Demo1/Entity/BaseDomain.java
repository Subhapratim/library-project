package com.example.Demo1.Entity;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseDomain implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Long Id;

    public BaseDomain() {
        super();
    }

    public BaseDomain(Long id) {
        super();
        Id = id;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    @SuppressWarnings({"JpaModelErrorInspection"})
    public boolean isNew() {
        return (Id == null);
    }
}
