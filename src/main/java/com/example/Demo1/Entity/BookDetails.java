package com.example.Demo1.Entity;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "BookDetails")
public class BookDetails extends BaseDomain{
    
    @Column(name = "noOfTotalBook")
    private int noOfTotalBook = 0;
    @Column(name = "noOfAvailableBook")
    private int noOfAvailableBook = 0;
    @Column(name = "noOfAllocateBook")
    private int noOfAllocateBook = 0;
    @Column(name = "noOfBookBinding")
    private int noOfBookBinding = 0;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bookId")
    public Book bookId;
    
    @Column(name = "isActive", columnDefinition="tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "createDate")
    private Date createDate = new Date();
    @Temporal(value = TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "updateDate")
    private Date updateDate = new Date();

    public BookDetails() {
    }

    public BookDetails(int noOfTotalBook, int noOfAvailableBook, int noOfAllocateBook, int noOfBookBinding) {
        this.noOfTotalBook = noOfTotalBook;
        this.noOfAvailableBook = noOfAvailableBook;
        this.noOfAllocateBook = noOfAllocateBook;
        this.noOfBookBinding = noOfBookBinding;
    }

    public int getNoOfTotalBook() {
        return noOfTotalBook;
    }

    public void setNoOfTotalBook(int noOfTotalBook) {
        this.noOfTotalBook = noOfTotalBook;
    }

    public int getNoOfAvailableBook() {
        return noOfAvailableBook;
    }

    public void setNoOfAvailableBook(int noOfAvailableBook) {
        this.noOfAvailableBook = noOfAvailableBook;
    }

    public int getNoOfAllocateBook() {
        return noOfAllocateBook;
    }

    public void setNoOfAllocateBook(int noOfAllocateBook) {
        this.noOfAllocateBook = noOfAllocateBook;
    }

    public int getNoOfBookBinding() {
        return noOfBookBinding;
    }

    public void setNoOfBookBinding(int noOfBookBinding) {
        this.noOfBookBinding = noOfBookBinding;
    }
    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Book getBookId() {
        return bookId;
    }

    public void setBookId(Book bookId) {
        this.bookId = bookId;
    }
}
