package com.example.Demo1.Entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "book")
public class Book extends BaseDomain{
    
    @Column(name = "libraryCode")
    private String libraryCode; 
    @Column(name = "accessionNo", unique = true)
    private String accessionNo;
    @Column(name = "callNo")
    private String callNo;   
    @Column(name = "publisherName")
    private String publisherName;   
    @Column(name = "subject")
    private String subject;
    @Column(name = "bookName")
    private String bookName;
    @Column(name = "authorName")
    private String authorName;
    @Column(name = "edition")
    private String edition;
    @Column(name = "volumeNo")
    private String volumeNo;
    @Column(name = "isActive", columnDefinition="tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "createDate")
    private Date createDate = new Date();
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "updateDate")
    private Date updateDate = new Date();

    public Book() {
    }

    public Book(String libraryCode, String accessionNo, String callNo, String publisherName, String subject, String bookName, String authorName, String edition, String volumeNo) {
        this.libraryCode = libraryCode;
        this.accessionNo = accessionNo;
        this.callNo = callNo;
        this.publisherName = publisherName;
        this.subject = subject;
        this.bookName = bookName;
        this.authorName = authorName;
        this.edition = edition;
        this.volumeNo = volumeNo;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getAccessionNo() {
        return accessionNo;
    }
    public void setAccessionNo(String accessionNo) {
        this.accessionNo = accessionNo;
    }

    public String getCallNo() {
        return callNo;
    }
    public void setCallNo(String callNo) {
        this.callNo = callNo;
    }

    public String getBookName() {
        return bookName;
    }
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
 
    public String getPublisherName() {
        return publisherName;
    }
    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getEdition() {
        return edition;
    }
    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getVolumeNo() {
        return volumeNo;
    }
    public void setVolumeNo(String volumeNo) {
        this.volumeNo = volumeNo;
    }

    public boolean isIsActive() {
        return isActive;
    }
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
}
