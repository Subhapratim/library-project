package com.example.Demo1.Entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "libraryDetails")
public class LibraryDetails extends BaseDomain {

    @Column(name = "libraryCode")
    private String libraryCode;
    @Column(name = "libraryName")
    private String libraryName;
    @Column(name = "librarianName")
    private String librarianName;
    @Column(name = "attachment")
    private String attachment;
    @Column(name = "streetNo")
    private String streetNo;
    @Column(name = "village")
    private String village;
    @Column(name = "municipality")
    private String municipality;
    @Column(name = "postOffice")
    private String postOffice;
    @Column(name = "policeStation")
    private String policeStation;
    @Column(name = "subDivisionName")
    private String subDivisionName;
    @Column(name = "district")
    private String district;
    @Column(name = "stateName")
    private String stateName;
    @Column(name = "pinCode")
    private String pinCode;
    @Column(name = "latitude")
    private String latitude;
    @Column(name = "longitude")
    private String longitude;
    @Column(name = "totalPermanentStuff")
    private String totalPermanentStuff;
    @Column(name = "totalCasualStuff")
    private String totalCasualStuff;
    @Column(name = "totalBooks")
    private String totalBooks;
    @Column(name = "totalLibraryArea")
    private String totalLibraryArea;
    @Lob
    @Column(name = "courseOffered", columnDefinition = "TEXT")
    private String courseOffered;
    @Lob
    @Column(name = "otherFacilities", columnDefinition = "TEXT")
    private String otherFacilities;
    @Lob
    @Column(name = "contactNumber", columnDefinition = "TEXT")
    private String contactNumber;
    @Lob
    @Column(name = "emailId", columnDefinition = "TEXT")
    private String emailId;

    @Column(name = "isActive", columnDefinition = "tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "createDate")
    private Date createDate = new Date();
    @Temporal(value = TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "updateDate")
    private Date updateDate = new Date();

    public LibraryDetails() {
    }

    public LibraryDetails(String libraryCode, String libraryName, String librarianName, String attachment, String streetNo, String village, String municipality, String postOffice, String policeStation, String subDivisionName, String district, String stateName, String pinCode, String latitude, String longitude, String totalPermanentStuff, String totalCasualStuff, String totalBooks, String totalLibraryArea, String courseOffered, String otherFacilities, String contactNumber, String emailId) {
        this.libraryCode = libraryCode;
        this.libraryName = libraryName;
        this.librarianName = librarianName;
        this.attachment = attachment;
        this.streetNo = streetNo;
        this.village = village;
        this.municipality = municipality;
        this.postOffice = postOffice;
        this.policeStation = policeStation;
        this.subDivisionName = subDivisionName;
        this.district = district;
        this.stateName = stateName;
        this.pinCode = pinCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.totalPermanentStuff = totalPermanentStuff;
        this.totalCasualStuff = totalCasualStuff;
        this.totalBooks = totalBooks;
        this.totalLibraryArea = totalLibraryArea;
        this.courseOffered = courseOffered;
        this.otherFacilities = otherFacilities;
        this.contactNumber = contactNumber;
        this.emailId = emailId;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibrarianName() {
        return librarianName;
    }

    public void setLibrarianName(String librarianName) {
        this.librarianName = librarianName;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }
    
    

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public void setPostOffice(String postOffice) {
        this.postOffice = postOffice;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getSubDivisionName() {
        return subDivisionName;
    }

    public void setSubDivisionName(String subDivisionName) {
        this.subDivisionName = subDivisionName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
    
    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTotalPermanentStuff() {
        return totalPermanentStuff;
    }

    public void setTotalPermanentStuff(String totalPermanentStuff) {
        this.totalPermanentStuff = totalPermanentStuff;
    }

    public String getTotalCasualStuff() {
        return totalCasualStuff;
    }

    public void setTotalCasualStuff(String totalCasualStuff) {
        this.totalCasualStuff = totalCasualStuff;
    }

    public String getTotalBooks() {
        return totalBooks;
    }

    public void setTotalBooks(String totalBooks) {
        this.totalBooks = totalBooks;
    }

    public String getTotalLibraryArea() {
        return totalLibraryArea;
    }

    public void setTotalLibraryArea(String totalLibraryArea) {
        this.totalLibraryArea = totalLibraryArea;
    }

    public String getCourseOffered() {
        return courseOffered;
    }

    public void setCourseOffered(String courseOffered) {
        this.courseOffered = courseOffered;
    }

    public String getOtherFacilities() {
        return otherFacilities;
    }

    public void setOtherFacilities(String otherFacilities) {
        this.otherFacilities = otherFacilities;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    
}
