package com.example.Demo1.Entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "course")
public class Course extends BaseDomain {

    @Column(name = "courseType")
    private String courseType;
    @Column(name = "education")
    private String education;
    @Column(name = "stream")
    private String stream;
    @Lob
    @Column(name="description",columnDefinition = "TEXT")
    private String description;
    @Column(name = "attachment")
    private String attachment;
    @Column(name = "url")
    private String url;
    
    @Column(name = "addedBy")
    private String addedBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "addedDate")
    private Date addedDate;
    
    @Column(name = "forwardBy")
    private String forwardBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "forwardDate")
    private Date forwardDate;
    @Column(name = "forwardStatus", columnDefinition = "tinyint(1) default 0")
    private boolean forwardStatus = false;
    
    @Column(name = "rejectBy")
    private String rejectBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "rejectDate")
    private Date rejectDate;
    @Column(name = "rejectStatus", columnDefinition = "tinyint(1) default 0")
    private boolean rejectStatus = false;
    
    @Column(name = "approvedBy")
    private String approvedBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "approvedDate")
    private Date approvedDate;
    @Column(name = "approvedStatus", columnDefinition = "tinyint(1) default 0")
    private boolean approvedStatus = false;
    
    @Column(name = "forwardTo")
    private String forwardTo;
    
//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "roleId")
//    private Role accessableArea;
    
    @Column(name = "isActive", columnDefinition = "tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "createDate")
    private Date createDate = new Date();
    @Temporal(value = TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "updateDate")
    private Date updateDate = new Date();

    public Course() {
    }

    public Course(String courseType, String education, String stream, String description, String attachment, String url, String addedBy, Date addedDate, String forwardBy, Date forwardDate, String rejectBy, Date rejectDate, String approvedBy, Date approvedDate, String forwardTo) {
        this.courseType = courseType;
        this.education = education;
        this.stream = stream;
        this.description = description;
        this.attachment = attachment;
        this.url = url;
        this.addedBy = addedBy;
        this.addedDate = addedDate;
        this.forwardBy = forwardBy;
        this.forwardDate = forwardDate;
        this.rejectBy = rejectBy;
        this.rejectDate = rejectDate;
        this.approvedBy = approvedBy;
        this.approvedDate = approvedDate;
        this.forwardTo = forwardTo;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getForwardBy() {
        return forwardBy;
    }

    public void setForwardBy(String forwardBy) {
        this.forwardBy = forwardBy;
    }

    public Date getForwardDate() {
        return forwardDate;
    }

    public void setForwardDate(Date forwardDate) {
        this.forwardDate = forwardDate;
    }

    public boolean isForwardStatus() {
        return forwardStatus;
    }

    public void setForwardStatus(boolean forwardStatus) {
        this.forwardStatus = forwardStatus;
    }

    public String getRejectBy() {
        return rejectBy;
    }

    public void setRejectBy(String rejectBy) {
        this.rejectBy = rejectBy;
    }

    public Date getRejectDate() {
        return rejectDate;
    }

    public void setRejectDate(Date rejectDate) {
        this.rejectDate = rejectDate;
    }

    public boolean isRejectStatus() {
        return rejectStatus;
    }

    public void setRejectStatus(boolean rejectStatus) {
        this.rejectStatus = rejectStatus;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public boolean isApprovedStatus() {
        return approvedStatus;
    }

    public void setApprovedStatus(boolean approvedStatus) {
        this.approvedStatus = approvedStatus;
    }

    public String getForwardTo() {
        return forwardTo;
    }

    public void setForwardTo(String forwardTo) {
        this.forwardTo = forwardTo;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

}
