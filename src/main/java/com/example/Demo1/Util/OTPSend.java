package com.example.Demo1.Util;

import java.util.HashMap;
import javax.servlet.http.HttpSession;

public class OTPSend {
    MailSend mainSend = new MailSend();
    public static void sendOTPMail(HttpSession session, String userName, String otp, String email) {
        try {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("mail_heading", "E-Subechha - One Time Password");
            hashMap.put("mail_content", "<p>Dear " + userName + ",</p>"
                    + "<p>Thank You For Registering on E-Subechha Portal.</p>"
                    + "<p>Please Use " + otp + " to complete your registration process.</p>"
                    + "<p>Please do not share this one time password with anyone else.</p>");
            hashMap.put("mail_button_text1", "No button");

            MailSend.sendHtmlTemplateMail(session, hashMap, email, "E-Subechha - One Time Password");
        } catch (Exception ex) {
        }
    }
}
