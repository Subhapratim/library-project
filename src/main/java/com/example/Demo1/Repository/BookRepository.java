package com.example.Demo1.Repository;

import com.example.Demo1.Entity.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book, Long>{

    @Query("from Book where id=?1")
    public Book findBookById(Long id);

    @Query("from Book where subject=?1 or bookName=?2 or publisherName=?3 or authorName=?4 or volumeNo=?5")
    public Iterable<Book> findBook(String subject, String bookName, String publisherName, String authorName, String volumeNo);

    @Query("from Book where libraryCode=?1")
    public Iterable<Book> findTotalBooks(String libraryCode);
    
}
