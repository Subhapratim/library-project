package com.example.Demo1.Repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.Demo1.Entity.Role;

@Repository()
public interface RoleRepository extends CrudRepository<Role, Long> {

	@Query("from Role where roleName=?1")
	Role getRoleIdByRoleName(String roleName);

}