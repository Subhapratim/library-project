package com.example.Demo1.Repository;

import com.example.Demo1.Entity.Course;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long> {

    @Query("from Course where courseType=?1 or education=?2 or stream=?3 or addedBy=?4")
    public Iterable<Course> findCourseBy(String courseType, String education, String stream, String addedBy);

    @Query("from Course where id=?1")
    public Course findCourseBy(Long id);

}
