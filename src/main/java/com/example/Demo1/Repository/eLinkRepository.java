package com.example.Demo1.Repository;

import com.example.Demo1.Entity.eLink;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface eLinkRepository extends CrudRepository<eLink, Long>{
    
}
