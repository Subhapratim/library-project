package com.example.Demo1.Repository;

import com.example.Demo1.Entity.Book;
import com.example.Demo1.Entity.BookDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookDetailsRepository extends CrudRepository<BookDetails, Long>{

    @Query("from BookDetails where bookId=?1")
    public BookDetails findBookById(Book book);
    
}
