package com.example.Demo1.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.Demo1.Entity.User;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface AdminRepository extends CrudRepository<User, Long> {

    @Query("from User where id=?1")
    User findUserById(Long id);
    
}
