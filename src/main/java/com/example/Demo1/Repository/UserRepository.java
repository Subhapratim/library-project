package com.example.Demo1.Repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.Demo1.Entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("from User where Email=?1 and Password=?2")
    User validateEmail(String Email, String Password);

    @Query("from User where Email=?1")
    public User findUserByEmail(String email);

}
