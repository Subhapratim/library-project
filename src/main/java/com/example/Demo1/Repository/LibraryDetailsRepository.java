package com.example.Demo1.Repository;

import com.example.Demo1.Entity.LibraryDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibraryDetailsRepository extends CrudRepository<LibraryDetails, Long>{
    
}
