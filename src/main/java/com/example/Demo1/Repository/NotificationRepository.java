package com.example.Demo1.Repository;

import com.example.Demo1.Entity.Notification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long>{

    @Query("from Notification where forwardBy=?1 and notificationDate between ?2 and ?3")
    public Iterable<Notification> findNotification(String addedBy, String startDate, String endDate);

    @Query("from Notification where Id=?1")
    public Notification findNotificationById(Long Id);
    
}
