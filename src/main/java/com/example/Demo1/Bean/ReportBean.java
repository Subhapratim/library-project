package com.example.Demo1.Bean;

import org.springframework.web.multipart.MultipartFile;

public class ReportBean {
    
    private Long id;
    private String reportType;
    private String libraryCode;
    private String libraryName;
    private String librarianName;
    private String attachmentPath;
    private MultipartFile attachmentFile;
    
    private String type;

    public ReportBean() {
    }

    public ReportBean(Long id, String reportType, String libraryCode, String libraryName, String librarianName, String attachmentPath, MultipartFile attachmentFile,String type) {
        this.id = id;
        this.reportType = reportType;
        this.libraryCode = libraryCode;
        this.libraryName = libraryName;
        this.librarianName = librarianName;
        this.attachmentPath = attachmentPath;
        this.attachmentFile = attachmentFile;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibrarianName() {
        return librarianName;
    }

    public void setLibrarianName(String librarianName) {
        this.librarianName = librarianName;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public MultipartFile getAttachmentFile() {
        return attachmentFile;
    }

    public void setAttachmentFile(MultipartFile attachmentFile) {
        this.attachmentFile = attachmentFile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
