package com.example.Demo1.Bean;

import org.springframework.web.multipart.MultipartFile;

public class CourseBean {
    
    private Long id;
    private String courseType;
    private String education;
    private String stream;
    private String description;
    private String attachmentFilePath;
    private MultipartFile attachmentFile;
    private String url;
    private String addedBy;
    private String forwardTo;
    
    public CourseBean() {
    }

    public CourseBean(Long id, String courseType, String education, String stream, String description, String attachmentFilePath, MultipartFile attachmentFile, String url, String addedBy, String forwardTo) {
        this.id = id;
        this.courseType = courseType;
        this.education = education;
        this.stream = stream;
        this.description = description;
        this.attachmentFilePath = attachmentFilePath;
        this.attachmentFile = attachmentFile;
        this.url = url;
        this.addedBy = addedBy;
        this.forwardTo = forwardTo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAttachmentFilePath() {
        return attachmentFilePath;
    }

    public void setAttachmentFilePath(String attachmentFilePath) {
        this.attachmentFilePath = attachmentFilePath;
    }

    public MultipartFile getAttachmentFile() {
        return attachmentFile;
    }

    public void setAttachmentFile(MultipartFile attachmentFile) {
        this.attachmentFile = attachmentFile;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getForwardTo() {
        return forwardTo;
    }

    public void setForwardTo(String forwardTo) {
        this.forwardTo = forwardTo;
    }
    
    
}
