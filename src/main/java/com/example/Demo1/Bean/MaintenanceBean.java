package com.example.Demo1.Bean;

import org.springframework.web.multipart.MultipartFile;

public class MaintenanceBean {
    private Long id;
    private String maintenanceType;
    private String libraryCode;
    private String libraryName;
    private String librarianName;
    private String itemName;
    private String totalCost;
    private String attachmentPath;
    private MultipartFile attachmentFile;

    public MaintenanceBean() {
    }

    public MaintenanceBean(Long id, String maintenanceType, String libraryCode, String libraryName, String librarianName, String itemName, String totalCost, String attachmentPath, MultipartFile attachmentFile) {
        this.id = id;
        this.maintenanceType = maintenanceType;
        this.libraryCode = libraryCode;
        this.libraryName = libraryName;
        this.librarianName = librarianName;
        this.itemName = itemName;
        this.totalCost = totalCost;
        this.attachmentPath = attachmentPath;
        this.attachmentFile = attachmentFile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMaintenanceType() {
        return maintenanceType;
    }

    public void setMaintenanceType(String maintenanceType) {
        this.maintenanceType = maintenanceType;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibrarianName() {
        return librarianName;
    }

    public void setLibrarianName(String librarianName) {
        this.librarianName = librarianName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public MultipartFile getAttachmentFile() {
        return attachmentFile;
    }

    public void setAttachmentFile(MultipartFile attachmentFile) {
        this.attachmentFile = attachmentFile;
    }
}
