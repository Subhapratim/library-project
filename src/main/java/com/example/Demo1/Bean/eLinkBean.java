package com.example.Demo1.Bean;

public class eLinkBean {
    
    private Long Id;
    private String topic;
    private String subject;
    private String description;
    private String url;

    public eLinkBean() {
    }

    public eLinkBean(Long Id, String topic, String subject, String description, String url) {
        this.Id = Id;
        this.topic = topic;
        this.subject = subject;
        this.description = description;
        this.url = url;
    }

    public Long getId() {
        return Id;
    }
    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getTopic() {
        return topic;
    }
    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
   
}
