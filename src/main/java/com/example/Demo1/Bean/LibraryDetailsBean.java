package com.example.Demo1.Bean;

import org.springframework.web.multipart.MultipartFile;

public class LibraryDetailsBean {

    private String libraryCode;
    private String libraryName;
    private String librarianName;
    private String attachmentPath;
    private MultipartFile attachmentFile;
    //Address
    private String streetNo;
    private String village;
    private String municipality;
    private String postOffice;
    private String policeStation;
    private String subDivisionName;
    private String district;
    private String stateName;
    private String pinCode;
    private String latitude;
    private String longitude;
    //Other Details
    private String totalPermanentStuff;
    private String totalCasualStuff;
    private String totalBooks;
    private String totalLibraryArea;
    private String courseOffered;
    private String otherFacilities;
    private String contactNumber;
    private String emailId;
    

    public LibraryDetailsBean() {
    }

    public LibraryDetailsBean(String libraryCode, String libraryName, String librarianName, String attachmentPath, MultipartFile attachmentFile, String streetNo, String village, String municipality, String postOffice, String policeStation, String subDivisionName, String district, String stateName, String pinCode, String latitude, String longitude, String totalPermanentStuff, String totalCasualStuff, String totalBooks, String totalLibraryArea, String courseOffered, String otherFacilities, String contactNumber, String emailId) {
        this.libraryCode = libraryCode;
        this.libraryName = libraryName;
        this.librarianName = librarianName;
        this.attachmentPath = attachmentPath;
        this.attachmentFile = attachmentFile;
        this.streetNo = streetNo;
        this.village = village;
        this.municipality = municipality;
        this.postOffice = postOffice;
        this.policeStation = policeStation;
        this.subDivisionName = subDivisionName;
        this.district = district;
        this.stateName = stateName;
        this.pinCode = pinCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.totalPermanentStuff = totalPermanentStuff;
        this.totalCasualStuff = totalCasualStuff;
        this.totalBooks = totalBooks;
        this.totalLibraryArea = totalLibraryArea;
        this.courseOffered = courseOffered;
        this.otherFacilities = otherFacilities;
        this.contactNumber = contactNumber;
        this.emailId = emailId;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibrarianName() {
        return librarianName;
    }

    public void setLibrarianName(String librarianName) {
        this.librarianName = librarianName;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public MultipartFile getAttachmentFile() {
        return attachmentFile;
    }

    public void setAttachmentFile(MultipartFile attachmentFile) {
        this.attachmentFile = attachmentFile;
    }
    
    

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public void setPostOffice(String postOffice) {
        this.postOffice = postOffice;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getSubDivisionName() {
        return subDivisionName;
    }

    public void setSubDivisionName(String subDivisionName) {
        this.subDivisionName = subDivisionName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTotalPermanentStuff() {
        return totalPermanentStuff;
    }

    public void setTotalPermanentStuff(String totalPermanentStuff) {
        this.totalPermanentStuff = totalPermanentStuff;
    }

    public String getTotalCasualStuff() {
        return totalCasualStuff;
    }

    public void setTotalCasualStuff(String totalCasualStuff) {
        this.totalCasualStuff = totalCasualStuff;
    }

    public String getTotalBooks() {
        return totalBooks;
    }

    public void setTotalBooks(String totalBooks) {
        this.totalBooks = totalBooks;
    }

    public String getTotalLibraryArea() {
        return totalLibraryArea;
    }

    public void setTotalLibraryArea(String totalLibraryArea) {
        this.totalLibraryArea = totalLibraryArea;
    }

    public String getCourseOffered() {
        return courseOffered;
    }

    public void setCourseOffered(String courseOffered) {
        this.courseOffered = courseOffered;
    }

    public String getOtherFacilities() {
        return otherFacilities;
    }

    public void setOtherFacilities(String otherFacilities) {
        this.otherFacilities = otherFacilities;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    
}
