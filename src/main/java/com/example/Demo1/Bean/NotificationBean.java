package com.example.Demo1.Bean;

import org.springframework.web.multipart.MultipartFile;

public class NotificationBean {
    
    private Long Id;
    private String subject;
    private String sendTo;
    private String forwardBy;
    private String notificationDate;
    private String Date;
    private String notificationTime;
    private String description;
    private String attachmentPath;
    private MultipartFile attachmentFile;
    
    private String type;

    public NotificationBean() {
    }

    public NotificationBean(Long Id, String subject, String sendTo, String forwardBy, String notificationDate, String Date, String notificationTime, String description, String attachmentPath, MultipartFile attachmentFile, String type) {
        this.Id = Id;
        this.subject = subject;
        this.sendTo = sendTo;
        this.forwardBy = forwardBy;
        this.notificationDate = notificationDate;
        this.Date = Date;
        this.notificationTime = notificationTime;
        this.description = description;
        this.attachmentPath = attachmentPath;
        this.attachmentFile = attachmentFile;
        this.type = type;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSendTo() {
        return sendTo;
    }
    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getForwardBy() {
        return forwardBy;
    }
    public void setForwardBy(String forwardBy) {
        this.forwardBy = forwardBy;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public MultipartFile getAttachmentFile() {
        return attachmentFile;
    }

    public void setAttachmentFile(MultipartFile attachmentFile) {
        this.attachmentFile = attachmentFile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
