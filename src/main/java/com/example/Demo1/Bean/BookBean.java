package com.example.Demo1.Bean;

import com.example.Demo1.Entity.Book;
import com.example.Demo1.Entity.BookDetails;

public class BookBean {
    
    private Long id;
    private String libraryCode;
    private String accessionNo;
    private String callNo;  
    private String publisherName;
    private String subject;
    private String bookName;
    private String authorName;
    private String edition;
    private String volumeNo;
    
    private int noOfTotalBook;
    private int noOfAvailableBook;
    private int noOfBookBinding;
    private int noOfAllocateBook;
    
    public BookBean() {
        
    }

    public BookBean(Long id, String libraryCode, String accessionNo, String callNo, String publisherName, String subject, String bookName, String authorName, String edition, String volumeNo, int noOfTotalBook, int noOfAvailableBook, int noOfBookBinding, int noOfAllocateBook) {
        this.id = id;
        this.libraryCode = libraryCode;
        this.accessionNo = accessionNo;
        this.callNo = callNo;
        this.publisherName = publisherName;
        this.subject = subject;
        this.bookName = bookName;
        this.authorName = authorName;
        this.edition = edition;
        this.volumeNo = volumeNo;
        this.noOfTotalBook = noOfTotalBook;
        this.noOfAvailableBook = noOfAvailableBook;
        this.noOfBookBinding = noOfBookBinding;
        this.noOfAllocateBook = noOfAllocateBook;
    }

    public BookBean(Book book, BookDetails bookDetails) {
        this.id = book.getId();
        this.libraryCode = book.getLibraryCode();
        this.accessionNo = book.getAccessionNo();
        this.callNo = book.getCallNo();
        this.publisherName = book.getPublisherName();
        this.subject = book.getSubject();
        this.bookName = book.getBookName();
        this.authorName = book.getAuthorName();
        this.edition = book.getEdition();
        this.volumeNo = book.getVolumeNo();
        this.noOfTotalBook = bookDetails.getNoOfTotalBook();
        this.noOfAvailableBook = bookDetails.getNoOfAvailableBook();
        this.noOfBookBinding = bookDetails.getNoOfBookBinding();
        this.noOfAllocateBook = bookDetails.getNoOfAllocateBook();
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getAccessionNo() {
        return accessionNo;
    }
    public void setAccessionNo(String accessionNo) {
        this.accessionNo = accessionNo;
    }

    public String getCallNo() {
        return callNo;
    }
    public void setCallNo(String callNo) {
        this.callNo = callNo;
    }

    public String getBookName() {
        return bookName;
    }
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
    
    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getPublisherName() {
        return publisherName;
    }
    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getEdition() {
        return edition;
    }
    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    
    public String getVolumeNo() {
        return volumeNo;
    }
    public void setVolumeNo(String volumeNo) {
        this.volumeNo = volumeNo;
    }

    public int getNoOfTotalBook() {
        return noOfTotalBook;
    }
    public void setNoOfTotalBook(int noOfTotalBook) {
        this.noOfTotalBook = noOfTotalBook;
    }

    public int getNoOfAvailableBook() {
        return noOfAvailableBook;
    }
    public void setNoOfAvailableBook(int noOfAvailableBook) {
        this.noOfAvailableBook = noOfAvailableBook;
    }

    public int getNoOfBookBinding() {
        return noOfBookBinding;
    }
    public void setNoOfBookBinding(int noOfBookBinding) {
        this.noOfBookBinding = noOfBookBinding;
    }

    public int getNoOfAllocateBook() {
        return noOfAllocateBook;
    }
    public void setNoOfAllocateBook(int noOfAllocateBook) {
        this.noOfAllocateBook = noOfAllocateBook;
    }
    
}