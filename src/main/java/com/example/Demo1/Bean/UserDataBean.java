package com.example.Demo1.Bean;

import java.util.List;

public class UserDataBean<T> {

    private List<T> data;

    public List<T> getData() {
        return data;
    }
    public void setData(List<T> data) {
        this.data = data;
    }

}
