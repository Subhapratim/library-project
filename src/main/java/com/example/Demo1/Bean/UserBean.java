package com.example.Demo1.Bean;

import org.springframework.web.multipart.MultipartFile;

public class UserBean {
	
	private Long id;
	private String username;
	private String email;
	private String password;
	private String imagePath;
	private MultipartFile imageUser;
	
	public UserBean() {
		super();
	}
	
	public UserBean(Long id, String username, String email, String password, String imagePath,
			MultipartFile imageUser) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.imagePath = imagePath;
		this.imageUser = imageUser;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public MultipartFile getImageUser() {
		return imageUser;
	}
	public void setImageUser(MultipartFile imageUser) {
		this.imageUser = imageUser;
	}
}
