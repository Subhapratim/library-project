package com.example.Demo1.Service;

import com.example.Demo1.Bean.BookBean;
import com.example.Demo1.Bean.CourseBean;
import com.example.Demo1.Bean.LibraryDetailsBean;
import com.example.Demo1.Bean.MaintenanceBean;
import com.example.Demo1.Bean.NotificationBean;
import com.example.Demo1.Bean.ReportBean;
import com.example.Demo1.Bean.eLinkBean;
import com.example.Demo1.Entity.Book;
import com.example.Demo1.Entity.BookDetails;
import com.example.Demo1.Entity.Course;
import com.example.Demo1.Entity.LibraryDetails;
import com.example.Demo1.Entity.Maintenance;
import com.example.Demo1.Entity.Notification;
import com.example.Demo1.Entity.Report;
import com.example.Demo1.Entity.User;
import com.example.Demo1.Entity.eLink;
import com.example.Demo1.Repository.AdminRepository;
import com.example.Demo1.Repository.BookDetailsRepository;
import com.example.Demo1.Repository.BookRepository;
import com.example.Demo1.Repository.CourseRepository;
import com.example.Demo1.Repository.LibraryDetailsRepository;
import com.example.Demo1.Repository.MaintenanceRepository;
import com.example.Demo1.Repository.NotificationRepository;
import com.example.Demo1.Repository.ReportRepository;
import com.example.Demo1.Repository.UserRepository;
import com.example.Demo1.Repository.eLinkRepository;
import com.example.Demo1.UtilityCustomPackage.BookSearchList;
import com.example.Demo1.UtilityCustomPackage.CourseSearchList;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookDetailsRepository bookDetailsRepository;
    @Autowired
    private eLinkRepository elinkRepository;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private MaintenanceRepository maintenanceRepository;
    @Autowired
    private LibraryDetailsRepository libraryDetailsRepository;

    public List<User> getAllUser() {
        List<User> list = new ArrayList<>();

        for (User user : adminRepository.findAll()) {
            if (user.getRoleId().roleName.equals("User")) {
                if (user.isIsActive() == true) {
                    list.add(user);
                }
            }
        }
        return list;
    }

    public String userApproval(Long id, HttpSession session, HttpServletRequest request) {
        User user = adminRepository.findUserById(id);
        if (user != null) {
            user.setStatus(1);
            userRepository.save(user);
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    public String userNonApprove(Long id, HttpSession session, HttpServletRequest request) {
        User user = adminRepository.findUserById(id);
        if (user != null) {
            user.setStatus(-1);
            userRepository.save(user);
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    public List<User> getAllApprovedUsers() {
        List<User> list = new ArrayList<>();

        for (User user : adminRepository.findAll()) {
            if (user.getRoleId().roleName.equals("User")) {
                if (user.isIsActive() == true) {
                    if (user.getStatus() == 1) {
                        list.add(user);
                    }
                }
            }
        }
        return list;
    }

    public List<User> getAllNonApprovedUsers() {
        List<User> list = new ArrayList<>();

        for (User user : adminRepository.findAll()) {
            if (user.getRoleId().roleName.equals("User")) {
                if (user.isIsActive() == true) {
                    if (user.getStatus() == -1) {
                        list.add(user);
                    }
                }
            }
        }
        return list;
    }

    public String addBooks(BookBean bookbean, HttpServletRequest request) {
        try {
            Book book = new Book();
            BookDetails bookDetails = new BookDetails();
            book.setAccessionNo(bookbean.getAccessionNo());
            book.setCallNo(bookbean.getCallNo());
            book.setPublisherName(bookbean.getPublisherName());
            book.setSubject(bookbean.getSubject());
            book.setBookName(bookbean.getBookName());
            book.setAuthorName(bookbean.getAuthorName());
            book.setEdition(bookbean.getEdition());
            book.setVolumeNo(bookbean.getVolumeNo());
            bookDetails.setNoOfTotalBook(bookbean.getNoOfTotalBook());
            bookDetails.setNoOfAvailableBook(bookbean.getNoOfTotalBook());
            bookDetails.setBookId(book);
            bookRepository.save(book);
            bookDetailsRepository.save(bookDetails);
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }

    }

    public List<BookBean> getAllBooks() {
        List<BookBean> list = new ArrayList<>();
        for (Book book : bookRepository.findAll()) {
            if (book.isIsActive() == true) {
                BookDetails bookDetails = bookDetailsRepository.findBookById(book);
                BookBean bookBean = new BookBean(book, bookDetails);
                list.add(bookBean);
            }
        }
        return list;
    }

    public List<BookBean> searchBook(BookBean bookBean) {
        List<BookBean> resultList = new ArrayList<>();

        String subject = bookBean.getSubject().trim();
        String bookName = bookBean.getBookName().trim();
        String publisherName = bookBean.getPublisherName().trim();
        String authorName = bookBean.getAuthorName().trim();
        String volumeNo = bookBean.getVolumeNo().trim();

        for (Book book : bookRepository.findBook(subject, bookName, publisherName, authorName, volumeNo)) {
            if (book.isIsActive() == true) {
                BookDetails bookDetails = bookDetailsRepository.findBookById(book);
                BookBean bookBean1 = new BookBean(book, bookDetails);
                resultList.add(bookBean1);
            }
        }
        BookSearchList bookSearchList = new BookSearchList();
        List<BookBean> list = bookSearchList.searchBookOrderedList(resultList, bookBean);
        return list;
    }

    public String booksUpdate(BookBean bookbean) {
        Book book = bookRepository.findBookById(bookbean.getId());
        if (book != null) {
            book.setAccessionNo(bookbean.getAccessionNo());
            book.setCallNo(bookbean.getCallNo());
            book.setPublisherName(bookbean.getPublisherName());
            book.setSubject(bookbean.getSubject());
            book.setBookName(bookbean.getBookName());
            book.setAuthorName(bookbean.getAuthorName());
            book.setEdition(bookbean.getEdition());
            book.setVolumeNo(bookbean.getVolumeNo());
            BookDetails bookDetails = bookDetailsRepository.findBookById(book);
            bookDetails.setNoOfTotalBook(bookbean.getNoOfTotalBook());
            bookRepository.save(book);
            bookDetailsRepository.save(bookDetails);
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    public String deleteBook(Long id) {
        Book book = bookRepository.findBookById(id);
        if (book != null) {
            book.setIsActive(false);
            bookRepository.save(book);
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    public String addeLinks(eLinkBean elinkbean, HttpServletRequest request) {
        try {
            eLink elink = new eLink();
            elink.setTopic(elinkbean.getTopic());
            elink.setSubject(elinkbean.getSubject());
            elink.setDescription(elinkbean.getDescription());
            elink.setUrl(elinkbean.getUrl());

            elinkRepository.save(elink);
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    public List<eLink> getAlleLinks() {
        List<eLink> list = new ArrayList<>();
        for (eLink elink : elinkRepository.findAll()) {
            if (elink.isIsActive() == true) {
                list.add(elink);
            }
        }
        return list;
    }

    public String addNotification(NotificationBean notificationBean, HttpSession session, HttpServletRequest request) {
        try {
            Notification notification = new Notification();

            notification.setSubject(notificationBean.getSubject());
            notification.setSendTo(notificationBean.getSendTo());
            switch (notificationBean.getSendTo()) {
                case "Library Members":
                    notification.setNoticeForUser(true);
                    notification.setNoticeForLibrarian(false);
                    notification.setNoticeForAdmin(false);
                    break;
                case "Librarian":
                    notification.setNoticeForUser(false);
                    notification.setNoticeForLibrarian(true);
                    notification.setNoticeForAdmin(false);
                    break;
                case "Admin":
                    notification.setNoticeForUser(false);
                    notification.setNoticeForLibrarian(false);
                    notification.setNoticeForAdmin(true);
                    break;
                case "All":
                    notification.setNoticeForUser(true);
                    notification.setNoticeForLibrarian(true);
                    notification.setNoticeForAdmin(true);
                    break;
            }
            User user = (User) session.getAttribute("UserSession");
            notification.setAddedBy(user.getRoleId().roleName);
            notification.setAddedDate(new Date());

            notification.setForwardBy(user.getRoleId().roleName);

            notification.setNotificationDate(notificationBean.getNotificationDate());
            notification.setNotificationTime(notificationBean.getNotificationTime());
            notification.setDescription(notificationBean.getDescription());

            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            byte[] noticeFile = notificationBean.getAttachmentFile().getBytes();

//            String subject = notificationBean.getSubject().replaceAll("\\s+", "");
//            String Date = new Date().toGMTString().replaceAll("\\s+","-").substring(0, 11);
//            String Date = notificationBean.getNotificationDate().replaceAll("\\/", "");
//            String attachmentName = subject + "_" + Date;
            String attachmentName = notificationBean.getAttachmentFile().getOriginalFilename();

            String noticeFileFolder = path + "/noticeFolder/" + attachmentName;
            notification.setAttachment("/noticeFolder/" + attachmentName);
            FileOutputStream perimageOutFile = new FileOutputStream(noticeFileFolder);
            perimageOutFile.write(noticeFile);

            notificationRepository.save(notification);
            return "SUCCESS";
        } catch (IOException e) {
            return "FAILURE";
        }
    }

    public List<Notification> getAllNotifications() {
        List<Notification> list = new ArrayList<>();
        for (Notification notification : notificationRepository.findAll()) {
            if (notification.isIsActive() == true && notification.isNoticeForLibrarian() == true) {
                list.add(notification);
            }
        }
        return list;
    }

    public List<Notification> searchNotification(NotificationBean notificationBean) {
        List<Notification> list = new ArrayList<>();
        String addedBy = notificationBean.getForwardBy();
        String startDate = notificationBean.getNotificationDate();
        String endDate = notificationBean.getDate();

        for (Notification notification : notificationRepository.findNotification(addedBy, startDate, endDate)) {
            if (notification.isIsActive() == true && notification.isNoticeForLibrarian() == true) {
                list.add(notification);
            }
        }
        return list;
    }

    public String notificationUpdate(NotificationBean notificationBean) {
        Notification notification = notificationRepository.findNotificationById(notificationBean.getId());
        if (notification != null && notification.isIsActive() == true && notification.isNoticeForLibrarian() == true) {
            notification.setForwardTo(notificationBean.getSendTo());
            switch (notificationBean.getSendTo()) {
                case "Admin":
                    notification.setNoticeForAdmin(true);
                    notification.setNoticeForUser(false);
                    break;
                case "Library Members":
                    notification.setNoticeForUser(true);
                    notification.setNoticeForAdmin(true);   //Notification added for Library Members by Librabian can also be seen by Admin 
                    break;
                default:
                    notification.setNoticeForUser(true);
                    notification.setNoticeForAdmin(true);
                    break;
            }
            notification.setForwardBy("Librarian");
            notification.setForwardDate(new Date());
            notification.setForwardStatus(true);
            notificationRepository.save(notification);
            return "SUCCESS";
        }
        return "FAILURE";
    }

    public String deleteNotification(NotificationBean notificationBean) {
        Notification notification = notificationRepository.findNotificationById(notificationBean.getId());
        if (notification != null && notification.isIsActive() == true && notification.isNoticeForLibrarian() == true) {
            notification.setIsActive(false);
            notificationRepository.save(notification);
            return "SUCCESS";
        }
        return "FAILURE";
    }

    public String addCourse(CourseBean courseBean, HttpSession session, HttpServletRequest request) {
        try {
            Course course = new Course();

            course.setCourseType(courseBean.getCourseType());
            course.setEducation(courseBean.getEducation());
            course.setStream(courseBean.getStream());
            course.setDescription(courseBean.getDescription());

            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            byte[] noticeFile = courseBean.getAttachmentFile().getBytes();
//            String subject = courseBean.getEducation().replaceAll("\\s+", "");
//            String Date = new Date().toGMTString().replaceAll("\\s+", "").substring(0, 9);
//            String attachmentName = subject + "_" + Date;
            String attachmentName = courseBean.getAttachmentFile().getOriginalFilename();
            String noticeFileFolder = path + "/noticeFolder/" + attachmentName;
            course.setAttachment("/noticeFolder/" + attachmentName);
            FileOutputStream file = new FileOutputStream(noticeFileFolder);
            file.write(noticeFile);

            course.setUrl(courseBean.getUrl());

            course.setAddedBy("Librarian");
            course.setAddedDate(new Date());

            courseRepository.save(course);
            return "SUCCESS";
        } catch (IOException e) {
            System.out.println("----asdasdaasdasd----");
            return "FAILURE";
        }
    }

    public List getAllCourse() {
        List<Course> list = new ArrayList<>();
        for (Course course : courseRepository.findAll()) {
            if (course.isIsActive() == true) {
                list.add(course);
            }
        }
        return list;
    }

    public List<Course> searchCourse(CourseBean courseBean) {
        List<Course> resultList = new ArrayList<>();
        String courseType = courseBean.getCourseType().trim();
        String education = courseBean.getEducation().trim();
        String stream = courseBean.getStream().trim();
        String addedBy = courseBean.getAddedBy().trim();

        for (Course course : courseRepository.findCourseBy(courseType, education, stream, addedBy)) {
            if (course.isIsActive() == true) {
                resultList.add(course);
            }
        }
        CourseSearchList courseSearchList = new CourseSearchList();
        List<Course> list = courseSearchList.searchCourseOrderedList(resultList, courseBean);
        return list;
    }

    public String courseUpdate(CourseBean courseBean) {
        Course course = courseRepository.findCourseBy(courseBean.getId());
        System.out.println(courseBean.getId());
        if (course != null && course.isIsActive() == true) {
            course.setForwardTo(courseBean.getForwardTo());
            course.setForwardBy("Librarian");
            course.setForwardDate(new Date());
            course.setForwardStatus(true);
            courseRepository.save(course);
            return "SUCCESS";
        }
        return "FAILURE";
    }

    public String deleteCourse(CourseBean courseBean) {
        Course course = courseRepository.findCourseBy(courseBean.getId());
        if (course != null && course.isIsActive() == true) {
            course.setIsActive(false);
            courseRepository.save(course);
            return "SUCCESS";
        }
        return "FAILURE";
    }

    public String reportSubmit(ReportBean reportBean, HttpSession session, HttpServletRequest request) {
        try {
            Report report = new Report();
            
            report.setReportType(reportBean.getReportType());
            report.setLibraryCode(reportBean.getLibraryCode());
            report.setLibraryName(reportBean.getLibraryName());
            report.setLibrarianName(reportBean.getLibrarianName());
            
            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            byte[] reportFile = reportBean.getAttachmentFile().getBytes();
            String attachmentName = reportBean.getAttachmentFile().getOriginalFilename();
            String reportFileFolder = path + "/reportFolder/" + attachmentName;
            report.setAttachment("/reportFolder/" + attachmentName);
            FileOutputStream file = new FileOutputStream(reportFileFolder);
            file.write(reportFile);
            
            report.setType(reportBean.getType());
            
            reportRepository.save(report);
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    public List getAllReports(String requestType) {
        List<Report> list = new ArrayList<>();
        for (Report report : reportRepository.findAll()) {
            if (report.isIsActive() == true && report.getType().equals(requestType)) {
                list.add(report);
            }
        }
        return list;
    }

    public String applyMaintenance(MaintenanceBean maintenanceBean, HttpSession session, HttpServletRequest request) {
        try {
            Maintenance maintenance = new Maintenance();
            
            maintenance.setMaintenanceType(maintenanceBean.getMaintenanceType());
            maintenance.setLibraryCode(maintenanceBean.getLibraryCode());
            maintenance.setLibraryName(maintenanceBean.getLibraryName());
            maintenance.setLibrarianName(maintenanceBean.getLibrarianName());
            maintenance.setItemName(maintenanceBean.getItemName());
            maintenance.setTotalCost(maintenanceBean.getTotalCost().substring(2));
            
            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            byte[] maintenanceGrantFile = maintenanceBean.getAttachmentFile().getBytes();
            String attachmentName = maintenanceBean.getAttachmentFile().getOriginalFilename();
            String maintenanceGrantFileFolder = path + "/maintenanceGrantFolder/" + attachmentName;
            maintenance.setAttachment("/maintenanceGrantFolder/" + attachmentName);
            FileOutputStream file = new FileOutputStream(maintenanceGrantFileFolder);
            file.write(maintenanceGrantFile);
            
            maintenanceRepository.save(maintenance);
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    public List getAllMaintenanceGrant() {
        List<Maintenance> list = new ArrayList<>();
        for (Maintenance maintenance : maintenanceRepository.findAll()) {
            if (maintenance.isIsActive() == true) {
                list.add(maintenance);
            }
        }
        return list;
    }

    public String addLibraryDetails(LibraryDetailsBean libraryDetailsBean, HttpSession session) {
        try {
            LibraryDetails libraryDetails = new LibraryDetails();
            
            libraryDetails.setLibraryCode(libraryDetailsBean.getLibraryCode());
            libraryDetails.setLibraryName(libraryDetailsBean.getLibraryName());
            libraryDetails.setLibrarianName(libraryDetailsBean.getLibrarianName());
            
            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            byte[] libraryImageFile = libraryDetailsBean.getAttachmentFile().getBytes();
            String attachmentName = libraryDetailsBean.getAttachmentFile().getOriginalFilename();
            String libraryImageFileFileFolder = path + "/libraryImage/" + attachmentName;
            libraryDetails.setAttachment("/libraryImage/" + attachmentName);
            FileOutputStream file = new FileOutputStream(libraryImageFileFileFolder);
            file.write(libraryImageFile);
            
            libraryDetails.setStreetNo(libraryDetailsBean.getStreetNo());
            libraryDetails.setVillage(libraryDetailsBean.getVillage());
            libraryDetails.setMunicipality(libraryDetailsBean.getMunicipality());
            libraryDetails.setPostOffice(libraryDetailsBean.getPostOffice());
            libraryDetails.setPoliceStation(libraryDetailsBean.getPoliceStation());
            libraryDetails.setSubDivisionName(libraryDetailsBean.getSubDivisionName());
            libraryDetails.setDistrict(libraryDetailsBean.getDistrict());
            libraryDetails.setStateName(libraryDetailsBean.getStateName());
            libraryDetails.setPinCode(libraryDetailsBean.getPinCode());
            libraryDetails.setLatitude(libraryDetailsBean.getLatitude());
            libraryDetails.setLongitude(libraryDetailsBean.getLongitude());
            
            libraryDetails.setTotalPermanentStuff(libraryDetailsBean.getTotalPermanentStuff());
            libraryDetails.setTotalCasualStuff(libraryDetailsBean.getTotalCasualStuff());
            libraryDetails.setTotalBooks(libraryDetailsBean.getTotalBooks());
            libraryDetails.setTotalLibraryArea(libraryDetailsBean.getTotalLibraryArea());
            libraryDetails.setCourseOffered(libraryDetailsBean.getCourseOffered());
            libraryDetails.setOtherFacilities(libraryDetailsBean.getOtherFacilities());
            libraryDetails.setContactNumber(libraryDetailsBean.getContactNumber());
            libraryDetails.setEmailId(libraryDetailsBean.getEmailId());
            
            libraryDetailsRepository.save(libraryDetails);
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    public int getLibraryTotalBooks(String libraryCode) {
        int totalBooks = 0;
        for (Book book : bookRepository.findTotalBooks(libraryCode)) {
            if (book.isIsActive() == true) {
                BookDetails bookDetails = bookDetailsRepository.findBookById(book);
                totalBooks = totalBooks + bookDetails.getNoOfTotalBook();
            }
        }
        return totalBooks;
    }
}
