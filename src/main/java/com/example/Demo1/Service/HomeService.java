package com.example.Demo1.Service;

import com.example.Demo1.Bean.UserBean;
import com.example.Demo1.Entity.Role;
import com.example.Demo1.Entity.User;
import com.example.Demo1.Repository.RoleRepository;
import com.example.Demo1.Repository.UserRepository;
import com.example.Demo1.Util.Mail;
import com.example.Demo1.Util.MailSend;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HomeService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    public String addUser(UserBean userbean, HttpSession session, HttpServletRequest request) {
        try {
            Role role = roleRepository.getRoleIdByRoleName("User");

            User user = new User();
            user.setEmail(userbean.getEmail());
            user.setUsername(userbean.getUsername());
            user.setPassword(userbean.getPassword());

            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            System.out.println(path);
            byte[] image = userbean.getImageUser().getBytes();
            String userImageFolder = path + "/user_Image/" + userbean.getUsername() + ".jpg";
            user.setImagePath("/user_Image/" + userbean.getUsername() + ".jpg");
            FileOutputStream perimageOutFile = new FileOutputStream(userImageFolder);
            perimageOutFile.write(image);

            user.setRoleId(role);
            User user1 = userRepository.save(user);

            session.invalidate();
            HttpSession newSession = request.getSession(true);
            newSession.setAttribute("UserSession", user1);
            return "Success";
        } catch (IOException e) {
            return "Failure";
        }
    }

    public Role getUser(UserBean userbean, HttpSession session, HttpServletRequest request) {
        User user = userRepository.validateEmail(userbean.getEmail(), userbean.getPassword());
        if (user != null) {
            session.invalidate();
            HttpSession newSession = request.getSession(true);
            newSession.setAttribute("UserSession", user);
            Role roleID = user.getRoleId();
            return roleID;
        } else {
            return null;
        }
    }

    public String forgetPassword(HttpSession session, UserBean userbean) {
        MailSend mailSend = new MailSend();
        String email = userbean.getEmail().trim();
        User user = userRepository.findUserByEmail(email);
        if (user != null) {
            try {
                
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("mail_heading", "Forget Password");
                hashMap.put("mail_content", "<p>Hello <b>" + user.getUsername() + "</b>,</p>"
                        + "<p><b>Email Id : </b>" + user.getEmail() + "</p>"
                        + "<p><b>Password : </b>" + user.getPassword() + "</p>"
                        + "<p>Please change your password at the next login.</p>");
                hashMap.put("mail_button_text1", "No button");

                MailSend.sendHtmlTemplateMail(session, hashMap, email, "Forget Password");
                return "SUCCESS";
            } catch (Exception e) {
                return "FAILURE";
            }
        } else {
            return "FAILURE";
        }
    }

}
