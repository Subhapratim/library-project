package com.example.Demo1.Interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.example.Demo1.Entity.User;

public class MyCustomInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("User");
        if (user == null) {
            String context = request.getServletContext().getContextPath();
            response.sendRedirect(context + "/");
            return false;
        } else {
            return true;
        }
    }

}
