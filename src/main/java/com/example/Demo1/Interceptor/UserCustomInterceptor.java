package com.example.Demo1.Interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.example.Demo1.Entity.User;

public class UserCustomInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("UserSession");
        
        String context = request.getServletContext().getContextPath();
        
        String pathUser = request.getRequestURI().substring(7);
        pathUser = pathUser.substring(0, pathUser.indexOf("/"));
        
        if (user == null) {
            response.sendRedirect(context + "/");
            return false;
        } else {
            if (!user.roleId.roleName.equals(pathUser)) {
                response.sendRedirect(context + "/");
                return false;
            }
            return true;
        }
    }

}
