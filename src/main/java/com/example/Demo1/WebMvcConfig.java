package com.example.Demo1;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.example.Demo1.Interceptor.AdminCustomInterceptor;
import com.example.Demo1.Interceptor.UserCustomInterceptor;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new UserCustomInterceptor()).addPathPatterns("/User/**");
        registry.addInterceptor(new AdminCustomInterceptor()).addPathPatterns("/Admin/**");
    }

}
