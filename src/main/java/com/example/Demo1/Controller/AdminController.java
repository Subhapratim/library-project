package com.example.Demo1.Controller;

import com.example.Demo1.Bean.BookBean;
import com.example.Demo1.Bean.BookDataBean;
import com.example.Demo1.Bean.CourseBean;
import com.example.Demo1.Bean.DataBean;
import com.example.Demo1.Bean.LibraryDetailsBean;
import com.example.Demo1.Bean.MaintenanceBean;
import com.example.Demo1.Bean.NotificationBean;
import com.example.Demo1.Bean.NotificationDataBean;
import com.example.Demo1.Bean.ReportBean;
import com.example.Demo1.Bean.UserBean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.Demo1.Bean.UserDataBean;
import com.example.Demo1.Bean.eLinkBean;
import com.example.Demo1.Bean.eLinkDataBean;
import com.example.Demo1.Service.AdminService;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "Admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @RequestMapping("dashboard")
    public ModelAndView dashboard() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("adminDashboard");
        return mav;
    }

    @RequestMapping("allUsers")
    public ModelAndView allUsers() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("allUsers");
        return mav;
    }

    @RequestMapping("allApprovedUsers")
    public ModelAndView allApprovedUsers() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("allApprovedUsers");
        return mav;
    }

    @RequestMapping("allNonApprovedUsers")
    public ModelAndView allNonApprovedUsers() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("allNon-ApprovedUsers");
        return mav;
    }

    @RequestMapping(value = "getAllUsers", method = RequestMethod.GET)
    public @ResponseBody
    UserDataBean AllUserList() {
        UserDataBean userDataBean = new UserDataBean();
        userDataBean.setData(adminService.getAllUser());
        return userDataBean;
    }

    @RequestMapping(value = "userApproval", method = RequestMethod.POST)
    @ResponseBody
    public String userApproval(@RequestBody UserBean userBean, HttpSession session, HttpServletRequest request) {
        String st = adminService.userApproval(userBean.getId(), session, request);
        return st;
    }

    @RequestMapping(value = "userNonApprove", method = RequestMethod.POST)
    @ResponseBody
    public String userNonApprove(@RequestBody UserBean userBean, HttpSession session, HttpServletRequest request) {
        String st = adminService.userNonApprove(userBean.getId(), session, request);
        return st;
    }

    @RequestMapping(value = "getAllApprovedUsers", method = RequestMethod.GET)
    public @ResponseBody
    UserDataBean getAllApprovedUsers() {
        UserDataBean userDataBean = new UserDataBean();
        userDataBean.setData(adminService.getAllApprovedUsers());
        return userDataBean;
    }

    @RequestMapping(value = "getAllNonApprovedUsers", method = RequestMethod.GET)
    public @ResponseBody
    UserDataBean getAllNonApprovedUsers() {
        UserDataBean userDataBean = new UserDataBean();
        userDataBean.setData(adminService.getAllNonApprovedUsers());
        return userDataBean;
    }

    @RequestMapping("addNewBook")
    public ModelAndView addNewBook() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("addNewBook");
        return mav;
    }

    @RequestMapping(value = "addBooks", method = RequestMethod.POST)
    public String addBooks(@ModelAttribute("addBooks") BookBean bookbean, RedirectAttributes redir, HttpServletRequest request) {
        String status = adminService.addBooks(bookbean, request);
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Admin/addNewBook";
    }

    @RequestMapping(value = "getAllBooks", method = RequestMethod.GET)
    public @ResponseBody
    BookDataBean getAllBooks() {
        BookDataBean bookDataBean = new BookDataBean();
        bookDataBean.setData(adminService.getAllBooks());
        return bookDataBean;
    }

    @RequestMapping(value = "searchBook", method = RequestMethod.POST)
    public @ResponseBody
    DataBean searchBook(BookBean bookBean) {
        DataBean dataBean = new DataBean();
        dataBean.setData(adminService.searchBook(bookBean));
        return dataBean;
    }

    @RequestMapping("updateBookDetails")
    public ModelAndView updateBookDetails() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("updateBookDetails");
        return mav;
    }

    @RequestMapping(value = "booksUpdate", method = RequestMethod.POST)
    @ResponseBody
    public String booksUpdate(@RequestBody BookBean bookbean) {
        String status = adminService.booksUpdate(bookbean);
        return status;
    }

    @RequestMapping(value = "deleteBooks", method = RequestMethod.POST)
    @ResponseBody
    public String deleteBooks(@RequestBody BookBean bookBean) {
        String status = adminService.deleteBook(bookBean.getId());
        return status;
    }

    @RequestMapping("addeLink")
    public ModelAndView addeLink() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("addeLink");
        return mav;
    }

    @RequestMapping(value = "addeLink", method = RequestMethod.POST)
    public String addeLink(@ModelAttribute("addeLink") eLinkBean elinkbean, RedirectAttributes redir, HttpServletRequest request) {
        String status = adminService.addeLinks(elinkbean, request);
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Admin/addeLink";

    }

    @RequestMapping(value = "getAlleLinks", method = RequestMethod.GET)
    public @ResponseBody
    eLinkDataBean getAlleLinks() {
        eLinkDataBean elinkDataBean = new eLinkDataBean();
        elinkDataBean.setData(adminService.getAlleLinks());
        return elinkDataBean;
    }

    @RequestMapping("addNotification")
    public String addNotificationforUsers() {
        return "addNotification";
    }

    @RequestMapping(value = "addNotification", method = RequestMethod.POST)
    public String addNotification(@ModelAttribute("addNotification") NotificationBean notificationBean, RedirectAttributes redir, HttpSession session, ModelMap modelMap, HttpServletRequest request) {
        String status = adminService.addNotification(notificationBean, session, request);
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Admin/addNotification";
    }

    @RequestMapping(value = "getAllNotifications", method = RequestMethod.GET)
    public @ResponseBody
    NotificationDataBean getAllNotifications() {
        NotificationDataBean notificationDataBean = new NotificationDataBean();
        notificationDataBean.setData(adminService.getAllNotifications());
        return notificationDataBean;
    }

    @RequestMapping("viewAllNotification")
    public String addNotificationforAdmin() {
        return "viewAllNotification";
    }

    @RequestMapping(value = "searchNotification", method = RequestMethod.POST)
    public @ResponseBody
    DataBean searchNotification(NotificationBean notificationBean) {
        DataBean dataBean = new DataBean();
        dataBean.setData(adminService.searchNotification(notificationBean));
        return dataBean;
    }

    @RequestMapping(value = "notificationUpdate", method = RequestMethod.POST)
    @ResponseBody
    public String notificationUpdate(@RequestBody NotificationBean notificationBean) {
        String status = adminService.notificationUpdate(notificationBean);
        return status;
    }

    @RequestMapping(value = "deleteNotification", method = RequestMethod.POST)
    @ResponseBody
    public String deleteNotification(@RequestBody NotificationBean notificationBean) {
        String status = adminService.deleteNotification(notificationBean);
        return status;
    }

    @RequestMapping("addNewCourse")
    public String addNewCourse() {
        return "addNewCourse";
    }

    @RequestMapping(value = "addNewCourse", method = RequestMethod.POST)
    public String addCourse(@ModelAttribute("addCourse") CourseBean courseBean, HttpSession session, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String status = adminService.addCourse(courseBean, session, request);
        if (status.equals("SUCCESS")) {
            redirectAttributes.addFlashAttribute("status", "SUCCESS");
        } else {
            redirectAttributes.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Admin/addNewCourse";
    }

    @RequestMapping(value = "getAllCourse", method = RequestMethod.GET)
    public @ResponseBody
    DataBean getAllCourse() {
        DataBean dataBean = new DataBean();
        dataBean.setData(adminService.getAllCourse());
        return dataBean;
    }

    @RequestMapping(value = "viewAllCourse", method = RequestMethod.GET)
    public String viewAllCourse() {
        return "viewAllCourse";
    }

    @RequestMapping(value = "searchCourse", method = RequestMethod.POST)
    public @ResponseBody
    DataBean searchCourse(CourseBean courseBean) {
        DataBean dataBean = new DataBean();
        dataBean.setData(adminService.searchCourse(courseBean));
        return dataBean;
    }

    @RequestMapping(value = "courseUpdate", method = RequestMethod.POST)
    @ResponseBody
    public String courseUpdate(@RequestBody CourseBean courseBean) {
        String status = adminService.courseUpdate(courseBean);
        return status;
    }

    @RequestMapping(value = "deleteCourse", method = RequestMethod.POST)
    @ResponseBody
    public String deleteCourse(@RequestBody CourseBean courseBean) {
        String status = adminService.deleteCourse(courseBean);
        return status;
    }

    @RequestMapping(value = "reportSubmission")
    public String reportSubmission() {
        return "reportSubmission";
    }

    @RequestMapping(value = "workingCertificate")
    public String workingCertificate() {
        return "workingCertificate";
    }

    @RequestMapping(value = "reportSubmission", method = RequestMethod.POST)
    public String reportSubmit(@ModelAttribute("reportSubmit") ReportBean reportBean, HttpSession session, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String status = adminService.reportSubmit(reportBean, session, request);
        if (status.equals("SUCCESS")) {
            redirectAttributes.addFlashAttribute("status", "SUCCESS");
        } else {
            redirectAttributes.addFlashAttribute("status", "FAILURE");
        }
        if (reportBean.getType().equals("report")) {
            return "redirect:/Admin/reportSubmission";
        } else {
            return "redirect:/Admin/workingCertificate";
        }

    }

    @RequestMapping(value = "getAllReports/{requestType}", method = RequestMethod.GET)
    public @ResponseBody
    DataBean getAllReports(@PathVariable String requestType) {
        DataBean dataBean = new DataBean();
        dataBean.setData(adminService.getAllReports(requestType));
        return dataBean;
    }

    @RequestMapping(value = "maintenance")
    public String maintenance() {
        return "maintenance";
    }

    @RequestMapping(value = "maintenance", method = RequestMethod.POST)
    public String applyMaintenance(@ModelAttribute("applyMaintenance") MaintenanceBean maintenanceBean, HttpSession session, HttpServletRequest request, RedirectAttributes redirectAttributes){
        String status = adminService.applyMaintenance(maintenanceBean, session, request);
        if (status.equals("SUCCESS")) {
            redirectAttributes.addFlashAttribute("status", "SUCCESS");
        } else {
            redirectAttributes.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Admin/maintenance";
    }
    
    @RequestMapping(value = "getAllMaintenanceGrant", method = RequestMethod.GET)
    public @ResponseBody
    DataBean getAllMaintenanceGrant() {
        DataBean dataBean = new DataBean();
        dataBean.setData(adminService.getAllMaintenanceGrant());
        return dataBean;
    }
    
    @RequestMapping(value = "suggestionBox")
    public String suggestionBox() {
        return "suggestionBox";
    }
    
    @RequestMapping(value = "addLibraryDetails")
    public String libraryDetails() {
        return "libraryDetails";
    }
    
    @RequestMapping(value = "addLibraryDetails", method = RequestMethod.POST)
    public String addLibraryDetails(@ModelAttribute("addLibraryDetails") LibraryDetailsBean libraryDetailsBean, HttpSession session, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String status = adminService.addLibraryDetails(libraryDetailsBean, session);
        if (status.equals("SUCCESS")) {
            redirectAttributes.addFlashAttribute("status", "SUCCESS");
        } else {
            redirectAttributes.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Admin/addLibraryDetails";
    }
    
    @RequestMapping(value = "getLibraryTotalBooks/{libraryCode}", method = RequestMethod.GET)
    public @ResponseBody
    int getLibraryTotalBooks(@PathVariable String libraryCode) {
        int totalBooks = adminService.getLibraryTotalBooks(libraryCode);
        return totalBooks;
    }

    @RequestMapping("logout")
    public ModelAndView logout(HttpSession session, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        session.removeAttribute("Admin");
        session.invalidate();
        mav.setViewName("redirect:/signin");
        return mav;
    }

    @RequestMapping("BLANK")
    public String BLANK() {
        return "BLANK";
    }

}
