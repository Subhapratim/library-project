package com.example.Demo1.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

	@RequestMapping("/User/dashboard")
	public ModelAndView dashboard() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("userDashboard");
		return mav;
	}
	
	/*@RequestMapping("/logout")
    public ModelAndView logout(HttpSession session, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        session.removeAttribute("User");
        session.invalidate();
        mav.setViewName("redirect:/signin");
        return mav;
    }*/
}
