package com.example.Demo1.Controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.Demo1.Bean.UserBean;
import com.example.Demo1.Entity.Role;
import com.example.Demo1.Service.HomeService;

@Controller
public class HomeController {

    @Autowired
    private HomeService homeService;

    /*@RequestMapping("/dashboard/index")
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        return mav;
    }*/

    @RequestMapping({"/", "/signin"})
    public ModelAndView signin() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("signin");
        return mav;
    }

    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public ModelAndView Login(@ModelAttribute("loginForm") UserBean userbean, RedirectAttributes redir, ModelMap modelMap, HttpSession session, HttpServletRequest request) throws IOException {
        ModelAndView mav = new ModelAndView();
        Role roleID = homeService.getUser(userbean, session, request);
	    if(roleID != null) {
	        if (roleID.getId() == 1) {
	            mav.setViewName("redirect:/User/dashboard");
	        } else if(roleID.getId() == 2) {
	        	mav.setViewName("redirect:/Admin/dashboard");
	        } else {
	        	return null;
	        }
        }else {
            redir.addFlashAttribute("status", "Invalid Email Id....");
            mav.setViewName("redirect:/signin");
        }
        return mav;
    }

    @RequestMapping("/signup")
    public ModelAndView signup() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("signup");
        return mav;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ModelAndView signup(@ModelAttribute("signupForm") UserBean userbean, RedirectAttributes redir, ModelMap modelMap, HttpSession session, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        String status = homeService.addUser(userbean, session, request);
        if (status.equals("Success")) {
            mav.setViewName("redirect:/User/dashboard");
        } else {
            redir.addFlashAttribute("status", "Provided email already exists....");
            mav.setViewName("redirect:/signup");
        }
        return mav;
    }

    @RequestMapping("/forgetpassword")
    public ModelAndView forgetpassword() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("forgetpassword");
        return mav;
    }
    
    @RequestMapping(value = "/forgetPassword", method = RequestMethod.POST)
    public String forgetPassword(@ModelAttribute("forgetPassword") UserBean userbean, RedirectAttributes redir, HttpSession session){
        String status = homeService.forgetPassword(session, userbean);
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/forgetpassword";
    }

    @RequestMapping(value = "/error", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView error() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("error");
        return mav;
    }

    @RequestMapping("/error500")
    public ModelAndView error500() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("error500");
        return mav;
    }

    @RequestMapping("/dashboard/logout")
    public ModelAndView logout(HttpSession session, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        session.invalidate();
        /*Cookie emailCookie = new Cookie("emailCookie", "");
		Cookie passwordCookie = new Cookie("passwordCookie", "");
		emailCookie.setMaxAge(0);
		passwordCookie.setMaxAge(0);
		response.addCookie(emailCookie);
		response.addCookie(passwordCookie);*/
        mav.setViewName("redirect:/signin");
        return mav;
    }
}
