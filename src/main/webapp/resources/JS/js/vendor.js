(function($) {
  showSwal = function(type){
        'use strict';
        if(type === 'basic'){
        	swal({
            text: 'Any fool can use a computer',
            button: {
              text: "OK",
              value: true,
              visible: true,
              className: "btn btn-primary"
            }
          })

    	}else if(type === 'title-and-text'){
        swal({
          title: 'Read the alert!',
          text: 'Click OK to close this alert',
          button: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-primary"
          }
        })

    	}else if(type === 'success-message'){
        swal({
          title: 'Congratulations!',
          text: 'You entered the correct answer',
          icon: 'success',
          button: {
            text: "Continue",
            value: true,
            visible: true,
            className: "btn btn-primary"
          }
        })

    	}else if(type === 'auto-close'){
            swal({
              title: 'Auto close alert!',
              text: 'I will close in 2 seconds.',
              timer: 2000,
              button: false
            }).then(
            function () {},
              // handling the promise rejection
            function (dismiss) {
              if (dismiss === 'timer') {
                console.log('I was closed by the timer')
              }
            }
          )
    	}else if(type === 'warning-message-and-cancel'){
            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3f51b5',
              cancelButtonColor: '#ff4081',
              confirmButtonText: 'Great ',
              buttons: {
                cancel: {
                  text: "Cancel",
                  value: null,
                  visible: true,
                  className: "btn btn-danger",
                  closeModal: true,
                },
                confirm: {
                  text: "OK",
                  value: true,
                  visible: true,
                  className: "btn btn-primary",
                  closeModal: true
                }
              }
            })

    	}else if(type === 'custom-html'){
        	swal({
            content: {
              element: "input",
              attributes: {
                placeholder: "Type your password",
                type: "password",
                class: 'form-control'
              },
            },
            button: {
              text: "OK",
              value: true,
              visible: true,
              className: "btn btn-primary"
            }
          })
    	}
        }

})(jQuery);

(function($) {
  'use strict';
  $(function() {
		$('#show').avgrund({
			height: 500,
			holderClass: 'custom',
			showClose: true,
			showCloseText: 'x',
			onBlurContainer: '.container-scroller',
			template: '<p>So implement your design and place content here! If you want to close modal, please hit "Esc", click somewhere on the screen or use special button.</p>' +
			'<div>' +
				'<a href="http://twitter.com/voronianski" target="_blank" class="twitter btn btn-twitter btn-block">Twitter</a>' +
				'<a href="http://dribbble.com/voronianski" target="_blank" class="dribble btn btn-dribbble btn-block">Dribbble</a>' +
			'</div>'+
			'<div class="text-center mt-4">' +
				'<a href="#" target="_blank" class="btn btn-success mr-2">Great!</a>' +
				'<a href="#" target="_blank" class="btn btn-light">Cancel</a>' +
			'</div>'
		});
	})
})(jQuery);

(function($) {
  'use strict';
  $('#defaultconfig').maxlength({
    warningClass: "badge mt-1 badge-success",
    limitReachedClass: "badge mt-1 badge-danger"
  });

  $('#defaultconfig-2').maxlength({
    alwaysShow: true,
    threshold: 20,
    warningClass: "badge mt-1 badge-success",
    limitReachedClass: "badge mt-1 badge-danger"
  });

  $('#defaultconfig-3').maxlength({
    alwaysShow: true,
    threshold: 10,
    warningClass: "badge mt-1 badge-success",
    limitReachedClass: "badge mt-1 badge-danger",
    separator: ' of ',
    preText: 'You have ',
    postText: ' chars remaining.',
    validate: true
  });

  $('#maxlength-textarea').maxlength({
    alwaysShow: true,
    warningClass: "badge mt-1 badge-success",
    limitReachedClass: "badge mt-1 badge-danger"
  });
})(jQuery);

(function($) {
  'use strict';
  var c3LineChart = c3.generate({
    bindto: '#c3-line-chart',
    data: {
      columns: [
        ['data1', 30, 200, 100, 400, 150, 250],
        ['data2', 50, 20, 10, 40, 15, 25]
      ]
    },
    color: {
        pattern: ['rgba(88,216,163,1)','rgba(237,28,36,0.6)','rgba(4,189,254,0.6)']
    },
    padding: {
        top: 0,
        right:0,
        bottom:30,
        left: 0,
    }
  });

  setTimeout(function() {
    c3LineChart.load({
      columns: [
        ['data1', 230, 190, 300, 500, 300, 400]
      ]
    });
  }, 1000);

  setTimeout(function() {
    c3LineChart.load({
      columns: [
        ['data3', 130, 150, 200, 300, 200, 100]
      ]
    });
  }, 1500);

  setTimeout(function() {
    c3LineChart.unload({
      ids: 'data1'
    });
  }, 2000);

  var c3SplineChart = c3.generate({
    bindto: '#c3-spline-chart',
    data: {
      columns: [
        ['data1', 30, 200, 100, 400, 150, 250],
        ['data2', 130, 100, 140, 200, 150, 50]
      ],
      type: 'spline'
    },
    color: {
        pattern: ['rgba(88,216,163,1)','rgba(237,28,36,0.6)','rgba(4,189,254,0.6)']
    },
    padding: {
        top: 0,
        right:0,
        bottom:30,
        left: 0,
    }
  });
  var c3BarChart = c3.generate({
    bindto: '#c3-bar-chart',
    data: {
      columns: [
        ['data1', 30, 200, 100, 400, 150, 250],
        ['data2', 130, 100, 140, 200, 150, 50]
      ],
      type: 'bar'
    },
    color: {
        pattern: ['rgba(88,216,163,1)', 'rgba(4,189,254,0.6)', 'rgba(237,28,36,0.6)']
    },
    padding: {
        top: 0,
        right:0,
        bottom:30,
        left: 0,
    },
    bar: {
      width: {
        ratio: 0.7 // this makes bar width 50% of length between ticks
      }
    }
  });

  setTimeout(function() {
    c3BarChart.load({
      columns: [
        ['data3', 130, -150, 200, 300, -200, 100]
      ]
    });
  }, 1000);

  var c3StepChart = c3.generate({
    bindto: '#c3-step-chart',
    data: {
      columns: [
        ['data1', 300, 350, 300, 0, 0, 100],
        ['data2', 130, 100, 140, 200, 150, 50]
      ],
      types: {
        data1: 'step',
        data2: 'area-step'
      }
    },
    color: {
        pattern: ['rgba(88,216,163,1)', 'rgba(4,189,254,0.6)', 'rgba(237,28,36,0.6)']
    },
    padding: {
        top: 0,
        right:0,
        bottom:30,
        left: 0,
    }
  });
  var c3PieChart = c3.generate({
    bindto: '#c3-pie-chart',
    data: {
      // iris data from R
      columns: [
        ['data1', 30],
        ['data2', 120],
      ],
      type: 'pie',
      onclick: function(d, i) {
        console.log("onclick", d, i);
      },
      onmouseover: function(d, i) {
        console.log("onmouseover", d, i);
      },
      onmouseout: function(d, i) {
        console.log("onmouseout", d, i);
      }
    },
    color: {
        pattern: ['#6153F9', '#8E97FC', '#A7B3FD']
    },
    padding: {
        top: 0,
        right:0,
        bottom:30,
        left: 0,
    }
  });

  setTimeout(function() {
    c3PieChart.load({
      columns: [
        ["Income", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
        ["Outcome", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
        ["Revenue", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
      ]
    });
  }, 1500);

  setTimeout(function() {
    c3PieChart.unload({
      ids: 'data1'
    });
    c3PieChart.unload({
      ids: 'data2'
    });
  }, 2500);
  var c3DonutChart = c3.generate({
    bindto: '#c3-donut-chart',
    data: {
      columns: [
        ['data1', 30],
        ['data2', 120],
      ],
      type: 'donut',
      onclick: function(d, i) {
        console.log("onclick", d, i);
      },
      onmouseover: function(d, i) {
        console.log("onmouseover", d, i);
      },
      onmouseout: function(d, i) {
        console.log("onmouseout", d, i);
      }
    },
    color: {
        pattern: ['rgba(88,216,163,1)', 'rgba(4,189,254,0.6)', 'rgba(237,28,36,0.6)']
    },
    padding: {
        top: 0,
        right:0,
        bottom:30,
        left: 0,
    },
    donut: {
      title: "Iris Petal Width"
    }
  });

  setTimeout(function() {
    c3DonutChart.load({
      columns: [
        ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
        ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
        ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
      ]
    });
  }, 1500);

  setTimeout(function() {
    c3DonutChart.unload({
      ids: 'data1'
    });
    c3DonutChart.unload({
      ids: 'data2'
    });
  }, 2500);


})(jQuery);

(function($) {
    'use strict';
    $(function() {
        if ($('#calendar').length) {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                defaultDate: '2017-07-12',
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: [{
                        title: 'All Day Event',
                        start: '2017-07-01'
                    },
                    {
                        title: 'Long Event',
                        start: '2017-07-07',
                        end: '2017-07-10'
                    },
                    {
                        id: 999,
                        title: 'Repeating Event',
                        start: '2017-07-09T16:00:00'
                    },
                    {
                        id: 999,
                        title: 'Repeating Event',
                        start: '2017-07-16T16:00:00'
                    },
                    {
                        title: 'Conference',
                        start: '2017-07-11',
                        end: '2017-07-13'
                    },
                    {
                        title: 'Meeting',
                        start: '2017-07-12T10:30:00',
                        end: '2017-07-12T12:30:00'
                    },
                    {
                        title: 'Lunch',
                        start: '2017-07-12T12:00:00'
                    },
                    {
                        title: 'Meeting',
                        start: '2017-07-12T14:30:00'
                    },
                    {
                        title: 'Happy Hour',
                        start: '2017-07-12T17:30:00'
                    },
                    {
                        title: 'Dinner',
                        start: '2017-07-12T20:00:00'
                    },
                    {
                        title: 'Birthday Party',
                        start: '2017-07-13T07:00:00'
                    },
                    {
                        title: 'Click for Google',
                        url: 'http://google.com/',
                        start: '2017-07-28'
                    }
                ]
            })
        }
    });
})(jQuery);

$(function() {
  /* ChartJS
   * -------
   * Data and config for chartjs
   */
  'use strict';
  var data = {
    labels: ["2013", "2014", "2014", "2015", "2016", "2017"],
    datasets: [{
      label: '# of Votes',
      data: [10, 19, 3, 5, 2, 3],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };
  var multiLineData = {
    labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
    datasets: [{
        label: 'Dataset 1',
        data: [12, 19, 3, 5, 2, 3],
        borderColor: [
          '#587ce4'
        ],
        borderWidth: 2,
        fill: false
      },
      {
        label: 'Dataset 2',
        data: [5, 23, 7, 12, 42, 23],
        borderColor: [
          '#ede190'
        ],
        borderWidth: 2,
        fill: false
      },
      {
        label: 'Dataset 3',
        data: [15, 10, 21, 32, 12, 33],
        borderColor: [
          '#f44252'
        ],
        borderWidth: 2,
        fill: false
      }
    ]
  };
  var options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 0
      }
    }

  };
  var doughnutPieData = {
    datasets: [{
      data: [30, 40, 30],
      backgroundColor: [
        'rgba(255, 99, 132, 0.5)',
        'rgba(54, 162, 235, 0.5)',
        'rgba(255, 206, 86, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(153, 102, 255, 0.5)',
        'rgba(255, 159, 64, 0.5)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
      'Pink',
      'Blue',
      'Yellow',
    ]
  };
  var doughnutPieOptions = {
    responsive: true,
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };
  var browserTrafficData = {
    datasets: [{
      data: [20, 20, 10, 30, 20],
      backgroundColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(75, 192, 117, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(75, 192, 117, 1)',
        'rgba(255, 159, 64, 1)'
      ],
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
      'Firefox',
      'Safari',
      'Explorer',
      'Chrome',
      'Opera Mini'
    ]
  };
  var areaData = {
    labels: ["2013", "2014", "2015", "2016", "2017"],
    datasets: [{
      label: '# of Votes',
      data: [12, 19, 3, 5, 2, 3],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1,
      fill: 'origin', // 0: fill to 'origin'
      fill: '+2', // 1: fill to dataset 3
      fill: 1, // 2: fill to dataset 1
      fill: false, // 3: no fill
      fill: '-2' // 4: fill to dataset 2
    }]
  };

  var areaOptions = {
    plugins: {
      filler: {
        propagate: true
      }
    }
  }

  var multiAreaData = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    datasets: [{
        label: 'Facebook',
        data: [8, 11, 13, 15, 12, 13, 16, 15, 13, 19, 11, 14],
        borderColor: ['rgba(255, 99, 132, 0.5)'],
        backgroundColor: ['rgba(255, 99, 132, 0.5)'],
        borderWidth: 1,
        fill: true
      },
      {
        label: 'Twitter',
        data: [7, 17, 12, 16, 14, 18, 16, 12, 15, 11, 13, 9],
        borderColor: ['rgba(54, 162, 235, 0.5)'],
        backgroundColor: ['rgba(54, 162, 235, 0.5)'],
        borderWidth: 1,
        fill: true
      },
      {
        label: 'Linkedin',
        data: [6, 14, 16, 20, 12, 18, 15, 12, 17, 19, 15, 11],
        borderColor: ['rgba(255, 206, 86, 0.5)'],
        backgroundColor: ['rgba(255, 206, 86, 0.5)'],
        borderWidth: 1,
        fill: true
      }
    ]
  };

  var multiAreaOptions = {
    plugins: {
      filler: {
        propagate: true
      }
    },
    elements: {
      point: {
        radius: 0
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false
        }
      }],
      yAxes: [{
        gridLines: {
          display: false
        }
      }]
    }
  }

  var scatterChartData = {
    datasets: [{
        label: 'First Dataset',
        data: [{
            x: -10,
            y: 0
          },
          {
            x: 0,
            y: 3
          },
          {
            x: -25,
            y: 5
          },
          {
            x: 40,
            y: 5
          }
        ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)'
        ],
        borderWidth: 1
      },
      {
        label: 'Second Dataset',
        data: [{
            x: 10,
            y: 5
          },
          {
            x: 20,
            y: -30
          },
          {
            x: -25,
            y: 15
          },
          {
            x: -10,
            y: 5
          }
        ],
        backgroundColor: [
          'rgba(54, 162, 235, 0.2)',
        ],
        borderColor: [
          'rgba(54, 162, 235, 1)',
        ],
        borderWidth: 1
      }
    ]
  }

  var scatterChartOptions = {
    scales: {
      xAxes: [{
        type: 'linear',
        position: 'bottom'
      }]
    }
  }
  // Get context with jQuery - using jQuery's .get() method.
  if ($("#barChart").length) {
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: data,
      options: options
    });
  }

  if ($("#lineChart").length) {
    var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
    var lineChart = new Chart(lineChartCanvas, {
      type: 'line',
      data: data,
      options: options
    });
  }

  if ($("#linechart-multi").length) {
    var multiLineCanvas = $("#linechart-multi").get(0).getContext("2d");
    var lineChart = new Chart(multiLineCanvas, {
      type: 'line',
      data: multiLineData,
      options: options
    });
  }

  if ($("#areachart-multi").length) {
    var multiAreaCanvas = $("#areachart-multi").get(0).getContext("2d");
    var multiAreaChart = new Chart(multiAreaCanvas, {
      type: 'line',
      data: multiAreaData,
      options: multiAreaOptions
    });
  }

  if ($("#doughnutChart").length) {
    var doughnutChartCanvas = $("#doughnutChart").get(0).getContext("2d");
    var doughnutChart = new Chart(doughnutChartCanvas, {
      type: 'doughnut',
      data: doughnutPieData,
      options: doughnutPieOptions
    });
  }

  if ($("#pieChart").length) {
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: doughnutPieData,
      options: doughnutPieOptions
    });
  }

  if ($("#areaChart").length) {
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    var areaChart = new Chart(areaChartCanvas, {
      type: 'line',
      data: areaData,
      options: areaOptions
    });
  }

  if ($("#scatterChart").length) {
    var scatterChartCanvas = $("#scatterChart").get(0).getContext("2d");
    var scatterChart = new Chart(scatterChartCanvas, {
      type: 'scatter',
      data: scatterChartData,
      options: scatterChartOptions
    });
  }

  if ($("#browserTrafficChart").length) {
    var doughnutChartCanvas = $("#browserTrafficChart").get(0).getContext("2d");
    var doughnutChart = new Chart(doughnutChartCanvas, {
      type: 'doughnut',
      data: browserTrafficData,
      options: doughnutPieOptions
    });
  }

  // if ($("#radarChart").length) {
  //   var myRadarChartCanvas = $("#radarChart").get(0).getContext("2d");
  //   var myRadarChart = new Chart(ctx, {
  //     type: 'radar',
  //     data: {
  //       labels: ['Running', 'Swimming', 'Eating', 'Cycling'],
  //       datasets: [{
  //         data: [20, 10, 4, 2]
  //       }]
  //     },
  //     options = {
  //       scale: {
  //         // Hides the scale
  //         display: false
  //       }
  //     }
  //   });
  // }
});

(function($) {
  //simple line
  'use strict';
  if ($('#ct-chart-line').length) {
    new Chartist.Line('#ct-chart-line', {
      labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
      series: [
        [12, 9, 7, 8, 5],
        [2, 1, 3.5, 7, 3],
        [1, 3, 4, 5, 6]
      ]
    }, {
      fullWidth: true,
      chartPadding: {
        right: 40
      }
    });
  }

  //Line scatterer
  var times = function(n) {
    return Array.apply(null, new Array(n));
  };

  var data = times(52).map(Math.random).reduce(function(data, rnd, index) {
    data.labels.push(index + 1);
    for (var i = 0; i < data.series.length; i++) {
      data.series[i].push(Math.random() * 100)
    }
    return data;
  }, {
    labels: [],
    series: times(4).map(function() {
      return new Array()
    })
  });

  var options = {
    showLine: false,
    axisX: {
      labelInterpolationFnc: function(value, index) {
        return index % 13 === 0 ? 'W' + value : null;
      }
    }
  };

  var responsiveOptions = [
    ['screen and (min-width: 640px)', {
      axisX: {
        labelInterpolationFnc: function(value, index) {
          return index % 4 === 0 ? 'W' + value : null;
        }
      }
    }]
  ];

  if ($('#ct-chart-line-scatterer').length) {
    new Chartist.Line('#ct-chart-line-scatterer', data, options, responsiveOptions);
  }

  //Stacked bar Chart
  if ($('#ct-chart-stacked-bar').length) {
    new Chartist.Bar('#ct-chart-stacked-bar', {
      labels: ['Q1', 'Q2', 'Q3', 'Q4'],
      series: [
        ['800000', '1200000', '1400000', '1300000'],
        ['200000', '400000', '500000', '300000'],
        ['100000', '200000', '400000', '600000'],
        ['400000', '600000', '200000', '0000']
      ]
    }, {
      stackBars: true,
      axisY: {
        labelInterpolationFnc: function(value) {
          return (value / 1000) + 'k';
        }
      }
    }).on('draw', function(data) {
      if (data.type === 'bar') {
        data.element.attr({
          style: 'stroke-width: 30px'
        });
      }
    });
  }


  //Horizontal bar chart
  if ($('#ct-chart-horizontal-bar').length) {
    new Chartist.Bar('#ct-chart-horizontal-bar', {
      labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
      series: [
        [5, 4, 3, 7, 5, 10, 3],
        [3, 2, 9, 5, 4, 6, 4],
        [2, 6, 7, 1, 3, 5, 9],
        [2, 6, 7, 1, 3, 5, 19],
      ]
    }, {
      seriesBarDistance: 10,
      reverseData: true,
      horizontalBars: true,
      axisY: {
        offset: 20
      }
    });
  }

  //Pie
  if ($('#ct-chart-pie').length) {
    var data = {
      series: [5, 3, 4]
    };

    var sum = function(a, b) {
      return a + b
    };

    new Chartist.Pie('#ct-chart-pie', data, {
      labelInterpolationFnc: function(value) {
        return Math.round(value / data.series.reduce(sum) * 100) + '%';
      }
    });
  }

  //Donut
  var labels = ['safari', 'chrome', 'explorer', 'firefox'];
  var data = {
    series: [20, 40, 10, 30]
  };

  if ($('#ct-chart-donut').length) {
    new Chartist.Pie('#ct-chart-donut', data, {
      donut: true,
      donutWidth: 60,
      donutSolid: true,
      startAngle: 270,
      showLabel: true,
      labelInterpolationFnc: function(value, index) {
        var percentage = Math.round(value / data.series.reduce(sum) * 100) + '%';
        return labels[index] + ' ' + percentage;
      }
    });
  }



  //Dashboard Tickets Chart
  if ($('#ct-chart-dash-barChart').length) {
    new Chartist.Bar('#ct-chart-dash-barChart', {
      labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4'],
      series: [
        [300, 140, 230, 140],
        [323, 529, 644, 230],
        [734, 539, 624, 334],
      ]
    }, {
      stackBars: true,
      axisY: {
        labelInterpolationFnc: function(value) {
          return (value / 100) + 'k';
        }
      }
    }).on('draw', function(data) {
      if (data.type === 'bar') {
        data.element.attr({
          style: 'stroke-width: 50px'
        });
      }
    });
  }

  //dashboard staked bar chart
  if ($('#ct-chart-vartical-stacked-bar').length) {
    new Chartist.Bar('#ct-chart-vartical-stacked-bar', {
      labels: ['J', 'F', 'M', 'A', 'M', 'J', 'A'],
      series: [{
          "name": "Income",
          "data": [8, 4, 6, 3, 7, 3, 8]
        },
        {
          "name": "Outcome",
          "data": [2, 7, 4, 8, 4, 6, 1]
        },
        {
          "name": "Revenue",
          "data": [4, 3, 3, 6, 7, 2, 4]
        }
      ]
    }, {
      seriesBarDistance: 10,
      reverseData: true,
      horizontalBars: false,
      height: '280px',
      fullWidth: true,
      chartPadding: {
        top: 30,
        left: 0,
        right: 0,
        bottom: 0
      },
      plugins: [
        Chartist.plugins.legend()
      ]
    });
  }

})(jQuery);

(function($) {
    'use strict';
    var clipboard = new Clipboard('.btn-clipboard');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
})(jQuery);

(function($) {
  'use strict';
  if ($('textarea[name=code-editable]').length) {
    var editableCodeMirror = CodeMirror.fromTextArea(document.getElementById('code-editable'), {
      mode: "javascript",
      theme: "ambiance",
      lineNumbers: true
    });
  }
  if ($('#code-readonly').length) {
    var readOnlyCodeMirror = CodeMirror.fromTextArea(document.getElementById('code-readonly'), {
      mode: "javascript",
      theme: "ambiance",
      lineNumbers: true,
      readOnly: "nocursor"
    });
  }
  if ($('#cm-js-mode').length) {
    var cm = CodeMirror(document.getElementById("cm-js-mode"), {
      mode: "javascript",
      lineNumbers: true
    });
  }

  //Use this method of there are multiple codes with same properties
  if ($('.multiple-codes').length) {
    var code_type = '';
    var editorTextarea = $('.multiple-codes');
    for (var i = 0; i < editorTextarea.length; i++) {
      $(editorTextarea[i]).attr('id', 'code-' + i);
      CodeMirror.fromTextArea(document.getElementById('code-' + i), {
        mode: "javascript",
        theme: "ambiance",
        lineNumbers: true,
        readOnly: "nocursor",
        maxHighlightLength: 0,
        workDelay: 0
      });
    }
  }

  //Use this method of there are multiple codes with same properties in shell mode
  if ($('.shell-mode').length) {
    var code_type = '';
    var shellEditor = $('.shell-mode');
    for (var i = 0; i < shellEditor.length; i++) {
      $(shellEditor[i]).attr('id', 'code-' + i);
      CodeMirror.fromTextArea(document.getElementById('code-' + i), {
        mode: "shell",
        theme: "ambiance",
        readOnly: "nocursor",
        maxHighlightLength: 0,
        workDelay: 0
      });
    }
  }
  if ($('#ace_html').length) {
    $(function() {
      var editor = ace.edit("ace_html");
      editor.setTheme("ace/theme/monokai");
      editor.getSession().setMode("ace/mode/html");
      document.getElementById('ace_html');
    });
  }
  if ($('#ace_javaScript').length) {
    $(function() {
      var editor = ace.edit("ace_javaScript");
      editor.setTheme("ace/theme/monokai");
      editor.getSession().setMode("ace/mode/javascript");
      document.getElementById('aceExample');
    });
  }
  if ($('#ace_json').length) {
    $(function() {
      var editor = ace.edit("ace_json");
      editor.setTheme("ace/theme/monokai");
      editor.getSession().setMode("ace/mode/json");
      document.getElementById('ace_json');
    });
  }
  if ($('#ace_css').length) {
    $(function() {
      var editor = ace.edit("ace_css");
      editor.setTheme("ace/theme/monokai");
      editor.getSession().setMode("ace/mode/css");
      document.getElementById('ace_css');
    });
  }
  if ($('#ace_scss').length) {
    $(function() {
      var editor = ace.edit("ace_scss");
      editor.setTheme("ace/theme/monokai");
      editor.getSession().setMode("ace/mode/scss");
      document.getElementById('ace_scss');
    });
  }
  if ($('#ace_php').length) {
    $(function() {
      var editor = ace.edit("ace_php");
      editor.setTheme("ace/theme/monokai");
      editor.getSession().setMode("ace/mode/php");
      document.getElementById('ace_php');
    });
  }
  if ($('#ace_ruby').length) {
    $(function() {
      var editor = ace.edit("ace_ruby");
      editor.setTheme("ace/theme/monokai");
      editor.getSession().setMode("ace/mode/ruby");
      document.getElementById('ace_ruby');
    });
  }
  if ($('#ace_coffee').length) {
    $(function() {
      var editor = ace.edit("ace_coffee");
      editor.setTheme("ace/theme/monokai");
      editor.getSession().setMode("ace/mode/coffee");
      document.getElementById('ace_coffee');
    });
  }
})(jQuery);

(function($) {
    'use strict';
    $.contextMenu({
        selector: '#context-menu-simple',
        callback: function(key, options) {
        },
        items: {
            "edit": {
                name: "Edit",
                icon: "edit"
            },
            "cut": {
                name: "Cut",
                icon: "cut"
            },
            copy: {
                name: "Copy",
                icon: "copy"
            },
            "paste": {
                name: "Paste",
                icon: "paste"
            },
            "delete": {
                name: "Delete",
                icon: "delete"
            },
            "sep1": "---------",
            "quit": {
                name: "Quit",
                icon: function() {
                    return 'context-menu-icon context-menu-icon-quit';
                }
            }
        }
    });
    $.contextMenu({
        selector: '#context-menu-access',
        callback: function(key, options) {
            var m = "clicked: " + key;
            window.console && console.log(m) || alert(m);
        },
        items: {
            "edit": {name: "Edit", icon: "edit", accesskey: "e"},
            "cut": {name: "Cut", icon: "cut", accesskey: "c"},
            // first unused character is taken (here: o)
            "copy": {name: "Copy", icon: "copy", accesskey: "c o p y"},
            // words are truncated to their first letter (here: p)
            "paste": {name: "Paste", icon: "paste", accesskey: "cool paste"},
            "delete": {name: "Delete", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Quit", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
    $.contextMenu({
        selector: '#context-menu-open',
        callback: function(key, options) {
            var m = "clicked: " + key;
            window.console && console.log(m) || alert(m);
        },
        items: {
            "edit": {
                name: "Closing on Click",
                icon: "edit",
                callback: function(){ return true; }
            },
            "cut": {
                name: "Open after Click",
                icon: "cut",
                callback: function(){ return false; }
            }
        }
    });
    $.contextMenu({
        selector: '#context-menu-multi',
        callback: function(key, options) {
            var m = "clicked: " + key;
            window.console && console.log(m) || alert(m);
        },
        items: {
            "edit": {"name": "Edit", "icon": "edit"},
            "cut": {"name": "Cut", "icon": "cut"},
            "sep1": "---------",
            "quit": {"name": "Quit", "icon": "quit"},
            "sep2": "---------",
            "fold1": {
                "name": "Sub group",
                "items": {
                    "fold1-key1": {"name": "Foo bar"},
                    "fold2": {
                        "name": "Sub group 2",
                        "items": {
                            "fold2-key1": {"name": "alpha"},
                            "fold2-key2": {"name": "bravo"},
                            "fold2-key3": {"name": "charlie"}
                        }
                    },
                    "fold1-key3": {"name": "delta"}
                }
            },
            "fold1a": {
                "name": "Other group",
                "items": {
                    "fold1a-key1": {"name": "echo"},
                    "fold1a-key2": {"name": "foxtrot"},
                    "fold1a-key3": {"name": "golf"}
                }
            }
        }
    });
    $.contextMenu({
        selector: '#context-menu-hover',
        trigger: 'hover',
        delay: 500,
        callback: function(key, options) {
            var m = "clicked: " + key;
            window.console && console.log(m) || alert(m);
        },
        items: {
            "edit": {name: "Edit", icon: "edit"},
            "cut": {name: "Cut", icon: "cut"},
            "copy": {name: "Copy", icon: "copy"},
            "paste": {name: "Paste", icon: "paste"},
            "delete": {name: "Delete", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Quit", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
    $.contextMenu({
        selector: '#context-menu-hover-autohide',
        trigger: 'hover',
        delay: 500,
        autoHide: true,
        callback: function(key, options) {
            var m = "clicked: " + key;
            window.console && console.log(m) || alert(m);
        },
        items: {
            "edit": {name: "Edit", icon: "edit"},
            "cut": {name: "Cut", icon: "cut"},
            "copy": {name: "Copy", icon: "copy"},
            "paste": {name: "Paste", icon: "paste"},
            "delete": {name: "Delete", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Quit", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
})(jQuery);

(function($) {
  'use strict';
  if ($(".datepicker").length) {
    $('.datepicker').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }
  if($("#business-survey-chart").length) {
    var businessSurveyData = {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [{
          label: "automobiles",
          data: [
              -60, 70, -20, 40, 0, 60
          ],
          backgroundColor: '#f86c6b',
          borderColor: '#f86c6b',
          fill: false,
          borderDash: [3, 3],
          pointRadius: 5,
          pointHoverRadius: 5,
          borderWidth: 2
        }, {
          label: "Electronics",
          data: [
              60, -40, 0, 80, 40, -20, 80
          ],
          backgroundColor: '#20a8d8',
          borderColor: '#20a8d8',
          fill: false,
          borderDash: [3, 3],
          pointRadius: 5,
          pointHoverRadius: 5,
          borderWidth: 2
        }, {
          label: "Beverages",
          data: [
              -20, 0, 40, -60, 60, 0, -100
          ],
          backgroundColor: '#4cc47f',
          borderColor: '#4cc47f',
          fill: false,
          pointRadius: 5,
          pointHoverRadius: 5,
          borderWidth: 2
        }, {
          label: "Accessories",
          data: [
              100, 20, 0, -20, 40, 20, 60
          ],
          backgroundColor: '#f8cb00',
          borderColor: '#f8cb00',
          fill: false,
          pointRadius: 5,
          pointHoverRadius: 5,
          borderWidth: 2
      }]
    };
    var businessSurveyOptions = {
      responsive: true,
      legend: {
          position: 'bottom',
      },
      hover: {
          mode: 'index'
      },
      scales: {
          xAxes: [{
              display: true,
              scaleLabel: {
                  display: true,
                  labelString: 'Month'
              }
          }],
          yAxes: [{
              display: true,
              scaleLabel: {
                  display: true,
                  labelString: 'Value'
              }
          }]
      },
    }
    var businessSurveyCanvas = $("#business-survey-chart").get(0).getContext("2d");
    var lineChart = new Chart(businessSurveyCanvas, {
      type: 'line',
      data: businessSurveyData,
      options: businessSurveyOptions
    });

  }
  if($('#visits-map').length) {
    $('#visits-map').vectorMap({
      map: 'world_mill_en',
      backgroundColor: '#FFF',
      scaleColors: ['#C8EEFF', '#0071A4'],
      zoomOnScroll: false,
      normalizeFunction: 'polynomial',
      hoverOpacity: 0.7,
      hoverColor: false,
      markerStyle: {
        initial: {
          fill: '#4db9ed',
          stroke: 'none'
        }
      },
      markers: [
        {latLng: [41.90, 12.45], name: 'Vatican City'},
        {latLng: [43.73, 7.41], name: 'Monaco'},
        {latLng: [43.93, 12.46], name: 'San Marino'},
        {latLng: [3.2, 73.22], name: 'Maldives'},
        {latLng: [35.88, 14.5], name: 'Malta'},
        {latLng: [12.05, -61.75], name: 'Grenada'},
        {latLng: [13.16, -59.55], name: 'Barbados'},
        {latLng: [-4.61, 55.45], name: 'Seychelles'},
        {latLng: [42.5, 1.51], name: 'Andorra'},
        {latLng: [15.3, -61.38], name: 'Dominica'},
        {latLng: [-20.2, 57.5], name: 'Mauritius'},
        {latLng: [26.02, 50.55], name: 'Bahrain'},
        {latLng: [0.33, 6.73], name: 'São Tomé and Príncipe'}
      ],
      regionStyle: {
        initial: {
          fill: 'white',
          "fill-opacity": 1,
          stroke: '#eaeef0',
          "stroke-width": 1,
          "stroke-opacity": 1
        },
        hover: {
          "fill-opacity": 0.8,
          cursor: 'pointer'
        },
        selected: {
          fill: '#4db9ed'
        },
        selectedHover: {
        }
      }
    });

  }
})(jQuery);

(function($) {
  (function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.Name || client.Name.indexOf(filter.Name) > -1)
                    && (filter.Age === undefined || client.Age === filter.Age)
                    && (!filter.Address || client.Address.indexOf(filter.Address) > -1)
                    && (!filter.Country || client.Country === filter.Country)
                    && (filter.Married === undefined || client.Married === filter.Married);
            });
        },

        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
        },

        updateItem: function(updatingClient) { },

        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1);
        }

    };

    window.db = db;


    db.countries = [
        { Name: "", Id: 0 },
        { Name: "United States", Id: 1 },
        { Name: "Canada", Id: 2 },
        { Name: "United Kingdom", Id: 3 },
        { Name: "France", Id: 4 },
        { Name: "Brazil", Id: 5 },
        { Name: "China", Id: 6 },
        { Name: "Russia", Id: 7 }
    ];

    db.clients = [
        {
            "Name": "Otto Clay",
            "Age": 61,
            "Country": 6,
            "Address": "Ap #897-1459 Quam Avenue",
            "Married": false
        },
        {
            "Name": "Connor Johnston",
            "Age": 73,
            "Country": 7,
            "Address": "Ap #370-4647 Dis Av.",
            "Married": false
        },
        {
            "Name": "Lacey Hess",
            "Age": 29,
            "Country": 7,
            "Address": "Ap #365-8835 Integer St.",
            "Married": false
        },
        {
            "Name": "Timothy Henson",
            "Age": 78,
            "Country": 1,
            "Address": "911-5143 Luctus Ave",
            "Married": false
        },
        {
            "Name": "Ramona Benton",
            "Age": 43,
            "Country": 5,
            "Address": "Ap #614-689 Vehicula Street",
            "Married": true
        },
        {
            "Name": "Ezra Tillman",
            "Age": 51,
            "Country": 1,
            "Address": "P.O. Box 738, 7583 Quisque St.",
            "Married": true
        },
        {
            "Name": "Dante Carter",
            "Age": 59,
            "Country": 1,
            "Address": "P.O. Box 976, 6316 Lorem, St.",
            "Married": false
        },
        {
            "Name": "Christopher Mcclure",
            "Age": 58,
            "Country": 1,
            "Address": "847-4303 Dictum Av.",
            "Married": true
        },
        {
            "Name": "Ruby Rocha",
            "Age": 62,
            "Country": 2,
            "Address": "5212 Sagittis Ave",
            "Married": false
        },
        {
            "Name": "Imelda Hardin",
            "Age": 39,
            "Country": 5,
            "Address": "719-7009 Auctor Av.",
            "Married": false
        },
        {
            "Name": "Jonah Johns",
            "Age": 28,
            "Country": 5,
            "Address": "P.O. Box 939, 9310 A Ave",
            "Married": false
        },
        {
            "Name": "Herman Rosa",
            "Age": 49,
            "Country": 7,
            "Address": "718-7162 Molestie Av.",
            "Married": true
        },
        {
            "Name": "Arthur Gay",
            "Age": 20,
            "Country": 7,
            "Address": "5497 Neque Street",
            "Married": false
        },
        {
            "Name": "Xena Wilkerson",
            "Age": 63,
            "Country": 1,
            "Address": "Ap #303-6974 Proin Street",
            "Married": true
        },
        {
            "Name": "Lilah Atkins",
            "Age": 33,
            "Country": 5,
            "Address": "622-8602 Gravida Ave",
            "Married": true
        },
        {
            "Name": "Malik Shepard",
            "Age": 59,
            "Country": 1,
            "Address": "967-5176 Tincidunt Av.",
            "Married": false
        },
        {
            "Name": "Keely Silva",
            "Age": 24,
            "Country": 1,
            "Address": "P.O. Box 153, 8995 Praesent Ave",
            "Married": false
        },
        {
            "Name": "Hunter Pate",
            "Age": 73,
            "Country": 7,
            "Address": "P.O. Box 771, 7599 Ante, Road",
            "Married": false
        },
        {
            "Name": "Mikayla Roach",
            "Age": 55,
            "Country": 5,
            "Address": "Ap #438-9886 Donec Rd.",
            "Married": true
        },
        {
            "Name": "Upton Joseph",
            "Age": 48,
            "Country": 4,
            "Address": "Ap #896-7592 Habitant St.",
            "Married": true
        },
        {
            "Name": "Jeanette Pate",
            "Age": 59,
            "Country": 2,
            "Address": "P.O. Box 177, 7584 Amet, St.",
            "Married": false
        },
        {
            "Name": "Kaden Hernandez",
            "Age": 79,
            "Country": 3,
            "Address": "366 Ut St.",
            "Married": true
        },
        {
            "Name": "Kenyon Stevens",
            "Age": 20,
            "Country": 3,
            "Address": "P.O. Box 704, 4580 Gravida Rd.",
            "Married": false
        },
        {
            "Name": "Jerome Harper",
            "Age": 31,
            "Country": 5,
            "Address": "2464 Porttitor Road",
            "Married": false
        },
        {
            "Name": "Jelani Patel",
            "Age": 36,
            "Country": 2,
            "Address": "P.O. Box 541, 5805 Nec Av.",
            "Married": true
        },
        {
            "Name": "Keaton Oconnor",
            "Age": 21,
            "Country": 1,
            "Address": "Ap #657-1093 Nec, Street",
            "Married": false
        },
        {
            "Name": "Bree Johnston",
            "Age": 31,
            "Country": 2,
            "Address": "372-5942 Vulputate Avenue",
            "Married": false
        },
        {
            "Name": "Maisie Hodges",
            "Age": 70,
            "Country": 7,
            "Address": "P.O. Box 445, 3880 Odio, Rd.",
            "Married": false
        },
        {
            "Name": "Kuame Calhoun",
            "Age": 39,
            "Country": 2,
            "Address": "P.O. Box 609, 4105 Rutrum St.",
            "Married": true
        },
        {
            "Name": "Carlos Cameron",
            "Age": 38,
            "Country": 5,
            "Address": "Ap #215-5386 A, Avenue",
            "Married": false
        },
        {
            "Name": "Fulton Parsons",
            "Age": 25,
            "Country": 7,
            "Address": "P.O. Box 523, 3705 Sed Rd.",
            "Married": false
        },
        {
            "Name": "Wallace Christian",
            "Age": 43,
            "Country": 3,
            "Address": "416-8816 Mauris Avenue",
            "Married": true
        },
        {
            "Name": "Caryn Maldonado",
            "Age": 40,
            "Country": 1,
            "Address": "108-282 Nonummy Ave",
            "Married": false
        },
        {
            "Name": "Whilemina Frank",
            "Age": 20,
            "Country": 7,
            "Address": "P.O. Box 681, 3938 Egestas. Av.",
            "Married": true
        },
        {
            "Name": "Emery Moon",
            "Age": 41,
            "Country": 4,
            "Address": "Ap #717-8556 Non Road",
            "Married": true
        },
        {
            "Name": "Price Watkins",
            "Age": 35,
            "Country": 4,
            "Address": "832-7810 Nunc Rd.",
            "Married": false
        },
        {
            "Name": "Lydia Castillo",
            "Age": 59,
            "Country": 7,
            "Address": "5280 Placerat, Ave",
            "Married": true
        },
        {
            "Name": "Lawrence Conway",
            "Age": 53,
            "Country": 1,
            "Address": "Ap #452-2808 Imperdiet St.",
            "Married": false
        },
        {
            "Name": "Kalia Nicholson",
            "Age": 67,
            "Country": 5,
            "Address": "P.O. Box 871, 3023 Tellus Road",
            "Married": true
        },
        {
            "Name": "Brielle Baxter",
            "Age": 45,
            "Country": 3,
            "Address": "Ap #822-9526 Ut, Road",
            "Married": true
        },
        {
            "Name": "Valentine Brady",
            "Age": 72,
            "Country": 7,
            "Address": "8014 Enim. Road",
            "Married": true
        },
        {
            "Name": "Rebecca Gardner",
            "Age": 57,
            "Country": 4,
            "Address": "8655 Arcu. Road",
            "Married": true
        },
        {
            "Name": "Vladimir Tate",
            "Age": 26,
            "Country": 1,
            "Address": "130-1291 Non, Rd.",
            "Married": true
        },
        {
            "Name": "Vernon Hays",
            "Age": 56,
            "Country": 4,
            "Address": "964-5552 In Rd.",
            "Married": true
        },
        {
            "Name": "Allegra Hull",
            "Age": 22,
            "Country": 4,
            "Address": "245-8891 Donec St.",
            "Married": true
        },
        {
            "Name": "Hu Hendrix",
            "Age": 65,
            "Country": 7,
            "Address": "428-5404 Tempus Ave",
            "Married": true
        },
        {
            "Name": "Kenyon Battle",
            "Age": 32,
            "Country": 2,
            "Address": "921-6804 Lectus St.",
            "Married": false
        },
        {
            "Name": "Gloria Nielsen",
            "Age": 24,
            "Country": 4,
            "Address": "Ap #275-4345 Lorem, Street",
            "Married": true
        },
        {
            "Name": "Illiana Kidd",
            "Age": 59,
            "Country": 2,
            "Address": "7618 Lacus. Av.",
            "Married": false
        },
        {
            "Name": "Adria Todd",
            "Age": 68,
            "Country": 6,
            "Address": "1889 Tincidunt Road",
            "Married": false
        },
        {
            "Name": "Kirsten Mayo",
            "Age": 71,
            "Country": 1,
            "Address": "100-8640 Orci, Avenue",
            "Married": false
        },
        {
            "Name": "Willa Hobbs",
            "Age": 60,
            "Country": 6,
            "Address": "P.O. Box 323, 158 Tristique St.",
            "Married": false
        },
        {
            "Name": "Alexis Clements",
            "Age": 69,
            "Country": 5,
            "Address": "P.O. Box 176, 5107 Proin Rd.",
            "Married": false
        },
        {
            "Name": "Akeem Conrad",
            "Age": 60,
            "Country": 2,
            "Address": "282-495 Sed Ave",
            "Married": true
        },
        {
            "Name": "Montana Silva",
            "Age": 79,
            "Country": 6,
            "Address": "P.O. Box 120, 9766 Consectetuer St.",
            "Married": false
        },
        {
            "Name": "Kaseem Hensley",
            "Age": 77,
            "Country": 6,
            "Address": "Ap #510-8903 Mauris. Av.",
            "Married": true
        },
        {
            "Name": "Christopher Morton",
            "Age": 35,
            "Country": 5,
            "Address": "P.O. Box 234, 3651 Sodales Avenue",
            "Married": false
        },
        {
            "Name": "Wade Fernandez",
            "Age": 49,
            "Country": 6,
            "Address": "740-5059 Dolor. Road",
            "Married": true
        },
        {
            "Name": "Illiana Kirby",
            "Age": 31,
            "Country": 2,
            "Address": "527-3553 Mi Ave",
            "Married": false
        },
        {
            "Name": "Kimberley Hurley",
            "Age": 65,
            "Country": 5,
            "Address": "P.O. Box 637, 9915 Dictum St.",
            "Married": false
        },
        {
            "Name": "Arthur Olsen",
            "Age": 74,
            "Country": 5,
            "Address": "887-5080 Eget St.",
            "Married": false
        },
        {
            "Name": "Brody Potts",
            "Age": 59,
            "Country": 2,
            "Address": "Ap #577-7690 Sem Road",
            "Married": false
        },
        {
            "Name": "Dillon Ford",
            "Age": 60,
            "Country": 1,
            "Address": "Ap #885-9289 A, Av.",
            "Married": true
        },
        {
            "Name": "Hannah Juarez",
            "Age": 61,
            "Country": 2,
            "Address": "4744 Sapien, Rd.",
            "Married": true
        },
        {
            "Name": "Vincent Shaffer",
            "Age": 25,
            "Country": 2,
            "Address": "9203 Nunc St.",
            "Married": true
        },
        {
            "Name": "George Holt",
            "Age": 27,
            "Country": 6,
            "Address": "4162 Cras Rd.",
            "Married": false
        },
        {
            "Name": "Tobias Bartlett",
            "Age": 74,
            "Country": 4,
            "Address": "792-6145 Mauris St.",
            "Married": true
        },
        {
            "Name": "Xavier Hooper",
            "Age": 35,
            "Country": 1,
            "Address": "879-5026 Interdum. Rd.",
            "Married": false
        },
        {
            "Name": "Declan Dorsey",
            "Age": 31,
            "Country": 2,
            "Address": "Ap #926-4171 Aenean Road",
            "Married": true
        },
        {
            "Name": "Clementine Tran",
            "Age": 43,
            "Country": 4,
            "Address": "P.O. Box 176, 9865 Eu Rd.",
            "Married": true
        },
        {
            "Name": "Pamela Moody",
            "Age": 55,
            "Country": 6,
            "Address": "622-6233 Luctus Rd.",
            "Married": true
        },
        {
            "Name": "Julie Leon",
            "Age": 43,
            "Country": 6,
            "Address": "Ap #915-6782 Sem Av.",
            "Married": true
        },
        {
            "Name": "Shana Nolan",
            "Age": 79,
            "Country": 5,
            "Address": "P.O. Box 603, 899 Eu St.",
            "Married": false
        },
        {
            "Name": "Vaughan Moody",
            "Age": 37,
            "Country": 5,
            "Address": "880 Erat Rd.",
            "Married": false
        },
        {
            "Name": "Randall Reeves",
            "Age": 44,
            "Country": 3,
            "Address": "1819 Non Street",
            "Married": false
        },
        {
            "Name": "Dominic Raymond",
            "Age": 68,
            "Country": 1,
            "Address": "Ap #689-4874 Nisi Rd.",
            "Married": true
        },
        {
            "Name": "Lev Pugh",
            "Age": 69,
            "Country": 5,
            "Address": "Ap #433-6844 Auctor Avenue",
            "Married": true
        },
        {
            "Name": "Desiree Hughes",
            "Age": 80,
            "Country": 4,
            "Address": "605-6645 Fermentum Avenue",
            "Married": true
        },
        {
            "Name": "Idona Oneill",
            "Age": 23,
            "Country": 7,
            "Address": "751-8148 Aliquam Avenue",
            "Married": true
        },
        {
            "Name": "Lani Mayo",
            "Age": 76,
            "Country": 1,
            "Address": "635-2704 Tristique St.",
            "Married": true
        },
        {
            "Name": "Cathleen Bonner",
            "Age": 40,
            "Country": 1,
            "Address": "916-2910 Dolor Av.",
            "Married": false
        },
        {
            "Name": "Sydney Murray",
            "Age": 44,
            "Country": 5,
            "Address": "835-2330 Fringilla St.",
            "Married": false
        },
        {
            "Name": "Brenna Rodriguez",
            "Age": 77,
            "Country": 6,
            "Address": "3687 Imperdiet Av.",
            "Married": true
        },
        {
            "Name": "Alfreda Mcdaniel",
            "Age": 38,
            "Country": 7,
            "Address": "745-8221 Aliquet Rd.",
            "Married": true
        },
        {
            "Name": "Zachery Atkins",
            "Age": 30,
            "Country": 1,
            "Address": "549-2208 Auctor. Road",
            "Married": true
        },
        {
            "Name": "Amelia Rich",
            "Age": 56,
            "Country": 4,
            "Address": "P.O. Box 734, 4717 Nunc Rd.",
            "Married": false
        },
        {
            "Name": "Kiayada Witt",
            "Age": 62,
            "Country": 3,
            "Address": "Ap #735-3421 Malesuada Avenue",
            "Married": false
        },
        {
            "Name": "Lysandra Pierce",
            "Age": 36,
            "Country": 1,
            "Address": "Ap #146-2835 Curabitur St.",
            "Married": true
        },
        {
            "Name": "Cara Rios",
            "Age": 58,
            "Country": 4,
            "Address": "Ap #562-7811 Quam. Ave",
            "Married": true
        },
        {
            "Name": "Austin Andrews",
            "Age": 55,
            "Country": 7,
            "Address": "P.O. Box 274, 5505 Sociis Rd.",
            "Married": false
        },
        {
            "Name": "Lillian Peterson",
            "Age": 39,
            "Country": 2,
            "Address": "6212 A Avenue",
            "Married": false
        },
        {
            "Name": "Adria Beach",
            "Age": 29,
            "Country": 2,
            "Address": "P.O. Box 183, 2717 Nunc Avenue",
            "Married": true
        },
        {
            "Name": "Oleg Durham",
            "Age": 80,
            "Country": 4,
            "Address": "931-3208 Nunc Rd.",
            "Married": false
        },
        {
            "Name": "Casey Reese",
            "Age": 60,
            "Country": 4,
            "Address": "383-3675 Ultrices, St.",
            "Married": false
        },
        {
            "Name": "Kane Burnett",
            "Age": 80,
            "Country": 1,
            "Address": "759-8212 Dolor. Ave",
            "Married": false
        },
        {
            "Name": "Stewart Wilson",
            "Age": 46,
            "Country": 7,
            "Address": "718-7845 Sagittis. Av.",
            "Married": false
        },
        {
            "Name": "Charity Holcomb",
            "Age": 31,
            "Country": 6,
            "Address": "641-7892 Enim. Ave",
            "Married": false
        },
        {
            "Name": "Kyra Cummings",
            "Age": 43,
            "Country": 4,
            "Address": "P.O. Box 702, 6621 Mus. Av.",
            "Married": false
        },
        {
            "Name": "Stuart Wallace",
            "Age": 25,
            "Country": 7,
            "Address": "648-4990 Sed Rd.",
            "Married": true
        },
        {
            "Name": "Carter Clarke",
            "Age": 59,
            "Country": 6,
            "Address": "Ap #547-2921 A Street",
            "Married": false
        }
    ];

    db.users = [
        {
            "ID": "x",
            "Account": "A758A693-0302-03D1-AE53-EEFE22855556",
            "Name": "Carson Kelley",
            "RegisterDate": "2002-04-20T22:55:52-07:00"
        },
        {
            "Account": "D89FF524-1233-0CE7-C9E1-56EFF017A321",
            "Name": "Prescott Griffin",
            "RegisterDate": "2011-02-22T05:59:55-08:00"
        },
        {
            "Account": "06FAAD9A-5114-08F6-D60C-961B2528B4F0",
            "Name": "Amir Saunders",
            "RegisterDate": "2014-08-13T09:17:49-07:00"
        },
        {
            "Account": "EED7653D-7DD9-A722-64A8-36A55ECDBE77",
            "Name": "Derek Thornton",
            "RegisterDate": "2012-02-27T01:31:07-08:00"
        },
        {
            "Account": "2A2E6D40-FEBD-C643-A751-9AB4CAF1E2F6",
            "Name": "Fletcher Romero",
            "RegisterDate": "2010-06-25T15:49:54-07:00"
        },
        {
            "Account": "3978F8FA-DFF0-DA0E-0A5D-EB9D281A3286",
            "Name": "Thaddeus Stein",
            "RegisterDate": "2013-11-10T07:29:41-08:00"
        },
        {
            "Account": "658DBF5A-176E-569A-9273-74FB5F69FA42",
            "Name": "Nash Knapp",
            "RegisterDate": "2005-06-24T09:11:19-07:00"
        },
        {
            "Account": "76D2EE4B-7A73-1212-F6F2-957EF8C1F907",
            "Name": "Quamar Vega",
            "RegisterDate": "2011-04-13T20:06:29-07:00"
        },
        {
            "Account": "00E46809-A595-CE82-C5B4-D1CAEB7E3E58",
            "Name": "Philip Galloway",
            "RegisterDate": "2008-08-21T18:59:38-07:00"
        },
        {
            "Account": "C196781C-DDCC-AF83-DDC2-CA3E851A47A0",
            "Name": "Mason French",
            "RegisterDate": "2000-11-15T00:38:37-08:00"
        },
        {
            "Account": "5911F201-818A-B393-5888-13157CE0D63F",
            "Name": "Ross Cortez",
            "RegisterDate": "2010-05-27T17:35:32-07:00"
        },
        {
            "Account": "B8BB78F9-E1A1-A956-086F-E12B6FE168B6",
            "Name": "Logan King",
            "RegisterDate": "2003-07-08T16:58:06-07:00"
        },
        {
            "Account": "06F636C3-9599-1A2D-5FD5-86B24ADDE626",
            "Name": "Cedric Leblanc",
            "RegisterDate": "2011-06-30T14:30:10-07:00"
        },
        {
            "Account": "FE880CDD-F6E7-75CB-743C-64C6DE192412",
            "Name": "Simon Sullivan",
            "RegisterDate": "2013-06-11T16:35:07-07:00"
        },
        {
            "Account": "BBEDD673-E2C1-4872-A5D3-C4EBD4BE0A12",
            "Name": "Jamal West",
            "RegisterDate": "2001-03-16T20:18:29-08:00"
        },
        {
            "Account": "19BC22FA-C52E-0CC6-9552-10365C755FAC",
            "Name": "Hector Morales",
            "RegisterDate": "2012-11-01T01:56:34-07:00"
        },
        {
            "Account": "A8292214-2C13-5989-3419-6B83DD637D6C",
            "Name": "Herrod Hart",
            "RegisterDate": "2008-03-13T19:21:04-07:00"
        },
        {
            "Account": "0285564B-F447-0E7F-EAA1-7FB8F9C453C8",
            "Name": "Clark Maxwell",
            "RegisterDate": "2004-08-05T08:22:24-07:00"
        },
        {
            "Account": "EA78F076-4F6E-4228-268C-1F51272498AE",
            "Name": "Reuben Walter",
            "RegisterDate": "2011-01-23T01:55:59-08:00"
        },
        {
            "Account": "6A88C194-EA21-426F-4FE2-F2AE33F51793",
            "Name": "Ira Ingram",
            "RegisterDate": "2008-08-15T05:57:46-07:00"
        },
        {
            "Account": "4275E873-439C-AD26-56B3-8715E336508E",
            "Name": "Damian Morrow",
            "RegisterDate": "2015-09-13T01:50:55-07:00"
        },
        {
            "Account": "A0D733C4-9070-B8D6-4387-D44F0BA515BE",
            "Name": "Macon Farrell",
            "RegisterDate": "2011-03-14T05:41:40-07:00"
        },
        {
            "Account": "B3683DE8-C2FA-7CA0-A8A6-8FA7E954F90A",
            "Name": "Joel Galloway",
            "RegisterDate": "2003-02-03T04:19:01-08:00"
        },
        {
            "Account": "01D95A8E-91BC-2050-F5D0-4437AAFFD11F",
            "Name": "Rigel Horton",
            "RegisterDate": "2015-06-20T11:53:11-07:00"
        },
        {
            "Account": "F0D12CC0-31AC-A82E-FD73-EEEFDBD21A36",
            "Name": "Sylvester Gaines",
            "RegisterDate": "2004-03-12T09:57:13-08:00"
        },
        {
            "Account": "874FCC49-9A61-71BC-2F4E-2CE88348AD7B",
            "Name": "Abbot Mckay",
            "RegisterDate": "2008-12-26T20:42:57-08:00"
        },
        {
            "Account": "B8DA1912-20A0-FB6E-0031-5F88FD63EF90",
            "Name": "Solomon Green",
            "RegisterDate": "2013-09-04T01:44:47-07:00"
        }
     ];

}());
})(jQuery);

(function($) {
  'use strict';
  $.fn.easyNotify = function(options) {

    var settings = $.extend({
      title: "Notification",
      options: {
        body: "",
        icon: "",
        lang: 'pt-BR',
        onClose: "",
        onClick: "",
        onError: ""
      }
    }, options);

    this.init = function() {
        var notify = this;
      if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
      } else if (Notification.permission === "granted") {

        var notification = new Notification(settings.title, settings.options);

        notification.onclose = function() {
            if (typeof settings.options.onClose === 'function') {
                settings.options.onClose();
            }
        };

        notification.onclick = function(){
            if (typeof settings.options.onClick === 'function') {
                settings.options.onClick();
            }
        };

        notification.onerror  = function(){
            if (typeof settings.options.onError  === 'function') {
                settings.options.onError();
            }
        };

      } else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function(permission) {
          if (permission === "granted") {
            notify.init();
          }

        });
      }

    };

    this.init();
    return this;
  };


  //Initialise notification
    var myFunction = function() {
      alert('Click function');
    };
    var myImg = "https://unsplash.it/600/600?image=777";

    $("form").submit(function(event) {
      event.preventDefault();

      var options = {
        title: $("#title").val(),
        options: {
          body: $("#message").val(),
          icon: myImg,
          lang: 'en-US',
          onClick: myFunction
        }
      };
      console.log(options);
      $("#easyNotify").easyNotify(options);
    });
}(jQuery));

(function($) {
    'use strict';
    var iconTochange;
    dragula([document.getElementById("dragula-left"), document.getElementById("dragula-right")]);
    dragula([document.getElementById("profile-list-left"), document.getElementById("profile-list-right")]);
    dragula([document.getElementById("dragula-event-left"), document.getElementById("dragula-event-right")])
        .on('drop', function(el) {
            console.log($(el));
            iconTochange = $(el).find('.mdi');
            if (iconTochange.hasClass('mdi-check')) {
                iconTochange.removeClass('mdi-check text-primary').addClass('mdi-check-all text-success');
            }
            else if (iconTochange.hasClass('mdi-check-all')) {
                iconTochange.removeClass('mdi-check-all text-success').addClass('mdi-check text-primary');
            }
        })
})(jQuery);

(function($) {
  'use strict';
  $('.dropify').dropify();
})(jQuery);

(function($) {
  'use strict';
  $("my-awesome-dropzone").dropzone({
      url: "bootstrapdash.com/"
  });
})(jQuery);

(function($) {
    'use strict';
    /*Quill editor*/
    if ($("#quillExample1").length) {
        var quill = new Quill('#quillExample1', {
            modules: {
                toolbar: [
                    [{
                        header: [1, 2, false]
                    }],
                    ['bold', 'italic', 'underline'],
                    ['image', 'code-block']
                ]
            },
            placeholder: 'Compose an epic...',
            theme: 'snow' // or 'bubble'
        });
    }

    /*simplemde editor*/
    if($("#simpleMde").length) {
      var simplemde = new SimpleMDE({ element: $("#simpleMde")[0] });
    }

    /*Tinymce editor*/
    if ($("#tinyMceExample").length) {
        tinymce.init({
            selector: '#tinyMceExample',
            height: 500,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab: true,
            templates: [{
                    title: 'Test template 1',
                    content: 'Test 1'
                },
                {
                    title: 'Test template 2',
                    content: 'Test 2'
                }
            ],
            content_css: []
        });
    }

    /*Summernote editor*/
    if ($("#summernoteExample").length) {
        $('#summernoteExample').summernote({
            height: 300,
            tabsize: 2
        });
    }

    /*X-editable editor*/
    if($('#editable-form').length) {
      $.fn.editable.defaults.mode = 'inline';
      $.fn.editableform.buttons =
          '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
          '<i class="fa fa-fw fa-check"></i>' +
          '</button>' +
          '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
          '<i class="fa fa-fw fa-times"></i>' +
          '</button>';
      $('#username').editable({
          type: 'text',
          pk: 1,
          name: 'username',
          title: 'Enter username'
      });

      $('#firstname').editable({
          validate: function(value) {
              if ($.trim(value) === '') return 'This field is required';
          }
      });

      $('#sex').editable({
          source: [{
                  value: 1,
                  text: 'Male'
              },
              {
                  value: 2,
                  text: 'Female'
              }
          ]
      });

      $('#status').editable();

      $('#group').editable({
          showbuttons: false
      });

      $('#vacation').editable({
          datepicker: {
              todayBtn: 'linked'
          }
      });

      $('#dob').editable();

      $('#event').editable({
          placement: 'right',
          combodate: {
              firstItem: 'name'
          }
      });

      $('#meeting_start').editable({
          format: 'yyyy-mm-dd hh:ii',
          viewformat: 'dd/mm/yyyy hh:ii',
          validate: function(v) {
              if (v && v.getDate() === 10) return 'Day cant be 10!';
          },
          datetimepicker: {
              todayBtn: 'linked',
              weekStart: 1
          }
      });

      $('#comments').editable({
          showbuttons: 'bottom'
      });

      $('#note').editable();
      $('#pencil').on("click", function (e) {
          e.stopPropagation();
          e.preventDefault();
          $('#note').editable('toggle');
      });

      $('#state').editable({
          source: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
      });

      $('#state2').editable({
          value: 'California',
          typeahead: {
              name: 'state',
              local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
          }
      });

      $('#fruits').editable({
          pk: 1,
          limit: 3,
          source: [{
                  value: 1,
                  text: 'banana'
              },
              {
                  value: 2,
                  text: 'peach'
              },
              {
                  value: 3,
                  text: 'apple'
              },
              {
                  value: 4,
                  text: 'watermelon'
              },
              {
                  value: 5,
                  text: 'orange'
              }
          ]
      });

      $('#tags').editable({
          inputclass: 'input-large',
          select2: {
              tags: ['html', 'javascript', 'css', 'ajax'],
              tokenSeparators: [",", " "]
          }
      });

      $('#address').editable({
          url: '/post',
          value: {
              city: "Moscow",
              street: "Lenina",
              building: "12"
          },
          validate: function(value) {
              if (value.city === '') return 'city is required!';
          },
          display: function(value) {
              if (!value) {
                  $(this).empty();
                  return;
              }
              var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $('<div>').text(value.street).html() + ' st., bld. ' + $('<div>').text(value.building).html();
              $(this).html(html);
          }
      });

      $('#user .editable').on('hidden', function(e, reason) {
          if (reason === 'save' || reason === 'nochange') {
              var $next = $(this).closest('tr').next().find('.editable');
              if ($('#autoopen').is(':checked')) {
                  setTimeout(function() {
                      $next.editable('show');
                  }, 300);
              } else {
                  $next.focus();
              }
          }
      });
    }
})(jQuery);

(function($) {
  'use strict';
  $(function() {
    $('.file-upload-browse').on('click', function(){
      var file = $(this).parent().parent().parent().find('.file-upload-default');
      file.trigger('click');
    });
    $('.file-upload-default').on('change', function(){
      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  });
})(jQuery);

(function($) {
    'use strict';
    var data = [{
            data: 18000,
            color: '#FABA66',
            label: 'Linda'
        },
        {
            data: 20000,
            color: '#F36368',
            label: 'John'
        },
        {
            data: 13000,
            color: '#76C1FA',
            label: 'Margaret'
        },
        {
            data: 15000,
            color: '#63CF72',
            label: 'Richard'
        }
    ];

    if($("#pie-chart").length) {
      $.plot("#pie-chart", data, {
          series: {
              pie: {
                  show: true,
                  radius: 1,
                  label: {
                      show: true,
                      radius: 3 / 4,
                      formatter: labelFormatter,
                      background: {
                          opacity: 0.5
                      }
                  }
              }
          },
          legend: {
              show: false
          }
      });
    }

    function labelFormatter(label, series) {
        return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
    }


    /*---------------------
     ----- LINE CHART -----
     ---------------------*/

    var d1 = [
        [0, 30],
        [1, 35],
        [2, 35],
        [3, 30],
        [4, 30]
    ];
    var d2 = [
        [0, 50],
        [1, 40],
        [2, 45],
        [3, 60],
        [4, 50]
    ];
    var d3 = [
        [0, 40],
        [1, 50],
        [2, 35],
        [3, 25],
        [4, 40]
    ];

    var stackedData = [
      {data: d1, color: "#76C1FA"},
      {data: d2, color: "#63CF72"},
      {data: d3, color: "#F36368"}
    ];
    /*---------------------------------------------------
        Make some random data for Recent Items chart
    ---------------------------------------------------*/


    var options = {
        series: {
            shadowSize: 0,
            lines: {
                show: true,
            },
        },
        grid: {
            borderWidth: 1,
            labelMargin: 10,
            mouseActiveRadius: 6,
            borderColor: '#eee',
            show: true,
            hoverable: true,
            clickable: true

        },
        xaxis: {
            tickColor: '#eee',
            tickDecimals: 0,
            font: {
                lineHeight: 15,
                style: "normal",
                color: "#000"
            },
            shadowSize: 0,
            ticks: [
                [0, "Jan"],
                [1, "Feb"],
                [2, "Mar"],
                [3, "Apr"],
                [4, "May"],
                [5, "Jun"],
                [6, "Jul"],
                [7, "Aug"],
                [8, "Sep"],
                [9, "Oct"],
                [10, "Nov"],
                [11, "Dec"]
            ]
        },

        yaxis: {
            tickColor: '#eee',
            tickDecimals: 0,
            font: {
                lineHeight: 15,
                style: "normal",
                color: "#000",
            },
            shadowSize: 0
        },

        legend: {
            container: '.flc-line',
            backgroundOpacity: 0.5,
            noColumns: 0,
            backgroundColor: "white",
            lineWidth: 0
        },
        colors: ["#F36368", "#63CF72", "#68B3C8"]
    };


    if ($("#line-chart").length) {
        $.plot($("#line-chart"), [{
                data: d1,
                lines: {
                    show: true
                },
                label: 'Product A',
                stack: true,
                color: '#F36368'
            },
            {
                data: d2,
                lines: {
                    show: true
                },
                label: 'Product B',
                stack: true,
                color: '#FABA66'
            },
            {
                data: d3,
                lines: {
                    show: true
                },
                label: 'Product C',
                stack: true,
                color: '#68B3C8'
            }
        ], options);
    }




    /*---------------------------------
        Tooltips for Flot Charts
    ---------------------------------*/
    if ($(".flot-chart-line").length) {
        $(".flot-chart-line").bind("plothover", function(event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                $(".flot-tooltip").html(item.series.label + " Sales " + " : " + y).css({
                    top: item.pageY + 5,
                    left: item.pageX + 5
                }).show();
            } else {
                $(".flot-tooltip").hide();
            }
        });

        $("<div class='flot-tooltip' class='chart-tooltip'></div>").appendTo("body");
    }




    /*---------------------
     ----- AREA CHART -----
     ---------------------*/


    var d1 = [
        [0, 0],
        [1, 35],
        [2, 35],
        [3, 30],
        [4, 30],
        [5, 5],
        [6, 32],
        [7, 37],
        [8, 30],
        [9, 35],
        [10, 30],
        [11, 5]
    ];


    var options = {
        series: {
            shadowSize: 0,
            curvedLines: { //This is a third party plugin to make curved lines
                apply: true,
                active: true,
                monotonicFit: true
            },
            lines: {
                show: false,
                fill: 0.98,
                lineWidth: 0,
            },
        },
        grid: {
            borderWidth: 0,
            labelMargin: 10,
            hoverable: true,
            clickable: true,
            mouseActiveRadius: 6,

        },
        xaxis: {
            tickDecimals: 0,
            tickLength:0
        },

        yaxis: {
            tickDecimals: 0,
            tickLength:0
        },

        legend: {
            show: false
        }
    };

    var curvedLineOptions = {
        series: {
            shadowSize: 0,
            curvedLines: { //This is a third party plugin to make curved lines
                apply: true,
                active: true,
                monotonicFit: true
            },
            lines: {
                show: false,
                lineWidth: 0,
            },
        },
        grid: {
            borderWidth: 0,
            labelMargin:10,
            hoverable: true,
            clickable: true,
            mouseActiveRadius:6,

        },
        xaxis: {
            tickDecimals: 0,
            ticks: false
        },

        yaxis: {
            tickDecimals: 0,
            ticks: false
        },

        legend: {
            noColumns: 4,
            container: $("#chartLegend")
        }
    };

    if ($("#area-chart").length) {
        $.plot($("#area-chart"), [{
            data: d1,
            lines: {
                show: true,
                fill: 0.6
            },
            label: 'Product 1',
            stack: true,
            color: '#76C1FA'
        }], options);
    }




    /*---------------------
     ----- COLUMN CHART -----
     ---------------------*/

    $(function() {

        var data = [
            ["January", 10],
            ["February", 8],
            ["March", 4],
            ["April", 13],
            ["May", 17],
            ["June", 9]
        ];

        if($("#column-chart").length) {
          $.plot("#column-chart", [data], {
              series: {
                  bars: {
                      show: true,
                      barWidth: 0.6,
                      align: "center"
                  }
              },
              xaxis: {
                  mode: "categories",
                  tickLength: 0
              },

              grid: {
                  borderWidth: 0,
                  labelMargin: 10,
                  hoverable: true,
                  clickable: true,
                  mouseActiveRadius: 6,
              }

          });
        }
    });



    /*--------------------------------
     ----- STACKED CHART -----
     --------------------------------*/

    $(function() {

        var d1 = [];
        for (var i = 0; i <= 10; i += 1) {
            d1.push([i, parseInt(Math.random() * 30)]);
        }

        var d2 = [];
        for (var i = 0; i <= 10; i += 1) {
            d2.push([i, parseInt(Math.random() * 30)]);
        }

        var d3 = [];
        for (var i = 0; i <= 10; i += 1) {
            d3.push([i, parseInt(Math.random() * 30)]);
        }

        if($("#stacked-bar-chart").length) {
          $.plot("#stacked-bar-chart", stackedData, {
              series: {
                  stack: 0,
                  lines: {
                      show: false,
                      fill: true,
                      steps: false
                  },
                  bars: {
                      show: true,
                      fill: true,
                      barWidth: 0.6
                  },
              },
              grid: {
                  borderWidth: 0,
                  labelMargin: 10,
                  hoverable: true,
                  clickable: true,
                  mouseActiveRadius: 6,
              }
          });
        }
    });

    /*--------------------------------
     ----- REALTIME CHART -----
     --------------------------------*/
    $(function() {

        // We use an inline data source in the example, usually data would
        // be fetched from a server

        var data = [],
            totalPoints = 300;

        function getRandomData() {

            if (data.length > 0)
                data = data.slice(1);

            // Do a random walk

            while (data.length < totalPoints) {

                var prev = data.length > 0 ? data[data.length - 1] : 50,
                    y = prev + Math.random() * 10 - 5;

                if (y < 0) {
                    y = 0;
                } else if (y > 100) {
                    y = 100;
                }

                data.push(y);
            }

            // Zip the generated y values with the x values

            var res = [];
            for (var i = 0; i < data.length; ++i) {
                res.push([i, data[i]])
            }

            return res;
        }

        // Set up the control widget

        var updateInterval = 30;
        if($("#realtime-chart").length) {
          var plot = $.plot("#realtime-chart", [getRandomData()], {
              series: {
                  shadowSize: 0 // Drawing is faster without shadows
              },
              yaxis: {
                  min: 0,
                  max: 100
              },
              xaxis: {
                  show: false
              },
              grid: {
                  borderWidth: 0,
                  labelMargin: 10,
                  hoverable: true,
                  clickable: true,
                  mouseActiveRadius: 6,
              }

          });
          function update() {

              plot.setData([getRandomData()]);

              // Since the axes don't change, we don't need to call plot.setupGrid()

              plot.draw();
              setTimeout(update, updateInterval);
          }

          update();
        }

    });
    /*--------------------------------
     ----- CURVED LINE CHART -----
     --------------------------------*/

    $(function() {

      var d1 = [
          [0, 6],
          [1, 14],
          [2, 10],
          [3, 14],
          [4, 5]
      ];
      var d2 = [
          [0, 6],
          [1, 7],
          [2, 11],
          [3, 8],
          [4, 11]
      ];
      var d3 = [
          [0, 6],
          [1, 5],
          [2, 6],
          [3, 10],
          [4, 5]
      ];

        if($("#curved-line-chart").length) {
          $.plot($("#curved-line-chart"), [
           {data: d1, lines: { show: true, fill: 0.98 }, label: 'Plans', stack: true, color: '#5E50F9' },
           {data: d2, lines: { show: true, fill: 0.98 }, label: 'Purchase', stack: true, color: '#8C95FC' },
           {data: d3, lines: { show: true, fill: 0.98 }, label: 'Services', stack: true, color: '#A8B4FD' }
         ], curvedLineOptions);
        }

    });

})(jQuery);

(function($) {
   'use strict';

    // Jquery Tag Input Starts

    $('#tags').tagsInput({
        'width': '100%',
        'height': '75%',
        'interactive': true,
        'defaultText': 'Add More',
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 20, // if not provided there is no limit
        'placeholderColor': '#666666'
    });

    // Jquery Tag Input Ends
    // Jquery Knob Starts Here

    $(function() {
        $(".dial").knob();
    });

    // Jquery Knob Ends Here
    // Jquery Bar Rating Starts

    $(function() {
        function ratingEnable() {
            $('#example-1to10').barrating('show', {
                theme: 'bars-1to10'
            });

            $('#example-movie').barrating('show', {
                theme: 'bars-movie'
            });

            $('#example-movie').barrating('set', 'Mediocre');

            $('#example-square').barrating('show', {
                theme: 'bars-square',
                showValues: true,
                showSelectedRating: false
            });

            $('#example-pill').barrating('show', {
                theme: 'bars-pill',
                initialRating: 'A',
                showValues: true,
                showSelectedRating: false,
                allowEmpty: true,
                emptyValue: '-- no rating selected --',
                onSelect: function(value, text) {
                    alert('Selected rating: ' + value);
                }
            });

            $('#example-reversed').barrating('show', {
                theme: 'bars-reversed',
                showSelectedRating: true,
                reverse: true
            });

            $('#example-horizontal').barrating('show', {
                theme: 'bars-horizontal',
                reverse: true,
                hoverState: false
            });

            $('#example-fontawesome').barrating({
                theme: 'fontawesome-stars',
                showSelectedRating: false
            });

            $('#example-css').barrating({
                theme: 'css-stars',
                showSelectedRating: false
            });

            $('#example-bootstrap').barrating({
                theme: 'bootstrap-stars',
                showSelectedRating: false
            });

            var currentRating = $('#example-fontawesome-o').data('current-rating');

            $('.stars-example-fontawesome-o .current-rating')
                .find('span')
                .html(currentRating);

            $('.stars-example-fontawesome-o .clear-rating').on('click', function(event) {
                event.preventDefault();

                $('#example-fontawesome-o')
                    .barrating('clear');
            });

            $('#example-fontawesome-o').barrating({
                theme: 'fontawesome-stars-o',
                showSelectedRating: false,
                initialRating: currentRating,
                onSelect: function(value, text) {
                    if (!value) {
                        $('#example-fontawesome-o')
                            .barrating('clear');
                    } else {
                        $('.stars-example-fontawesome-o .current-rating')
                            .addClass('hidden');

                        $('.stars-example-fontawesome-o .your-rating')
                            .removeClass('hidden')
                            .find('span')
                            .html(value);
                    }
                },
                onClear: function(value, text) {
                    $('.stars-example-fontawesome-o')
                        .find('.current-rating')
                        .removeClass('hidden')
                        .end()
                        .find('.your-rating')
                        .addClass('hidden');
                }
            });
        }

        function ratingDisable() {
            $('select').barrating('destroy');
        }

        $('.rating-enable').click(function(event) {
            event.preventDefault();

            ratingEnable();

            $(this).addClass('deactivated');
            $('.rating-disable').removeClass('deactivated');
        });

        $('.rating-disable').click(function(event) {
            event.preventDefault();

            ratingDisable();

            $(this).addClass('deactivated');
            $('.rating-enable').removeClass('deactivated');
        });

        ratingEnable();
    });


    // Jquery Bar Rating Ends

})(jQuery);

(function($) {
  'use strict';
  $(function() {
    $('.repeater').repeater({
      // (Optional)
      // "defaultValues" sets the values of added items.  The keys of
      // defaultValues refer to the value of the input's name attribute.
      // If a default value is not specified for an input, then it will
      // have its value cleared.
      defaultValues: {
        'text-input': 'foo'
      },
      // (Optional)
      // "show" is called just after an item is added.  The item is hidden
      // at this point.  If a show callback is not given the item will
      // have $(this).show() called on it.
      show: function() {
        $(this).slideDown();
      },
      // (Optional)
      // "hide" is called when a user clicks on a data-repeater-delete
      // element.  The item is still visible.  "hide" is passed a function
      // as its first argument which will properly remove the item.
      // "hide" allows for a confirmation step, to send a delete request
      // to the server, etc.  If a hide callback is not given the item
      // will be deleted.
      hide: function(deleteElement) {
        if (confirm('Are you sure you want to delete this element?')) {
          $(this).slideUp(deleteElement);
        }
      },
      // (Optional)
      // Removes the delete button from the first list item,
      // defaults to false.
      isFirstItemUndeletable: true
    })
  });
})(jQuery);

(function($) {
    'use strict';
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    $(function() {
        // validate the comment form when it is submitted
        $("#commentForm").validate({
          errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
          },
          highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
          }
        });
        // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                username: {
                    required: true,
                    minlength: 2
                },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
                topic: {
                    required: "#newsletter:checked",
                    minlength: 2
                },
                agree: "required"
            },
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",
                username: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                agree: "Please accept our policy",
                topic: "Please select at least 2 topics"
            },
            errorPlacement: function(label, element) {
              label.addClass('mt-2 text-danger');
              label.insertAfter(element);
            },
            highlight: function(element, errorClass) {
              $(element).parent().addClass('has-danger')
              $(element).addClass('form-control-danger')
            }
        });
        // propose username by combining first- and lastname
        $("#username").focus(function() {
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            if (firstname && lastname && !this.value) {
                this.value = firstname + "." + lastname;
            }
        });
        //code to hide topic selection, disable for demo
        var newsletter = $("#newsletter");
        // newsletter topics are optional, hide at first
        var inital = newsletter.is(":checked");
        var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
        var topicInputs = topics.find("input").attr("disabled", !inital);
        // show when newsletter is checked
        newsletter.on("click", function () {
            topics[this.checked ? "removeClass" : "addClass"]("gray");
            topicInputs.attr("disabled", !this.checked);
        });
    });
})(jQuery);

(function($) {
  'use strict';
  if($("#timepicker-example").length) {
    $('#timepicker-example').datetimepicker({
      format: 'LT'
    });
  }
  if ($(".color-picker").length) {
    $('.color-picker').asColorPicker();
  }
  if ($("#datepicker-popup").length) {
    $('#datepicker-popup').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }
  if ($("#inline-datepicker").length) {
    $('#inline-datepicker').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }
  if ($(".datepicker-autoclose").length) {
    $('.datepicker-autoclose').datepicker({
      autoclose: true
    });
  }
  if ($('input[name="date-range"]').length) {
    $('input[name="date-range"]').daterangepicker();
  }
  if ($('input[name="date-time-range"]').length) {
    $('input[name="date-time-range"]').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY h:mm A'
      }
    });
  }
})(jQuery);

'use strict';


function initMap() {
  //Map location
  var MapLocation = {
    lat: 40.6971494,
    lng: -74.2598719
  };

  // Map Zooming
  var MapZoom = 14;


  // Basic Map
  var MapWithMarker = new google.maps.Map(document.getElementById('map-with-marker'), {
    zoom: MapZoom,
    center: MapLocation
  });
  var marker_1 = new google.maps.Marker({
    position: MapLocation,
    map: MapWithMarker
  });

  // Basic map with cutom marker
  var CutomMarker = new google.maps.Map(document.getElementById('cutom-marker'), {
    zoom: MapZoom,
    center: MapLocation
  });
  var iconBase = '../../images/sprites/';
  var marker_2 = new google.maps.Marker({
    position: MapLocation,
    map: CutomMarker,
    icon: iconBase + 'flag.png'
  });

  // Map without controls
  var MinimalMap = new google.maps.Map(document.getElementById('map-minimal'), {
    zoom: MapZoom,
    center: MapLocation,
    disableDefaultUI: true
  });
  var marker_3 = new google.maps.Marker({
    position: MapLocation,
    map: MinimalMap
  });

  // Night Mode
  var NightModeMap = new google.maps.Map(document.getElementById('night-mode-map'), {
    zoom: MapZoom,
    center: MapLocation,
    styles: [{
      "featureType": "all",
      "elementType": "all",
      "stylers": [{
          "saturation": -100
        },
        {
          "gamma": 0.5
        }
      ]
    }]
  });

  // Apple Theme
  var AppletThemeMap = new google.maps.Map(document.getElementById('apple-map-theme'), {
    zoom: MapZoom,
    center: MapLocation,
    styles: [{
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [{
          "color": "#f7f1df"
        }]
      },
      {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [{
          "color": "#d0e3b4"
        }]
      },
      {
        "featureType": "landscape.natural.terrain",
        "elementType": "geometry",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "poi.business",
        "elementType": "all",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "poi.medical",
        "elementType": "geometry",
        "stylers": [{
          "color": "#fbd3da"
        }]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [{
          "color": "#bde6ab"
        }]
      },
      {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#ffe15f"
        }]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{
          "color": "#efd151"
        }]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#ffffff"
        }]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "black"
        }]
      },
      {
        "featureType": "transit.station.airport",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#cfb2db"
        }]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
          "color": "#a2daf2"
        }]
      }
    ]
  });

  // Nature Theme
  var NatureThemeMap = new google.maps.Map(document.getElementById('nature-map-theme'), {
    zoom: MapZoom,
    center: MapLocation,
    styles: [{
        "featureType": "landscape",
        "stylers": [{
            "hue": "#FFA800"
          },
          {
            "saturation": 0
          },
          {
            "lightness": 0
          },
          {
            "gamma": 1
          }
        ]
      },
      {
        "featureType": "road.highway",
        "stylers": [{
            "hue": "#53FF00"
          },
          {
            "saturation": -73
          },
          {
            "lightness": 40
          },
          {
            "gamma": 1
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "stylers": [{
            "hue": "#FBFF00"
          },
          {
            "saturation": 0
          },
          {
            "lightness": 0
          },
          {
            "gamma": 1
          }
        ]
      },
      {
        "featureType": "road.local",
        "stylers": [{
            "hue": "#00FFFD"
          },
          {
            "saturation": 0
          },
          {
            "lightness": 30
          },
          {
            "gamma": 1
          }
        ]
      },
      {
        "featureType": "water",
        "stylers": [{
            "hue": "#00BFFF"
          },
          {
            "saturation": 6
          },
          {
            "lightness": 8
          },
          {
            "gamma": 1
          }
        ]
      },
      {
        "featureType": "poi",
        "stylers": [{
            "hue": "#679714"
          },
          {
            "saturation": 33.4
          },
          {
            "lightness": -25.4
          },
          {
            "gamma": 1
          }
        ]
      }
    ]
  });

  // Captor Theme
  var CaptorThemeMap = new google.maps.Map(document.getElementById('captor-map-theme'), {
    zoom: MapZoom,
    center: MapLocation,
    styles: [{
        "featureType": "water",
        "stylers": [{
          "color": "#0e171d"
        }]
      },
      {
        "featureType": "landscape",
        "stylers": [{
          "color": "#1e303d"
        }]
      },
      {
        "featureType": "road",
        "stylers": [{
          "color": "#1e303d"
        }]
      },
      {
        "featureType": "poi.park",
        "stylers": [{
          "color": "#1e303d"
        }]
      },
      {
        "featureType": "transit",
        "stylers": [{
            "color": "#182731"
          },
          {
            "visibility": "simplified"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [{
            "color": "#f0c514"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.stroke",
        "stylers": [{
            "color": "#1e303d"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#e77e24"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#94a5a6"
        }]
      },
      {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [{
            "visibility": "simplified"
          },
          {
            "color": "#e84c3c"
          }
        ]
      },
      {
        "featureType": "poi",
        "stylers": [{
            "color": "#e84c3c"
          },
          {
            "visibility": "off"
          }
        ]
      }
    ]
  });

  // Avagardo Theme
  var AvagardoThemeMap = new google.maps.Map(document.getElementById('avocado-map-theme'), {
    zoom: MapZoom,
    center: MapLocation,
    styles: [{
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
            "visibility": "on"
          },
          {
            "color": "#aee2e0"
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#abce83"
        }]
      },
      {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#769E72"
        }]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#7B8758"
        }]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.stroke",
        "stylers": [{
          "color": "#EBF4A4"
        }]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [{
            "visibility": "simplified"
          },
          {
            "color": "#8dab68"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [{
          "visibility": "simplified"
        }]
      },
      {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#5B5B3F"
        }]
      },
      {
        "featureType": "road",
        "elementType": "labels.text.stroke",
        "stylers": [{
          "color": "#ABCE83"
        }]
      },
      {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [{
          "color": "#A4C67D"
        }]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [{
          "color": "#9BBF72"
        }]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [{
          "color": "#EBF4A4"
        }]
      },
      {
        "featureType": "transit",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [{
            "visibility": "on"
          },
          {
            "color": "#87ae79"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#7f2200"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "labels.text.stroke",
        "stylers": [{
            "color": "#ffffff"
          },
          {
            "visibility": "on"
          },
          {
            "weight": 4.1
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#495421"
        }]
      },
      {
        "featureType": "administrative.neighborhood",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      }
    ]
  });

  // Propia Theme
  var PropiaThemeMap = new google.maps.Map(document.getElementById('propia-map-theme'), {
    zoom: MapZoom,
    center: MapLocation,
    styles: [{
        "featureType": "landscape",
        "stylers": [{
            "visibility": "simplified"
          },
          {
            "color": "#2b3f57"
          },
          {
            "weight": 0.1
          }
        ]
      },
      {
        "featureType": "administrative",
        "stylers": [{
            "visibility": "on"
          },
          {
            "hue": "#ff0000"
          },
          {
            "weight": 0.4
          },
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels.text",
        "stylers": [{
            "weight": 1.3
          },
          {
            "color": "#FFFFFF"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [{
            "color": "#f55f77"
          },
          {
            "weight": 3
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [{
            "color": "#f55f77"
          },
          {
            "weight": 1.1
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [{
            "color": "#f55f77"
          },
          {
            "weight": 0.4
          }
        ]
      },
      {},
      {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [{
            "weight": 0.8
          },
          {
            "color": "#ffffff"
          },
          {
            "visibility": "on"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [{
            "color": "#ffffff"
          },
          {
            "weight": 0.7
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "poi",
        "stylers": [{
          "color": "#6c5b7b"
        }]
      },
      {
        "featureType": "water",
        "stylers": [{
          "color": "#f3b191"
        }]
      },
      {
        "featureType": "transit.line",
        "stylers": [{
          "visibility": "on"
        }]
      }
    ]
  });
}

(function($) {
  'use strict';
  //Open submenu on hover in compact sidebar mode and horizontal menu mode
  $(document).on('mouseenter mouseleave', '.sidebar .nav-item', function (ev) {
      var body = $('body');
      var sidebarIconOnly = body.hasClass("sidebar-icon-only");
      var horizontalMenu = body.hasClass("horizontal-menu");
      var sidebarFixed = body.hasClass("sidebar-fixed");
      if(!('ontouchstart' in document.documentElement)) {
        if(sidebarIconOnly || horizontalMenu) {
          if(sidebarFixed) {
            if(ev.type === 'mouseenter') {
              body.removeClass('sidebar-icon-only');
            }
          }
          else {
            var $menuItem = $(this);
            if(ev.type === 'mouseenter') {
              $menuItem.addClass('hover-open')
            }
            else {
              $menuItem.removeClass('hover-open')
            }
          }
        }
      }
  });
})(jQuery);


(function($) {
  'use strict';
  $(function() {
    $('.icheck input').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal',
        increaseArea: '20%'
    });
    $('.icheck-square input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square',
        increaseArea: '20%'
    });
    $('.icheck-flat input').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat',
        increaseArea: '20%'
    });
    var icheckLineArray = $('.icheck-line input');
    for (var i = 0; i < icheckLineArray.length; i++) {
      var self = $(icheckLineArray[i]);
      var label = self.next();
      var label_text = label.text();

      label.remove();
      self.iCheck({
          checkboxClass: 'icheckbox_line-blue',
          radioClass: 'iradio_line',
          insert: '<div class="icheck_line-icon"></div>' + label_text
      });
    }
    $('.icheck-polaris input').iCheck({
        checkboxClass: 'icheckbox_polaris',
        radioClass: 'iradio_polaris',
        increaseArea: '20%'
    });
    $('.icheck-futurico input').iCheck({
        checkboxClass: 'icheckbox_futurico',
        radioClass: 'iradio_futurico',
        increaseArea: '20%'
    });
  });
})(jQuery);


(function($) {
  'use strict';

  if($('#range_01').length) {
    $("#range_01").ionRangeSlider();
  }

  if($("#range_02").length) {
    $("#range_02").ionRangeSlider({
      min: 100,
      max: 1000,
      from: 550
    });
  }

  if($("#range_03").length) {
    $("#range_03").ionRangeSlider({
      type: "double",
      grid: true,
      min: 0,
      max: 1000,
      from: 200,
      to: 800,
      prefix: "$"
    });
  }

  if($("#range_04").length) {
    $("#range_04").ionRangeSlider({
      type: "double",
      min: 100,
      max: 200,
      from: 145,
      to: 155,
      prefix: "Weight: ",
      postfix: " million pounds",
      decorate_both: true
    });
  }

  if($("#range_05").length) {
    $("#range_05").ionRangeSlider({
      type: "double",
      min: 1000,
      max: 2000,
      from: 1200,
      to: 1800,
      hide_min_max: true,
      hide_from_to: true,
      grid: false
    });
  }

  if($("#range_06").length) {
    $("#range_06").ionRangeSlider({
      type: "double",
      min: 1000,
      max: 2000,
      from: 1200,
      to: 1800,
      hide_min_max: true,
      hide_from_to: true,
      grid: true
    });
  }

  if($("#range_07").length) {
    $("#range_07").ionRangeSlider({
      type: "double",
      grid: true,
      min: 0,
      max: 10000,
      from: 1000,
      prefix: "$"
    });
  }

  if($("#range_08").length) {
    $("#range_08").ionRangeSlider({
      type: "single",
      grid: true,
      min: -90,
      max: 90,
      from: 0,
      postfix: "Â°"
    });
  }

  if($("#range_09").length) {
    $("#range_09").ionRangeSlider({
      type: "double",
      min: 0,
      max: 10000,
      grid: true
    });
  }

  if($("#range_10").length) {
    $("#range_10").ionRangeSlider({
      type: "double",
      min: 0,
      max: 10000,
      grid: true,
      grid_num: 10
    });
  }

  if($("#range_11").length) {
    $("#range_11").ionRangeSlider({
      type: "double",
      min: 0,
      max: 10000,
      step: 500,
      grid: true,
      grid_snap: true
    });
  }

  if($("#range_12").length) {
    $("#range_12").ionRangeSlider({
      type: "single",
      min: 0,
      max: 10,
      step: 2.34,
      grid: true,
      grid_snap: true
    });
  }

  if($("#range_13").length) {
    $("#range_13").ionRangeSlider({
      type: "double",
      min: 0,
      max: 100,
      from: 30,
      to: 70,
      from_fixed: true
    });
  }

  if($("#range_14").length) {
    $("#range_14").ionRangeSlider({
      min: 0,
      max: 100,
      from: 30,
      from_min: 10,
      from_max: 50
    });
  }

  if($("#range_15").length) {
    $("#range_15").ionRangeSlider({
      min: 0,
      max: 100,
      from: 30,
      from_min: 10,
      from_max: 50,
      from_shadow: true
    });
  }

  if($("#range_16").length) {
    $("#range_16").ionRangeSlider({
      type: "double",
      min: 0,
      max: 100,
      from: 20,
      from_min: 10,
      from_max: 30,
      from_shadow: true,
      to: 80,
      to_min: 70,
      to_max: 90,
      to_shadow: true,
      grid: true,
      grid_num: 10
    });
  }

  if($("#range_17").length) {
    $("#range_17").ionRangeSlider({
      min: 0,
      max: 100,
      from: 30,
      disable: true
    });
  }

})(jQuery);

/*
 * jq.TableSort -- jQuery Table sorter Plug-in.
 *
 * Version 1.0.0.
 *
 * Copyright (c) 2017 Dmitry Zavodnikov.
 *
 * Licensed under the MIT License.
 */
(function($) {
    'use strict';
    var SORT    = 'sort'    ;
    var ASC     = 'asc'     ;
    var DESC    = 'desc'    ;
    var UNSORT  = 'unsort'  ;

    var config  = {
        defaultColumn:  0,
        defaultOrder:   'asc',
        styles:         {
            'sort'  :   'sortStyle',
            'asc'   :   'ascStyle',
            'desc'  :   'descStyle',
            'unsort':   'unsortStyle'
        },
        selector:        function(tableBody, column) {
          var groups = [];

          var tableRows = $(tableBody).find('tr');
          for (var i=0; i<tableRows.length; i++) {
            var td = $(tableRows[i]).find('td')[column];

            groups.push({
                'values':   [ tableRows[i] ],
                'key'   :   $(td).text()
            });
          }
          return groups;
        },
        comparator:        function(group1, group2) {
            return group1.key.localeCompare(group2.key);
        }
    };

    function getTableHeaders(table) {
        return $(table).find('thead > tr > th');
    }

    function getSortableTableHeaders(table) {
        return getTableHeaders(table).filter(function(index){
            return $(this).hasClass(config.styles[SORT]);
        });
    }

    function changeOrder(table, column) {
        var sortedHeader = getTableHeaders(table).filter(function(index) {
            return $(this).hasClass(config.styles[ASC]) || $(this).hasClass(config.styles[DESC]);
        });

        var sordOrder = config.defaultOrder;
        if (sortedHeader.hasClass(config.styles[ASC ])) {
            sordOrder = ASC;
        }
        if (sortedHeader.hasClass(config.styles[DESC])) {
            sordOrder = DESC;
        }

        var th = getTableHeaders(table)[column];

        if (th === sortedHeader[0]) {
            if (sordOrder === ASC) {
                sordOrder = DESC;
            } else {
                sordOrder = ASC;
            }
        }

        var headers = getSortableTableHeaders(table);
        headers.removeClass(config.styles[ASC   ]);
        headers.removeClass(config.styles[DESC  ]);
        headers.addClass(   config.styles[UNSORT]);

        $(th).removeClass(config.styles[UNSORT]);
        $(th).addClass(config.styles[sordOrder]);

        var tbody = $(table).find('tbody')[0];
        var groups = config.selector(tbody, column);

        // Sorting.
        groups.sort(function(a, b){
            var res = config.comparator(a, b);
            return sordOrder === ASC ? res : -1 * res;
        });

        for(var i=0;i<groups.length;i++) {
          var trList = groups[i];
          var trListValues = trList.values;
          for( var j=0;j<trListValues.length;j++) {
            tbody.append(trListValues[j]);
          }
        }
    }

    $.fn.tablesort = function(userConfig) {
        // Create and save table sort configuration.
        $.extend(config, userConfig);

        // Process all selected tables.
        var selectedTables = this;
        for ( var i=0;i<selectedTables.length;i++) {
          var table = selectedTables[i];
          var tableHeader = getSortableTableHeaders(table);
          for ( var j=0;j<tableHeader.length;j++) {
            var th = tableHeader[j];
            $(th).on("click", function (event) {
                var clickColumn = $.inArray(event.currentTarget, getTableHeaders(table));
                changeOrder(table, clickColumn);
            });
          }
        }
        return this;
    };
})(jQuery);

(function($) {
  'use strict';
  if($("#fileuploader").length) {
    $("#fileuploader").uploadFile({
        url: "YOUR_FILE_UPLOAD_URL",
        fileName: "myfile"
    });
  }
})(jQuery);

(function($) {
    'use strict';
    $(function() {

        //basic config
        if($("#js-grid").length) {
          $("#js-grid").jsGrid({
              height: "500px",
              width: "100%",
              filtering: true,
              editing: true,
              inserting: true,
              sorting: true,
              paging: true,
              autoload: true,
              pageSize: 15,
              pageButtonCount: 5,
              deleteConfirm: "Do you really want to delete the client?",
              data: db.clients,
              fields: [{
                      name: "Name",
                      type: "text",
                      width: 150
                  },
                  {
                      name: "Age",
                      type: "number",
                      width: 50
                  },
                  {
                      name: "Address",
                      type: "text",
                      width: 200
                  },
                  {
                      name: "Country",
                      type: "select",
                      items: db.countries,
                      valueField: "Id",
                      textField: "Name"
                  },
                  {
                    name: "Married",
                    title: "Is Married",
                    itemTemplate: function(value, item) {
                      return $("<div>")
                      .addClass("form-check mt-0")
                        .append(
                          $("<label>").addClass("form-check-label")
                          .append(
                            $("<input>").attr("type","checkbox")
                            .addClass("form-check-input")
                            .attr("checked", value || item.Checked)
                            .on("change", function() {
                               item.Checked = $(this).is(":checked");
                            })
                          )
                          .append('<i class="input-helper"></i>')
                        );
                    }
                  },
                  {
                      type: "control"
                  }
              ]
          });
        }


        //Static
        if($("#js-grid-static").length) {
          $("#js-grid-static").jsGrid({
              height: "500px",
              width: "100%",

              sorting: true,
              paging: true,

              data: db.clients,

              fields: [{
                      name: "Name",
                      type: "text",
                      width: 150
                  },
                  {
                      name: "Age",
                      type: "number",
                      width: 50
                  },
                  {
                      name: "Address",
                      type: "text",
                      width: 200
                  },
                  {
                      name: "Country",
                      type: "select",
                      items: db.countries,
                      valueField: "Id",
                      textField: "Name"
                  },
                  {
                      name: "Married",
                      title: "Is Married",
                      itemTemplate: function(value, item) {
              	        return $("<div>")
                        .addClass("form-check mt-0")
                          .append(
                            $("<label>").addClass("form-check-label")
                            .append(
                              $("<input>").attr("type","checkbox")
                              .addClass("form-check-input")
                              .attr("checked", value || item.Checked)
                              .on("change", function() {
                          	     item.Checked = $(this).is(":checked");
                              })
                            )
                            .append('<i class="input-helper"></i>')
                          );
                      }
                  }
              ]
          });
        }

        //sortable
        if($("#js-grid-sortable").length) {
          $("#js-grid-sortable").jsGrid({
              height: "500px",
              width: "100%",

              autoload: true,
              selecting: false,

              controller: db,

              fields: [{
                      name: "Name",
                      type: "text",
                      width: 150
                  },
                  {
                      name: "Age",
                      type: "number",
                      width: 50
                  },
                  {
                      name: "Address",
                      type: "text",
                      width: 200
                  },
                  {
                      name: "Country",
                      type: "select",
                      items: db.countries,
                      valueField: "Id",
                      textField: "Name"
                  },
                  {
                      name: "Married",
                      title: "Is Married",
                      itemTemplate: function(value, item) {
              	        return $("<div>")
                        .addClass("form-check mt-0")
                          .append(
                            $("<label>").addClass("form-check-label")
                            .append(
                              $("<input>").attr("type","checkbox")
                              .addClass("form-check-input")
                              .attr("checked", value || item.Checked)
                              .on("change", function() {
                          	     item.Checked = $(this).is(":checked");
                              })
                            )
                            .append('<i class="input-helper"></i>')
                          );
                      }
                  }
              ]
          });
        }

        if($("#sort").length) {
          $("#sort").on("click", function () {
              var field = $("#sortingField").val();
              $("#js-grid-sortable").jsGrid("sort", field);
          });
        }

    });
})(jQuery);

var g1, g2, gg1,g7,g8,g9,g10;

window.onload = function() {
  var g1 = new JustGage({
    id: "g1",
    value: getRandomInt(0, 100),
    min: 0,
    max: 100,
    title: "Big Fella",
    label: "pounds"
  });


  setInterval(function() {
    g1.refresh(getRandomInt(50, 100));
  }, 2500);
};




document.addEventListener("DOMContentLoaded", function(event) {
  g2 = new JustGage({
    id: "g2",
    value: 72,
    min: 0,
    max: 100,
    donut: true,
    gaugeWidthScale: 0.6,
    counter: true,
    hideInnerShadow: true
  });

  document.getElementById('g2_refresh').addEventListener('click', function() {
    g2.refresh(getRandomInt(0, 100));
  });

  var g3 = new JustGage({
        id: 'g3',
        value: 65,
        min: 0,
        max: 100,
        symbol: '%',
        pointer: true,
        gaugeWidthScale: 0.6,
        customSectors: [{
          color: '#ff0000',
          lo: 50,
          hi: 100
        }, {
          color: '#00ff00',
          lo: 0,
          hi: 50
        }],
        counter: true
      });

      var g4 = new JustGage({
        id: 'g4',
        value: 45,
        min: 0,
        max: 100,
        symbol: '%',
        pointer: true,
        pointerOptions: {
          toplength: -15,
          bottomlength: 10,
          bottomwidth: 12,
          color: '#8e8e93',
          stroke: '#ffffff',
          stroke_width: 3,
          stroke_linecap: 'round'
        },
        gaugeWidthScale: 0.6,
        counter: true
      });

      var g5 = new JustGage({
        id: 'g5',
        value: 40,
        min: 0,
        max: 100,
        symbol: '%',
        donut: true,
        pointer: true,
        gaugeWidthScale: 0.4,
        pointerOptions: {
          toplength: 10,
          bottomlength: 10,
          bottomwidth: 8,
          color: '#000'
        },
        customSectors: [{
          color: "#ff0000",
          lo: 50,
          hi: 100
        }, {
          color: "#00ff00",
          lo: 0,
          hi: 50
        }],
        counter: true
      });

      var g6 = new JustGage({
        id: 'g6',
        value: 70,
        min: 0,
        max: 100,
        symbol: '%',
        pointer: true,
        pointerOptions: {
          toplength: 8,
          bottomlength: -20,
          bottomwidth: 6,
          color: '#8e8e93'
        },
        gaugeWidthScale: 0.1,
        counter: true
      });

      var g7 = new JustGage({
        id: 'g7',
        value: 65,
        min: 0,
        max: 100,
        reverse: true,
        gaugeWidthScale: 0.6,
        customSectors: [{
          color: '#ff0000',
          lo: 50,
          hi: 100
        }, {
          color: '#00ff00',
          lo: 0,
          hi: 50
        }],
        counter: true
      });

      var g8 = new JustGage({
        id: 'g8',
        value: 45,
        min: 0,
        max: 500,
        reverse: true,
        gaugeWidthScale: 0.6,
        counter: true
      });

      var g9 = new JustGage({
        id: 'g9',
        value: 25000,
        min: 0,
        max: 100000,
        humanFriendly : true,
        reverse: true,
        gaugeWidthScale: 1.3,
        customSectors: [{
          color: "#ff0000",
          lo: 50000,
          hi: 100000
        }, {
          color: "#00ff00",
          lo: 0,
          hi: 50000
        }],
        counter: true
      });

      var g10 = new JustGage({
        id: 'g10',
        value: 90,
        min: 0,
        max: 100,
        symbol: '%',
        reverse: true,
        gaugeWidthScale: 0.1,
        counter: true
      });

      document.getElementById('gauge_refresh').addEventListener('click', function() {
        g3.refresh(getRandomInt(0, 100));
        g4.refresh(getRandomInt(0, 100));
        g5.refresh(getRandomInt(0, 100));
        g6.refresh(getRandomInt(0, 100));
        g7.refresh(getRandomInt(0, 100));
        g8.refresh(getRandomInt(0, 100));
        g9.refresh(getRandomInt(0, 100));
        g10.refresh(getRandomInt(0, 100));
      });

});

(function($) {
  'use strict';
  if($("#lightgallery").length) {
    $("#lightgallery").lightGallery();
  }

  if($("#lightgallery-without-thumb").length) {
    $("#lightgallery-without-thumb").lightGallery({
      thumbnail:true,
      animateThumb: false,
      showThumbByDefault: false
    });
  }

  if($("#video-gallery").length) {
    $("#video-gallery").lightGallery();
  }
})(jQuery);

$(function() {
  'use strict';
  if ($(".mapeal-container").length) {
    $(".mapeal-container").mapael({
      map: {
        name: "world_countries"
      }
    });
  }
});

$(function() {
  'use strict';
  if ($(".mapeal-example-2").length) {
    $(".mapeal-example-2").mapael({
      map: {
        name: "france_departments",
        defaultArea: {
          attrsHover: {
            fill: "#343434",
            stroke: "#5d5d5d",
            "stroke-width": 1,
            "stroke-linejoin": "round"
          }
        }
      },
      legend: {
        plot: {
          cssClass: 'myLegend',
          mode: 'horizontal',
          labelAttrs: {
            fill: "#4a4a4a"
          },
          titleAttrs: {
            fill: "#4a4a4a"
          },
          marginBottom: 20,
          marginLeft: 10,
          hideElemsOnClick: {
            opacity: 0
          },
          title: "French cities population",
          slices: [{
            size: 5,
            type: "circle",
            max: 20000,
            attrs: {
              fill: "#89ff72"
            },
            label: "200 +"
          }, {
            size: 15,
            type: "circle",
            min: 20000,
            max: 100000,
            attrs: {
              fill: "#fffd72"
            },
            label: "200 - 100"
          }, {
            size: 20,
            type: "circle",
            min: 100000,
            max: 200000,
            attrs: {
              fill: "#ffbd54"
            },
            label: "100 - 200"
          }, {
            size: 25,
            type: "circle",
            min: 200000,
            attrs: {
              fill: "#ff5454"
            },
            label: "200 +"
          }]
        }
      },
      plots: {
        "town-75056": {
          value: "2268265",
          latitude: 48.86,
          longitude: 2.3444444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Paris (75056)</span><br />Population : 2268265"
          }
        },
        "town-13055": {
          value: "859368",
          latitude: 43.296666666667,
          longitude: 5.3763888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Marseille (13055)</span><br />Population : 859368"
          }
        },
        "town-69123": {
          value: "492578",
          latitude: 45.758888888889,
          longitude: 4.8413888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lyon (69123)</span><br />Population : 492578"
          }
        },
        "town-31555": {
          value: "449328",
          latitude: 43.604444444444,
          longitude: 1.4419444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Toulouse (31555)</span><br />Population : 449328"
          }
        },
        "town-06088": {
          value: "347105",
          latitude: 43.701944444444,
          longitude: 7.2683333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Nice (06088)</span><br />Population : 347105"
          }
        },
        "town-44109": {
          value: "293234",
          latitude: 47.217222222222,
          longitude: -1.5538888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Nantes (44109)</span><br />Population : 293234"
          }
        },
        "town-67482": {
          value: "276401",
          latitude: 48.583611111111,
          longitude: 7.7480555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Strasbourg (67482)</span><br />Population : 276401"
          }
        },
        "town-34172": {
          value: "260572",
          latitude: 43.611111111111,
          longitude: 3.8766666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montpellier (34172)</span><br />Population : 260572"
          }
        },
        "town-33063": {
          value: "242945",
          latitude: 44.837777777778,
          longitude: -0.57944444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bordeaux (33063)</span><br />Population : 242945"
          }
        },
        "town-59350": {
          value: "234058",
          latitude: 50.631944444444,
          longitude: 3.0575,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lille (59350)</span><br />Population : 234058"
          }
        },
        "town-35238": {
          value: "212939",
          latitude: 48.114166666667,
          longitude: -1.6808333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rennes (35238)</span><br />Population : 212939"
          }
        },
        "town-51454": {
          value: "184011",
          latitude: 49.265277777778,
          longitude: 4.0286111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Reims (51454)</span><br />Population : 184011"
          }
        },
        "town-76351": {
          value: "178070",
          latitude: 49.498888888889,
          longitude: 0.12111111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Havre (76351)</span><br />Population : 178070"
          }
        },
        "town-42218": {
          value: "174566",
          latitude: 45.433888888889,
          longitude: 4.3897222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Ã‰tienne (42218)</span><br />Population : 174566"
          }
        },
        "town-83137": {
          value: "166851",
          latitude: 43.125,
          longitude: 5.9305555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Toulon (83137)</span><br />Population : 166851"
          }
        },
        "town-38185": {
          value: "158249",
          latitude: 45.186944444444,
          longitude: 5.7263888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Grenoble (38185)</span><br />Population : 158249"
          }
        },
        "town-21231": {
          value: "155233",
          latitude: 47.323055555556,
          longitude: 5.0419444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dijon (21231)</span><br />Population : 155233"
          }
        },
        "town-49007": {
          value: "151957",
          latitude: 47.472777777778,
          longitude: -0.55555555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Angers (49007)</span><br />Population : 151957"
          }
        },
        "town-72181": {
          value: "147108",
          latitude: 48.004166666667,
          longitude: 0.19694444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Mans (72181)</span><br />Population : 147108"
          }
        },
        "town-69266": {
          value: "146729",
          latitude: 45.766111111111,
          longitude: 4.8794444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villeurbanne (69266)</span><br />Population : 146729"
          }
        },
        "town-97411": {
          value: "146489",
          latitude: -20.878888888889,
          longitude: 55.448055555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Denis (97411)</span><br />Population : 146489"
          }
        },
        "town-29019": {
          value: "145561",
          latitude: 48.39,
          longitude: -4.4869444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Brest (29019)</span><br />Population : 145561"
          }
        },
        "town-30189": {
          value: "145501",
          latitude: 43.836944444444,
          longitude: 4.36,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">NÃ®mes (30189)</span><br />Population : 145501"
          }
        },
        "town-13001": {
          value: "144884",
          latitude: 43.527777777778,
          longitude: 5.4455555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aix-en-Provence (13001)</span><br />Population : 144884"
          }
        },
        "town-63113": {
          value: "143669",
          latitude: 45.779722222222,
          longitude: 3.0869444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Clermont-Ferrand (63113)</span><br />Population : 143669"
          }
        },
        "town-87085": {
          value: "141540",
          latitude: 45.834444444444,
          longitude: 1.2616666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Limoges (87085)</span><br />Population : 141540"
          }
        },
        "town-37261": {
          value: "138268",
          latitude: 47.392777777778,
          longitude: 0.68833333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tours (37261)</span><br />Population : 138268"
          }
        },
        "town-80021": {
          value: "136512",
          latitude: 49.891944444444,
          longitude: 2.2977777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Amiens (80021)</span><br />Population : 136512"
          }
        },
        "town-57463": {
          value: "122928",
          latitude: 49.119722222222,
          longitude: 6.1769444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Metz (57463)</span><br />Population : 122928"
          }
        },
        "town-25056": {
          value: "121038",
          latitude: 47.242222222222,
          longitude: 6.0213888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">BesanÃ§on (25056)</span><br />Population : 121038"
          }
        },
        "town-66136": {
          value: "119536",
          latitude: 42.6975,
          longitude: 2.8947222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Perpignan (66136)</span><br />Population : 119536"
          }
        },
        "town-45234": {
          value: "117833",
          latitude: 47.902222222222,
          longitude: 1.9041666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">OrlÃ©ans (45234)</span><br />Population : 117833"
          }
        },
        "town-92012": {
          value: "115264",
          latitude: 48.835277777778,
          longitude: 2.2413888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Boulogne-Billancourt (92012)</span><br />Population : 115264"
          }
        },
        "town-76540": {
          value: "113461",
          latitude: 49.443055555556,
          longitude: 1.1025,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rouen (76540)</span><br />Population : 113461"
          }
        },
        "town-14118": {
          value: "111949",
          latitude: 49.182222222222,
          longitude: -0.37055555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Caen (14118)</span><br />Population : 111949"
          }
        },
        "town-68224": {
          value: "111273",
          latitude: 47.748611111111,
          longitude: 7.3391666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mulhouse (68224)</span><br />Population : 111273"
          }
        },
        "town-93066": {
          value: "107959",
          latitude: 48.935555555556,
          longitude: 2.3538888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Denis (93066)</span><br />Population : 107959"
          }
        },
        "town-93066": {
          value: "107959",
          latitude: 48.935555555556,
          longitude: 2.3538888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Denis (93066)</span><br />Population : 107959"
          }
        },
        "town-54395": {
          value: "107710",
          latitude: 48.692777777778,
          longitude: 6.1836111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Nancy (54395)</span><br />Population : 107710"
          }
        },
        "town-95018": {
          value: "104843",
          latitude: 48.947777777778,
          longitude: 2.2475,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Argenteuil (95018)</span><br />Population : 104843"
          }
        },
        "town-97415": {
          value: "104818",
          latitude: -21.009722222222,
          longitude: 55.269722222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Paul (97415)</span><br />Population : 104818"
          }
        },
        "town-93048": {
          value: "103675",
          latitude: 48.860277777778,
          longitude: 2.4430555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montreuil (93048)</span><br />Population : 103675"
          }
        },
        "town-59512": {
          value: "95506",
          latitude: 50.689166666667,
          longitude: 3.1808333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Roubaix (59512)</span><br />Population : 95506"
          }
        },
        "town-59183": {
          value: "93489",
          latitude: 51.037777777778,
          longitude: 2.3763888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dunkerque (59183)</span><br />Population : 93489"
          }
        },
        "town-59599": {
          value: "92620",
          latitude: 50.7225,
          longitude: 3.1602777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tourcoing (59599)</span><br />Population : 92620"
          }
        },
        "town-84007": {
          value: "91657",
          latitude: 43.948611111111,
          longitude: 4.8083333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Avignon (84007)</span><br />Population : 91657"
          }
        },
        "town-92050": {
          value: "91114",
          latitude: 48.890555555556,
          longitude: 2.2036111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Nanterre (92050)</span><br />Population : 91114"
          }
        },
        "town-94028": {
          value: "90779",
          latitude: 48.790555555556,
          longitude: 2.4619444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">CrÃ©teil (94028)</span><br />Population : 90779"
          }
        },
        "town-86194": {
          value: "90386",
          latitude: 46.581111111111,
          longitude: 0.33527777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Poitiers (86194)</span><br />Population : 90386"
          }
        },
        "town-97209": {
          value: "88623",
          latitude: 14.607222222222,
          longitude: -61.069444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fort-de-France (97209)</span><br />Population : 88623"
          }
        },
        "town-78646": {
          value: "88253",
          latitude: 48.804722222222,
          longitude: 2.1341666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Versailles (78646)</span><br />Population : 88253"
          }
        },
        "town-92026": {
          value: "88169",
          latitude: 48.897222222222,
          longitude: 2.2522222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Courbevoie (92026)</span><br />Population : 88169"
          }
        },
        "town-94081": {
          value: "86210",
          latitude: 48.7875,
          longitude: 2.3927777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vitry-sur-Seine (94081)</span><br />Population : 86210"
          }
        },
        "town-92025": {
          value: "86094",
          latitude: 48.923611111111,
          longitude: 2.2522222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Colombes (92025)</span><br />Population : 86094"
          }
        },
        "town-92004": {
          value: "82998",
          latitude: 48.911111111111,
          longitude: 2.2855555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AsniÃ¨res-sur-Seine (92004)</span><br />Population : 82998"
          }
        },
        "town-93005": {
          value: "82778",
          latitude: 48.936388888889,
          longitude: 2.4930555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aulnay-sous-Bois (93005)</span><br />Population : 82778"
          }
        },
        "town-64445": {
          value: "82776",
          latitude: 43.300833333333,
          longitude: -0.37,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pau (64445)</span><br />Population : 82776"
          }
        },
        "town-92063": {
          value: "80905",
          latitude: 48.877777777778,
          longitude: 2.1883333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rueil-Malmaison (92063)</span><br />Population : 80905"
          }
        },
        "town-97416": {
          value: "80027",
          latitude: -21.341944444444,
          longitude: 55.477777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Pierre (97416)</span><br />Population : 80027"
          }
        },
        "town-17300": {
          value: "77875",
          latitude: 46.159444444444,
          longitude: -1.1513888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Rochelle (17300)</span><br />Population : 77875"
          }
        },
        "town-93001": {
          value: "76728",
          latitude: 48.911111111111,
          longitude: 2.3825,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aubervilliers (93001)</span><br />Population : 76728"
          }
        },
        "town-94017": {
          value: "76235",
          latitude: 48.817222222222,
          longitude: 2.5155555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Champigny-sur-Marne (94017)</span><br />Population : 76235"
          }
        },
        "town-94068": {
          value: "75772",
          latitude: 48.798611111111,
          longitude: 2.4988888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Maur-des-FossÃ©s (94068)</span><br />Population : 75772"
          }
        },
        "town-06004": {
          value: "75174",
          latitude: 43.58,
          longitude: 7.1230555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Antibes (06004)</span><br />Population : 75174"
          }
        },
        "town-62193": {
          value: "74573",
          latitude: 50.9475,
          longitude: 1.8555555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Calais (62193)</span><br />Population : 74573"
          }
        },
        "town-06029": {
          value: "74273",
          latitude: 43.5525,
          longitude: 7.0213888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cannes (06029)</span><br />Population : 74273"
          }
        },
        "town-97422": {
          value: "74174",
          latitude: -21.278055555556,
          longitude: 55.515277777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Tampon (97422)</span><br />Population : 74174"
          }
        },
        "town-34032": {
          value: "72466",
          latitude: 43.343333333333,
          longitude: 3.2161111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">BÃ©ziers (34032)</span><br />Population : 72466"
          }
        },
        "town-44184": {
          value: "69724",
          latitude: 47.279444444444,
          longitude: -2.21,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Nazaire (44184)</span><br />Population : 69724"
          }
        },
        "town-68066": {
          value: "69187",
          latitude: 48.081111111111,
          longitude: 7.355,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Colmar (68066)</span><br />Population : 69187"
          }
        },
        "town-18033": {
          value: "68590",
          latitude: 47.083611111111,
          longitude: 2.3955555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bourges (18033)</span><br />Population : 68590"
          }
        },
        "town-93029": {
          value: "67202",
          latitude: 48.923333333333,
          longitude: 2.445,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Drancy (93029)</span><br />Population : 67202"
          }
        },
        "town-33281": {
          value: "67136",
          latitude: 44.8425,
          longitude: -0.645,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">MÃ©rignac (33281)</span><br />Population : 67136"
          }
        },
        "town-29232": {
          value: "67131",
          latitude: 47.995833333333,
          longitude: -4.0977777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Quimper (29232)</span><br />Population : 67131"
          }
        },
        "town-2A004": {
          value: "66203",
          latitude: 41.925555555556,
          longitude: 8.7363888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ajaccio (2A004)</span><br />Population : 66203"
          }
        },
        "town-92040": {
          value: "65178",
          latitude: 48.823055555556,
          longitude: 2.2691666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Issy-les-Moulineaux (92040)</span><br />Population : 65178"
          }
        },
        "town-26362": {
          value: "65043",
          latitude: 44.9325,
          longitude: 4.8908333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Valence (26362)</span><br />Population : 65043"
          }
        },
        "town-92044": {
          value: "64757",
          latitude: 48.893333333333,
          longitude: 2.2877777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Levallois-Perret (92044)</span><br />Population : 64757"
          }
        },
        "town-59009": {
          value: "64328",
          latitude: 50.622777777778,
          longitude: 3.1441666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villeneuve-d'Ascq (59009)</span><br />Population : 64328"
          }
        },
        "town-93051": {
          value: "63526",
          latitude: 48.843888888889,
          longitude: 2.5580555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Noisy-le-Grand (93051)</span><br />Population : 63526"
          }
        },
        "town-83126": {
          value: "62883",
          latitude: 43.103055555556,
          longitude: 5.8783333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Seyne-sur-Mer (83126)</span><br />Population : 62883"
          }
        },
        "town-92002": {
          value: "62644",
          latitude: 48.753333333333,
          longitude: 2.2966666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Antony (92002)</span><br />Population : 62644"
          }
        },
        "town-92051": {
          value: "62565",
          latitude: 48.887222222222,
          longitude: 2.2675,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Neuilly-sur-Seine (92051)</span><br />Population : 62565"
          }
        },
        "town-10387": {
          value: "61936",
          latitude: 48.298888888889,
          longitude: 4.0780555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Troyes (10387)</span><br />Population : 61936"
          }
        },
        "town-69259": {
          value: "60448",
          latitude: 45.696944444444,
          longitude: 4.8858333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">VÃ©nissieux (69259)</span><br />Population : 60448"
          }
        },
        "town-79191": {
          value: "59504",
          latitude: 46.325,
          longitude: -0.46222222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Niort (79191)</span><br />Population : 59504"
          }
        },
        "town-97101": {
          value: "59267",
          latitude: 16.270555555556,
          longitude: -61.504722222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Abymes (97101)</span><br />Population : 59267"
          }
        },
        "town-92024": {
          value: "59228",
          latitude: 48.903611111111,
          longitude: 2.3055555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Clichy (92024)</span><br />Population : 59228"
          }
        },
        "town-95585": {
          value: "59204",
          latitude: 48.997222222222,
          longitude: 2.3780555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sarcelles (95585)</span><br />Population : 59204"
          }
        },
        "town-73065": {
          value: "59184",
          latitude: 45.566388888889,
          longitude: 5.9208333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChambÃ©ry (73065)</span><br />Population : 59184"
          }
        },
        "town-33318": {
          value: "58977",
          latitude: 44.805833333333,
          longitude: -0.63222222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pessac (33318)</span><br />Population : 58977"
          }
        },
        "town-56121": {
          value: "58831",
          latitude: 47.745833333333,
          longitude: -3.3663888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lorient (56121)</span><br />Population : 58831"
          }
        },
        "town-94041": {
          value: "58189",
          latitude: 48.813888888889,
          longitude: 2.3877777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ivry-sur-Seine (94041)</span><br />Population : 58189"
          }
        },
        "town-82121": {
          value: "58014",
          latitude: 44.017222222222,
          longitude: 1.355,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montauban (82121)</span><br />Population : 58014"
          }
        },
        "town-95127": {
          value: "57900",
          latitude: 49.035833333333,
          longitude: 2.0625,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cergy (95127)</span><br />Population : 57900"
          }
        },
        "town-02691": {
          value: "57533",
          latitude: 49.847777777778,
          longitude: 3.2855555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Quentin (02691)</span><br />Population : 57533"
          }
        },
        "town-60057": {
          value: "56181",
          latitude: 49.434166666667,
          longitude: 2.0875,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Beauvais (60057)</span><br />Population : 56181"
          }
        },
        "town-49099": {
          value: "56137",
          latitude: 47.058888888889,
          longitude: -0.87972222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cholet (49099)</span><br />Population : 56137"
          }
        },
        "town-85191": {
          value: "56101",
          latitude: 46.669722222222,
          longitude: -1.4277777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Roche-sur-Yon (85191)</span><br />Population : 56101"
          }
        },
        "town-97302": {
          value: "56002",
          latitude: 4.9386111111111,
          longitude: -52.335,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cayenne (97302)</span><br />Population : 56002"
          }
        },
        "town-83069": {
          value: "55906",
          latitude: 43.118888888889,
          longitude: 6.1286111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">HyÃ¨res (83069)</span><br />Population : 55906"
          }
        },
        "town-94076": {
          value: "55879",
          latitude: 48.793888888889,
          longitude: 2.3611111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villejuif (94076)</span><br />Population : 55879"
          }
        },
        "town-56260": {
          value: "55116",
          latitude: 47.655,
          longitude: -2.7616666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vannes (56260)</span><br />Population : 55116"
          }
        },
        "town-93031": {
          value: "54775",
          latitude: 48.954722222222,
          longitude: 2.3083333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰pinay-sur-Seine (93031)</span><br />Population : 54775"
          }
        },
        "town-93055": {
          value: "54464",
          latitude: 48.898055555556,
          longitude: 2.4072222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pantin (93055)</span><br />Population : 54464"
          }
        },
        "town-97409": {
          value: "54311",
          latitude: -20.960555555556,
          longitude: 55.650555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-AndrÃ© (97409)</span><br />Population : 54311"
          }
        },
        "town-53130": {
          value: "54100",
          latitude: 48.072777777778,
          longitude: -0.77,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Laval (53130)</span><br />Population : 54100"
          }
        },
        "town-93010": {
          value: "53934",
          latitude: 48.902777777778,
          longitude: 2.4836111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bondy (93010)</span><br />Population : 53934"
          }
        },
        "town-13004": {
          value: "53785",
          latitude: 43.676944444444,
          longitude: 4.6286111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Arles (13004)</span><br />Population : 53785"
          }
        },
        "town-94033": {
          value: "53667",
          latitude: 48.851666666667,
          longitude: 2.4772222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fontenay-sous-Bois (94033)</span><br />Population : 53667"
          }
        },
        "town-94046": {
          value: "53513",
          latitude: 48.805833333333,
          longitude: 2.4377777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Maisons-Alfort (94046)</span><br />Population : 53513"
          }
        },
        "town-27229": {
          value: "53260",
          latitude: 49.023333333333,
          longitude: 1.1525,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰vreux (27229)</span><br />Population : 53260"
          }
        },
        "town-77108": {
          value: "53238",
          latitude: 48.878611111111,
          longitude: 2.5888888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chelles (77108)</span><br />Population : 53238"
          }
        },
        "town-92023": {
          value: "53113",
          latitude: 48.800833333333,
          longitude: 2.2619444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Clamart (92023)</span><br />Population : 53113"
          }
        },
        "town-91228": {
          value: "53019",
          latitude: 48.633888888889,
          longitude: 2.4441666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰vry (91228)</span><br />Population : 53019"
          }
        },
        "town-83061": {
          value: "52580",
          latitude: 43.433055555556,
          longitude: 6.7355555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">FrÃ©jus (83061)</span><br />Population : 52580"
          }
        },
        "town-77284": {
          value: "52540",
          latitude: 48.959444444444,
          longitude: 2.8877777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Meaux (77284)</span><br />Population : 52540"
          }
        },
        "town-97414": {
          value: "52507",
          latitude: -21.286666666667,
          longitude: 55.409166666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Louis (97414)</span><br />Population : 52507"
          }
        },
        "town-11262": {
          value: "52489",
          latitude: 43.184722222222,
          longitude: 3.0036111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Narbonne (11262)</span><br />Population : 52489"
          }
        },
        "town-74010": {
          value: "52375",
          latitude: 45.899166666667,
          longitude: 6.1294444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Annecy (74010)</span><br />Population : 52375"
          }
        },
        "town-06069": {
          value: "52185",
          latitude: 43.658055555556,
          longitude: 6.9252777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Grasse (06069)</span><br />Population : 52185"
          }
        },
        "town-93007": {
          value: "51735",
          latitude: 48.938611111111,
          longitude: 2.4611111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Blanc-Mesnil (93007)</span><br />Population : 51735"
          }
        },
        "town-08105": {
          value: "51647",
          latitude: 49.771388888889,
          longitude: 4.7194444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Charleville-MÃ©ziÃ¨res (08105)</span><br />Population : 51647"
          }
        },
        "town-78586": {
          value: "51504",
          latitude: 48.945277777778,
          longitude: 2.17,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sartrouville (78586)</span><br />Population : 51504"
          }
        },
        "town-90010": {
          value: "51233",
          latitude: 47.641111111111,
          longitude: 6.8494444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Belfort (90010)</span><br />Population : 51233"
          }
        },
        "town-81004": {
          value: "51181",
          latitude: 43.928055555556,
          longitude: 2.1458333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Albi (81004)</span><br />Population : 51181"
          }
        },
        "town-19031": {
          value: "50272",
          latitude: 45.158888888889,
          longitude: 1.5330555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Brive-la-Gaillarde (19031)</span><br />Population : 50272"
          }
        },
        "town-93071": {
          value: "50225",
          latitude: 48.941388888889,
          longitude: 2.5227777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sevran (93071)</span><br />Population : 50225"
          }
        },
        "town-92049": {
          value: "48983",
          latitude: 48.816388888889,
          longitude: 2.3211111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montrouge (92049)</span><br />Population : 48983"
          }
        },
        "town-94080": {
          value: "48955",
          latitude: 48.847777777778,
          longitude: 2.4391666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vincennes (94080)</span><br />Population : 48955"
          }
        },
        "town-11069": {
          value: "48893",
          latitude: 43.215833333333,
          longitude: 2.3513888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Carcassonne (11069)</span><br />Population : 48893"
          }
        },
        "town-41018": {
          value: "48568",
          latitude: 47.593055555556,
          longitude: 1.3272222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Blois (41018)</span><br />Population : 48568"
          }
        },
        "town-13056": {
          value: "48261",
          latitude: 43.405277777778,
          longitude: 5.0475,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Martigues (13056)</span><br />Population : 48261"
          }
        },
        "town-22278": {
          value: "48246",
          latitude: 48.513611111111,
          longitude: -2.7602777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Brieuc (22278)</span><br />Population : 48246"
          }
        },
        "town-36044": {
          value: "48187",
          latitude: 46.809722222222,
          longitude: 1.6902777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢teauroux (36044)</span><br />Population : 48187"
          }
        },
        "town-35288": {
          value: "48133",
          latitude: 48.647222222222,
          longitude: -2.0088888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Malo (35288)</span><br />Population : 48133"
          }
        },
        "town-93008": {
          value: "47855",
          latitude: 48.909722222222,
          longitude: 2.4386111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bobigny (93008)</span><br />Population : 47855"
          }
        },
        "town-06027": {
          value: "47711",
          latitude: 43.663611111111,
          longitude: 7.1483333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cagnes-sur-Mer (06027)</span><br />Population : 47711"
          }
        },
        "town-93070": {
          value: "47604",
          latitude: 48.906944444444,
          longitude: 2.3330555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Ouen (93070)</span><br />Population : 47604"
          }
        },
        "town-92073": {
          value: "47121",
          latitude: 48.871111111111,
          longitude: 2.2269444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Suresnes (92073)</span><br />Population : 47121"
          }
        },
        "town-13005": {
          value: "46892",
          latitude: 43.290833333333,
          longitude: 5.5708333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aubagne (13005)</span><br />Population : 46892"
          }
        },
        "town-71076": {
          value: "46791",
          latitude: 46.793611111111,
          longitude: 4.8475,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chalon-sur-SaÃ´ne (71076)</span><br />Population : 46791"
          }
        },
        "town-51108": {
          value: "46668",
          latitude: 48.956666666667,
          longitude: 4.3644444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢lons-en-Champagne (51108)</span><br />Population : 46668"
          }
        },
        "town-64102": {
          value: "46191",
          latitude: 43.4925,
          longitude: -1.4763888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bayonne (64102)</span><br />Population : 46191"
          }
        },
        "town-92048": {
          value: "45834",
          latitude: 48.8075,
          longitude: 2.2402777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Meudon (92048)</span><br />Population : 45834"
          }
        },
        "town-92062": {
          value: "45093",
          latitude: 48.884166666667,
          longitude: 2.2380555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Puteaux (92062)</span><br />Population : 45093"
          }
        },
        "town-65440": {
          value: "44952",
          latitude: 43.232777777778,
          longitude: 0.07444444444444399,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tarbes (65440)</span><br />Population : 44952"
          }
        },
        "town-94002": {
          value: "44439",
          latitude: 48.7975,
          longitude: 2.4241666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Alfortville (94002)</span><br />Population : 44439"
          }
        },
        "town-59606": {
          value: "44362",
          latitude: 50.359166666667,
          longitude: 3.525,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Valenciennes (59606)</span><br />Population : 44362"
          }
        },
        "town-16015": {
          value: "44219",
          latitude: 45.649444444444,
          longitude: 0.15944444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AngoulÃªme (16015)</span><br />Population : 44219"
          }
        },
        "town-44162": {
          value: "44078",
          latitude: 47.211388888889,
          longitude: -1.6511111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Herblain (44162)</span><br />Population : 44078"
          }
        },
        "town-81065": {
          value: "43995",
          latitude: 43.605833333333,
          longitude: 2.24,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Castres (81065)</span><br />Population : 43995"
          }
        },
        "town-13103": {
          value: "43830",
          latitude: 43.640555555556,
          longitude: 5.0972222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Salon-de-Provence (13103)</span><br />Population : 43830"
          }
        },
        "town-62160": {
          value: "43805",
          latitude: 50.725555555556,
          longitude: 1.6138888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Boulogne-sur-Mer (62160)</span><br />Population : 43805"
          }
        },
        "town-91174": {
          value: "43747",
          latitude: 48.610277777778,
          longitude: 2.4747222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Corbeil-Essonnes (91174)</span><br />Population : 43747"
          }
        },
        "town-13047": {
          value: "43651",
          latitude: 43.514166666667,
          longitude: 4.9888888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Istres (13047)</span><br />Population : 43651"
          }
        },
        "town-2B033": {
          value: "43615",
          latitude: 42.7,
          longitude: 9.449444444444399,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bastia (2B033)</span><br />Population : 43615"
          }
        },
        "town-59178": {
          value: "43530",
          latitude: 50.370833333333,
          longitude: 3.0791666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Douai (59178)</span><br />Population : 43530"
          }
        },
        "town-34301": {
          value: "43436",
          latitude: 43.404444444444,
          longitude: 3.6966666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">SÃ¨te (34301)</span><br />Population : 43436"
          }
        },
        "town-62041": {
          value: "43289",
          latitude: 50.289166666667,
          longitude: 2.78,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Arras (62041)</span><br />Population : 43289"
          }
        },
        "town-78361": {
          value: "43268",
          latitude: 48.990555555556,
          longitude: 1.7166666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mantes-la-Jolie (78361)</span><br />Population : 43268"
          }
        },
        "town-91377": {
          value: "43006",
          latitude: 48.730555555556,
          longitude: 2.2763888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Massy (91377)</span><br />Population : 43006"
          }
        },
        "town-06030": {
          value: "42780",
          latitude: 43.576111111111,
          longitude: 7.0186111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Cannet (06030)</span><br />Population : 42780"
          }
        },
        "town-30007": {
          value: "42697",
          latitude: 44.127222222222,
          longitude: 4.0808333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AlÃ¨s (30007)</span><br />Population : 42697"
          }
        },
        "town-69290": {
          value: "42428",
          latitude: 45.696388888889,
          longitude: 4.9438888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Priest (69290)</span><br />Population : 42428"
          }
        },
        "town-60159": {
          value: "42295",
          latitude: 49.414166666667,
          longitude: 2.8222222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">CompiÃ¨gne (60159)</span><br />Population : 42295"
          }
        },
        "town-01053": {
          value: "42184",
          latitude: 46.204722222222,
          longitude: 5.2280555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bourg-en-Bresse (01053)</span><br />Population : 42184"
          }
        },
        "town-93046": {
          value: "42060",
          latitude: 48.918333333333,
          longitude: 2.5352777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Livry-Gargan (93046)</span><br />Population : 42060"
          }
        },
        "town-78551": {
          value: "42009",
          latitude: 48.896388888889,
          longitude: 2.0905555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Germain-en-Laye (78551)</span><br />Population : 42009"
          }
        },
        "town-33522": {
          value: "41971",
          latitude: 44.808333333333,
          longitude: -0.5891666666666699,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Talence (33522)</span><br />Population : 41971"
          }
        },
        "town-57672": {
          value: "41971",
          latitude: 49.358055555556,
          longitude: 6.1683333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Thionville (57672)</span><br />Population : 41971"
          }
        },
        "town-69256": {
          value: "41970",
          latitude: 45.786944444444,
          longitude: 4.925,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vaulx-en-Velin (69256)</span><br />Population : 41970"
          }
        },
        "town-69034": {
          value: "41840",
          latitude: 45.794722222222,
          longitude: 4.8463888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Caluire-et-Cuire (69034)</span><br />Population : 41840"
          }
        },
        "town-59650": {
          value: "41809",
          latitude: 50.701111111111,
          longitude: 3.2133333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Wattrelos (59650)</span><br />Population : 41809"
          }
        },
        "town-92036": {
          value: "41676",
          latitude: 48.9325,
          longitude: 2.3047222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gennevilliers (92036)</span><br />Population : 41676"
          }
        },
        "town-05061": {
          value: "41659",
          latitude: 44.558611111111,
          longitude: 6.0777777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gap (05061)</span><br />Population : 41659"
          }
        },
        "town-93064": {
          value: "41431",
          latitude: 48.873055555556,
          longitude: 2.4852777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rosny-sous-Bois (93064)</span><br />Population : 41431"
          }
        },
        "town-94022": {
          value: "41275",
          latitude: 48.766388888889,
          longitude: 2.4077777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Choisy-le-Roi (94022)</span><br />Population : 41275"
          }
        },
        "town-77288": {
          value: "40609",
          latitude: 48.539722222222,
          longitude: 2.6591666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Melun (77288)</span><br />Population : 40609"
          }
        },
        "town-28085": {
          value: "40420",
          latitude: 48.446666666667,
          longitude: 1.4883333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chartres (28085)</span><br />Population : 40420"
          }
        },
        "town-95268": {
          value: "40274",
          latitude: 48.971944444444,
          longitude: 2.4,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Garges-lÃ¨s-Gonesse (95268)</span><br />Population : 40274"
          }
        },
        "town-97213": {
          value: "39996",
          latitude: 14.615277777778,
          longitude: -61.001944444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Lamentin (97213)</span><br />Population : 39996"
          }
        },
        "town-93053": {
          value: "39949",
          latitude: 48.890833333333,
          longitude: 2.4536111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Noisy-le-Sec (93053)</span><br />Population : 39949"
          }
        },
        "town-59378": {
          value: "39782",
          latitude: 50.670277777778,
          longitude: 3.0963888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Marcq-en-BarÅ“ul (59378)</span><br />Population : 39782"
          }
        },
        "town-50129": {
          value: "39772",
          latitude: 49.638611111111,
          longitude: -1.6158333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cherbourg-Octeville (50129)</span><br />Population : 39772"
          }
        },
        "town-03185": {
          value: "39712",
          latitude: 46.34,
          longitude: 2.6025,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">MontluÃ§on (03185)</span><br />Population : 39712"
          }
        },
        "town-44143": {
          value: "39683",
          latitude: 47.190555555556,
          longitude: -1.5691666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">RezÃ© (44143)</span><br />Population : 39683"
          }
        },
        "town-64024": {
          value: "39432",
          latitude: 43.484166666667,
          longitude: -1.5194444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Anglet (64024)</span><br />Population : 39432"
          }
        },
        "town-93032": {
          value: "39350",
          latitude: 48.881666666667,
          longitude: 2.5388888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gagny (93032)</span><br />Population : 39350"
          }
        },
        "town-69029": {
          value: "39238",
          latitude: 45.738611111111,
          longitude: 4.9130555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bron (69029)</span><br />Population : 39238"
          }
        },
        "town-97407": {
          value: "38668",
          latitude: -20.939444444444,
          longitude: 55.287222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Port (97407)</span><br />Population : 38668"
          }
        },
        "town-97311": {
          value: "38657",
          latitude: 5.5038888888889,
          longitude: -54.028888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Laurent-du-Maroni (97311)</span><br />Population : 38657"
          }
        },
        "town-92007": {
          value: "38384",
          latitude: 48.797777777778,
          longitude: 2.3125,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bagneux (92007)</span><br />Population : 38384"
          }
        },
        "town-93027": {
          value: "38361",
          latitude: 48.931388888889,
          longitude: 2.3958333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Courneuve (93027)</span><br />Population : 38361"
          }
        },
        "town-58194": {
          value: "38352",
          latitude: 46.9925,
          longitude: 3.1566666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Nevers (58194)</span><br />Population : 38352"
          }
        },
        "town-89024": {
          value: "38248",
          latitude: 47.7975,
          longitude: 3.5669444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Auxerre (89024)</span><br />Population : 38248"
          }
        },
        "town-42187": {
          value: "38225",
          latitude: 46.036111111111,
          longitude: 4.0680555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Roanne (42187)</span><br />Population : 38225"
          }
        },
        "town-78498": {
          value: "38049",
          latitude: 48.928888888889,
          longitude: 2.0447222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Poissy (78498)</span><br />Population : 38049"
          }
        },
        "town-83050": {
          value: "37295",
          latitude: 43.539444444444,
          longitude: 6.4661111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Draguignan (83050)</span><br />Population : 37295"
          }
        },
        "town-91589": {
          value: "37203",
          latitude: 48.673888888889,
          longitude: 2.3525,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Savigny-sur-Orge (91589)</span><br />Population : 37203"
          }
        },
        "town-26198": {
          value: "36669",
          latitude: 44.558611111111,
          longitude: 4.7508333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">MontÃ©limar (26198)</span><br />Population : 36669"
          }
        },
        "town-37122": {
          value: "36525",
          latitude: 47.350555555556,
          longitude: 0.66166666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">JouÃ©-lÃ¨s-Tours (37122)</span><br />Population : 36525"
          }
        },
        "town-38421": {
          value: "36504",
          latitude: 45.166388888889,
          longitude: 5.7647222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Martin-d'HÃ¨res (38421)</span><br />Population : 36504"
          }
        },
        "town-97412": {
          value: "36459",
          latitude: -21.378611111111,
          longitude: 55.619166666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Joseph (97412)</span><br />Population : 36459"
          }
        },
        "town-42207": {
          value: "36397",
          latitude: 45.476388888889,
          longitude: 4.5147222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Chamond (42207)</span><br />Population : 36397"
          }
        },
        "town-38151": {
          value: "36054",
          latitude: 45.142777777778,
          longitude: 5.7177777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰chirolles (38151)</span><br />Population : 36054"
          }
        },
        "town-93078": {
          value: "35931",
          latitude: 48.960555555556,
          longitude: 2.5302777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villepinte (93078)</span><br />Population : 35931"
          }
        },
        "town-69264": {
          value: "35900",
          latitude: 45.989444444444,
          longitude: 4.7197222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villefranche-sur-SaÃ´ne (69264)</span><br />Population : 35900"
          }
        },
        "town-77373": {
          value: "35873",
          latitude: 48.798333333333,
          longitude: 2.6052777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pontault-Combault (77373)</span><br />Population : 35873"
          }
        },
        "town-78172": {
          value: "35840",
          latitude: 48.997222222222,
          longitude: 2.0944444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Conflans-Sainte-Honorine (78172)</span><br />Population : 35840"
          }
        },
        "town-62498": {
          value: "35748",
          latitude: 50.431388888889,
          longitude: 2.8325,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lens (62498)</span><br />Population : 35748"
          }
        },
        "town-31149": {
          value: "35480",
          latitude: 43.612777777778,
          longitude: 1.3358333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Colomiers (31149)</span><br />Population : 35480"
          }
        },
        "town-13117": {
          value: "35459",
          latitude: 43.46,
          longitude: 5.2486111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vitrolles (13117)</span><br />Population : 35459"
          }
        },
        "town-83129": {
          value: "35415",
          latitude: 43.093333333333,
          longitude: 5.8394444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Six-Fours-les-Plages (83129)</span><br />Population : 35415"
          }
        },
        "town-47001": {
          value: "35293",
          latitude: 44.203055555556,
          longitude: 0.61861111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Agen (47001)</span><br />Population : 35293"
          }
        },
        "town-74281": {
          value: "35257",
          latitude: 46.370555555556,
          longitude: 6.4797222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Thonon-les-Bains (74281)</span><br />Population : 35257"
          }
        },
        "town-97410": {
          value: "35252",
          latitude: -21.033888888889,
          longitude: 55.712777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-BenoÃ®t (97410)</span><br />Population : 35252"
          }
        },
        "town-71270": {
          value: "35118",
          latitude: 46.306666666667,
          longitude: 4.8319444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">MÃ¢con (71270)</span><br />Population : 35118"
          }
        },
        "town-67180": {
          value: "34913",
          latitude: 48.816666666667,
          longitude: 7.7877777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Haguenau (67180)</span><br />Population : 34913"
          }
        },
        "town-13054": {
          value: "34773",
          latitude: 43.416944444444,
          longitude: 5.2147222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Marignane (13054)</span><br />Population : 34773"
          }
        },
        "town-93073": {
          value: "34744",
          latitude: 48.956111111111,
          longitude: 2.5763888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tremblay-en-France (93073)</span><br />Population : 34744"
          }
        },
        "town-88160": {
          value: "34575",
          latitude: 48.173611111111,
          longitude: 6.4516666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰pinal (88160)</span><br />Population : 34575"
          }
        },
        "town-91549": {
          value: "34514",
          latitude: 48.637777777778,
          longitude: 2.3322222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-GeneviÃ¨ve-des-Bois (91549)</span><br />Population : 34514"
          }
        },
        "town-26281": {
          value: "34321",
          latitude: 45.045555555556,
          longitude: 5.0508333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Romans-sur-IsÃ¨re (26281)</span><br />Population : 34321"
          }
        },
        "town-13028": {
          value: "34258",
          latitude: 43.176111111111,
          longitude: 5.6080555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Ciotat (13028)</span><br />Population : 34258"
          }
        },
        "town-93006": {
          value: "34232",
          latitude: 48.866944444444,
          longitude: 2.4169444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bagnolet (93006)</span><br />Population : 34232"
          }
        },
        "town-83118": {
          value: "34220",
          latitude: 43.424722222222,
          longitude: 6.7677777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-RaphaÃ«l (83118)</span><br />Population : 34220"
          }
        },
        "town-83118": {
          value: "34220",
          latitude: 43.424722222222,
          longitude: 6.7677777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-RaphaÃ«l (83118)</span><br />Population : 34220"
          }
        },
        "town-93072": {
          value: "34048",
          latitude: 48.955277777778,
          longitude: 2.3822222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Stains (93072)</span><br />Population : 34048"
          }
        },
        "town-60175": {
          value: "34001",
          latitude: 49.257777777778,
          longitude: 2.4827777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Creil (60175)</span><br />Population : 34001"
          }
        },
        "town-78423": {
          value: "33899",
          latitude: 48.770555555556,
          longitude: 2.0325,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montigny-le-Bretonneux (78423)</span><br />Population : 33899"
          }
        },
        "town-93050": {
          value: "33781",
          latitude: 48.857777777778,
          longitude: 2.5311111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Neuilly-sur-Marne (93050)</span><br />Population : 33781"
          }
        },
        "town-86066": {
          value: "33420",
          latitude: 46.816944444444,
          longitude: 0.54527777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢tellerault (86066)</span><br />Population : 33420"
          }
        },
        "town-59122": {
          value: "33345",
          latitude: 50.175833333333,
          longitude: 3.2347222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cambrai (59122)</span><br />Population : 33345"
          }
        },
        "town-95252": {
          value: "33324",
          latitude: 48.988055555556,
          longitude: 2.2305555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Franconville (95252)</span><br />Population : 33324"
          }
        },
        "town-40192": {
          value: "33124",
          latitude: 43.890277777778,
          longitude: -0.50055555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mont-de-Marsan (40192)</span><br />Population : 33124"
          }
        },
        "town-76217": {
          value: "32966",
          latitude: 49.921666666667,
          longitude: 1.0777777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dieppe (76217)</span><br />Population : 32966"
          }
        },
        "town-92020": {
          value: "32947",
          latitude: 48.801111111111,
          longitude: 2.2886111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢tillon (92020)</span><br />Population : 32947"
          }
        },
        "town-94058": {
          value: "32799",
          latitude: 48.842222222222,
          longitude: 2.5036111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Perreux-sur-Marne (94058)</span><br />Population : 32799"
          }
        },
        "town-74012": {
          value: "32790",
          latitude: 46.195,
          longitude: 6.2355555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Annemasse (74012)</span><br />Population : 32790"
          }
        },
        "town-92019": {
          value: "32573",
          latitude: 48.765277777778,
          longitude: 2.2780555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢tenay-Malabry (92019)</span><br />Population : 32573"
          }
        },
        "town-94078": {
          value: "32506",
          latitude: 48.7325,
          longitude: 2.4497222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villeneuve-Saint-Georges (94078)</span><br />Population : 32506"
          }
        },
        "town-91687": {
          value: "32396",
          latitude: 48.669444444444,
          longitude: 2.3758333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Viry-ChÃ¢tillon (91687)</span><br />Population : 32396"
          }
        },
        "town-62510": {
          value: "32328",
          latitude: 50.421944444444,
          longitude: 2.7777777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">LiÃ©vin (62510)</span><br />Population : 32328"
          }
        },
        "town-94052": {
          value: "31975",
          latitude: 48.836666666667,
          longitude: 2.4825,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Nogent-sur-Marne (94052)</span><br />Population : 31975"
          }
        },
        "town-78311": {
          value: "31849",
          latitude: 48.925555555556,
          longitude: 2.1883333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Houilles (78311)</span><br />Population : 31849"
          }
        },
        "town-28134": {
          value: "31610",
          latitude: 48.736388888889,
          longitude: 1.3655555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dreux (28134)</span><br />Population : 31610"
          }
        },
        "town-54547": {
          value: "31464",
          latitude: 48.656111111111,
          longitude: 6.1675,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">VandÅ“uvre-lÃ¨s-Nancy (54547)</span><br />Population : 31464"
          }
        },
        "town-59392": {
          value: "31435",
          latitude: 50.276944444444,
          longitude: 3.9725,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Maubeuge (59392)</span><br />Population : 31435"
          }
        },
        "town-78490": {
          value: "31360",
          latitude: 48.817777777778,
          longitude: 1.9463888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Plaisir (78490)</span><br />Population : 31360"
          }
        },
        "town-92046": {
          value: "31325",
          latitude: 48.817222222222,
          longitude: 2.2991666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Malakoff (92046)</span><br />Population : 31325"
          }
        },
        "town-97413": {
          value: "31298",
          latitude: -21.166388888889,
          longitude: 55.286944444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Leu (97413)</span><br />Population : 31298"
          }
        },
        "town-95280": {
          value: "31237",
          latitude: 49.031666666667,
          longitude: 2.4736111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Goussainville (95280)</span><br />Population : 31237"
          }
        },
        "town-67447": {
          value: "31218",
          latitude: 48.606944444444,
          longitude: 7.7491666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Schiltigheim (67447)</span><br />Population : 31218"
          }
        },
        "town-91477": {
          value: "31175",
          latitude: 48.718333333333,
          longitude: 2.2497222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Palaiseau (91477)</span><br />Population : 31175"
          }
        },
        "town-78440": {
          value: "31116",
          latitude: 48.993055555556,
          longitude: 1.9083333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Mureaux (78440)</span><br />Population : 31116"
          }
        },
        "town-95500": {
          value: "31011",
          latitude: 49.050833333333,
          longitude: 2.1008333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pontoise (95500)</span><br />Population : 31011"
          }
        },
        "town-24322": {
          value: "31000",
          latitude: 45.184166666667,
          longitude: 0.71805555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">PÃ©rigueux (24322)</span><br />Population : 31000"
          }
        },
        "town-91027": {
          value: "30845",
          latitude: 48.708611111111,
          longitude: 2.3891666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Athis-Mons (91027)</span><br />Population : 30845"
          }
        },
        "town-97408": {
          value: "30784",
          latitude: -20.926388888889,
          longitude: 55.335833333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Possession (97408)</span><br />Population : 30784"
          }
        },
        "town-97103": {
          value: "30775",
          latitude: 16.2675,
          longitude: -61.586944444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Baie-Mahault (97103)</span><br />Population : 30775"
          }
        },
        "town-69282": {
          value: "30672",
          latitude: 45.766388888889,
          longitude: 5.0027777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Meyzieu (69282)</span><br />Population : 30672"
          }
        },
        "town-78146": {
          value: "30667",
          latitude: 48.890555555556,
          longitude: 2.1569444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chatou (78146)</span><br />Population : 30667"
          }
        },
        "town-94038": {
          value: "30588",
          latitude: 48.779166666667,
          longitude: 2.3372222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">L'HaÃ¿-les-Roses (94038)</span><br />Population : 30588"
          }
        },
        "town-92064": {
          value: "30416",
          latitude: 48.846388888889,
          longitude: 2.2152777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Cloud (92064)</span><br />Population : 30416"
          }
        },
        "town-69286": {
          value: "30375",
          latitude: 45.820555555556,
          longitude: 4.8975,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rillieux-la-Pape (69286)</span><br />Population : 30375"
          }
        },
        "town-84031": {
          value: "30360",
          latitude: 44.055,
          longitude: 5.0480555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Carpentras (84031)</span><br />Population : 30360"
          }
        },
        "town-97418": {
          value: "30293",
          latitude: -20.896944444444,
          longitude: 55.549166666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Marie (97418)</span><br />Population : 30293"
          }
        },
        "town-06123": {
          value: "30235",
          latitude: 43.673333333333,
          longitude: 7.19,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Laurent-du-Var (06123)</span><br />Population : 30235"
          }
        },
        "town-38544": {
          value: "30169",
          latitude: 45.525555555556,
          longitude: 4.8747222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vienne (38544)</span><br />Population : 30169"
          }
        },
        "town-93014": {
          value: "29998",
          latitude: 48.909166666667,
          longitude: 2.5472222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Clichy-sous-Bois (93014)</span><br />Population : 29998"
          }
        },
        "town-94073": {
          value: "29949",
          latitude: 48.764444444444,
          longitude: 2.3913888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Thiais (94073)</span><br />Population : 29949"
          }
        },
        "town-02722": {
          value: "29846",
          latitude: 49.381111111111,
          longitude: 3.3225,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Soissons (02722)</span><br />Population : 29846"
          }
        },
        "town-84087": {
          value: "29791",
          latitude: 44.1375,
          longitude: 4.8088888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Orange (84087)</span><br />Population : 29791"
          }
        },
        "town-78621": {
          value: "29705",
          latitude: 48.776666666667,
          longitude: 2.0016666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Trappes (78621)</span><br />Population : 29705"
          }
        },
        "town-78158": {
          value: "29682",
          latitude: 48.820277777778,
          longitude: 2.1302777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Chesnay (78158)</span><br />Population : 29682"
          }
        },
        "town-15014": {
          value: "29677",
          latitude: 44.925277777778,
          longitude: 2.4397222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aurillac (15014)</span><br />Population : 29677"
          }
        },
        "town-94018": {
          value: "29664",
          latitude: 48.821388888889,
          longitude: 2.4119444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Charenton-le-Pont (94018)</span><br />Population : 29664"
          }
        },
        "town-92009": {
          value: "29519",
          latitude: 48.9175,
          longitude: 2.2683333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bois-Colombes (92009)</span><br />Population : 29519"
          }
        },
        "town-76681": {
          value: "29518",
          latitude: 49.408611111111,
          longitude: 1.0891666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sotteville-lÃ¨s-Rouen (76681)</span><br />Population : 29518"
          }
        },
        "town-91691": {
          value: "29392",
          latitude: 48.716111111111,
          longitude: 2.4908333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Yerres (91691)</span><br />Population : 29392"
          }
        },
        "town-06083": {
          value: "29389",
          latitude: 43.774722222222,
          longitude: 7.4997222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Menton (06083)</span><br />Population : 29389"
          }
        },
        "town-33550": {
          value: "28905",
          latitude: 44.779444444444,
          longitude: -0.56694444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villenave-d'Ornon (33550)</span><br />Population : 28905"
          }
        },
        "town-59328": {
          value: "28870",
          latitude: 50.649444444444,
          longitude: 3.0241666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lambersart (59328)</span><br />Population : 28870"
          }
        },
        "town-77445": {
          value: "28838",
          latitude: 48.575833333333,
          longitude: 2.5827777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Savigny-le-Temple (77445)</span><br />Population : 28838"
          }
        },
        "town-91201": {
          value: "28802",
          latitude: 48.686111111111,
          longitude: 2.4094444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Draveil (91201)</span><br />Population : 28802"
          }
        },
        "town-49328": {
          value: "28772",
          latitude: 47.259166666667,
          longitude: -0.078055555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saumur (49328)</span><br />Population : 28772"
          }
        },
        "town-24037": {
          value: "28691",
          latitude: 44.851111111111,
          longitude: 0.48194444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bergerac (24037)</span><br />Population : 28691"
          }
        },
        "town-76575": {
          value: "28601",
          latitude: 49.377777777778,
          longitude: 1.1041666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Ã‰tienne-du-Rouvray (76575)</span><br />Population : 28601"
          }
        },
        "town-94016": {
          value: "28550",
          latitude: 48.791944444444,
          longitude: 2.3319444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cachan (94016)</span><br />Population : 28550"
          }
        },
        "town-78297": {
          value: "28518",
          latitude: 48.770555555556,
          longitude: 2.0730555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Guyancourt (78297)</span><br />Population : 28518"
          }
        },
        "town-06155": {
          value: "28450",
          latitude: 43.579722222222,
          longitude: 7.0533333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vallauris (06155)</span><br />Population : 28450"
          }
        },
        "town-73008": {
          value: "28439",
          latitude: 45.688611111111,
          longitude: 5.915,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aix-les-Bains (73008)</span><br />Population : 28439"
          }
        },
        "town-97307": {
          value: "28407",
          latitude: 4.8505555555556,
          longitude: -52.331111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Matoury (97307)</span><br />Population : 28407"
          }
        },
        "town-33449": {
          value: "28396",
          latitude: 44.895555555556,
          longitude: -0.7175,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-MÃ©dard-en-Jalles (33449)</span><br />Population : 28396"
          }
        },
        "town-95063": {
          value: "28277",
          latitude: 48.925555555556,
          longitude: 2.2169444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bezons (95063)</span><br />Population : 28277"
          }
        },
        "town-93077": {
          value: "28257",
          latitude: 48.890277777778,
          longitude: 2.5111111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villemomble (93077)</span><br />Population : 28257"
          }
        },
        "town-93059": {
          value: "28076",
          latitude: 48.964722222222,
          longitude: 2.3608333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pierrefitte-sur-Seine (93059)</span><br />Population : 28076"
          }
        },
        "town-92060": {
          value: "27931",
          latitude: 48.783333333333,
          longitude: 2.2636111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Plessis-Robinson (92060)</span><br />Population : 27931"
          }
        },
        "town-92035": {
          value: "27923",
          latitude: 48.905,
          longitude: 2.2436111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Garenne-Colombes (92035)</span><br />Population : 27923"
          }
        },
        "town-61001": {
          value: "27863",
          latitude: 48.429722222222,
          longitude: 0.091944444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AlenÃ§on (61001)</span><br />Population : 27863"
          }
        },
        "town-95219": {
          value: "27713",
          latitude: 48.991388888889,
          longitude: 2.2594444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ermont (95219)</span><br />Population : 27713"
          }
        },
        "town-91521": {
          value: "27689",
          latitude: 48.651111111111,
          longitude: 2.4130555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ris-Orangis (91521)</span><br />Population : 27689"
          }
        },
        "town-18279": {
          value: "27675",
          latitude: 47.221944444444,
          longitude: 2.0683333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vierzon (18279)</span><br />Population : 27675"
          }
        },
        "town-94079": {
          value: "27568",
          latitude: 48.8275,
          longitude: 2.5447222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villiers-sur-Marne (94079)</span><br />Population : 27568"
          }
        },
        "town-67218": {
          value: "27556",
          latitude: 48.524722222222,
          longitude: 7.7144444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Illkirch-Graffenstaden (67218)</span><br />Population : 27556"
          }
        },
        "town-91657": {
          value: "27546",
          latitude: 48.700277777778,
          longitude: 2.4172222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vigneux-sur-Seine (91657)</span><br />Population : 27546"
          }
        },
        "town-17415": {
          value: "27430",
          latitude: 45.745277777778,
          longitude: -0.63444444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saintes (17415)</span><br />Population : 27430"
          }
        },
        "town-92075": {
          value: "27314",
          latitude: 48.82,
          longitude: 2.2888888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vanves (92075)</span><br />Population : 27314"
          }
        },
        "town-78208": {
          value: "27262",
          latitude: 48.783888888889,
          longitude: 1.9580555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰lancourt (78208)</span><br />Population : 27262"
          }
        },
        "town-95680": {
          value: "27004",
          latitude: 49.008888888889,
          longitude: 2.3902777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villiers-le-Bel (95680)</span><br />Population : 27004"
          }
        },
        "town-78517": {
          value: "27001",
          latitude: 48.643611111111,
          longitude: 1.83,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rambouillet (78517)</span><br />Population : 27001"
          }
        },
        "town-02408": {
          value: "26991",
          latitude: 49.563333333333,
          longitude: 3.6236111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Laon (02408)</span><br />Population : 26991"
          }
        },
        "town-38053": {
          value: "26841",
          latitude: 45.590833333333,
          longitude: 5.2791666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bourgoin-Jallieu (38053)</span><br />Population : 26841"
          }
        },
        "town-91286": {
          value: "26796",
          latitude: 48.656666666667,
          longitude: 2.3880555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Grigny (91286)</span><br />Population : 26796"
          }
        },
        "town-97113": {
          value: "26743",
          latitude: 16.205555555556,
          longitude: -61.491944444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Gosier (97113)</span><br />Population : 26743"
          }
        },
        "town-62427": {
          value: "26728",
          latitude: 50.421111111111,
          longitude: 2.95,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">HÃ©nin-Beaumont (62427)</span><br />Population : 26728"
          }
        },
        "town-95582": {
          value: "26659",
          latitude: 48.971666666667,
          longitude: 2.2569444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sannois (95582)</span><br />Population : 26659"
          }
        },
        "town-95277": {
          value: "26627",
          latitude: 48.986666666667,
          longitude: 2.4486111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gonesse (95277)</span><br />Population : 26627"
          }
        },
        "town-52448": {
          value: "26549",
          latitude: 48.637777777778,
          longitude: 4.9488888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Dizier (52448)</span><br />Population : 26549"
          }
        },
        "town-95306": {
          value: "26533",
          latitude: 48.990277777778,
          longitude: 2.1655555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Herblay (95306)</span><br />Population : 26533"
          }
        },
        "town-62119": {
          value: "26530",
          latitude: 50.529722222222,
          longitude: 2.64,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">BÃ©thune (62119)</span><br />Population : 26530"
          }
        },
        "town-25388": {
          value: "26501",
          latitude: 47.509722222222,
          longitude: 6.7983333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">MontbÃ©liard (25388)</span><br />Population : 26501"
          }
        },
        "town-94034": {
          value: "26446",
          latitude: 48.758888888889,
          longitude: 2.3236111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fresnes (94034)</span><br />Population : 26446"
          }
        },
        "town-95607": {
          value: "26440",
          latitude: 49.025833333333,
          longitude: 2.2266666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Taverny (95607)</span><br />Population : 26440"
          }
        },
        "town-83062": {
          value: "26321",
          latitude: 43.124722222222,
          longitude: 6.0105555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Garde (83062)</span><br />Population : 26321"
          }
        },
        "town-27681": {
          value: "26306",
          latitude: 49.091666666667,
          longitude: 1.485,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vernon (27681)</span><br />Population : 26306"
          }
        },
        "town-94043": {
          value: "26267",
          latitude: 48.81,
          longitude: 2.3580555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Kremlin-BicÃªtre (94043)</span><br />Population : 26267"
          }
        },
        "town-94071": {
          value: "26150",
          latitude: 48.769722222222,
          longitude: 2.5227777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sucy-en-Brie (94071)</span><br />Population : 26150"
          }
        },
        "town-93063": {
          value: "26025",
          latitude: 48.883611111111,
          longitude: 2.4361111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Romainville (93063)</span><br />Population : 26025"
          }
        },
        "town-64122": {
          value: "25994",
          latitude: 43.480555555556,
          longitude: -1.5572222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Biarritz (64122)</span><br />Population : 25994"
          }
        },
        "town-69275": {
          value: "25988",
          latitude: 45.768611111111,
          longitude: 4.9588888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">DÃ©cines-Charpieu (69275)</span><br />Population : 25988"
          }
        },
        "town-12202": {
          value: "25974",
          latitude: 44.35,
          longitude: 2.5741666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rodez (12202)</span><br />Population : 25974"
          }
        },
        "town-17299": {
          value: "25962",
          latitude: 45.941944444444,
          longitude: -0.9669444444444401,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rochefort (17299)</span><br />Population : 25962"
          }
        },
        "town-31557": {
          value: "25854",
          latitude: 43.584444444444,
          longitude: 1.3436111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tournefeuille (31557)</span><br />Population : 25854"
          }
        },
        "town-44190": {
          value: "25832",
          latitude: 47.207222222222,
          longitude: -1.5025,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-SÃ©bastien-sur-Loire (44190)</span><br />Population : 25832"
          }
        },
        "town-13063": {
          value: "25823",
          latitude: 43.581388888889,
          longitude: 5.0013888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Miramas (13063)</span><br />Population : 25823"
          }
        },
        "town-59017": {
          value: "25786",
          latitude: 50.687222222222,
          longitude: 2.8802777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ArmentiÃ¨res (59017)</span><br />Population : 25786"
          }
        },
        "town-91114": {
          value: "25785",
          latitude: 48.698055555556,
          longitude: 2.5033333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Brunoy (91114)</span><br />Population : 25785"
          }
        },
        "town-39198": {
          value: "25776",
          latitude: 47.092222222222,
          longitude: 5.4897222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dole (39198)</span><br />Population : 25776"
          }
        },
        "town-89387": {
          value: "25676",
          latitude: 48.197222222222,
          longitude: 3.2833333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sens (89387)</span><br />Population : 25676"
          }
        },
        "town-34145": {
          value: "25509",
          latitude: 43.676944444444,
          longitude: 4.1352777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lunel (34145)</span><br />Population : 25509"
          }
        },
        "town-93047": {
          value: "25499",
          latitude: 48.898333333333,
          longitude: 2.5647222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montfermeil (93047)</span><br />Population : 25499"
          }
        },
        "town-84035": {
          value: "25440",
          latitude: 43.836666666667,
          longitude: 5.0372222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cavaillon (84035)</span><br />Population : 25440"
          }
        },
        "town-69149": {
          value: "25413",
          latitude: 45.714166666667,
          longitude: 4.8075,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Oullins (69149)</span><br />Population : 25413"
          }
        },
        "town-97304": {
          value: "25404",
          latitude: 5.1583333333333,
          longitude: -52.642777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Kourou (97304)</span><br />Population : 25404"
          }
        },
        "town-92078": {
          value: "25374",
          latitude: 48.937222222222,
          longitude: 2.3277777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villeneuve-la-Garenne (92078)</span><br />Population : 25374"
          }
        },
        "town-03310": {
          value: "25235",
          latitude: 46.126944444444,
          longitude: 3.4258333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vichy (03310)</span><br />Population : 25235"
          }
        },
        "town-44114": {
          value: "25216",
          latitude: 47.270833333333,
          longitude: -1.6236111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Orvault (44114)</span><br />Population : 25216"
          }
        },
        "town-33039": {
          value: "25205",
          latitude: 44.807777777778,
          longitude: -0.54888888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">BÃ¨gles (33039)</span><br />Population : 25205"
          }
        },
        "town-76322": {
          value: "25189",
          latitude: 49.406388888889,
          longitude: 1.0522222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Grand-Quevilly (76322)</span><br />Population : 25189"
          }
        },
        "town-91692": {
          value: "25055",
          latitude: 48.682222222222,
          longitude: 2.1675,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Ulis (91692)</span><br />Population : 25055"
          }
        },
        "town-33529": {
          value: "25018",
          latitude: 44.6325,
          longitude: -1.145,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Teste-de-Buch (33529)</span><br />Population : 25018"
          }
        },
        "town-34003": {
          value: "24972",
          latitude: 43.31,
          longitude: 3.4752777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Agde (34003)</span><br />Population : 24972"
          }
        },
        "town-80001": {
          value: "24953",
          latitude: 50.105277777778,
          longitude: 1.8352777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Abbeville (80001)</span><br />Population : 24953"
          }
        },
        "town-51230": {
          value: "24733",
          latitude: 49.04,
          longitude: 3.9591666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰pernay (51230)</span><br />Population : 24733"
          }
        },
        "town-47323": {
          value: "24700",
          latitude: 44.406944444444,
          longitude: 0.70416666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villeneuve-sur-Lot (47323)</span><br />Population : 24700"
          }
        },
        "town-31395": {
          value: "24653",
          latitude: 43.460277777778,
          longitude: 1.3258333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Muret (31395)</span><br />Population : 24653"
          }
        },
        "town-77083": {
          value: "24636",
          latitude: 48.852777777778,
          longitude: 2.6019444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Champs-sur-Marne (77083)</span><br />Population : 24636"
          }
        },
        "town-97128": {
          value: "24611",
          latitude: 16.225555555556,
          longitude: -61.386111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Anne (97128)</span><br />Population : 24611"
          }
        },
        "town-52121": {
          value: "24500",
          latitude: 48.110833333333,
          longitude: 5.1386111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chaumont (52121)</span><br />Population : 24500"
          }
        },
        "town-95203": {
          value: "24386",
          latitude: 48.991388888889,
          longitude: 2.2797222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Eaubonne (95203)</span><br />Population : 24386"
          }
        },
        "town-33243": {
          value: "24302",
          latitude: 44.915277777778,
          longitude: -0.24388888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Libourne (33243)</span><br />Population : 24302"
          }
        },
        "town-77514": {
          value: "24296",
          latitude: 48.942777777778,
          longitude: 2.6063888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villeparisis (77514)</span><br />Population : 24296"
          }
        },
        "town-97222": {
          value: "24095",
          latitude: 14.6775,
          longitude: -60.939166666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Robert (97222)</span><br />Population : 24095"
          }
        },
        "town-95572": {
          value: "23889",
          latitude: 49.044166666667,
          longitude: 2.1102777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Ouen-l'AumÃ´ne (95572)</span><br />Population : 23889"
          }
        },
        "town-62178": {
          value: "23869",
          latitude: 50.481111111111,
          longitude: 2.5477777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bruay-la-BuissiÃ¨re (62178)</span><br />Population : 23869"
          }
        },
        "town-91103": {
          value: "23812",
          latitude: 48.609444444444,
          longitude: 2.3077777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">BrÃ©tigny-sur-Orge (91103)</span><br />Population : 23812"
          }
        },
        "town-77058": {
          value: "23663",
          latitude: 48.841666666667,
          longitude: 2.6977777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bussy-Saint-Georges (77058)</span><br />Population : 23663"
          }
        },
        "town-97118": {
          value: "23606",
          latitude: 16.191388888889,
          longitude: -61.590277777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Petit-Bourg (97118)</span><br />Population : 23606"
          }
        },
        "town-92032": {
          value: "23603",
          latitude: 48.789166666667,
          longitude: 2.2855555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fontenay-aux-Roses (92032)</span><br />Population : 23603"
          }
        },
        "town-91223": {
          value: "23575",
          latitude: 48.435,
          longitude: 2.1622222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰tampes (91223)</span><br />Population : 23575"
          }
        },
        "town-33192": {
          value: "23546",
          latitude: 44.771388888889,
          longitude: -0.61694444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gradignan (33192)</span><br />Population : 23546"
          }
        },
        "town-33069": {
          value: "23539",
          latitude: 44.864722222222,
          longitude: -0.59861111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Bouscat (33069)</span><br />Population : 23539"
          }
        },
        "town-92072": {
          value: "23412",
          latitude: 48.823055555556,
          longitude: 2.2108333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">SÃ¨vres (92072)</span><br />Population : 23412"
          }
        },
        "town-95176": {
          value: "23318",
          latitude: 48.973055555556,
          longitude: 2.2005555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cormeilles-en-Parisis (95176)</span><br />Population : 23318"
          }
        },
        "town-01283": {
          value: "23308",
          latitude: 46.255555555556,
          longitude: 5.655,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Oyonnax (01283)</span><br />Population : 23308"
          }
        },
        "town-78358": {
          value: "23287",
          latitude: 48.946111111111,
          longitude: 2.145,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Maisons-Laffitte (78358)</span><br />Population : 23287"
          }
        },
        "town-71153": {
          value: "23186",
          latitude: 46.800555555556,
          longitude: 4.4402777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Creusot (71153)</span><br />Population : 23186"
          }
        },
        "town-21054": {
          value: "23135",
          latitude: 47.024166666667,
          longitude: 4.8388888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Beaune (21054)</span><br />Population : 23135"
          }
        },
        "town-91421": {
          value: "23131",
          latitude: 48.7075,
          longitude: 2.4552777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montgeron (91421)</span><br />Population : 23131"
          }
        },
        "town-57480": {
          value: "23049",
          latitude: 49.099722222222,
          longitude: 6.1533333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montigny-lÃ¨s-Metz (57480)</span><br />Population : 23049"
          }
        },
        "town-32013": {
          value: "22931",
          latitude: 43.645277777778,
          longitude: 0.58861111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Auch (32013)</span><br />Population : 22931"
          }
        },
        "town-59155": {
          value: "22918",
          latitude: 51.024722222222,
          longitude: 2.3908333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Coudekerque-Branche (59155)</span><br />Population : 22918"
          }
        },
        "town-04112": {
          value: "22852",
          latitude: 43.833333333333,
          longitude: 5.7830555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Manosque (04112)</span><br />Population : 22852"
          }
        },
        "town-12145": {
          value: "22775",
          latitude: 44.097777777778,
          longitude: 3.0777777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Millau (12145)</span><br />Population : 22775"
          }
        },
        "town-59368": {
          value: "22758",
          latitude: 50.655277777778,
          longitude: 3.0702777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Madeleine (59368)</span><br />Population : 22758"
          }
        },
        "town-56098": {
          value: "22744",
          latitude: 47.763333333333,
          longitude: -3.3388888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lanester (56098)</span><br />Population : 22744"
          }
        },
        "town-34108": {
          value: "22743",
          latitude: 43.447222222222,
          longitude: 3.7555555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Frontignan (34108)</span><br />Population : 22743"
          }
        },
        "town-97117": {
          value: "22716",
          latitude: 16.331111111111,
          longitude: -61.343611111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Moule (97117)</span><br />Population : 22716"
          }
        },
        "town-94067": {
          value: "22666",
          latitude: 48.841388888889,
          longitude: 2.4177777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-MandÃ© (94067)</span><br />Population : 22666"
          }
        },
        "town-77468": {
          value: "22639",
          latitude: 48.850277777778,
          longitude: 2.6508333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Torcy (77468)</span><br />Population : 22639"
          }
        },
        "town-97420": {
          value: "22627",
          latitude: -20.905555555556,
          longitude: 55.607222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Suzanne (97420)</span><br />Population : 22627"
          }
        },
        "town-33119": {
          value: "22588",
          latitude: 44.856944444444,
          longitude: -0.53277777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cenon (33119)</span><br />Population : 22588"
          }
        },
        "town-14366": {
          value: "22547",
          latitude: 49.145555555556,
          longitude: 0.22555555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lisieux (14366)</span><br />Population : 22547"
          }
        },
        "town-77390": {
          value: "22514",
          latitude: 48.791111111111,
          longitude: 2.6513888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Roissy-en-Brie (77390)</span><br />Population : 22514"
          }
        },
        "town-06079": {
          value: "22498",
          latitude: 43.545555555556,
          longitude: 6.9375,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mandelieu-la-Napoule (06079)</span><br />Population : 22498"
          }
        },
        "town-38169": {
          value: "22485",
          latitude: 45.193055555556,
          longitude: 5.6847222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fontaine (38169)</span><br />Population : 22485"
          }
        },
        "town-93045": {
          value: "22410",
          latitude: 48.88,
          longitude: 2.4169444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Lilas (93045)</span><br />Population : 22410"
          }
        },
        "town-69202": {
          value: "22229",
          latitude: 45.733611111111,
          longitude: 4.8025,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Foy-lÃ¨s-Lyon (69202)</span><br />Population : 22229"
          }
        },
        "town-88413": {
          value: "22225",
          latitude: 48.284166666667,
          longitude: 6.9491666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-DiÃ©-des-Vosges (88413)</span><br />Population : 22225"
          }
        },
        "town-76498": {
          value: "22215",
          latitude: 49.430555555556,
          longitude: 1.0527777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Petit-Quevilly (76498)</span><br />Population : 22215"
          }
        },
        "town-31069": {
          value: "22119",
          latitude: 43.635555555556,
          longitude: 1.39,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Blagnac (31069)</span><br />Population : 22119"
          }
        },
        "town-44215": {
          value: "22117",
          latitude: 47.168055555556,
          longitude: -1.4713888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vertou (44215)</span><br />Population : 22117"
          }
        },
        "town-57631": {
          value: "22094",
          latitude: 49.110555555556,
          longitude: 7.0672222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sarreguemines (57631)</span><br />Population : 22094"
          }
        },
        "town-59295": {
          value: "22086",
          latitude: 50.724444444444,
          longitude: 2.5383333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Hazebrouck (59295)</span><br />Population : 22086"
          }
        },
        "town-59360": {
          value: "22081",
          latitude: 50.612222222222,
          longitude: 3.0136111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Loos (59360)</span><br />Population : 22081"
          }
        },
        "town-59410": {
          value: "22036",
          latitude: 50.641944444444,
          longitude: 3.1077777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mons-en-BarÅ“ul (59410)</span><br />Population : 22036"
          }
        },
        "town-93057": {
          value: "21972",
          latitude: 48.905833333333,
          longitude: 2.5105555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Pavillons-sous-Bois (93057)</span><br />Population : 21972"
          }
        },
        "town-57227": {
          value: "21920",
          latitude: 49.188055555556,
          longitude: 6.9,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Forbach (57227)</span><br />Population : 21920"
          }
        },
        "town-76108": {
          value: "21876",
          latitude: 49.460555555556,
          longitude: 1.1080555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bois-Guillaume (76108)</span><br />Population : 21876"
          }
        },
        "town-76108": {
          value: "21876",
          latitude: 49.460555555556,
          longitude: 1.1080555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bois-Guillaume - Bihorel (76108)</span><br />Population : 21876"
          }
        },
        "town-77122": {
          value: "21845",
          latitude: 48.661944444444,
          longitude: 2.5630555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Combs-la-Ville (77122)</span><br />Population : 21845"
          }
        },
        "town-14327": {
          value: "21829",
          latitude: 49.203611111111,
          longitude: -0.32638888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">HÃ©rouville-Saint-Clair (14327)</span><br />Population : 21829"
          }
        },
        "town-95197": {
          value: "21741",
          latitude: 48.975,
          longitude: 2.3286111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Deuil-la-Barre (95197)</span><br />Population : 21741"
          }
        },
        "town-40088": {
          value: "21702",
          latitude: 43.706944444444,
          longitude: -1.0513888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dax (40088)</span><br />Population : 21702"
          }
        },
        "town-94054": {
          value: "21691",
          latitude: 48.743611111111,
          longitude: 2.3927777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Orly (94054)</span><br />Population : 21691"
          }
        },
        "town-91345": {
          value: "21574",
          latitude: 48.696944444444,
          longitude: 2.2955555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Longjumeau (91345)</span><br />Population : 21574"
          }
        },
        "town-95428": {
          value: "21475",
          latitude: 48.989722222222,
          longitude: 2.3219444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montmorency (95428)</span><br />Population : 21475"
          }
        },
        "town-45147": {
          value: "21450",
          latitude: 47.931944444444,
          longitude: 1.9211111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fleury-les-Aubrais (45147)</span><br />Population : 21450"
          }
        },
        "town-78126": {
          value: "21374",
          latitude: 48.85,
          longitude: 2.145,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Celle-Saint-Cloud (78126)</span><br />Population : 21374"
          }
        },
        "town-46042": {
          value: "21333",
          latitude: 44.4475,
          longitude: 1.4405555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cahors (46042)</span><br />Population : 21333"
          }
        },
        "town-91272": {
          value: "21259",
          latitude: 48.701388888889,
          longitude: 2.1336111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gif-sur-Yvette (91272)</span><br />Population : 21259"
          }
        },
        "town-59271": {
          value: "21235",
          latitude: 51.013055555556,
          longitude: 2.3022222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Grande-Synthe (59271)</span><br />Population : 21235"
          }
        },
        "town-97229": {
          value: "21209",
          latitude: 14.616111111111,
          longitude: -61.101388888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">SchÅ“lcher (97229)</span><br />Population : 21209"
          }
        },
        "town-91434": {
          value: "21113",
          latitude: 48.663333333333,
          longitude: 2.3513888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Morsang-sur-Orge (91434)</span><br />Population : 21113"
          }
        },
        "town-83144": {
          value: "21035",
          latitude: 43.137222222222,
          longitude: 5.9825,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Valette-du-Var (83144)</span><br />Population : 21035"
          }
        },
        "town-22113": {
          value: "20983",
          latitude: 48.7325,
          longitude: -3.4552777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lannion (22113)</span><br />Population : 20983"
          }
        },
        "town-69204": {
          value: "20982",
          latitude: 45.695277777778,
          longitude: 4.7930555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Genis-Laval (69204)</span><br />Population : 20982"
          }
        },
        "town-59163": {
          value: "20962",
          latitude: 50.674722222222,
          longitude: 3.1538888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Croix (59163)</span><br />Population : 20962"
          }
        },
        "town-77152": {
          value: "20923",
          latitude: 48.515277777778,
          longitude: 2.6344444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dammarie-les-Lys (77152)</span><br />Population : 20923"
          }
        },
        "town-74011": {
          value: "20881",
          latitude: 45.919166666667,
          longitude: 6.1419444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Annecy-le-Vieux (74011)</span><br />Population : 20881"
          }
        },
        "town-77285": {
          value: "20830",
          latitude: 48.5375,
          longitude: 2.6319444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le MÃ©e-sur-Seine (77285)</span><br />Population : 20830"
          }
        },
        "town-13041": {
          value: "20799",
          latitude: 43.454444444444,
          longitude: 5.4761111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gardanne (13041)</span><br />Population : 20799"
          }
        },
        "town-93049": {
          value: "20683",
          latitude: 48.860833333333,
          longitude: 2.5097222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Neuilly-Plaisance (93049)</span><br />Population : 20683"
          }
        },
        "town-35115": {
          value: "20637",
          latitude: 48.351666666667,
          longitude: -1.2,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">FougÃ¨res (35115)</span><br />Population : 20637"
          }
        },
        "town-77350": {
          value: "20598",
          latitude: 48.769166666667,
          longitude: 2.6791666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ozoir-la-FerriÃ¨re (77350)</span><br />Population : 20598"
          }
        },
        "town-38563": {
          value: "20573",
          latitude: 45.363333333333,
          longitude: 5.59,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Voiron (38563)</span><br />Population : 20573"
          }
        },
        "town-77243": {
          value: "20538",
          latitude: 48.878055555556,
          longitude: 2.7066666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lagny-sur-Marne (77243)</span><br />Population : 20538"
          }
        },
        "town-59172": {
          value: "20523",
          latitude: 50.328611111111,
          longitude: 3.395,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Denain (59172)</span><br />Population : 20523"
          }
        },
        "town-68297": {
          value: "20481",
          latitude: 47.585277777778,
          longitude: 7.565,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Louis (68297)</span><br />Population : 20481"
          }
        },
        "town-97129": {
          value: "20443",
          latitude: 16.333055555556,
          longitude: -61.698055555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Rose (97129)</span><br />Population : 20443"
          }
        },
        "town-59279": {
          value: "20370",
          latitude: 50.782777777778,
          longitude: 3.1247222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Halluin (59279)</span><br />Population : 20370"
          }
        },
        "town-78640": {
          value: "20348",
          latitude: 48.783333333333,
          longitude: 2.1883333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">VÃ©lizy-Villacoublay (78640)</span><br />Population : 20348"
          }
        },
        "town-91570": {
          value: "20345",
          latitude: 48.6325,
          longitude: 2.3027777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Michel-sur-Orge (91570)</span><br />Population : 20345"
          }
        },
        "town-95555": {
          value: "20326",
          latitude: 48.971111111111,
          longitude: 2.2819444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Gratien (95555)</span><br />Population : 20326"
          }
        },
        "town-92014": {
          value: "20303",
          latitude: 48.778055555556,
          longitude: 2.3158333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bourg-la-Reine (92014)</span><br />Population : 20303"
          }
        },
        "town-59646": {
          value: "20293",
          latitude: 50.668611111111,
          longitude: 3.13,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Wasquehal (59646)</span><br />Population : 20293"
          }
        },
        "town-54329": {
          value: "20286",
          latitude: 48.589444444444,
          longitude: 6.5016666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">LunÃ©ville (54329)</span><br />Population : 20286"
          }
        },
        "town-33249": {
          value: "20271",
          latitude: 44.879166666667,
          longitude: -0.5216666666666701,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lormont (33249)</span><br />Population : 20271"
          }
        },
        "town-03190": {
          value: "20229",
          latitude: 46.564722222222,
          longitude: 3.3325,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Moulins (03190)</span><br />Population : 20229"
          }
        },
        "town-45232": {
          value: "20196",
          latitude: 47.863055555556,
          longitude: 1.8997222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Olivet (45232)</span><br />Population : 20196"
          }
        },
        "town-94044": {
          value: "20112",
          latitude: 48.746388888889,
          longitude: 2.4883333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Limeil-BrÃ©vannes (94044)</span><br />Population : 20112"
          }
        },
        "town-33162": {
          value: "19998",
          latitude: 44.884444444444,
          longitude: -0.65138888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Eysines (33162)</span><br />Population : 19998"
          }
        },
        "town-92071": {
          value: "19986",
          latitude: 48.778611111111,
          longitude: 2.2905555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sceaux (92071)</span><br />Population : 19986"
          }
        },
        "town-94003": {
          value: "19964",
          latitude: 48.806666666667,
          longitude: 2.3352777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Arcueil (94003)</span><br />Population : 19964"
          }
        },
        "town-50502": {
          value: "19944",
          latitude: 49.114444444444,
          longitude: -1.0916666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-LÃ´ (50502)</span><br />Population : 19944"
          }
        },
        "town-69244": {
          value: "19938",
          latitude: 45.763333333333,
          longitude: 4.78,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tassin-la-Demi-Lune (69244)</span><br />Population : 19938"
          }
        },
        "town-76451": {
          value: "19880",
          latitude: 49.4625,
          longitude: 1.0872222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mont-Saint-Aignan (76451)</span><br />Population : 19880"
          }
        },
        "town-33199": {
          value: "19877",
          latitude: 44.635277777778,
          longitude: -1.0677777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gujan-Mestras (33199)</span><br />Population : 19877"
          }
        },
        "town-71306": {
          value: "19855",
          latitude: 46.666944444444,
          longitude: 4.3688888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montceau-les-Mines (71306)</span><br />Population : 19855"
          }
        },
        "town-13002": {
          value: "19775",
          latitude: 43.336111111111,
          longitude: 5.4822222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Allauch (13002)</span><br />Population : 19775"
          }
        },
        "town-78005": {
          value: "19754",
          latitude: 48.962222222222,
          longitude: 2.0686111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AchÃ¨res (78005)</span><br />Population : 19754"
          }
        },
        "town-55545": {
          value: "19714",
          latitude: 49.159722222222,
          longitude: 5.3827777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Verdun (55545)</span><br />Population : 19714"
          }
        },
        "town-73011": {
          value: "19713",
          latitude: 45.675833333333,
          longitude: 6.3925,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Albertville (73011)</span><br />Population : 19713"
          }
        },
        "town-63124": {
          value: "19709",
          latitude: 45.741111111111,
          longitude: 3.1963888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cournon-d'Auvergne (63124)</span><br />Population : 19709"
          }
        },
        "town-13071": {
          value: "19706",
          latitude: 43.41,
          longitude: 5.3094444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Pennes-Mirabeau (13071)</span><br />Population : 19706"
          }
        },
        "town-97309": {
          value: "19691",
          latitude: 4.905,
          longitude: -52.276388888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Remire-Montjoly (97309)</span><br />Population : 19691"
          }
        },
        "town-29039": {
          value: "19688",
          latitude: 47.875277777778,
          longitude: -3.9188888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Concarneau (29039)</span><br />Population : 19688"
          }
        },
        "town-79049": {
          value: "19676",
          latitude: 46.84,
          longitude: -0.48861111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bressuire (79049)</span><br />Population : 19676"
          }
        },
        "town-43157": {
          value: "19665",
          latitude: 45.043333333333,
          longitude: 3.885,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Puy-en-Velay (43157)</span><br />Population : 19665"
          }
        },
        "town-45284": {
          value: "19623",
          latitude: 47.911944444444,
          longitude: 1.9711111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Jean-de-Braye (45284)</span><br />Population : 19623"
          }
        },
        "town-76259": {
          value: "19581",
          latitude: 49.7575,
          longitude: 0.37916666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">FÃ©camp (76259)</span><br />Population : 19581"
          }
        },
        "town-67462": {
          value: "19576",
          latitude: 48.259444444444,
          longitude: 7.4541666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">SÃ©lestat (67462)</span><br />Population : 19576"
          }
        },
        "town-97210": {
          value: "19547",
          latitude: 14.615277777778,
          longitude: -60.9025,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le FranÃ§ois (97210)</span><br />Population : 19547"
          }
        },
        "town-97107": {
          value: "19544",
          latitude: 16.0425,
          longitude: -61.564722222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Capesterre-Belle-Eau (97107)</span><br />Population : 19544"
          }
        },
        "town-84054": {
          value: "19525",
          latitude: 43.919444444444,
          longitude: 5.0513888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">L'Isle-sur-la-Sorgue (84054)</span><br />Population : 19525"
          }
        },
        "town-74268": {
          value: "19499",
          latitude: 45.888888888889,
          longitude: 6.0961111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Seynod (74268)</span><br />Population : 19499"
          }
        },
        "town-06157": {
          value: "19489",
          latitude: 43.722777777778,
          longitude: 7.1136111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vence (06157)</span><br />Population : 19489"
          }
        },
        "town-78362": {
          value: "19418",
          latitude: 48.974166666667,
          longitude: 1.7108333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mantes-la-Ville (78362)</span><br />Population : 19418"
          }
        },
        "town-85047": {
          value: "19341",
          latitude: 46.845833333333,
          longitude: -1.8791666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Challans (85047)</span><br />Population : 19341"
          }
        },
        "town-16102": {
          value: "19335",
          latitude: 45.695833333333,
          longitude: -0.32916666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cognac (16102)</span><br />Population : 19335"
          }
        },
        "town-94059": {
          value: "19304",
          latitude: 48.811111111111,
          longitude: 2.5716666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Plessis-TrÃ©vise (94059)</span><br />Population : 19304"
          }
        },
        "town-95424": {
          value: "19296",
          latitude: 48.993888888889,
          longitude: 2.195,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montigny-lÃ¨s-Cormeilles (95424)</span><br />Population : 19296"
          }
        },
        "town-06085": {
          value: "19267",
          latitude: 43.6,
          longitude: 6.9947222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mougins (06085)</span><br />Population : 19267"
          }
        },
        "town-84089": {
          value: "19265",
          latitude: 43.694166666667,
          longitude: 5.5030555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pertuis (84089)</span><br />Population : 19265"
          }
        },
        "town-69091": {
          value: "19258",
          latitude: 45.590555555556,
          longitude: 4.7688888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Givors (69091)</span><br />Population : 19258"
          }
        },
        "town-25462": {
          value: "19227",
          latitude: 46.906111111111,
          longitude: 6.3547222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pontarlier (25462)</span><br />Population : 19227"
          }
        },
        "town-60463": {
          value: "19155",
          latitude: 49.274722222222,
          longitude: 2.4675,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Nogent-sur-Oise (60463)</span><br />Population : 19155"
          }
        },
        "town-26058": {
          value: "19133",
          latitude: 44.9475,
          longitude: 4.8952777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bourg-lÃ¨s-Valence (26058)</span><br />Population : 19133"
          }
        },
        "town-47157": {
          value: "19113",
          latitude: 44.5,
          longitude: 0.16527777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Marmande (47157)</span><br />Population : 19113"
          }
        },
        "town-08409": {
          value: "19099",
          latitude: 49.701944444444,
          longitude: 4.9402777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sedan (08409)</span><br />Population : 19099"
          }
        },
        "town-78383": {
          value: "19014",
          latitude: 48.762777777778,
          longitude: 1.9455555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Maurepas (78383)</span><br />Population : 19014"
          }
        },
        "town-92022": {
          value: "18887",
          latitude: 48.808611111111,
          longitude: 2.1886111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chaville (92022)</span><br />Population : 18887"
          }
        },
        "town-44047": {
          value: "18861",
          latitude: 47.214722222222,
          longitude: -1.7238888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">CouÃ«ron (44047)</span><br />Population : 18861"
          }
        },
        "town-44020": {
          value: "18762",
          latitude: 47.179166666667,
          longitude: -1.6247222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bouguenais (44020)</span><br />Population : 18762"
          }
        },
        "town-30028": {
          value: "18705",
          latitude: 44.1625,
          longitude: 4.62,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bagnols-sur-CÃ¨ze (30028)</span><br />Population : 18705"
          }
        },
        "town-38553": {
          value: "18703",
          latitude: 45.613333333333,
          longitude: 5.1486111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villefontaine (38553)</span><br />Population : 18703"
          }
        },
        "town-63300": {
          value: "18684",
          latitude: 45.893611111111,
          longitude: 3.1125,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Riom (63300)</span><br />Population : 18684"
          }
        },
        "town-17306": {
          value: "18674",
          latitude: 45.627777777778,
          longitude: -1.0255555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Royan (17306)</span><br />Population : 18674"
          }
        },
        "town-77294": {
          value: "18671",
          latitude: 48.983888888889,
          longitude: 2.6163888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mitry-Mory (77294)</span><br />Population : 18671"
          }
        },
        "town-91161": {
          value: "18664",
          latitude: 48.705277777778,
          longitude: 2.3161111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chilly-Mazarin (91161)</span><br />Population : 18664"
          }
        },
        "town-94021": {
          value: "18659",
          latitude: 48.766388888889,
          longitude: 2.3533333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chevilly-Larue (94021)</span><br />Population : 18659"
          }
        },
        "town-97228": {
          value: "18622",
          latitude: 14.781388888889,
          longitude: -60.993611111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Marie (97228)</span><br />Population : 18622"
          }
        },
        "town-56162": {
          value: "18591",
          latitude: 47.735833333333,
          longitude: -3.4311111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ploemeur (56162)</span><br />Population : 18591"
          }
        },
        "town-94077": {
          value: "18568",
          latitude: 48.734444444444,
          longitude: 2.4108333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villeneuve-le-Roi (94077)</span><br />Population : 18568"
          }
        },
        "town-39300": {
          value: "18560",
          latitude: 46.674444444444,
          longitude: 5.5538888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lons-le-Saunier (39300)</span><br />Population : 18560"
          }
        },
        "town-92033": {
          value: "18469",
          latitude: 48.845555555556,
          longitude: 2.1869444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Garches (92033)</span><br />Population : 18469"
          }
        },
        "town-69081": {
          value: "18413",
          latitude: 45.774444444444,
          longitude: 4.7775,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰cully (69081)</span><br />Population : 18413"
          }
        },
        "town-27375": {
          value: "18332",
          latitude: 49.215277777778,
          longitude: 1.1655555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Louviers (27375)</span><br />Population : 18332"
          }
        },
        "town-44026": {
          value: "18275",
          latitude: 47.296944444444,
          longitude: -1.4927777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Carquefou (44026)</span><br />Population : 18275"
          }
        },
        "town-59507": {
          value: "18235",
          latitude: 50.604722222222,
          longitude: 3.0877777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ronchin (59507)</span><br />Population : 18235"
          }
        },
        "town-94019": {
          value: "18227",
          latitude: 48.798333333333,
          longitude: 2.5338888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChenneviÃ¨res-sur-Marne (94019)</span><br />Population : 18227"
          }
        },
        "town-84129": {
          value: "18220",
          latitude: 44.008333333333,
          longitude: 4.8725,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sorgues (84129)</span><br />Population : 18220"
          }
        },
        "town-93061": {
          value: "18171",
          latitude: 48.885,
          longitude: 2.4038888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le PrÃ©-Saint-Gervais (93061)</span><br />Population : 18171"
          }
        },
        "town-38229": {
          value: "18065",
          latitude: 45.208611111111,
          longitude: 5.7794444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Meylan (38229)</span><br />Population : 18065"
          }
        },
        "town-67043": {
          value: "18038",
          latitude: 48.613888888889,
          longitude: 7.7519444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bischheim (67043)</span><br />Population : 18038"
          }
        },
        "town-94042": {
          value: "17990",
          latitude: 48.821388888889,
          longitude: 2.4727777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Joinville-le-Pont (94042)</span><br />Population : 17990"
          }
        },
        "town-78545": {
          value: "17976",
          latitude: 48.800277777778,
          longitude: 2.0625,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Cyr-l'Ã‰cole (78545)</span><br />Population : 17976"
          }
        },
        "town-04070": {
          value: "17969",
          latitude: 44.0925,
          longitude: 6.2355555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Digne-les-Bains (04070)</span><br />Population : 17969"
          }
        },
        "town-50173": {
          value: "17942",
          latitude: 49.648333333333,
          longitude: -1.6547222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰queurdreville-Hainneville (50173)</span><br />Population : 17942"
          }
        },
        "town-74081": {
          value: "17877",
          latitude: 46.060277777778,
          longitude: 6.5786111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cluses (74081)</span><br />Population : 17877"
          }
        },
        "town-44035": {
          value: "17814",
          latitude: 47.298888888889,
          longitude: -1.5527777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Chapelle-sur-Erdre (44035)</span><br />Population : 17814"
          }
        },
        "town-78165": {
          value: "17773",
          latitude: 48.820555555556,
          longitude: 1.9836111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Clayes-sous-Bois (78165)</span><br />Population : 17773"
          }
        },
        "town-41194": {
          value: "17758",
          latitude: 47.358333333333,
          longitude: 1.7427777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Romorantin-Lanthenay (41194)</span><br />Population : 17758"
          }
        },
        "town-41269": {
          value: "17687",
          latitude: 47.792777777778,
          longitude: 1.0655555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">VendÃ´me (41269)</span><br />Population : 17687"
          }
        },
        "town-63075": {
          value: "17683",
          latitude: 45.773611111111,
          longitude: 3.0669444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChamaliÃ¨res (63075)</span><br />Population : 17683"
          }
        },
        "town-95598": {
          value: "17670",
          latitude: 48.987777777778,
          longitude: 2.2997222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Soisy-sous-Montmorency (95598)</span><br />Population : 17670"
          }
        },
        "town-74093": {
          value: "17605",
          latitude: 45.903611111111,
          longitude: 6.1038888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cran-Gevrier (74093)</span><br />Population : 17605"
          }
        },
        "town-59220": {
          value: "17581",
          latitude: 50.598888888889,
          longitude: 3.0736111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Faches-Thumesnil (59220)</span><br />Population : 17581"
          }
        },
        "town-13077": {
          value: "17546",
          latitude: 43.405,
          longitude: 4.9886111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Port-de-Bouc (13077)</span><br />Population : 17546"
          }
        },
        "town-59299": {
          value: "17538",
          latitude: 50.655277777778,
          longitude: 3.1877777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Hem (59299)</span><br />Population : 17538"
          }
        },
        "town-76231": {
          value: "17452",
          latitude: 49.285833333333,
          longitude: 1.0083333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Elbeuf (76231)</span><br />Population : 17452"
          }
        },
        "town-62065": {
          value: "17429",
          latitude: 50.409722222222,
          longitude: 2.8327777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Avion (62065)</span><br />Population : 17429"
          }
        },
        "town-77296": {
          value: "17415",
          latitude: 48.626111111111,
          longitude: 2.5922222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Moissy-Cramayel (77296)</span><br />Population : 17415"
          }
        },
        "town-35360": {
          value: "17393",
          latitude: 48.123333333333,
          longitude: -1.2094444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">VitrÃ© (35360)</span><br />Population : 17393"
          }
        },
        "town-42095": {
          value: "17380",
          latitude: 45.388055555556,
          longitude: 4.2872222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Firminy (42095)</span><br />Population : 17380"
          }
        },
        "town-07010": {
          value: "17275",
          latitude: 45.24,
          longitude: 4.6708333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Annonay (07010)</span><br />Population : 17275"
          }
        },
        "town-62215": {
          value: "17275",
          latitude: 50.493055555556,
          longitude: 2.9580555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Carvin (62215)</span><br />Population : 17275"
          }
        },
        "town-83047": {
          value: "17225",
          latitude: 43.149722222222,
          longitude: 6.0741666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Crau (83047)</span><br />Population : 17225"
          }
        },
        "town-94037": {
          value: "17222",
          latitude: 48.813333333333,
          longitude: 2.3444444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gentilly (94037)</span><br />Population : 17222"
          }
        },
        "town-97207": {
          value: "17209",
          latitude: 14.575833333333,
          longitude: -60.975833333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ducos (97207)</span><br />Population : 17209"
          }
        },
        "town-95218": {
          value: "17145",
          latitude: 49.017222222222,
          longitude: 2.0913888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰ragny (95218)</span><br />Population : 17145"
          }
        },
        "town-97224": {
          value: "17057",
          latitude: 14.670833333333,
          longitude: -61.038055555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Joseph (97224)</span><br />Population : 17057"
          }
        },
        "town-78372": {
          value: "17019",
          latitude: 48.866944444444,
          longitude: 2.0941666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Marly-le-Roi (78372)</span><br />Population : 17019"
          }
        },
        "town-45285": {
          value: "16951",
          latitude: 47.913055555556,
          longitude: 1.8733333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Jean-de-la-Ruelle (45285)</span><br />Population : 16951"
          }
        },
        "town-94004": {
          value: "16945",
          latitude: 48.750277777778,
          longitude: 2.5097222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Boissy-Saint-LÃ©ger (94004)</span><br />Population : 16945"
          }
        },
        "town-70550": {
          value: "16934",
          latitude: 47.622222222222,
          longitude: 6.1552777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vesoul (70550)</span><br />Population : 16934"
          }
        },
        "town-84092": {
          value: "16930",
          latitude: 43.964166666667,
          longitude: 4.86,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Pontet (84092)</span><br />Population : 16930"
          }
        },
        "town-77305": {
          value: "16926",
          latitude: 48.383055555556,
          longitude: 2.9480555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montereau-Fault-Yonne (77305)</span><br />Population : 16926"
          }
        },
        "town-97116": {
          value: "16895",
          latitude: 16.331944444444,
          longitude: -61.456944444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Morne-Ã -l'Eau (97116)</span><br />Population : 16895"
          }
        },
        "town-59526": {
          value: "16894",
          latitude: 50.448055555556,
          longitude: 3.4269444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Amand-les-Eaux (59526)</span><br />Population : 16894"
          }
        },
        "town-94011": {
          value: "16888",
          latitude: 48.774166666667,
          longitude: 2.4875,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bonneuil-sur-Marne (94011)</span><br />Population : 16888"
          }
        },
        "town-35047": {
          value: "16875",
          latitude: 48.024722222222,
          longitude: -1.7458333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bruz (35047)</span><br />Population : 16875"
          }
        },
        "town-60612": {
          value: "16867",
          latitude: 49.207222222222,
          longitude: 2.5866666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Senlis (60612)</span><br />Population : 16867"
          }
        },
        "town-76447": {
          value: "16852",
          latitude: 49.546111111111,
          longitude: 0.18805555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montivilliers (76447)</span><br />Population : 16852"
          }
        },
        "town-55029": {
          value: "16830",
          latitude: 48.771666666667,
          longitude: 5.1672222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bar-le-Duc (55029)</span><br />Population : 16830"
          }
        },
        "town-78481": {
          value: "16821",
          latitude: 48.896666666667,
          longitude: 2.1061111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Pecq (78481)</span><br />Population : 16821"
          }
        },
        "town-33122": {
          value: "16802",
          latitude: 44.744444444444,
          longitude: -0.68222222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cestas (33122)</span><br />Population : 16802"
          }
        },
        "town-95323": {
          value: "16796",
          latitude: 49.010833333333,
          longitude: 2.0386111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Jouy-le-Moutier (95323)</span><br />Population : 16796"
          }
        },
        "town-69199": {
          value: "16787",
          latitude: 45.708611111111,
          longitude: 4.8533333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Fons (69199)</span><br />Population : 16787"
          }
        },
        "town-83023": {
          value: "16757",
          latitude: 43.405833333333,
          longitude: 6.0616666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Brignoles (83023)</span><br />Population : 16757"
          }
        },
        "town-78650": {
          value: "16753",
          latitude: 48.893888888889,
          longitude: 2.1322222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le VÃ©sinet (78650)</span><br />Population : 16753"
          }
        },
        "town-57606": {
          value: "16723",
          latitude: 49.104166666667,
          longitude: 6.7080555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Avold (57606)</span><br />Population : 16723"
          }
        },
        "town-83123": {
          value: "16643",
          latitude: 43.119166666667,
          longitude: 5.8022222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sanary-sur-Mer (83123)</span><br />Population : 16643"
          }
        },
        "town-67267": {
          value: "16639",
          latitude: 48.5575,
          longitude: 7.6830555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lingolsheim (67267)</span><br />Population : 16639"
          }
        },
        "town-44055": {
          value: "16623",
          latitude: 47.285833333333,
          longitude: -2.3922222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Baule-Escoublac (44055)</span><br />Population : 16623"
          }
        },
        "town-77053": {
          value: "16604",
          latitude: 48.6925,
          longitude: 2.6111111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Brie-Comte-Robert (77053)</span><br />Population : 16604"
          }
        },
        "town-97120": {
          value: "16550",
          latitude: 16.241111111111,
          longitude: -61.533055555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pointe-Ã -Pitre (97120)</span><br />Population : 16550"
          }
        },
        "town-29151": {
          value: "16547",
          latitude: 48.5775,
          longitude: -3.8277777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Morlaix (29151)</span><br />Population : 16547"
          }
        },
        "town-95476": {
          value: "16537",
          latitude: 49.059166666667,
          longitude: 2.0625,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Osny (95476)</span><br />Population : 16537"
          }
        },
        "town-78335": {
          value: "16534",
          latitude: 48.993333333333,
          longitude: 1.7358333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Limay (78335)</span><br />Population : 16534"
          }
        },
        "town-34154": {
          value: "16504",
          latitude: 43.616388888889,
          longitude: 4.0075,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mauguio (34154)</span><br />Population : 16504"
          }
        },
        "town-37214": {
          value: "16503",
          latitude: 47.402777777778,
          longitude: 0.67805555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Cyr-sur-Loire (37214)</span><br />Population : 16503"
          }
        },
        "town-57757": {
          value: "16475",
          latitude: 49.358888888889,
          longitude: 6.1886111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Yutz (57757)</span><br />Population : 16475"
          }
        },
        "town-33167": {
          value: "16457",
          latitude: 44.836388888889,
          longitude: -0.52583333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Floirac (33167)</span><br />Population : 16457"
          }
        },
        "town-09225": {
          value: "16450",
          latitude: 43.116388888889,
          longitude: 1.6108333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pamiers (09225)</span><br />Population : 16450"
          }
        },
        "town-95637": {
          value: "16443",
          latitude: 49.034444444444,
          longitude: 2.0319444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">VaurÃ©al (95637)</span><br />Population : 16443"
          }
        },
        "town-31424": {
          value: "16442",
          latitude: 43.565555555556,
          longitude: 1.2963888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Plaisance-du-Touch (31424)</span><br />Population : 16442"
          }
        },
        "town-50602": {
          value: "16377",
          latitude: 49.640833333333,
          longitude: -1.5788888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tourlaville (50602)</span><br />Population : 16377"
          }
        },
        "town-59569": {
          value: "16363",
          latitude: 50.363055555556,
          longitude: 3.1130555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sin-le-Noble (59569)</span><br />Population : 16363"
          }
        },
        "town-38382": {
          value: "16355",
          latitude: 45.231666666667,
          longitude: 5.6830555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Ã‰grÃ¨ve (38382)</span><br />Population : 16355"
          }
        },
        "town-44069": {
          value: "16263",
          latitude: 47.328055555556,
          longitude: -2.4291666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">GuÃ©rande (44069)</span><br />Population : 16263"
          }
        },
        "town-28218": {
          value: "16262",
          latitude: 48.438333333333,
          longitude: 1.465,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">LucÃ© (28218)</span><br />Population : 16262"
          }
        },
        "town-72154": {
          value: "16249",
          latitude: 47.699722222222,
          longitude: -0.076111111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La FlÃ¨che (72154)</span><br />Population : 16249"
          }
        },
        "town-91471": {
          value: "16231",
          latitude: 48.698055555556,
          longitude: 2.1875,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Orsay (91471)</span><br />Population : 16231"
          }
        },
        "town-78686": {
          value: "16224",
          latitude: 48.8,
          longitude: 2.1722222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Viroflay (78686)</span><br />Population : 16224"
          }
        },
        "town-97115": {
          value: "16191",
          latitude: 16.271666666667,
          longitude: -61.632777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lamentin (97115)</span><br />Population : 16191"
          }
        },
        "town-74256": {
          value: "16184",
          latitude: 45.936388888889,
          longitude: 6.6319444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sallanches (74256)</span><br />Population : 16184"
          }
        },
        "town-30032": {
          value: "16183",
          latitude: 43.807222222222,
          longitude: 4.6433333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Beaucaire (30032)</span><br />Population : 16183"
          }
        },
        "town-34129": {
          value: "16166",
          latitude: 43.568888888889,
          longitude: 3.9086111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lattes (34129)</span><br />Population : 16166"
          }
        },
        "town-54528": {
          value: "16080",
          latitude: 48.675,
          longitude: 5.8916666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Toul (54528)</span><br />Population : 16080"
          }
        },
        "town-31157": {
          value: "16042",
          latitude: 43.537777777778,
          longitude: 1.3436111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cugnaux (31157)</span><br />Population : 16042"
          }
        },
        "town-38193": {
          value: "15980",
          latitude: 45.619444444444,
          longitude: 5.2330555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">L'Isle-d'Abeau (38193)</span><br />Population : 15980"
          }
        },
        "town-35051": {
          value: "15975",
          latitude: 48.120833333333,
          longitude: -1.6036111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cesson-SÃ©vignÃ© (35051)</span><br />Population : 15975"
          }
        },
        "town-29103": {
          value: "15903",
          latitude: 48.450833333333,
          longitude: -4.2494444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Landerneau (29103)</span><br />Population : 15903"
          }
        },
        "town-42147": {
          value: "15899",
          latitude: 45.6075,
          longitude: 4.0652777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montbrison (42147)</span><br />Population : 15899"
          }
        },
        "town-19272": {
          value: "15838",
          latitude: 45.265833333333,
          longitude: 1.7722222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tulle (19272)</span><br />Population : 15838"
          }
        },
        "town-61169": {
          value: "15837",
          latitude: 48.748333333333,
          longitude: -0.56944444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Flers (61169)</span><br />Population : 15837"
          }
        },
        "town-57306": {
          value: "15835",
          latitude: 49.329722222222,
          longitude: 6.0619444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Hayange (57306)</span><br />Population : 15835"
          }
        },
        "town-91645": {
          value: "15830",
          latitude: 48.7475,
          longitude: 2.2627777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">VerriÃ¨res-le-Buisson (91645)</span><br />Population : 15830"
          }
        },
        "town-94015": {
          value: "15825",
          latitude: 48.841111111111,
          longitude: 2.5222222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bry-sur-Marne (94015)</span><br />Population : 15825"
          }
        },
        "town-64260": {
          value: "15802",
          latitude: 43.358611111111,
          longitude: -1.7744444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Hendaye (64260)</span><br />Population : 15802"
          }
        },
        "town-62108": {
          value: "15783",
          latitude: 50.408333333333,
          longitude: 1.5927777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Berck (62108)</span><br />Population : 15783"
          }
        },
        "town-77337": {
          value: "15782",
          latitude: 48.854722222222,
          longitude: 2.6288888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Noisiel (77337)</span><br />Population : 15782"
          }
        },
        "town-85109": {
          value: "15727",
          latitude: 46.871111111111,
          longitude: -1.0136111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Herbiers (85109)</span><br />Population : 15727"
          }
        },
        "town-77186": {
          value: "15665",
          latitude: 48.408888888889,
          longitude: 2.7016666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fontainebleau (77186)</span><br />Population : 15665"
          }
        },
        "town-13039": {
          value: "15662",
          latitude: 43.436388888889,
          longitude: 4.9452777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fos-sur-Mer (13039)</span><br />Population : 15662"
          }
        },
        "town-37233": {
          value: "15651",
          latitude: 47.390833333333,
          longitude: 0.72805555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Pierre-des-Corps (37233)</span><br />Population : 15651"
          }
        },
        "town-71014": {
          value: "15630",
          latitude: 46.951111111111,
          longitude: 4.2986111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Autun (71014)</span><br />Population : 15630"
          }
        },
        "town-78124": {
          value: "15614",
          latitude: 48.908055555556,
          longitude: 2.1780555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">CarriÃ¨res-sur-Seine (78124)</span><br />Population : 15614"
          }
        },
        "town-45208": {
          value: "15583",
          latitude: 47.996944444444,
          longitude: 2.7325,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montargis (45208)</span><br />Population : 15583"
          }
        },
        "town-78642": {
          value: "15581",
          latitude: 48.979722222222,
          longitude: 1.9738888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Verneuil-sur-Seine (78642)</span><br />Population : 15581"
          }
        },
        "town-56083": {
          value: "15545",
          latitude: 47.804166666667,
          longitude: -3.2788888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Hennebont (56083)</span><br />Population : 15545"
          }
        },
        "town-29046": {
          value: "15540",
          latitude: 48.092222222222,
          longitude: -4.3302777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Douarnenez (29046)</span><br />Population : 15540"
          }
        },
        "town-33056": {
          value: "15508",
          latitude: 44.910555555556,
          longitude: -0.6375,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Blanquefort (33056)</span><br />Population : 15508"
          }
        },
        "town-45302": {
          value: "15423",
          latitude: 47.951388888889,
          longitude: 1.8802777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saran (45302)</span><br />Population : 15423"
          }
        },
        "town-78418": {
          value: "15412",
          latitude: 48.908611111111,
          longitude: 2.1494444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montesson (78418)</span><br />Population : 15412"
          }
        },
        "town-78123": {
          value: "15389",
          latitude: 48.947777777778,
          longitude: 2.0386111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">CarriÃ¨res-sous-Poissy (78123)</span><br />Population : 15389"
          }
        },
        "town-34057": {
          value: "15326",
          latitude: 43.636111111111,
          longitude: 3.9013888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Castelnau-le-Lez (34057)</span><br />Population : 15326"
          }
        },
        "town-76157": {
          value: "15281",
          latitude: 49.440277777778,
          longitude: 1.0252777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Canteleu (76157)</span><br />Population : 15281"
          }
        },
        "town-06161": {
          value: "15258",
          latitude: 43.658055555556,
          longitude: 7.1213888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villeneuve-Loubet (06161)</span><br />Population : 15258"
          }
        },
        "town-45155": {
          value: "15254",
          latitude: 47.688888888889,
          longitude: 2.6294444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gien (45155)</span><br />Population : 15254"
          }
        },
        "town-62765": {
          value: "15231",
          latitude: 50.748333333333,
          longitude: 2.2608333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Omer (62765)</span><br />Population : 15231"
          }
        },
        "town-42186": {
          value: "15153",
          latitude: 45.529444444444,
          longitude: 4.6169444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rive-de-Gier (42186)</span><br />Population : 15153"
          }
        },
        "town-54304": {
          value: "15139",
          latitude: 48.685555555556,
          longitude: 6.1522222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Laxou (54304)</span><br />Population : 15139"
          }
        },
        "town-65286": {
          value: "15102",
          latitude: 43.095,
          longitude: -0.045277777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lourdes (65286)</span><br />Population : 15102"
          }
        },
        "town-25031": {
          value: "15094",
          latitude: 47.482777777778,
          longitude: 6.8397222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Audincourt (25031)</span><br />Population : 15094"
          }
        },
        "town-33075": {
          value: "15082",
          latitude: 44.882777777778,
          longitude: -0.6125,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bruges (33075)</span><br />Population : 15082"
          }
        },
        "town-61006": {
          value: "15082",
          latitude: 48.744444444444,
          longitude: -0.020277777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Argentan (61006)</span><br />Population : 15082"
          }
        },
        "town-13027": {
          value: "15079",
          latitude: 43.8825,
          longitude: 4.855,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢teaurenard (13027)</span><br />Population : 15079"
          }
        },
        "town-95199": {
          value: "15075",
          latitude: 49.0275,
          longitude: 2.3266666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Domont (95199)</span><br />Population : 15075"
          }
        },
        "town-85092": {
          value: "15043",
          latitude: 46.466944444444,
          longitude: -0.80638888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fontenay-le-Comte (85092)</span><br />Population : 15043"
          }
        },
        "town-02168": {
          value: "15020",
          latitude: 49.046388888889,
          longitude: 3.4030555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢teau-Thierry (02168)</span><br />Population : 15020"
          }
        },
        "town-97125": {
          value: "14998",
          latitude: 16.251388888889,
          longitude: -61.273888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-FranÃ§ois (97125)</span><br />Population : 14998"
          }
        },
        "town-95563": {
          value: "14962",
          latitude: 49.016944444444,
          longitude: 2.2463888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Leu-la-ForÃªt (95563)</span><br />Population : 14962"
          }
        },
        "town-93013": {
          value: "14943",
          latitude: 48.934444444444,
          longitude: 2.4244444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Bourget (93013)</span><br />Population : 14943"
          }
        },
        "town-77131": {
          value: "14920",
          latitude: 48.815555555556,
          longitude: 3.0836111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Coulommiers (77131)</span><br />Population : 14920"
          }
        },
        "town-83116": {
          value: "14907",
          latitude: 43.453333333333,
          longitude: 5.8619444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Maximin-la-Sainte-Baume (83116)</span><br />Population : 14907"
          }
        },
        "town-68154": {
          value: "14903",
          latitude: 47.782222222222,
          longitude: 7.3480555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Illzach (68154)</span><br />Population : 14903"
          }
        },
        "town-85194": {
          value: "14888",
          latitude: 46.496388888889,
          longitude: -1.7847222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Sables-d'Olonne (85194)</span><br />Population : 14888"
          }
        },
        "town-56178": {
          value: "14860",
          latitude: 48.068611111111,
          longitude: -2.9627777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pontivy (56178)</span><br />Population : 14860"
          }
        },
        "town-54431": {
          value: "14832",
          latitude: 48.904444444444,
          longitude: 6.0541666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pont-Ã -Mousson (54431)</span><br />Population : 14832"
          }
        },
        "town-59043": {
          value: "14772",
          latitude: 50.7375,
          longitude: 2.7338888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bailleul (59043)</span><br />Population : 14772"
          }
        },
        "town-91326": {
          value: "14756",
          latitude: 48.688333333333,
          longitude: 2.3775,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Juvisy-sur-Orge (91326)</span><br />Population : 14756"
          }
        },
        "town-54578": {
          value: "14753",
          latitude: 48.673055555556,
          longitude: 6.1547222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villers-lÃ¨s-Nancy (54578)</span><br />Population : 14753"
          }
        },
        "town-62643": {
          value: "14717",
          latitude: 50.703888888889,
          longitude: 1.5938888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Outreau (62643)</span><br />Population : 14717"
          }
        },
        "town-54323": {
          value: "14707",
          latitude: 49.519722222222,
          longitude: 5.7605555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Longwy (54323)</span><br />Population : 14707"
          }
        },
        "town-77258": {
          value: "14697",
          latitude: 48.836111111111,
          longitude: 2.6277777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lognes (77258)</span><br />Population : 14697"
          }
        },
        "town-94069": {
          value: "14647",
          latitude: 48.818333333333,
          longitude: 2.4347222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Maurice (94069)</span><br />Population : 14647"
          }
        },
        "town-59139": {
          value: "14632",
          latitude: 50.125,
          longitude: 3.4116666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Caudry (59139)</span><br />Population : 14632"
          }
        },
        "town-23096": {
          value: "14577",
          latitude: 46.170555555556,
          longitude: 1.8683333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">GuÃ©ret (23096)</span><br />Population : 14577"
          }
        },
        "town-59286": {
          value: "14569",
          latitude: 50.609166666667,
          longitude: 2.9869444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Haubourdin (59286)</span><br />Population : 14569"
          }
        },
        "town-95539": {
          value: "14487",
          latitude: 48.998611111111,
          longitude: 2.3569444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Brice-sous-ForÃªt (95539)</span><br />Population : 14487"
          }
        },
        "town-63178": {
          value: "14475",
          latitude: 45.544166666667,
          longitude: 3.2488888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Issoire (63178)</span><br />Population : 14475"
          }
        },
        "town-44131": {
          value: "14450",
          latitude: 47.115555555556,
          longitude: -2.1033333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pornic (44131)</span><br />Population : 14450"
          }
        },
        "town-42279": {
          value: "14425",
          latitude: 45.499444444444,
          longitude: 4.24,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Just-Saint-Rambert (42279)</span><br />Population : 14425"
          }
        },
        "town-95427": {
          value: "14423",
          latitude: 48.973611111111,
          longitude: 2.3458333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montmagny (95427)</span><br />Population : 14423"
          }
        },
        "town-68376": {
          value: "14403",
          latitude: 47.8075,
          longitude: 7.3369444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Wittenheim (68376)</span><br />Population : 14403"
          }
        },
        "town-22187": {
          value: "14393",
          latitude: 48.534444444444,
          longitude: -2.7708333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">PlÃ©rin (22187)</span><br />Population : 14393"
          }
        },
        "town-37208": {
          value: "14375",
          latitude: 47.366666666667,
          longitude: 0.72666666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Avertin (37208)</span><br />Population : 14375"
          }
        },
        "town-60176": {
          value: "14364",
          latitude: 49.234444444444,
          longitude: 2.8875,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">CrÃ©py-en-Valois (60176)</span><br />Population : 14364"
          }
        },
        "town-59291": {
          value: "14358",
          latitude: 50.248055555556,
          longitude: 3.9244444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Hautmont (59291)</span><br />Population : 14358"
          }
        },
        "town-02738": {
          value: "14320",
          latitude: 49.655833333333,
          longitude: 3.2872222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tergnier (02738)</span><br />Population : 14320"
          }
        },
        "town-01004": {
          value: "14316",
          latitude: 45.958055555556,
          longitude: 5.3577777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AmbÃ©rieu-en-Bugey (01004)</span><br />Population : 14316"
          }
        },
        "town-85166": {
          value: "14316",
          latitude: 46.536111111111,
          longitude: -1.7727777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Olonne-sur-Mer (85166)</span><br />Population : 14316"
          }
        },
        "town-77014": {
          value: "14287",
          latitude: 48.408888888889,
          longitude: 2.725,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Avon (77014)</span><br />Population : 14287"
          }
        },
        "town-53147": {
          value: "14264",
          latitude: 48.303055555556,
          longitude: -0.61361111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mayenne (53147)</span><br />Population : 14264"
          }
        },
        "town-21166": {
          value: "14233",
          latitude: 47.291111111111,
          longitude: 5.0072222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChenÃ´ve (21166)</span><br />Population : 14233"
          }
        },
        "town-93062": {
          value: "14194",
          latitude: 48.899166666667,
          longitude: 2.5230555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Raincy (93062)</span><br />Population : 14194"
          }
        },
        "town-84019": {
          value: "14092",
          latitude: 44.280277777778,
          longitude: 4.7488888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">BollÃ¨ne (84019)</span><br />Population : 14092"
          }
        },
        "town-28088": {
          value: "14035",
          latitude: 48.070833333333,
          longitude: 1.3377777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢teaudun (28088)</span><br />Population : 14035"
          }
        },
        "town-13015": {
          value: "14028",
          latitude: 43.454444444444,
          longitude: 5.4144444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bouc-Bel-Air (13015)</span><br />Population : 14028"
          }
        },
        "town-91182": {
          value: "13968",
          latitude: 48.618055555556,
          longitude: 2.4069444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Courcouronnes (91182)</span><br />Population : 13968"
          }
        },
        "town-97230": {
          value: "13965",
          latitude: 14.738611111111,
          longitude: -60.963055555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La TrinitÃ© (97230)</span><br />Population : 13965"
          }
        },
        "town-60471": {
          value: "13907",
          latitude: 49.581111111111,
          longitude: 2.9988888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Noyon (60471)</span><br />Population : 13907"
          }
        },
        "town-74225": {
          value: "13892",
          latitude: 45.866111111111,
          longitude: 5.9444444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rumilly (74225)</span><br />Population : 13892"
          }
        },
        "town-78073": {
          value: "13880",
          latitude: 48.801388888889,
          longitude: 2.0316666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bois-d'Arcy (78073)</span><br />Population : 13880"
          }
        },
        "town-03095": {
          value: "13873",
          latitude: 46.134444444444,
          longitude: 3.4563888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cusset (03095)</span><br />Population : 13873"
          }
        },
        "town-29075": {
          value: "13845",
          latitude: 48.433611111111,
          longitude: -4.4008333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Guipavas (29075)</span><br />Population : 13845"
          }
        },
        "town-31044": {
          value: "13832",
          latitude: 43.610277777778,
          longitude: 1.4986111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Balma (31044)</span><br />Population : 13832"
          }
        },
        "town-51649": {
          value: "13826",
          latitude: 48.724722222222,
          longitude: 4.5844444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vitry-le-FranÃ§ois (51649)</span><br />Population : 13826"
          }
        },
        "town-85060": {
          value: "13802",
          latitude: 46.504166666667,
          longitude: -1.7372222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢teau-d'Olonne (85060)</span><br />Population : 13802"
          }
        },
        "town-10323": {
          value: "13774",
          latitude: 48.515833333333,
          longitude: 3.7266666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Romilly-sur-Seine (10323)</span><br />Population : 13774"
          }
        },
        "town-57160": {
          value: "13770",
          latitude: 49.205277777778,
          longitude: 6.6958333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Creutzwald (57160)</span><br />Population : 13770"
          }
        },
        "town-30258": {
          value: "13767",
          latitude: 43.677777777778,
          longitude: 4.4311111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Gilles (30258)</span><br />Population : 13767"
          }
        },
        "town-59421": {
          value: "13752",
          latitude: 50.703333333333,
          longitude: 3.1405555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mouvaux (59421)</span><br />Population : 13752"
          }
        },
        "town-50218": {
          value: "13723",
          latitude: 48.838055555556,
          longitude: -1.5869444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Granville (50218)</span><br />Population : 13723"
          }
        },
        "town-91386": {
          value: "13710",
          latitude: 48.565277777778,
          longitude: 2.4361111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mennecy (91386)</span><br />Population : 13710"
          }
        },
        "town-14047": {
          value: "13702",
          latitude: 49.278611111111,
          longitude: -0.70388888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bayeux (14047)</span><br />Population : 13702"
          }
        },
        "town-13014": {
          value: "13696",
          latitude: 43.475555555556,
          longitude: 5.1680555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Berre-l'Ã‰tang (13014)</span><br />Population : 13696"
          }
        },
        "town-27701": {
          value: "13688",
          latitude: 49.274444444444,
          longitude: 1.2102777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Val-de-Reuil (27701)</span><br />Population : 13688"
          }
        },
        "town-06012": {
          value: "13684",
          latitude: 43.741944444444,
          longitude: 7.4236111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Beausoleil (06012)</span><br />Population : 13684"
          }
        },
        "town-97404": {
          value: "13659",
          latitude: -21.266111111111,
          longitude: 55.366944444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">L'Ã‰tang-SalÃ© (97404)</span><br />Population : 13659"
          }
        },
        "town-95019": {
          value: "13656",
          latitude: 48.987222222222,
          longitude: 2.4166666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Arnouville (95019)</span><br />Population : 13656"
          }
        },
        "town-59014": {
          value: "13639",
          latitude: 50.371388888889,
          longitude: 3.5044444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Anzin (59014)</span><br />Population : 13639"
          }
        },
        "town-29189": {
          value: "13587",
          latitude: 48.3725,
          longitude: -4.3705555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Plougastel-Daoulas (29189)</span><br />Population : 13587"
          }
        },
        "town-81099": {
          value: "13558",
          latitude: 43.900555555556,
          longitude: 1.8983333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gaillac (81099)</span><br />Population : 13558"
          }
        },
        "town-03321": {
          value: "13545",
          latitude: 46.565833333333,
          longitude: 3.3544444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Yzeure (03321)</span><br />Population : 13545"
          }
        },
        "town-66037": {
          value: "13528",
          latitude: 42.705555555556,
          longitude: 3.0072222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Canet-en-Roussillon (66037)</span><br />Population : 13528"
          }
        },
        "town-33003": {
          value: "13511",
          latitude: 44.924722222222,
          longitude: -0.48666666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AmbarÃ¨s-et-Lagrave (33003)</span><br />Population : 13511"
          }
        },
        "town-57240": {
          value: "13481",
          latitude: 49.141666666667,
          longitude: 6.7988888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Freyming-Merlebach (57240)</span><br />Population : 13481"
          }
        },
        "town-60395": {
          value: "13473",
          latitude: 49.235833333333,
          longitude: 2.135,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">MÃ©ru (60395)</span><br />Population : 13473"
          }
        },
        "town-36088": {
          value: "13452",
          latitude: 46.948055555556,
          longitude: 1.9933333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Issoudun (36088)</span><br />Population : 13452"
          }
        },
        "town-64483": {
          value: "13448",
          latitude: 43.390277777778,
          longitude: -1.6597222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Jean-de-Luz (64483)</span><br />Population : 13448"
          }
        },
        "town-64129": {
          value: "13439",
          latitude: 43.3025,
          longitude: -0.39722222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">BillÃ¨re (64129)</span><br />Population : 13439"
          }
        },
        "town-10081": {
          value: "13436",
          latitude: 48.311944444444,
          longitude: 4.0444444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Chapelle-Saint-Luc (10081)</span><br />Population : 13436"
          }
        },
        "town-59648": {
          value: "13427",
          latitude: 50.585,
          longitude: 3.0430555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Wattignies (59648)</span><br />Population : 13427"
          }
        },
        "town-13108": {
          value: "13426",
          latitude: 43.805,
          longitude: 4.6594444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tarascon (13108)</span><br />Population : 13426"
          }
        },
        "town-45068": {
          value: "13398",
          latitude: 48.011666666667,
          longitude: 2.7358333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢lette-sur-Loing (45068)</span><br />Population : 13398"
          }
        },
        "town-26235": {
          value: "13337",
          latitude: 44.3775,
          longitude: 4.6961111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pierrelatte (26235)</span><br />Population : 13337"
          }
        },
        "town-97220": {
          value: "13325",
          latitude: 14.486666666667,
          longitude: -60.903333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">RiviÃ¨re-Pilote (97220)</span><br />Population : 13325"
          }
        },
        "town-68278": {
          value: "13251",
          latitude: 47.748611111111,
          longitude: 7.4044444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rixheim (68278)</span><br />Population : 13251"
          }
        },
        "town-82033": {
          value: "13249",
          latitude: 44.04,
          longitude: 1.1069444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Castelsarrasin (82033)</span><br />Population : 13249"
          }
        },
        "town-37003": {
          value: "13242",
          latitude: 47.411388888889,
          longitude: 0.9825,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Amboise (37003)</span><br />Population : 13242"
          }
        },
        "town-83115": {
          value: "13220",
          latitude: 43.308888888889,
          longitude: 6.6377777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Maxime (83115)</span><br />Population : 13220"
          }
        },
        "town-48095": {
          value: "13213",
          latitude: 44.518333333333,
          longitude: 3.5005555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mende (48095)</span><br />Population : 13213"
          }
        },
        "town-57751": {
          value: "13203",
          latitude: 49.151111111111,
          longitude: 6.1513888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Woippy (57751)</span><br />Population : 13203"
          }
        },
        "town-69089": {
          value: "13159",
          latitude: 45.736388888889,
          longitude: 4.7636111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Francheville (69089)</span><br />Population : 13159"
          }
        },
        "town-83107": {
          value: "13125",
          latitude: 43.443333333333,
          longitude: 6.6377777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Roquebrune-sur-Argens (83107)</span><br />Population : 13125"
          }
        },
        "town-68166": {
          value: "13068",
          latitude: 47.791388888889,
          longitude: 7.3380555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Kingersheim (68166)</span><br />Population : 13068"
          }
        },
        "town-59367": {
          value: "13067",
          latitude: 50.671388888889,
          longitude: 3.2144444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lys-lez-Lannoy (59367)</span><br />Population : 13067"
          }
        },
        "town-97221": {
          value: "13040",
          latitude: 14.528888888889,
          longitude: -60.981388888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">RiviÃ¨re-SalÃ©e (97221)</span><br />Population : 13040"
          }
        },
        "town-83090": {
          value: "13037",
          latitude: 43.139444444444,
          longitude: 5.8469444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ollioules (83090)</span><br />Population : 13037"
          }
        },
        "town-42044": {
          value: "13023",
          latitude: 45.396111111111,
          longitude: 4.325,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Chambon-Feugerolles (42044)</span><br />Population : 13023"
          }
        },
        "town-59508": {
          value: "13016",
          latitude: 50.753611111111,
          longitude: 3.1202777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Roncq (59508)</span><br />Population : 13016"
          }
        },
        "town-72264": {
          value: "12989",
          latitude: 47.84,
          longitude: -0.33416666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">SablÃ©-sur-Sarthe (72264)</span><br />Population : 12989"
          }
        },
        "town-49015": {
          value: "12951",
          latitude: 47.506944444444,
          longitude: -0.58888888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AvrillÃ© (49015)</span><br />Population : 12951"
          }
        },
        "town-59249": {
          value: "12941",
          latitude: 50.017222222222,
          longitude: 4.0533333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fourmies (59249)</span><br />Population : 12941"
          }
        },
        "town-77333": {
          value: "12907",
          latitude: 48.268611111111,
          longitude: 2.6936111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Nemours (77333)</span><br />Population : 12907"
          }
        },
        "town-40279": {
          value: "12904",
          latitude: 43.725555555556,
          longitude: -1.0527777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Paul-lÃ¨s-Dax (40279)</span><br />Population : 12904"
          }
        },
        "town-57630": {
          value: "12886",
          latitude: 48.734722222222,
          longitude: 7.0538888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sarrebourg (57630)</span><br />Population : 12886"
          }
        },
        "town-12300": {
          value: "12881",
          latitude: 44.3525,
          longitude: 2.0341666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villefranche-de-Rouergue (12300)</span><br />Population : 12881"
          }
        },
        "town-30351": {
          value: "12872",
          latitude: 43.966388888889,
          longitude: 4.7958333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villeneuve-lÃ¨s-Avignon (30351)</span><br />Population : 12872"
          }
        },
        "town-78242": {
          value: "12865",
          latitude: 48.813611111111,
          longitude: 2.0486111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fontenay-le-Fleury (78242)</span><br />Population : 12865"
          }
        },
        "town-59491": {
          value: "12860",
          latitude: 50.389166666667,
          longitude: 3.4858333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Raismes (59491)</span><br />Population : 12860"
          }
        },
        "town-57206": {
          value: "12829",
          latitude: 49.299166666667,
          longitude: 6.1097222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fameck (57206)</span><br />Population : 12829"
          }
        },
        "town-06152": {
          value: "12803",
          latitude: 43.641388888889,
          longitude: 7.0088888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Valbonne (06152)</span><br />Population : 12803"
          }
        },
        "town-67046": {
          value: "12800",
          latitude: 48.766388888889,
          longitude: 7.8569444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bischwiller (67046)</span><br />Population : 12800"
          }
        },
        "town-06104": {
          value: "12700",
          latitude: 43.757222222222,
          longitude: 7.4741666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Roquebrune-Cap-Martin (06104)</span><br />Population : 12700"
          }
        },
        "town-56007": {
          value: "12695",
          latitude: 47.667777777778,
          longitude: -2.9825,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Auray (56007)</span><br />Population : 12695"
          }
        },
        "town-77379": {
          value: "12684",
          latitude: 48.558888888889,
          longitude: 3.2994444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Provins (77379)</span><br />Population : 12684"
          }
        },
        "town-93079": {
          value: "12662",
          latitude: 48.964444444444,
          longitude: 2.3441666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villetaneuse (93079)</span><br />Population : 12662"
          }
        },
        "town-60414": {
          value: "12661",
          latitude: 49.255555555556,
          longitude: 2.4383333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montataire (60414)</span><br />Population : 12661"
          }
        },
        "town-68271": {
          value: "12661",
          latitude: 47.748333333333,
          longitude: 7.3669444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Riedisheim (68271)</span><br />Population : 12661"
          }
        },
        "town-14762": {
          value: "12638",
          latitude: 48.838611111111,
          longitude: -0.88916666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vire (14762)</span><br />Population : 12638"
          }
        },
        "town-44036": {
          value: "12630",
          latitude: 47.716944444444,
          longitude: -1.3761111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢teaubriant (44036)</span><br />Population : 12630"
          }
        },
        "town-82112": {
          value: "12620",
          latitude: 44.104722222222,
          longitude: 1.0852777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Moissac (82112)</span><br />Population : 12620"
          }
        },
        "town-57660": {
          value: "12609",
          latitude: 49.2,
          longitude: 6.9291666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Stiring-Wendel (57660)</span><br />Population : 12609"
          }
        },
        "town-59574": {
          value: "12602",
          latitude: 50.3575,
          longitude: 3.2802777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Somain (59574)</span><br />Population : 12602"
          }
        },
        "town-77407": {
          value: "12602",
          latitude: 48.532777777778,
          longitude: 2.5447222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Fargeau-Ponthierry (77407)</span><br />Population : 12602"
          }
        },
        "town-49353": {
          value: "12571",
          latitude: 47.446111111111,
          longitude: -0.46638888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">TrÃ©lazÃ© (49353)</span><br />Population : 12571"
          }
        },
        "town-64348": {
          value: "12564",
          latitude: 43.315,
          longitude: -0.41083333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lons (64348)</span><br />Population : 12564"
          }
        },
        "town-22093": {
          value: "12539",
          latitude: 48.468611111111,
          longitude: -2.5177777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lamballe (22093)</span><br />Population : 12539"
          }
        },
        "town-44154": {
          value: "12521",
          latitude: 47.246388888889,
          longitude: -2.1669444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Brevin-les-Pins (44154)</span><br />Population : 12521"
          }
        },
        "town-40046": {
          value: "12492",
          latitude: 44.393055555556,
          longitude: -1.1638888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Biscarrosse (40046)</span><br />Population : 12492"
          }
        },
        "town-59152": {
          value: "12469",
          latitude: 50.761111111111,
          longitude: 3.0077777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Comines (59152)</span><br />Population : 12469"
          }
        },
        "town-62186": {
          value: "12469",
          latitude: 50.441944444444,
          longitude: 2.7244444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bully-les-Mines (62186)</span><br />Population : 12469"
          }
        },
        "town-77479": {
          value: "12459",
          latitude: 48.874166666667,
          longitude: 2.6380555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vaires-sur-Marne (77479)</span><br />Population : 12459"
          }
        },
        "town-62413": {
          value: "12451",
          latitude: 50.445,
          longitude: 2.9058333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Harnes (62413)</span><br />Population : 12451"
          }
        },
        "town-29233": {
          value: "12443",
          latitude: 47.872777777778,
          longitude: -3.5497222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">QuimperlÃ© (29233)</span><br />Population : 12443"
          }
        },
        "town-59560": {
          value: "12429",
          latitude: 50.548333333333,
          longitude: 3.0294444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Seclin (59560)</span><br />Population : 12429"
          }
        },
        "town-02173": {
          value: "12420",
          latitude: 49.615555555556,
          longitude: 3.2191666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chauny (02173)</span><br />Population : 12420"
          }
        },
        "town-59112": {
          value: "12413",
          latitude: 50.398333333333,
          longitude: 3.5394444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bruay-sur-l'Escaut (59112)</span><br />Population : 12413"
          }
        },
        "town-76057": {
          value: "12371",
          latitude: 49.544444444444,
          longitude: 0.95361111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Barentin (76057)</span><br />Population : 12371"
          }
        },
        "town-67437": {
          value: "12354",
          latitude: 48.741388888889,
          longitude: 7.3619444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saverne (67437)</span><br />Population : 12354"
          }
        },
        "town-69277": {
          value: "12340",
          latitude: 45.731388888889,
          longitude: 5.0022222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Genas (69277)</span><br />Population : 12340"
          }
        },
        "town-76758": {
          value: "12328",
          latitude: 49.616944444444,
          longitude: 0.75305555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Yvetot (76758)</span><br />Population : 12328"
          }
        },
        "town-31446": {
          value: "12327",
          latitude: 43.546111111111,
          longitude: 1.4755555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ramonville-Saint-Agne (31446)</span><br />Population : 12327"
          }
        },
        "town-78015": {
          value: "12327",
          latitude: 48.980833333333,
          longitude: 2.0583333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AndrÃ©sy (78015)</span><br />Population : 12327"
          }
        },
        "town-74042": {
          value: "12321",
          latitude: 46.078888888889,
          longitude: 6.4008333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bonneville (74042)</span><br />Population : 12321"
          }
        },
        "town-62617": {
          value: "12317",
          latitude: 50.479722222222,
          longitude: 2.6647222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">NÅ“ux-les-Mines (62617)</span><br />Population : 12317"
          }
        },
        "town-38485": {
          value: "12293",
          latitude: 45.181388888889,
          longitude: 5.6991666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Seyssinet-Pariset (38485)</span><br />Population : 12293"
          }
        },
        "town-91432": {
          value: "12248",
          latitude: 48.706388888889,
          longitude: 2.3347222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Morangis (91432)</span><br />Population : 12248"
          }
        },
        "town-91215": {
          value: "12246",
          latitude: 48.693055555556,
          longitude: 2.5158333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰pinay-sous-SÃ©nart (91215)</span><br />Population : 12246"
          }
        },
        "town-49246": {
          value: "12240",
          latitude: 47.424444444444,
          longitude: -0.52527777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Ponts-de-CÃ© (49246)</span><br />Population : 12240"
          }
        },
        "town-45004": {
          value: "12237",
          latitude: 47.973055555556,
          longitude: 2.7702777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Amilly (45004)</span><br />Population : 12237"
          }
        },
        "town-94074": {
          value: "12228",
          latitude: 48.745,
          longitude: 2.4672222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Valenton (94074)</span><br />Population : 12228"
          }
        },
        "town-11076": {
          value: "12220",
          latitude: 43.318055555556,
          longitude: 1.9538888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Castelnaudary (11076)</span><br />Population : 12220"
          }
        },
        "town-07019": {
          value: "12205",
          latitude: 44.619722222222,
          longitude: 4.3902777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aubenas (07019)</span><br />Population : 12205"
          }
        },
        "town-81105": {
          value: "12200",
          latitude: 43.760833333333,
          longitude: 1.9886111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Graulhet (81105)</span><br />Population : 12200"
          }
        },
        "town-44172": {
          value: "12187",
          latitude: 47.249444444444,
          longitude: -1.4866666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Luce-sur-Loire (44172)</span><br />Population : 12187"
          }
        },
        "town-01033": {
          value: "12161",
          latitude: 46.1075,
          longitude: 5.8258333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bellegarde-sur-Valserine (01033)</span><br />Population : 12161"
          }
        },
        "town-97105": {
          value: "12145",
          latitude: 15.996944444444,
          longitude: -61.732777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Basse-Terre (97105)</span><br />Population : 12145"
          }
        },
        "town-53062": {
          value: "12143",
          latitude: 47.828611111111,
          longitude: -0.7027777777777801,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢teau-Gontier (53062)</span><br />Population : 12143"
          }
        },
        "town-40312": {
          value: "12141",
          latitude: 43.540555555556,
          longitude: -1.4613888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tarnos (40312)</span><br />Population : 12141"
          }
        },
        "town-74243": {
          value: "12125",
          latitude: 46.144166666667,
          longitude: 6.0841666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Julien-en-Genevois (74243)</span><br />Population : 12125"
          }
        },
        "town-78688": {
          value: "12122",
          latitude: 48.758333333333,
          longitude: 2.0508333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Voisins-le-Bretonneux (78688)</span><br />Population : 12122"
          }
        },
        "town-05023": {
          value: "12094",
          latitude: 44.895833333333,
          longitude: 6.635,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">BrianÃ§on (05023)</span><br />Population : 12094"
          }
        },
        "town-31561": {
          value: "12093",
          latitude: 43.656388888889,
          longitude: 1.4844444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">L'Union (31561)</span><br />Population : 12093"
          }
        },
        "town-78029": {
          value: "12092",
          latitude: 48.958333333333,
          longitude: 1.855,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aubergenville (78029)</span><br />Population : 12092"
          }
        },
        "town-84003": {
          value: "12064",
          latitude: 43.876111111111,
          longitude: 5.3963888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Apt (84003)</span><br />Population : 12064"
          }
        },
        "town-62570": {
          value: "12057",
          latitude: 50.402222222222,
          longitude: 2.8658333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">MÃ©ricourt (62570)</span><br />Population : 12057"
          }
        },
        "town-29212": {
          value: "12012",
          latitude: 48.382222222222,
          longitude: -4.6202777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">PlouzanÃ© (29212)</span><br />Population : 12012"
          }
        },
        "town-95313": {
          value: "11979",
          latitude: 49.111111111111,
          longitude: 2.2227777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">L'Isle-Adam (95313)</span><br />Population : 11979"
          }
        },
        "town-83112": {
          value: "11972",
          latitude: 43.183611111111,
          longitude: 5.7086111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Cyr-sur-Mer (83112)</span><br />Population : 11972"
          }
        },
        "town-95210": {
          value: "11959",
          latitude: 48.969722222222,
          longitude: 2.3080555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Enghien-les-Bains (95210)</span><br />Population : 11959"
          }
        },
        "town-59383": {
          value: "11958",
          latitude: 50.348888888889,
          longitude: 3.5441666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Marly (59383)</span><br />Population : 11958"
          }
        },
        "town-60509": {
          value: "11948",
          latitude: 49.301111111111,
          longitude: 2.6036111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pont-Sainte-Maxence (60509)</span><br />Population : 11948"
          }
        },
        "town-76114": {
          value: "11941",
          latitude: 49.572222222222,
          longitude: 0.4725,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bolbec (76114)</span><br />Population : 11941"
          }
        },
        "town-69283": {
          value: "11931",
          latitude: 45.663055555556,
          longitude: 4.9530555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mions (69283)</span><br />Population : 11931"
          }
        },
        "town-13081": {
          value: "11928",
          latitude: 43.487777777778,
          longitude: 5.2322222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rognac (13081)</span><br />Population : 11928"
          }
        },
        "town-74208": {
          value: "11917",
          latitude: 45.923611111111,
          longitude: 6.6863888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Passy (74208)</span><br />Population : 11917"
          }
        },
        "town-74208": {
          value: "11917",
          latitude: 45.923611111111,
          longitude: 6.6863888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Passy (74208)</span><br />Population : 11917"
          }
        },
        "town-28404": {
          value: "11881",
          latitude: 48.720833333333,
          longitude: 1.3605555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vernouillet (28404)</span><br />Population : 11881"
          }
        },
        "town-13007": {
          value: "11870",
          latitude: 43.369444444444,
          longitude: 5.6313888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Auriol (13007)</span><br />Population : 11870"
          }
        },
        "town-59273": {
          value: "11868",
          latitude: 50.986388888889,
          longitude: 2.1275,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gravelines (59273)</span><br />Population : 11868"
          }
        },
        "town-27284": {
          value: "11864",
          latitude: 49.280555555556,
          longitude: 1.7763888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gisors (27284)</span><br />Population : 11864"
          }
        },
        "town-97402": {
          value: "11860",
          latitude: -20.995277777778,
          longitude: 55.676111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bras-Panon (97402)</span><br />Population : 11860"
          }
        },
        "town-87154": {
          value: "11831",
          latitude: 45.887222222222,
          longitude: 0.90111111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Junien (87154)</span><br />Population : 11831"
          }
        },
        "town-83098": {
          value: "11830",
          latitude: 43.105555555556,
          longitude: 6.0233333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Pradet (83098)</span><br />Population : 11830"
          }
        },
        "town-13026": {
          value: "11796",
          latitude: 43.383055555556,
          longitude: 5.1641666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ChÃ¢teauneuf-les-Martigues (13026)</span><br />Population : 11796"
          }
        },
        "town-78624": {
          value: "11777",
          latitude: 48.980833333333,
          longitude: 2.0061111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Triel-sur-Seine (78624)</span><br />Population : 11777"
          }
        },
        "town-68112": {
          value: "11757",
          latitude: 47.9075,
          longitude: 7.2102777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Guebwiller (68112)</span><br />Population : 11757"
          }
        },
        "town-31483": {
          value: "11753",
          latitude: 43.108055555556,
          longitude: 0.7233333333333301,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Gaudens (31483)</span><br />Population : 11753"
          }
        },
        "town-21617": {
          value: "11743",
          latitude: 47.336388888889,
          longitude: 5.0055555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Talant (21617)</span><br />Population : 11743"
          }
        },
        "town-64430": {
          value: "11674",
          latitude: 43.488055555556,
          longitude: -0.77083333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Orthez (64430)</span><br />Population : 11674"
          }
        },
        "town-97405": {
          value: "11671",
          latitude: -21.355833333333,
          longitude: 55.565833333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Petite-ÃŽle (97405)</span><br />Population : 11671"
          }
        },
        "town-63430": {
          value: "11645",
          latitude: 45.856388888889,
          longitude: 3.5475,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Thiers (63430)</span><br />Population : 11645"
          }
        },
        "town-06033": {
          value: "11639",
          latitude: 43.7925,
          longitude: 7.1877777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Carros (06033)</span><br />Population : 11639"
          }
        },
        "town-12176": {
          value: "11639",
          latitude: 44.365555555556,
          longitude: 2.5936111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Onet-le-ChÃ¢teau (12176)</span><br />Population : 11639"
          }
        },
        "town-25580": {
          value: "11633",
          latitude: 47.4625,
          longitude: 6.8322222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Valentigney (25580)</span><br />Population : 11633"
          }
        },
        "town-73179": {
          value: "11620",
          latitude: 45.596666666667,
          longitude: 5.8775,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Motte-Servolex (73179)</span><br />Population : 11620"
          }
        },
        "town-76484": {
          value: "11613",
          latitude: 49.341944444444,
          longitude: 1.0913888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Oissel (76484)</span><br />Population : 11613"
          }
        },
        "town-57221": {
          value: "11580",
          latitude: 49.321388888889,
          longitude: 6.1183333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Florange (57221)</span><br />Population : 11580"
          }
        },
        "town-62525": {
          value: "11576",
          latitude: 50.735555555556,
          longitude: 2.2372222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Longuenesse (62525)</span><br />Population : 11576"
          }
        },
        "town-10333": {
          value: "11553",
          latitude: 48.279722222222,
          longitude: 4.0538888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-AndrÃ©-les-Vergers (10333)</span><br />Population : 11553"
          }
        },
        "town-22215": {
          value: "11537",
          latitude: 48.489444444444,
          longitude: -2.7958333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ploufragan (22215)</span><br />Population : 11537"
          }
        },
        "town-68063": {
          value: "11527",
          latitude: 47.806666666667,
          longitude: 7.1758333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cernay (68063)</span><br />Population : 11527"
          }
        },
        "town-69027": {
          value: "11518",
          latitude: 45.673888888889,
          longitude: 4.7541666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Brignais (69027)</span><br />Population : 11518"
          }
        },
        "town-59527": {
          value: "11505",
          latitude: 50.660277777778,
          longitude: 3.0438888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-AndrÃ©-lez-Lille (59527)</span><br />Population : 11505"
          }
        },
        "town-94060": {
          value: "11494",
          latitude: 48.789444444444,
          longitude: 2.5766666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Queue-en-Brie (94060)</span><br />Population : 11494"
          }
        },
        "town-76410": {
          value: "11486",
          latitude: 49.481944444444,
          longitude: 1.0419444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Maromme (76410)</span><br />Population : 11486"
          }
        },
        "town-39478": {
          value: "11481",
          latitude: 46.387222222222,
          longitude: 5.8633333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Claude (39478)</span><br />Population : 11481"
          }
        },
        "town-62758": {
          value: "11469",
          latitude: 50.725833333333,
          longitude: 1.6322222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Martin-Boulogne (62758)</span><br />Population : 11469"
          }
        },
        "town-64422": {
          value: "11449",
          latitude: 43.194166666667,
          longitude: -0.60666666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Oloron-Sainte-Marie (64422)</span><br />Population : 11449"
          }
        },
        "town-62318": {
          value: "11442",
          latitude: 50.517777777778,
          longitude: 1.6405555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰taples (62318)</span><br />Population : 11442"
          }
        },
        "town-28280": {
          value: "11436",
          latitude: 48.321666666667,
          longitude: 0.82166666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Nogent-le-Rotrou (28280)</span><br />Population : 11436"
          }
        },
        "town-33005": {
          value: "11415",
          latitude: 44.7425,
          longitude: -1.0902777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Andernos-les-Bains (33005)</span><br />Population : 11415"
          }
        },
        "town-67365": {
          value: "11410",
          latitude: 48.541666666667,
          longitude: 7.7094444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ostwald (67365)</span><br />Population : 11410"
          }
        },
        "town-13097": {
          value: "11396",
          latitude: 43.639722222222,
          longitude: 4.8125,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Martin-de-Crau (13097)</span><br />Population : 11396"
          }
        },
        "town-38317": {
          value: "11386",
          latitude: 45.123055555556,
          longitude: 5.6980555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Pont-de-Claix (38317)</span><br />Population : 11386"
          }
        },
        "town-74133": {
          value: "11345",
          latitude: 46.185,
          longitude: 6.2075,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gaillard (74133)</span><br />Population : 11345"
          }
        },
        "town-38474": {
          value: "11317",
          latitude: 45.205,
          longitude: 5.665,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sassenage (38474)</span><br />Population : 11317"
          }
        },
        "town-2A247": {
          value: "11308",
          latitude: 41.590833333333,
          longitude: 9.279722222222199,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Porto-Vecchio (2A247)</span><br />Population : 11308"
          }
        },
        "town-31187": {
          value: "11301",
          latitude: 43.536111111111,
          longitude: 1.2311111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fonsorbes (31187)</span><br />Population : 11301"
          }
        },
        "town-83042": {
          value: "11292",
          latitude: 43.2525,
          longitude: 6.53,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cogolin (83042)</span><br />Population : 11292"
          }
        },
        "town-07102": {
          value: "11291",
          latitude: 44.934444444444,
          longitude: 4.8747222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Guilherand-Granges (07102)</span><br />Population : 11291"
          }
        },
        "town-07324": {
          value: "11287",
          latitude: 45.067222222222,
          longitude: 4.8327777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tournon-sur-RhÃ´ne (07324)</span><br />Population : 11287"
          }
        },
        "town-31113": {
          value: "11285",
          latitude: 43.515555555556,
          longitude: 1.4980555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Castanet-Tolosan (31113)</span><br />Population : 11285"
          }
        },
        "town-67348": {
          value: "11284",
          latitude: 48.462222222222,
          longitude: 7.4819444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Obernai (67348)</span><br />Population : 11284"
          }
        },
        "town-22050": {
          value: "11280",
          latitude: 48.455555555556,
          longitude: -2.0502777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dinan (22050)</span><br />Population : 11280"
          }
        },
        "town-33009": {
          value: "11278",
          latitude: 44.658611111111,
          longitude: -1.1688888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Arcachon (33009)</span><br />Population : 11278"
          }
        },
        "town-13106": {
          value: "11258",
          latitude: 43.398333333333,
          longitude: 5.3658333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">SeptÃ¨mes-les-Vallons (13106)</span><br />Population : 11258"
          }
        },
        "town-31506": {
          value: "11244",
          latitude: 43.551388888889,
          longitude: 1.5341666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Orens-de-Gameville (31506)</span><br />Population : 11244"
          }
        },
        "town-63032": {
          value: "11229",
          latitude: 45.751666666667,
          longitude: 3.0830555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Beaumont (63032)</span><br />Population : 11229"
          }
        },
        "town-30341": {
          value: "11220",
          latitude: 43.693333333333,
          longitude: 4.2761111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vauvert (30341)</span><br />Population : 11220"
          }
        },
        "town-83130": {
          value: "11214",
          latitude: 43.19,
          longitude: 6.0411111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">SolliÃ¨s-Pont (83130)</span><br />Population : 11214"
          }
        },
        "town-18197": {
          value: "11204",
          latitude: 46.722777777778,
          longitude: 2.505,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Amand-Montrond (18197)</span><br />Population : 11204"
          }
        },
        "town-72003": {
          value: "11202",
          latitude: 47.968611111111,
          longitude: 0.16055555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Allonnes (72003)</span><br />Population : 11202"
          }
        },
        "town-14341": {
          value: "11192",
          latitude: 49.138333333333,
          longitude: -0.35305555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ifs (14341)</span><br />Population : 11192"
          }
        },
        "town-84080": {
          value: "11191",
          latitude: 44.035555555556,
          longitude: 4.9972222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Monteux (84080)</span><br />Population : 11191"
          }
        },
        "town-77118": {
          value: "11190",
          latitude: 48.945,
          longitude: 2.6866666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Claye-Souilly (77118)</span><br />Population : 11190"
          }
        },
        "town-35093": {
          value: "11169",
          latitude: 48.6325,
          longitude: -2.0616666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dinard (35093)</span><br />Population : 11169"
          }
        },
        "town-59544": {
          value: "11134",
          latitude: 50.369722222222,
          longitude: 3.5547222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Saulve (59544)</span><br />Population : 11134"
          }
        },
        "town-60141": {
          value: "11132",
          latitude: 49.186944444444,
          longitude: 2.4608333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chantilly (60141)</span><br />Population : 11132"
          }
        },
        "town-62048": {
          value: "11116",
          latitude: 50.508333333333,
          longitude: 2.4736111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Auchel (62048)</span><br />Population : 11116"
          }
        },
        "town-77487": {
          value: "11078",
          latitude: 48.526388888889,
          longitude: 2.6822222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vaux-le-PÃ©nil (77487)</span><br />Population : 11078"
          }
        },
        "town-79202": {
          value: "11066",
          latitude: 46.648611111111,
          longitude: -0.24694444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Parthenay (79202)</span><br />Population : 11066"
          }
        },
        "town-29235": {
          value: "11041",
          latitude: 48.408611111111,
          longitude: -4.3969444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Le Relecq-Kerhuon (29235)</span><br />Population : 11041"
          }
        },
        "town-66172": {
          value: "11033",
          latitude: 42.713333333333,
          longitude: 2.8419444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-EstÃ¨ve (66172)</span><br />Population : 11033"
          }
        },
        "town-58086": {
          value: "11031",
          latitude: 47.411388888889,
          longitude: 2.9266666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cosne-Cours-sur-Loire (58086)</span><br />Population : 11031"
          }
        },
        "town-42184": {
          value: "11022",
          latitude: 46.042777777778,
          longitude: 4.0405555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Riorges (42184)</span><br />Population : 11022"
          }
        },
        "town-92077": {
          value: "11013",
          latitude: 48.826111111111,
          longitude: 2.1933333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ville-d'Avray (92077)</span><br />Population : 11013"
          }
        },
        "town-27056": {
          value: "11000",
          latitude: 49.088611111111,
          longitude: 0.5983333333333301,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bernay (27056)</span><br />Population : 11000"
          }
        },
        "town-37050": {
          value: "10986",
          latitude: 47.3375,
          longitude: 0.71388888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chambray-lÃ¨s-Tours (37050)</span><br />Population : 10986"
          }
        },
        "town-13075": {
          value: "10982",
          latitude: 43.346944444444,
          longitude: 5.4630555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Plan-de-Cuques (13075)</span><br />Population : 10982"
          }
        },
        "town-67130": {
          value: "10954",
          latitude: 48.421944444444,
          longitude: 7.6611111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Erstein (67130)</span><br />Population : 10954"
          }
        },
        "town-84141": {
          value: "10905",
          latitude: 43.9775,
          longitude: 4.9030555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">VedÃ¨ne (84141)</span><br />Population : 10905"
          }
        },
        "town-63284": {
          value: "10891",
          latitude: 45.798333333333,
          longitude: 3.2483333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pont-du-ChÃ¢teau (63284)</span><br />Population : 10891"
          }
        },
        "town-91312": {
          value: "10878",
          latitude: 48.742222222222,
          longitude: 2.2261111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Igny (91312)</span><br />Population : 10878"
          }
        },
        "town-37109": {
          value: "10843",
          latitude: 47.404166666667,
          longitude: 0.59888888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Fondettes (37109)</span><br />Population : 10843"
          }
        },
        "town-57433": {
          value: "10842",
          latitude: 49.212222222222,
          longitude: 6.1611111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">MaiziÃ¨res-lÃ¨s-Metz (57433)</span><br />Population : 10842"
          }
        },
        "town-37156": {
          value: "10833",
          latitude: 47.388333333333,
          longitude: 0.82722222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montlouis-sur-Loire (37156)</span><br />Population : 10833"
          }
        },
        "town-13100": {
          value: "10819",
          latitude: 43.789444444444,
          longitude: 4.8316666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-RÃ©my-de-Provence (13100)</span><br />Population : 10819"
          }
        },
        "town-74224": {
          value: "10814",
          latitude: 46.066944444444,
          longitude: 6.3119444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Roche-sur-Foron (74224)</span><br />Population : 10814"
          }
        },
        "town-44132": {
          value: "10796",
          latitude: 47.265833333333,
          longitude: -2.34,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pornichet (44132)</span><br />Population : 10796"
          }
        },
        "town-60157": {
          value: "10762",
          latitude: 49.378888888889,
          longitude: 2.4125,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Clermont (60157)</span><br />Population : 10762"
          }
        },
        "town-19275": {
          value: "10748",
          latitude: 45.548055555556,
          longitude: 2.3091666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ussel (19275)</span><br />Population : 10748"
          }
        },
        "town-56206": {
          value: "10746",
          latitude: 47.686666666667,
          longitude: -2.7344444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-AvÃ© (56206)</span><br />Population : 10746"
          }
        },
        "town-11206": {
          value: "10738",
          latitude: 43.056944444444,
          longitude: 2.2186111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Limoux (11206)</span><br />Population : 10738"
          }
        },
        "town-97212": {
          value: "10737",
          latitude: 14.708055555556,
          longitude: -61.0075,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gros-Morne (97212)</span><br />Population : 10737"
          }
        },
        "town-93030": {
          value: "10735",
          latitude: 48.953611111111,
          longitude: 2.4163888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dugny (93030)</span><br />Population : 10735"
          }
        },
        "town-97401": {
          value: "10730",
          latitude: -21.241944444444,
          longitude: 55.333333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Les Avirons (97401)</span><br />Population : 10730"
          }
        },
        "town-56078": {
          value: "10718",
          latitude: 47.790555555556,
          longitude: -3.4886111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Guidel (56078)</span><br />Population : 10718"
          }
        },
        "town-91021": {
          value: "10712",
          latitude: 48.590277777778,
          longitude: 2.2477777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Arpajon (91021)</span><br />Population : 10712"
          }
        },
        "town-77251": {
          value: "10711",
          latitude: 48.632222222222,
          longitude: 2.5486111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lieusaint (77251)</span><br />Population : 10711"
          }
        },
        "town-85226": {
          value: "10697",
          latitude: 46.721111111111,
          longitude: -1.9455555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Hilaire-de-Riez (85226)</span><br />Population : 10697"
          }
        },
        "town-30202": {
          value: "10696",
          latitude: 44.256388888889,
          longitude: 4.6483333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pont-Saint-Esprit (30202)</span><br />Population : 10696"
          }
        },
        "town-02810": {
          value: "10691",
          latitude: 49.253055555556,
          longitude: 3.0902777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villers-CotterÃªts (02810)</span><br />Population : 10691"
          }
        },
        "town-11203": {
          value: "10690",
          latitude: 43.200555555556,
          longitude: 2.7577777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">LÃ©zignan-CorbiÃ¨res (11203)</span><br />Population : 10690"
          }
        },
        "town-97124": {
          value: "10688",
          latitude: 16.027222222222,
          longitude: -61.698333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Claude (97124)</span><br />Population : 10688"
          }
        },
        "town-89206": {
          value: "10676",
          latitude: 47.982222222222,
          longitude: 3.3972222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Joigny (89206)</span><br />Population : 10676"
          }
        },
        "town-62250": {
          value: "10673",
          latitude: 50.458055555556,
          longitude: 2.9472222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">CourriÃ¨res (62250)</span><br />Population : 10673"
          }
        },
        "town-34157": {
          value: "10668",
          latitude: 43.426666666667,
          longitude: 3.6052777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">MÃ¨ze (34157)</span><br />Population : 10668"
          }
        },
        "town-50147": {
          value: "10660",
          latitude: 49.045277777778,
          longitude: -1.4452777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Coutances (50147)</span><br />Population : 10660"
          }
        },
        "town-84088": {
          value: "10654",
          latitude: 43.997777777778,
          longitude: 5.0591666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pernes-les-Fontaines (84088)</span><br />Population : 10654"
          }
        },
        "town-81140": {
          value: "10649",
          latitude: 43.698888888889,
          longitude: 1.8188888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lavaur (81140)</span><br />Population : 10649"
          }
        },
        "town-35281": {
          value: "10647",
          latitude: 48.090277777778,
          longitude: -1.6955555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Jacques-de-la-Lande (35281)</span><br />Population : 10647"
          }
        },
        "town-70285": {
          value: "10635",
          latitude: 47.5775,
          longitude: 6.7616666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">HÃ©ricourt (70285)</span><br />Population : 10635"
          }
        },
        "town-01173": {
          value: "10634",
          latitude: 46.333333333333,
          longitude: 6.0577777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gex (01173)</span><br />Population : 10634"
          }
        },
        "town-66171": {
          value: "10630",
          latitude: 42.618055555556,
          longitude: 3.0063888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Cyprien (66171)</span><br />Population : 10630"
          }
        },
        "town-87114": {
          value: "10627",
          latitude: 45.838888888889,
          longitude: 1.31,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Panazol (87114)</span><br />Population : 10627"
          }
        },
        "town-67204": {
          value: "10620",
          latitude: 48.624166666667,
          longitude: 7.7547222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">HÅ“nheim (67204)</span><br />Population : 10620"
          }
        },
        "town-28229": {
          value: "10600",
          latitude: 48.453055555556,
          longitude: 1.4619444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mainvilliers (28229)</span><br />Population : 10600"
          }
        },
        "town-95487": {
          value: "10592",
          latitude: 49.153333333333,
          longitude: 2.2711111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Persan (95487)</span><br />Population : 10592"
          }
        },
        "town-59616": {
          value: "10590",
          latitude: 50.459444444444,
          longitude: 3.5683333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vieux-CondÃ© (59616)</span><br />Population : 10590"
          }
        },
        "town-10362": {
          value: "10587",
          latitude: 48.294722222222,
          longitude: 4.0488888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Savine (10362)</span><br />Population : 10587"
          }
        },
        "town-46102": {
          value: "10571",
          latitude: 44.608611111111,
          longitude: 2.0316666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Figeac (46102)</span><br />Population : 10571"
          }
        },
        "town-63164": {
          value: "10524",
          latitude: 45.825833333333,
          longitude: 3.1447222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gerzat (63164)</span><br />Population : 10524"
          }
        },
        "town-69243": {
          value: "10523",
          latitude: 45.896111111111,
          longitude: 4.4330555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tarare (69243)</span><br />Population : 10523"
          }
        },
        "town-42189": {
          value: "10522",
          latitude: 45.433888888889,
          longitude: 4.3236111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Roche-la-MoliÃ¨re (42189)</span><br />Population : 10522"
          }
        },
        "town-64335": {
          value: "10517",
          latitude: 43.3325,
          longitude: -0.43583333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lescar (64335)</span><br />Population : 10517"
          }
        },
        "town-51573": {
          value: "10496",
          latitude: 49.25,
          longitude: 3.9908333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Tinqueux (51573)</span><br />Population : 10496"
          }
        },
        "town-59179": {
          value: "10486",
          latitude: 50.301388888889,
          longitude: 3.3933333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Douchy-les-Mines (59179)</span><br />Population : 10486"
          }
        },
        "town-59008": {
          value: "10469",
          latitude: 50.33,
          longitude: 3.2511111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aniche (59008)</span><br />Population : 10469"
          }
        },
        "town-13110": {
          value: "10463",
          latitude: 43.446944444444,
          longitude: 5.6858333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Trets (13110)</span><br />Population : 10463"
          }
        },
        "town-06149": {
          value: "10453",
          latitude: 43.740833333333,
          longitude: 7.3141666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La TrinitÃ© (06149)</span><br />Population : 10453"
          }
        },
        "town-35024": {
          value: "10447",
          latitude: 48.1825,
          longitude: -1.6438888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Betton (35024)</span><br />Population : 10447"
          }
        },
        "town-68375": {
          value: "10444",
          latitude: 47.805277777778,
          longitude: 7.2375,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Wittelsheim (68375)</span><br />Population : 10444"
          }
        },
        "town-06084": {
          value: "10443",
          latitude: 43.62,
          longitude: 6.9719444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mouans-Sartoux (06084)</span><br />Population : 10443"
          }
        },
        "town-81163": {
          value: "10437",
          latitude: 43.491666666667,
          longitude: 2.3733333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Mazamet (81163)</span><br />Population : 10437"
          }
        },
        "town-35236": {
          value: "10413",
          latitude: 47.651388888889,
          longitude: -2.0847222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Redon (35236)</span><br />Population : 10413"
          }
        },
        "town-31488": {
          value: "10402",
          latitude: 43.665277777778,
          longitude: 1.505,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Jean (31488)</span><br />Population : 10402"
          }
        },
        "town-83049": {
          value: "10389",
          latitude: 43.2375,
          longitude: 6.0708333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Cuers (83049)</span><br />Population : 10389"
          }
        },
        "town-26057": {
          value: "10381",
          latitude: 45.037777777778,
          longitude: 5.05,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bourg-de-PÃ©age (26057)</span><br />Population : 10381"
          }
        },
        "town-78190": {
          value: "10361",
          latitude: 48.877777777778,
          longitude: 2.1422222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Croissy-sur-Seine (78190)</span><br />Population : 10361"
          }
        },
        "town-81060": {
          value: "10361",
          latitude: 44.049166666667,
          longitude: 2.1580555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Carmaux (81060)</span><br />Population : 10361"
          }
        },
        "town-09122": {
          value: "10358",
          latitude: 42.965277777778,
          longitude: 1.6069444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Foix (09122)</span><br />Population : 10358"
          }
        },
        "town-69273": {
          value: "10327",
          latitude: 45.668055555556,
          longitude: 4.9019444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Corbas (69273)</span><br />Population : 10327"
          }
        },
        "town-22136": {
          value: "10324",
          latitude: 48.177777777778,
          longitude: -2.7533333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">LoudÃ©ac (22136)</span><br />Population : 10324"
          }
        },
        "town-83148": {
          value: "10312",
          latitude: 43.427222222222,
          longitude: 6.4319444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Vidauban (83148)</span><br />Population : 10312"
          }
        },
        "town-76216": {
          value: "10286",
          latitude: 49.469722222222,
          longitude: 1.0497222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">DÃ©ville-lÃ¨s-Rouen (76216)</span><br />Population : 10286"
          }
        },
        "town-24520": {
          value: "10279",
          latitude: 44.89,
          longitude: 1.2166666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sarlat-la-CanÃ©da (24520)</span><br />Population : 10279"
          }
        },
        "town-37195": {
          value: "10279",
          latitude: 47.389166666667,
          longitude: 0.66055555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Riche (37195)</span><br />Population : 10279"
          }
        },
        "town-86041": {
          value: "10269",
          latitude: 46.5975,
          longitude: 0.34916666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Buxerolles (86041)</span><br />Population : 10269"
          }
        },
        "town-35210": {
          value: "10240",
          latitude: 48.147777777778,
          longitude: -1.7738888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">PacÃ© (35210)</span><br />Population : 10240"
          }
        },
        "town-54159": {
          value: "10239",
          latitude: 48.625,
          longitude: 6.3497222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dombasle-sur-Meurthe (54159)</span><br />Population : 10239"
          }
        },
        "town-59426": {
          value: "10223",
          latitude: 50.746666666667,
          longitude: 3.1580555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Neuville-en-Ferrain (59426)</span><br />Population : 10223"
          }
        },
        "town-91216": {
          value: "10222",
          latitude: 48.673888888889,
          longitude: 2.3272222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ã‰pinay-sur-Orge (91216)</span><br />Population : 10222"
          }
        },
        "town-16374": {
          value: "10216",
          latitude: 45.640277777778,
          longitude: 0.19777777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Soyaux (16374)</span><br />Population : 10216"
          }
        },
        "town-62516": {
          value: "10189",
          latitude: 50.563611111111,
          longitude: 2.4819444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Lillers (62516)</span><br />Population : 10189"
          }
        },
        "town-21515": {
          value: "10179",
          latitude: 47.314444444444,
          longitude: 5.1061111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Quetigny (21515)</span><br />Population : 10179"
          }
        },
        "town-57019": {
          value: "10167",
          latitude: 49.260833333333,
          longitude: 6.1419444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AmnÃ©ville (57019)</span><br />Population : 10167"
          }
        },
        "town-62014": {
          value: "10164",
          latitude: 50.638611111111,
          longitude: 2.3966666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Aire-sur-la-Lys (62014)</span><br />Population : 10164"
          }
        },
        "town-62040": {
          value: "10163",
          latitude: 50.735555555556,
          longitude: 2.3025,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Arques (62040)</span><br />Population : 10163"
          }
        },
        "town-91200": {
          value: "10151",
          latitude: 48.528888888889,
          longitude: 2.0108333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Dourdan (91200)</span><br />Population : 10151"
          }
        },
        "town-66008": {
          value: "10149",
          latitude: 42.546111111111,
          longitude: 3.0238888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ArgelÃ¨s-sur-Mer (66008)</span><br />Population : 10149"
          }
        },
        "town-38565": {
          value: "10146",
          latitude: 45.297777777778,
          longitude: 5.6369444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Voreppe (38565)</span><br />Population : 10146"
          }
        },
        "town-35055": {
          value: "10145",
          latitude: 48.088611111111,
          longitude: -1.6163888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chantepie (35055)</span><br />Population : 10145"
          }
        },
        "town-21171": {
          value: "10132",
          latitude: 47.301666666667,
          longitude: 5.1355555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chevigny-Saint-Sauveur (21171)</span><br />Population : 10132"
          }
        },
        "town-97227": {
          value: "10131",
          latitude: 14.468333333333,
          longitude: -60.921666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sainte-Luce (97227)</span><br />Population : 10131"
          }
        },
        "town-59090": {
          value: "10130",
          latitude: 50.701666666667,
          longitude: 3.0933333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bondues (59090)</span><br />Population : 10130"
          }
        },
        "town-62587": {
          value: "10113",
          latitude: 50.427777777778,
          longitude: 2.9297222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Montigny-en-Gohelle (62587)</span><br />Population : 10113"
          }
        },
        "town-78674": {
          value: "10106",
          latitude: 48.83,
          longitude: 2.0022222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villepreux (78674)</span><br />Population : 10106"
          }
        },
        "town-85128": {
          value: "10094",
          latitude: 46.454722222222,
          longitude: -1.1658333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">LuÃ§on (85128)</span><br />Population : 10094"
          }
        },
        "town-84138": {
          value: "10077",
          latitude: 44.384166666667,
          longitude: 4.9902777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">ValrÃ©as (84138)</span><br />Population : 10077"
          }
        },
        "town-54482": {
          value: "10070",
          latitude: 48.701111111111,
          longitude: 6.2066666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-Max (54482)</span><br />Population : 10070"
          }
        },
        "town-62771": {
          value: "10063",
          latitude: 50.419722222222,
          longitude: 2.8622222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Sallaumines (62771)</span><br />Population : 10063"
          }
        },
        "town-69152": {
          value: "10061",
          latitude: 45.703611111111,
          longitude: 4.8241666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Pierre-BÃ©nite (69152)</span><br />Population : 10061"
          }
        },
        "town-79329": {
          value: "10061",
          latitude: 46.975,
          longitude: -0.21527777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Thouars (79329)</span><br />Population : 10061"
          }
        },
        "town-83034": {
          value: "10060",
          latitude: 43.095,
          longitude: 6.0736111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Carqueiranne (83034)</span><br />Population : 10060"
          }
        },
        "town-57591": {
          value: "10045",
          latitude: 49.249444444444,
          longitude: 6.0947222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Rombas (57591)</span><br />Population : 10045"
          }
        },
        "town-83071": {
          value: "10017",
          latitude: 43.138055555556,
          longitude: 6.2344444444444,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Londe-les-Maures (83071)</span><br />Population : 10017"
          }
        },
        "town-80016": {
          value: "10008",
          latitude: 50.001944444444,
          longitude: 2.6522222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Albert (80016)</span><br />Population : 10008"
          }
        },
        "town-67067": {
          value: "10002",
          latitude: 48.731944444444,
          longitude: 7.7083333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Brumath (67067)</span><br />Population : 10002"
          }
        },
        "town-94055": {
          value: "9990",
          latitude: 48.785833333333,
          longitude: 2.5383333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Ormesson-sur-Marne (94055)</span><br />Population : 9990"
          }
        },
        "town-57447": {
          value: "9984",
          latitude: 49.061111111111,
          longitude: 6.1497222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Marly (57447)</span><br />Population : 9984"
          }
        },
        "town-44129": {
          value: "9961",
          latitude: 47.436944444444,
          longitude: -2.0877777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">PontchÃ¢teau (44129)</span><br />Population : 9961"
          }
        },
        "town-59324": {
          value: "9935",
          latitude: 50.294444444444,
          longitude: 4.1013888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Jeumont (59324)</span><br />Population : 9935"
          }
        },
        "town-62637": {
          value: "9934",
          latitude: 50.469166666667,
          longitude: 2.9936111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Oignies (62637)</span><br />Population : 9934"
          }
        },
        "town-76319": {
          value: "9908",
          latitude: 49.3575,
          longitude: 1.0072222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Grand-Couronne (76319)</span><br />Population : 9908"
          }
        },
        "town-76165": {
          value: "9907",
          latitude: 49.280833333333,
          longitude: 1.0211111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Caudebec-lÃ¨s-Elbeuf (76165)</span><br />Population : 9907"
          }
        },
        "town-42005": {
          value: "9893",
          latitude: 45.526111111111,
          longitude: 4.2602777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AndrÃ©zieux-BouthÃ©on (42005)</span><br />Population : 9893"
          }
        },
        "town-58303": {
          value: "9891",
          latitude: 47.012222222222,
          longitude: 3.1463888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Varennes-Vauzelles (58303)</span><br />Population : 9891"
          }
        },
        "town-59386": {
          value: "9877",
          latitude: 50.675833333333,
          longitude: 3.0661111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Marquette-lez-Lille (59386)</span><br />Population : 9877"
          }
        },
        "town-59636": {
          value: "9864",
          latitude: 50.685277777778,
          longitude: 3.0486111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Wambrechies (59636)</span><br />Population : 9864"
          }
        },
        "town-45075": {
          value: "9840",
          latitude: 47.889722222222,
          longitude: 1.8397222222222,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">La Chapelle-Saint-Mesmin (45075)</span><br />Population : 9840"
          }
        },
        "town-59153": {
          value: "9829",
          latitude: 50.449166666667,
          longitude: 3.5905555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">CondÃ©-sur-l'Escaut (59153)</span><br />Population : 9829"
          }
        },
        "town-33051": {
          value: "9826",
          latitude: 44.644166666667,
          longitude: -0.9783333333333299,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Biganos (33051)</span><br />Population : 9826"
          }
        },
        "town-91661": {
          value: "9825",
          latitude: 48.701388888889,
          longitude: 2.245,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Villebon-sur-Yvette (91661)</span><br />Population : 9825"
          }
        },
        "town-63014": {
          value: "9824",
          latitude: 45.750833333333,
          longitude: 3.1108333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AubiÃ¨re (63014)</span><br />Population : 9824"
          }
        },
        "town-60282": {
          value: "9819",
          latitude: 49.187777777778,
          longitude: 2.4161111111111,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Gouvieux (60282)</span><br />Population : 9819"
          }
        },
        "town-69271": {
          value: "9813",
          latitude: 45.744444444444,
          longitude: 4.9663888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Chassieu (69271)</span><br />Population : 9813"
          }
        },
        "town-33366": {
          value: "9809",
          latitude: 44.994722222222,
          longitude: -0.44583333333333,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Saint-AndrÃ©-de-Cubzac (33366)</span><br />Population : 9809"
          }
        },
        "town-31451": {
          value: "9795",
          latitude: 43.458611111111,
          longitude: 2.0041666666667,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Revel (31451)</span><br />Population : 9795"
          }
        },
        "town-59011": {
          value: "9775",
          latitude: 50.529444444444,
          longitude: 2.9327777777778,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">AnnÅ“ullin (59011)</span><br />Population : 9775"
          }
        },
        "town-13069": {
          value: "9771",
          latitude: 43.631388888889,
          longitude: 5.1505555555556,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">PÃ©lissanne (13069)</span><br />Population : 9771"
          }
        },
        "town-91122": {
          value: "9769",
          latitude: 48.696666666667,
          longitude: 2.1613888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Bures-sur-Yvette (91122)</span><br />Population : 9769"
          }
        },
        "town-02381": {
          value: "9756",
          latitude: 49.921666666667,
          longitude: 4.0838888888889,
          href: "#",
          tooltip: {
            content: "<span style=\"font-weight:bold;\">Hirson (02381)</span><br />Population : 9756"
          }
        }
      }
    });
  }
});

$(function() {
  'use strict';
  if ($(".mapeal-example-3").length) {
    $(".mapeal-example-3").mapael({
      map: {
        name: "france_departments",
        defaultArea: {
          attrs: {
            fill: "#f4f4e8",
            stroke: "#00a1fe"
          },
          attrsHover: {
            fill: "#a4e100"
          }
        }
      },
      legend: {
        plot: [{
          labelAttrs: {
            fill: "#4a4a4a"
          },
          titleAttrs: {
            fill: "#4a4a4a"
          },
          cssClass: 'population',
          mode: 'horizontal',
          title: "Population",
          marginBottomTitle: 5,
          slices: [{
            size: 25,
            legendSpecificAttrs: {
              fill: '#00a1fe'
            },
            label: "< 10 000",
            max: "10000"
          }, {
            size: 25,
            legendSpecificAttrs: {
              fill: '#00a1fe'
            },
            label: "> 10 000 and < 100 000",
            min: "10000",
            max: "100000"
          }, {
            size: 25,
            legendSpecificAttrs: {
              fill: '#00a1fe'
            },
            label: "> 100 000",
            min: "100000"
          }]
        }]
      },
      plots: {
        'ny': {
          latitude: 40.717079,
          longitude: -74.00116,
          tooltip: {
            content: "New York"
          },
          value: [5000, 20]
        },
        'an': {
          latitude: 61.2108398,
          longitude: -149.9019557,
          tooltip: {
            content: "Anchorage"
          },
          value: [50000, 20]
        },
        'sf': {
          latitude: 37.792032,
          longitude: -122.394613,
          tooltip: {
            content: "San Francisco"
          },
          value: [150000, 20]
        },
        'pa': {
          latitude: 19.493204,
          longitude: -154.8199569,
          tooltip: {
            content: "Pahoa"
          },
          value: [5000, 200]
        },
        'la': {
          latitude: 34.025052,
          longitude: -118.192006,
          tooltip: {
            content: "Los Angeles"
          },
          value: [50000, 200]
        },
        'dallas': {
          latitude: 32.784881,
          longitude: -96.808244,
          tooltip: {
            content: "Dallas"
          },
          value: [150000, 200]
        },
        'miami': {
          latitude: 25.789125,
          longitude: -80.205674,
          tooltip: {
            content: "Miami"
          },
          value: [5000, 2000]
        },
        'washington': {
          latitude: 38.905761,
          longitude: -77.020746,
          tooltip: {
            content: "Washington"
          },
          value: [50000, 2000]
        },
        'seattle': {
          latitude: 47.599571,
          longitude: -122.319426,
          tooltip: {
            content: "Seattle"
          },
          value: [150000, 2000]
        },
        'test1': {
          latitude: 44.671504,
          longitude: -110.957968,
          tooltip: {
            content: "Test 1"
          },
          value: [5000, 2000]
        },
        'test2': {
          latitude: 40.667013,
          longitude: -101.465781,
          tooltip: {
            content: "Test 2"
          },
          value: [50000, 200]
        },
        'test3': {
          latitude: 38.362031,
          longitude: -86.875938,
          tooltip: {
            content: "Test 3"
          },
          value: [150000, 20]
        }
      }
    });
  }
});

var map;
if($('#map').length) {
  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -34.397, lng: 150.644},
      zoom: 8
    });
  };
}
(function($) {
  'use strict';
  $('#vmap').vectorMap({
    map: 'world_mill_en',
    panOnDrag: true,
    focusOn: {
      x: 0.5,
      y: 0.5,
      scale: 1,
      animate: true
    },
    series: {
      regions: [{
        scale: ['#812e2e', '#d87474'],
        normalizeFunction: 'polynomial',
        values: {
          "AF": 16.63,
          "AL": 11.58,
          "DZ": 158.97,
          "AO": 85.81,
          "AG": 1.1,
          "AR": 351.02,
          "AM": 8.83,
          "AU": 1219.72,
          "AT": 366.26,
          "AZ": 52.17,
          "BS": 7.54,
          "BH": 21.73,
          "BD": 105.4,
          "BB": 3.96,
          "BY": 52.89,
          "BE": 461.33,
          "BZ": 1.43,
          "BJ": 6.49,
          "BT": 1.4,
          "BO": 19.18,
          "BA": 16.2,
          "BW": 12.5,
          "BR": 2023.53,
          "BN": 11.96,
          "BG": 44.84,
          "BF": 8.67,
          "BI": 1.47,
          "KH": 11.36,
          "CM": 21.88,
          "CA": 1563.66,
          "CV": 1.57,
          "CF": 2.11,
          "TD": 7.59,
          "CL": 199.18,
          "CN": 5745.13,
          "CO": 283.11,
          "KM": 0.56,
          "CD": 12.6,
          "CG": 11.88,
          "CR": 35.02,
          "CI": 22.38,
          "HR": 59.92,
          "CY": 22.75,
          "CZ": 195.23,
          "DK": 304.56,
          "DJ": 1.14,
          "DM": 0.38,
          "DO": 50.87,
          "EC": 61.49,
          "EG": 216.83,
          "SV": 21.8,
          "GQ": 14.55,
          "ER": 2.25,
          "EE": 19.22,
          "ET": 30.94,
          "FJ": 3.15,
          "FI": 231.98,
          "FR": 2555.44,
          "GA": 12.56,
          "GM": 1.04,
          "GE": 11.23,
          "DE": 3305.9,
          "GH": 18.06,
          "GR": 305.01,
          "GD": 0.65,
          "GT": 40.77,
          "GN": 4.34,
          "GW": 0.83,
          "GY": 2.2,
          "HT": 6.5,
          "HN": 15.34,
          "HK": 226.49,
          "HU": 132.28,
          "IS": 12.77,
          "IN": 1430.02,
          "ID": 695.06,
          "IR": 337.9,
          "IQ": 84.14,
          "IE": 204.14,
          "IL": 201.25,
          "IT": 2036.69,
          "JM": 13.74,
          "JP": 5390.9,
          "JO": 27.13,
          "KZ": 129.76,
          "KE": 32.42,
          "KI": 0.15,
          "KR": 986.26,
          "KW": 117.32,
          "KG": 4.44,
          "LA": 6.34,
          "LV": 23.39,
          "LB": 39.15,
          "LS": 1.8,
          "LR": 0.98,
          "LY": 77.91,
          "LT": 35.73,
          "LU": 52.43,
          "MK": 9.58,
          "MG": 8.33,
          "MW": 5.04,
          "MY": 218.95,
          "MV": 1.43,
          "ML": 9.08,
          "MT": 7.8,
          "MR": 3.49,
          "MU": 9.43,
          "MX": 1004.04,
          "MD": 5.36,
          "MN": 5.81,
          "ME": 3.88,
          "MA": 91.7,
          "MZ": 10.21,
          "MM": 35.65,
          "NA": 11.45,
          "NP": 15.11,
          "NL": 770.31,
          "NZ": 138,
          "NI": 6.38,
          "NE": 5.6,
          "NG": 206.66,
          "NO": 413.51,
          "OM": 53.78,
          "PK": 174.79,
          "PA": 27.2,
          "PG": 8.81,
          "PY": 17.17,
          "PE": 153.55,
          "PH": 189.06,
          "PL": 438.88,
          "PT": 223.7,
          "QA": 126.52,
          "RO": 158.39,
          "RU": 1476.91,
          "RW": 5.69,
          "WS": 0.55,
          "ST": 0.19,
          "SA": 434.44,
          "SN": 12.66,
          "RS": 38.92,
          "SC": 0.92,
          "SL": 1.9,
          "SG": 217.38,
          "SK": 86.26,
          "SI": 46.44,
          "SB": 0.67,
          "ZA": 354.41,
          "ES": 1374.78,
          "LK": 48.24,
          "KN": 0.56,
          "LC": 1,
          "VC": 0.58,
          "SD": 65.93,
          "SR": 3.3,
          "SZ": 3.17,
          "SE": 444.59,
          "CH": 522.44,
          "SY": 59.63,
          "TW": 426.98,
          "TJ": 5.58,
          "TZ": 22.43,
          "TH": 312.61,
          "TL": 0.62,
          "TG": 3.07,
          "TO": 0.3,
          "TT": 21.2,
          "TN": 43.86,
          "TR": 729.05,
          "TM": 0,
          "UG": 17.12,
          "UA": 136.56,
          "AE": 239.65,
          "GB": 2258.57,
          "US": 14624.18,
          "UY": 40.71,
          "UZ": 37.72,
          "VU": 0.72,
          "VE": 285.21,
          "VN": 101.99,
          "YE": 30.02,
          "ZM": 15.69,
          "ZW": 5.57
        }
      }]
    }
  });
})(jQuery);

(function($) {
  'use strict';
  $(function() {
    var body = $('body');
    var contentWrapper = $('.content-wrapper');
    var scroller = $('.container-scroller');
    var footer = $('.footer');
    var sidebar = $('.sidebar');

    //Add active class to nav-link based on url dynamically
    //Active class can be hard coded directly in html file also as required
    var current = location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '');
    $('.nav li a', sidebar).each(function(){
        var $this = $(this);
        if(current === "") {
          //for root url
          if($this.attr('href').indexOf("index.html") !== -1){
              $(this).parents('.nav-item').last().addClass('active');
              if ($(this).parents('.sub-menu').length) {
                $(this).closest('.collapse').addClass('show');
                $(this).addClass('active');
              }
          }
        }
        else {
          //for other url
          if($this.attr('href').indexOf(current) !== -1){
              $(this).parents('.nav-item').last().addClass('active');
              if ($(this).parents('.sub-menu').length) {
                $(this).closest('.collapse').addClass('show');
                $(this).addClass('active');
              }
          }
        }
    })

    //Close other submenu in sidebar on opening any

    sidebar.on('show.bs.collapse','.collapse', function() {
      sidebar.find('.collapse.show').collapse('hide');
    });


    //Change sidebar and content-wrapper height
    applyStyles();
    function applyStyles() {
      //Applying perfect scrollbar
      if(!body.hasClass("rtl")) {
        if($('.settings-panel .tab-content .tab-pane.scroll-wrapper').length) {
          const settingsPanelScroll = new PerfectScrollbar('.settings-panel .tab-content .tab-pane.scroll-wrapper');
        }
        if($('.chats').length) {
          const chatsScroll = new PerfectScrollbar('.chats');
        }
        if(body.hasClass("sidebar-fixed")) {
          var fixedSidebarScroll = new PerfectScrollbar('#sidebar .nav');
        }
      }
    }

    $('[data-toggle="minimize"]').on("click", function () {
      if((body.hasClass('sidebar-toggle-display'))||(body.hasClass('sidebar-absolute'))) {
        body.toggleClass('sidebar-hidden');
      }
      else {
        body.toggleClass('sidebar-icon-only');
      }
    });

    //checkbox and radios
    $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');

    //fullscreen
    $("#fullscreen-button").on("click",function toggleFullScreen() {
      if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
          if (document.documentElement.requestFullScreen) {
              document.documentElement.requestFullScreen();
          } else if (document.documentElement.mozRequestFullScreen) {
              document.documentElement.mozRequestFullScreen();
          } else if (document.documentElement.webkitRequestFullScreen) {
              document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
          } else if (document.documentElement.msRequestFullscreen) {
              document.documentElement.msRequestFullscreen();
          }
      }
      else {
          if (document.cancelFullScreen) {
              document.cancelFullScreen();
          } else if (document.mozCancelFullScreen) {
              document.mozCancelFullScreen();
          } else if (document.webkitCancelFullScreen) {
              document.webkitCancelFullScreen();
          } else if (document.msExitFullscreen) {
              document.msExitFullscreen();
          }
      }
    })
  });
})(jQuery);

(function ($) {
    'use strict';
    $('#exampleModal-4').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var recipient = button.data('whatever') // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      modal.find('.modal-title').text('New message to ' + recipient)
      modal.find('.modal-body input').val(recipient)
    })
})(jQuery);

$(function() {
    'use strict';
    if($('#morris-line-example').length) {
      Morris.Line({
          element: 'morris-line-example',
          lineColors: ['#63CF72', '#F36368', '#76C1FA', '#FABA66'],
          data: [{
                  y: '2006',
                  a: 100,
                  b: 150
              },
              {
                  y: '2007',
                  a: 75,
                  b: 65
              },
              {
                  y: '2008',
                  a: 50,
                  b: 40
              },
              {
                  y: '2009',
                  a: 75,
                  b: 65
              },
              {
                  y: '2010',
                  a: 50,
                  b: 40
              },
              {
                  y: '2011',
                  a: 75,
                  b: 65
              },
              {
                  y: '2012',
                  a: 100,
                  b: 90
              }
          ],
          xkey: 'y',
          ykeys: ['a', 'b'],
          labels: ['Series A', 'Series B']
      });
    }
    if($('#morris-area-example').length){
      Morris.Area({
          element: 'morris-area-example',
          lineColors: ['#76C1FA', '#F36368', '#63CF72', '#FABA66'],
          data: [{
                  y: '2006',
                  a: 100,
                  b: 90
              },
              {
                  y: '2007',
                  a: 75,
                  b: 105
              },
              {
                  y: '2008',
                  a: 50,
                  b: 40
              },
              {
                  y: '2009',
                  a: 75,
                  b: 65
              },
              {
                  y: '2010',
                  a: 50,
                  b: 40
              },
              {
                  y: '2011',
                  a: 75,
                  b: 65
              },
              {
                  y: '2012',
                  a: 100,
                  b: 90
              }
          ],
          xkey: 'y',
          ykeys: ['a', 'b'],
          labels: ['Series A', 'Series B']
      });
    }
    if($("#morris-bar-example").length){
      Morris.Bar({
          element: 'morris-bar-example',
          barColors: ['#63CF72', '#F36368', '#76C1FA', '#FABA66'],
          data: [{
                  y: '2006',
                  a: 100,
                  b: 90
              },
              {
                  y: '2007',
                  a: 75,
                  b: 65
              },
              {
                  y: '2008',
                  a: 50,
                  b: 40
              },
              {
                  y: '2009',
                  a: 75,
                  b: 65
              },
              {
                  y: '2010',
                  a: 50,
                  b: 40
              },
              {
                  y: '2011',
                  a: 75,
                  b: 65
              },
              {
                  y: '2012',
                  a: 100,
                  b: 90
              }
          ],
          xkey: 'y',
          ykeys: ['a', 'b'],
          labels: ['Series A', 'Series B']
      });
    }
    if($("#morris-donut-example").length){
      Morris.Donut({
          element: 'morris-donut-example',
          colors: ['#76C1FA', '#F36368', '#63CF72', '#FABA66'],
          data: [{
                  label: "Download Sales",
                  value: 12
              },
              {
                  label: "In-Store Sales",
                  value: 30
              },
              {
                  label: "Mail-Order Sales",
                  value: 20
              }
          ]
      });
    }
    if($('#morris-dashboard-taget').length){
      Morris.Area({
          element: 'morris-dashboard-taget',
          parseTime : false,
          lineColors: ['#76C1FA', '#F36368', '#63CF72', '#FABA66'],
          data: [{
                  y: 'Jan',
                  Revenue: 190,
                  Target: 170
              },
              {
                  y: 'Feb',
                  Revenue: 60,
                  Target: 90
              },
              {
                  y: 'March',
                  Revenue: 100,
                  Target: 120
              },
              {
                  y: 'Apr',
                  Revenue: 150,
                  Target: 140
              },
              {
                  y: 'May',
                  Revenue: 130,
                  Target: 170
              },
              {
                  y: 'Jun',
                  Revenue: 200,
                  Target: 160
              },
              {
                  y: 'Jul',
                  Revenue: 150,
                  Target: 180
              },
              {
                  y: 'Aug',
                  Revenue: 170,
                  Target: 180
              },
              {
                  y: 'Sep',
                  Revenue: 140,
                  Target: 90
              }
          ],
          xkey: 'y',
          ykeys: ['Target', 'Revenue'],
          labels: ['Monthly Target', 'Monthly Revenue'],
          hideHover: 'auto',
          behaveLikeLine: true,
          resize: true,
          axes: 'x'
      });
    }
});

(function($) {
  'use strict';

  // Horizontal slider
  if ($("#ul-slider-1").length) {
    var startSlider = document.getElementById('ul-slider-1');
    noUiSlider.create(startSlider, {
      start: [72],
      connect: [true, false],
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }
  if ($("#ul-slider-2").length) {
    var startSlider = document.getElementById('ul-slider-2');
    noUiSlider.create(startSlider, {
      start: [92],
      connect: [true, false],
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }
  if ($("#ul-slider-3").length) {
    var startSlider = document.getElementById('ul-slider-3');
    noUiSlider.create(startSlider, {
      start: [43],
      connect: [true, false],
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }
  if ($("#ul-slider-4").length) {
    var startSlider = document.getElementById('ul-slider-4');
    noUiSlider.create(startSlider, {
      start: [20],
      connect: [true, false],
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }
  if ($("#ul-slider-5").length) {
    var startSlider = document.getElementById('ul-slider-5');
    noUiSlider.create(startSlider, {
      start: [75],
      connect: [true, false],
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }

  // Vertical slider
  if ($("#ul-slider-6").length) {
    var startSlider = document.getElementById('ul-slider-6');
    noUiSlider.create(startSlider, {
      start: [72],
      connect: [true, false],
      orientation: "vertical",
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }
  if ($("#ul-slider-7").length) {
    var startSlider = document.getElementById('ul-slider-7');
    noUiSlider.create(startSlider, {
      start: [92],
      connect: [true, false],
      orientation: "vertical",
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }
  if ($("#ul-slider-8").length) {
    var startSlider = document.getElementById('ul-slider-8');
    noUiSlider.create(startSlider, {
      start: [43],
      connect: [true, false],
      orientation: "vertical",
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }
  if ($("#ul-slider-9").length) {
    var startSlider = document.getElementById('ul-slider-9');
    noUiSlider.create(startSlider, {
      start: [20],
      connect: [true, false],
      orientation: "vertical",
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }
  if ($("#ul-slider-10").length) {
    var startSlider = document.getElementById('ul-slider-10');
    noUiSlider.create(startSlider, {
      start: [75],
      connect: [true, false],
      orientation: "vertical",
      range: {
        'min': [0],
        'max': [100]
      }
    });
  }

  // Range Slider
  if ($("#value-range").length) {
    var bigValueSlider = document.getElementById('value-range'),
      bigValueSpan = document.getElementById('huge-value');

    noUiSlider.create(bigValueSlider, {
      start: 1,
      step: 0,
      range: {
        min: 0,
        max: 14
      }
    });

    var range = [
      '0', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'
    ];
    bigValueSlider.noUiSlider.on('update', function(values, handle) {
      console.log(range[Math.floor(values)]);

      bigValueSpan.innerHTML = range[Math.floor(values)];
    });
  }
  if ($("#skipstep").length) {
    var skipSlider = document.getElementById('skipstep');
    noUiSlider.create(skipSlider, {
      range: {
        'min': 0,
        '10%': 10,
        '20%': 20,
        '30%': 30,
        // Nope, 40 is no fun.
        '50%': 50,
        '60%': 60,
        '70%': 70,
        // I never liked 80.
        '90%': 90,
        'max': 100
      },
      snap: true,
      start: [20, 90]
    });
    var skipValues = [
      document.getElementById('skip-value-lower'),
      document.getElementById('skip-value-upper')
    ];

    skipSlider.noUiSlider.on('update', function(values, handle) {
      skipValues[handle].innerHTML = values[handle];
    });
  }

  // Connected Slider
  if ($("#skipstep-connect").length) {
    $(function() {
      var skipSlider = document.getElementById('skipstep-connect');
      noUiSlider.create(skipSlider, {
        connect: true,
        range: {
          'min': 0,
          '10%': 10,
          '20%': 20,
          '30%': 30,
          // Nope, 40 is no fun.
          '50%': 50,
          '60%': 60,
          '70%': 70,
          // I never liked 80.
          '90%': 90,
          'max': 100
        },
        snap: true,
        start: [20, 90]
      });
      var skipValues = [
        document.getElementById('skip-value-lower-2'),
        document.getElementById('skip-value-upper-2')
      ];

      skipSlider.noUiSlider.on('update', function(values, handle) {
        skipValues[handle].innerHTML = values[handle];
      });
    });
  }
  if ($("#skipstep-connect-3").length) {
    $(function() {
      var skipSlider = document.getElementById('skipstep-connect-3');
      noUiSlider.create(skipSlider, {
        connect: true,
        range: {
          'min': 0,
          '10%': 10,
          '20%': 20,
          '30%': 30,
          // Nope, 40 is no fun.
          '50%': 50,
          '60%': 60,
          '70%': 70,
          // I never liked 80.
          '90%': 90,
          'max': 100
        },
        snap: true,
        start: [20, 90]
      });
      var skipValues = [
        document.getElementById('skip-value-lower-3'),
        document.getElementById('skip-value-upper-3')
      ];

      skipSlider.noUiSlider.on('update', function(values, handle) {
        skipValues[handle].innerHTML = values[handle];
      });
    });
  }

  // Tooltip Slider
  if ($("#soft-limit").length) {
    var softSlider = document.getElementById('soft-limit');

    noUiSlider.create(softSlider, {
      start: [24, 50],
      tooltips: true,
      connect: true,
      range: {
        min: 0,
        max: 100
      },
      pips: {
        mode: 'values',
        values: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
        density: 10
      }
    });
  }
  if ($("#soft-limit-2").length) {
    var softSlider = document.getElementById('soft-limit-2');

    noUiSlider.create(softSlider, {
      start: [24, 50],
      tooltips: [true, true],
      connect: true,
      range: {
        min: 0,
        max: 100
      },
      pips: {
        mode: 'values',
        values: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
        density: 10
      }
    });
  }
  if ($("#soft-limit-3").length) {
    var softSlider = document.getElementById('soft-limit-3');

    noUiSlider.create(softSlider, {
      start: [24, 82],
      tooltips: [true, true],
      connect: true,
      range: {
        min: 0,
        max: 100
      },
      pips: {
        mode: 'values',
        values: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
        density: 10
      }
    });
  }
})(jQuery);

(function($) {
  'use strict';
  $(function() {
    $('[data-toggle="offcanvas"]').on("click", function () {
      $('.sidebar-offcanvas').toggleClass('active')
    });
  });
})(jQuery);

(function($) {
  'use strict';
  $.fn.andSelf = function() {
    return this.addBack.apply(this, arguments);
  }

  if($('.example-1').length) {
    $('.example-1').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      autoplay: true,
      autoplayTimeout:4500,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 5
        }
      }
    });
  }

  if($('.full-width').length) {
    $('.full-width').owlCarousel({
      loop: true,
      margin: 10,
      items: 1,
      nav: true,
      autoplay: true,
      autoplayTimeout:5500,
      navText: ["<i class='mdi mdi-chevron-left'></i>","<i class='mdi mdi-chevron-right'></i>"]
    });
  }

  if($('.loop').length) {
    $('.loop').owlCarousel({
      center: true,
      items: 2,
      loop: true,
      margin: 10,
      autoplay: true,
      autoplayTimeout:8500,
      responsive: {
        600: {
          items: 4
        }
      }
    });
  }

  if($('.nonloop').length) {
    $('.nonloop').owlCarousel({
      items: 5,
      loop: false,
      margin: 10,
      autoplay: true,
      autoplayTimeout:6000,
      responsive: {
        600: {
          items: 4
        }
      }
    });
  }

  if($('.auto-width').length) {
    $('.auto-width').owlCarousel({
      items: 2,
      margin: 10,
      loop:true,
      autoplay: true,
      autoplayTimeout:3500,
      autoWidth:true,
    });
  }

  if($('.lazy-load').length) {
    $('.lazy-load').owlCarousel({
      items:4,
      lazyLoad:true,
      loop:true,
      margin:10,
      auto: true,
      autoplay: true,
      autoplayTimeout:2500,
    });
  }

  if($('.rtl-carousel').length) {
    $('.rtl-carousel').owlCarousel({
      rtl:true,
      loop:true,
      margin:10,
      autoplay: true,
      autoplayTimeout:3000,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:5
          }
      }
    });
  }

  if($('.video-carousel').length) {
    $('.video-carousel').owlCarousel({
      loop:false,
      margin:10,
      video:true,
      lazyLoad:true,
      autoplay: true,
      autoplayTimeout:7000,
      responsive:{
          480:{
              items:4
          },
          600:{
              items:4
          }
      }
    });
  }

})(jQuery);

(function($) {
    'use strict';

    if($('#pagination-demo').length) {
      $('#pagination-demo').twbsPagination({
          totalPages: 35,
          visiblePages: 7,
          onPageClick: function(event, page) {
              $('#page-content').text('Page ' + page);
          }
      });
    }

    if($('.sync-pagination').length) {
      $('.sync-pagination').twbsPagination({
          totalPages: 20,
          onPageClick: function(evt, page) {
              $('#content').text('Page ' + page);
          }
      });
    }

})(jQuery);

(function($) {
	'use strict';
	$(function() {
		/* Code for attribute data-custom-class for adding custom class to tooltip */ 
		if (typeof $.fn.popover.Constructor === 'undefined') {
			throw new Error('Bootstrap Popover must be included first!');
		}
		
		var Popover = $.fn.popover.Constructor;
		
		// add customClass option to Bootstrap Tooltip
		$.extend( Popover.Default, {
				customClass: ''
		});
		
		var _show = Popover.prototype.show;
		
		Popover.prototype.show = function () {
			
			// invoke parent method
			_show.apply(this,Array.prototype.slice.apply(arguments));
			
			if ( this.config.customClass ) {
				var tip = this.getTipElement();
				$(tip).addClass(this.config.customClass);
			}
			
		};

		$('[data-toggle="popover"]').popover()
	});
})(jQuery);
  
(function($) {
  'use strict';
  // ProgressBar JS Starts Here

  if ($('#circleProgress1').length) {
    var bar = new ProgressBar.Circle(circleProgress1, {
      color: '#000',
      // This has to be the same size as the maximum width to
      // prevent clipping
      strokeWidth: 4,
      trailWidth: 4,
      easing: 'easeInOut',
      duration: 1400,
      text: {
        autoStyleContainer: false
      },
      from: {
        color: '#aaa',
        width: 4
      },
      to: {
        color: '#677ae4',
        width: 4
      },
      // Set default step function for all animate calls
      step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value);
        }

      }
    });

    bar.text.style.fontSize = '1rem';
    bar.animate(.34); // Number from 0.0 to 1.0
  }

  if ($('#circleProgress2').length) {
    var bar = new ProgressBar.Circle(circleProgress2, {
      color: '#000',
      // This has to be the same size as the maximum width to
      // prevent clipping
      strokeWidth: 4,
      trailWidth: 4,
      easing: 'easeInOut',
      duration: 1400,
      text: {
        autoStyleContainer: false
      },
      from: {
        color: '#aaa',
        width: 4
      },
      to: {
        color: '#46c35f',
        width: 4
      },
      // Set default step function for all animate calls
      step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value);
        }

      }
    });

    bar.text.style.fontSize = '1rem';
    bar.animate(.54); // Number from 0.0 to 1.0
  }

  if ($('#circleProgress3').length) {
    var bar = new ProgressBar.Circle(circleProgress3, {
      color: '#000',
      // This has to be the same size as the maximum width to
      // prevent clipping
      strokeWidth: 4,
      trailWidth: 4,
      easing: 'easeInOut',
      duration: 1400,
      text: {
        autoStyleContainer: false
      },
      from: {
        color: '#aaa',
        width: 4
      },
      to: {
        color: '#f96868',
        width: 4
      },
      // Set default step function for all animate calls
      step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value);
        }

      }
    });

    bar.text.style.fontSize = '1rem';
    bar.animate(.45); // Number from 0.0 to 1.0
  }

  if ($('#circleProgress4').length) {
    var bar = new ProgressBar.Circle(circleProgress4, {
      color: '#000',
      // This has to be the same size as the maximum width to
      // prevent clipping
      strokeWidth: 4,
      trailWidth: 4,
      easing: 'easeInOut',
      duration: 1400,
      text: {
        autoStyleContainer: false
      },
      from: {
        color: '#aaa',
        width: 4
      },
      to: {
        color: '#f2a654',
        width: 4
      },
      // Set default step function for all animate calls
      step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value);
        }

      }
    });

    bar.text.style.fontSize = '1rem';
    bar.animate(.27); // Number from 0.0 to 1.0
  }

  if ($('#circleProgress5').length) {
    var bar = new ProgressBar.Circle(circleProgress5, {
      color: '#000',
      // This has to be the same size as the maximum width to
      // prevent clipping
      strokeWidth: 4,
      trailWidth: 4,
      easing: 'easeInOut',
      duration: 1400,
      text: {
        autoStyleContainer: false
      },
      from: {
        color: '#aaa',
        width: 4
      },
      to: {
        color: '#57c7d4',
        width: 4
      },
      // Set default step function for all animate calls
      step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value);
        }

      }
    });

    bar.text.style.fontSize = '1rem';
    bar.animate(.67); // Number from 0.0 to 1.0
  }

  if ($('#circleProgress6').length) {
    var bar = new ProgressBar.Circle(circleProgress6, {
      color: '#000',
      // This has to be the same size as the maximum width to
      // prevent clipping
      strokeWidth: 4,
      trailWidth: 4,
      easing: 'easeInOut',
      duration: 1400,
      text: {
        autoStyleContainer: false
      },
      from: {
        color: '#aaa',
        width: 4
      },
      to: {
        color: '#2a2e3b',
        width: 4
      },
      // Set default step function for all animate calls
      step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value);
        }

      }
    });

    bar.text.style.fontSize = '1rem';
    bar.animate(.95); // Number from 0.0 to 1.0
  }

})(jQuery);

(function($) {
    'use strict';

    //Simple graph
    if($("#rickshaw-simple").length) {
      var rickshawSimple = new Rickshaw.Graph({
          element: document.getElementById("rickshaw-simple"),
          renderer: 'line',
          series: [{
              data: [{
                  x: 0,
                  y: 40
              }, {
                  x: 1,
                  y: 49
              }, {
                  x: 2,
                  y: 38
              }, {
                  x: 3,
                  y: 30
              }, {
                  x: 4,
                  y: 32
              }],
              color: 'steelblue'
          }, {
              data: [{
                  x: 0,
                  y: 19
              }, {
                  x: 1,
                  y: 22
              }, {
                  x: 2,
                  y: 32
              }, {
                  x: 3,
                  y: 20
              }, {
                  x: 4,
                  y: 21
              }],
              color: 'lightblue'
          }, {
              data: [{
                  x: 0,
                  y: 39
              }, {
                  x: 1,
                  y: 32
              }, {
                  x: 2,
                  y: 12
              }, {
                  x: 3,
                  y: 5
              }, {
                  x: 4,
                  y: 12
              }],
              color: 'steelblue',
              strokeWidth: 10,
              opacity: 0.5
          }]
      });
      rickshawSimple.render();
    }

    //Timescale
    if($("#rickshaw-time-scale").length) {
      var seriesData = [
          [],
          []
      ];
      var random = new Rickshaw.Fixtures.RandomData(1500000);

      for (var i = 0; i < 30; i++) {
          random.addData(seriesData);
      }

      var timeScaleGraph = new Rickshaw.Graph({
          element: document.getElementById("rickshaw-time-scale"),
          width: 400,
          height: 200,
          stroke: true,
          strokeWidth: 0.5,
          renderer: 'area',
          xScale: d3.time.scale(),
          yScale: d3.scale.sqrt(),
          series: [{
                  color: '#2796E9',
                  data: seriesData[0]
              },
              {
                  color: '#05BDFE',
                  data: seriesData[1]
              }
          ]
      });

      timeScaleGraph.render();

      var xAxis = new Rickshaw.Graph.Axis.X({
          graph: timeScaleGraph,
          tickFormat: timeScaleGraph.x.tickFormat()
      });

      xAxis.render();

      var yAxis = new Rickshaw.Graph.Axis.Y({
          graph: timeScaleGraph
      });

      yAxis.render();

      var slider = new Rickshaw.Graph.RangeSlider.Preview({
          graph: timeScaleGraph,
          element: document.getElementById('slider')
      });
    }

    //Bar chart
    if($("#rickshaw-bar").length) {
      var seriesData = [
          [],
          [],
          []
      ];
      var random = new Rickshaw.Fixtures.RandomData(150);

      for (var i = 0; i < 15; i++) {
          random.addData(seriesData);
      }

      var rickshawBar = new Rickshaw.Graph({
          element: document.getElementById("rickshaw-bar"),
          width: 400,
          height: 300,
          renderer: 'bar',
          series: [{
              color: "#2796E9",
              data: seriesData[0],
          }, {
              color: "#05BDFE",
              data: seriesData[1],
          }, {
              color: "#05BDFE",
              data: seriesData[2],
              opacity: 0.4
          }]
      });

      rickshawBar.render();
    }

    //Line chart

    if($("#rickshaw-line").length) {
      // set up our data series with 50 random data points

      var seriesData = [
          [],
          [],
          []
      ];
      var random = new Rickshaw.Fixtures.RandomData(150);

      for (var i = 0; i < 30; i++) {
          random.addData(seriesData);
      }

      // instantiate our graph!

      var rickshawLine = new Rickshaw.Graph({
          element: document.getElementById("rickshaw-line"),
          width: 400,
          height: 300,
          renderer: 'line',
          series: [{
              color: "#2796E9",
              data: seriesData[0],
              name: 'New York',
              strokeWidth: 5,
              opacity: 0.3
          }, {
              color: "#05BDFE",
              data: seriesData[1],
              name: 'London'
          }, {
              color: "#05BDFE",
              data: seriesData[2],
              name: 'Tokyo',
              opacity: 0.4
          }]
      });

      rickshawLine.render();

      var hoverDetail = new Rickshaw.Graph.HoverDetail({
          graph: rickshawLine
      });
    }

    //scatter plot

    if($("#rickshaw-scatter").length) {
      // set up our data series with 50 random data points

      var seriesData = [ [], [], [] ];
      var random = new Rickshaw.Fixtures.RandomData(150);

      for (var i = 0; i < 30; i++) {
      	random.addData(seriesData);
      	seriesData[0][i].r = 0|Math.random() * 2 + 8
      	seriesData[1][i].r = 0|Math.random() * 5 + 5
      	seriesData[2][i].r = 0|Math.random() * 8 + 2
      }

      // instantiate our graph!

      var rickshawScatter = new Rickshaw.Graph( {
      	element: document.getElementById("rickshaw-scatter"),
      	width: 400,
      	height: 300,
      	renderer: 'scatterplot',
      	series: [
      		{
      			color: "#2796E9",
      			data: seriesData[0],
      			opacity: 0.5
      		}, {
      			color: "#f7981c",
      			data: seriesData[1],
      			opacity: 0.3
      		}, {
      			color: "#36af47",
      			data: seriesData[2],
            opacity: 0.6
      		}
      	]
      } );

      rickshawScatter.renderer.dotSize = 6;
      new Rickshaw.Graph.HoverDetail({ graph: rickshawScatter });
      rickshawScatter.render();
    }


    //Multiple renderer

    if($("#rickshaw-multiple").length) {
      var seriesData = [ [], [], [], [], [] ];
      var random = new Rickshaw.Fixtures.RandomData(50);

      for (var i = 0; i < 15; i++) {
      	random.addData(seriesData);
      }

      var rickshawMultiple = new Rickshaw.Graph( {
      	element: document.getElementById("rickshaw-multiple"),
      	renderer: 'multi',
      	width: 400,
      	height: 300,
      	dotSize: 5,
      	series: [
      		{
      			name: 'temperature',
      			data: seriesData.shift(),
      			color: '#2796E9',
      			renderer: 'stack',
            opacity: 0.4,
      		}, {
      			name: 'heat index',
      			data: seriesData.shift(),
      			color: '#f7981c',
      			renderer: 'stack',
            opacity: 0.4,
      		}, {
      			name: 'dewpoint',
      			data: seriesData.shift(),
      			color: '#36af47',
      			renderer: 'scatterplot',
            opacity: 0.4,
      		}, {
      			name: 'pop',
      			data: seriesData.shift().map(function(d) { return { x: d.x, y: d.y / 4 } }),
      			color: '#ed1c24',
            opacity: 0.4,
      			renderer: 'bar'
      		}, {
      			name: 'humidity',
      			data: seriesData.shift().map(function(d) { return { x: d.x, y: d.y * 1.5 } }),
      			renderer: 'line',
      			color: 'rgba(0, 0, 127, 0.25)',
            opacity: 0.4
      		}
      	]
      } );

      var slider = new Rickshaw.Graph.RangeSlider.Preview({
      	graph: rickshawMultiple,
      	element: document.querySelector('#multiple-slider')
      });

      rickshawMultiple.render();
      var detail = new Rickshaw.Graph.HoverDetail({
      	graph: rickshawMultiple
      });
    }

})(jQuery);

(function($) {
  'use strict';

  if($(".js-example-basic-single").length){
    $(".js-example-basic-single").select2();
  }
  if($(".js-example-basic-multiple").length){
    $(".js-example-basic-multiple").select2();
  }
})(jQuery);

(function($) {
  'use strict';
  $(function() {
    $(".nav-settings").click(function(){
      $("#right-sidebar").toggleClass("open");
    });
    $(".settings-close").click(function(){
      $("#right-sidebar,#theme-settings").removeClass("open");
    });

    $("#settings-trigger").on("click" , function(){
      $("#theme-settings").toggleClass("open");
    });


    //background constants
    var navbar_classes = "navbar-danger navbar-success navbar-warning navbar-dark navbar-light navbar-primary navbar-info navbar-pink";
    var sidebar_classes = "sidebar-light sidebar-dark";
    var $body = $("body");

    //sidebar backgrounds
    $("#sidebar-light-theme").on("click" , function(){
      $body.removeClass(sidebar_classes);
      $body.addClass("sidebar-light");
      $(".sidebar-bg-options").removeClass("selected");
      $(this).addClass("selected");
    });
    $("#sidebar-dark-theme").on("click" , function(){
      $body.removeClass(sidebar_classes);
      $body.addClass("sidebar-dark");
      $(".sidebar-bg-options").removeClass("selected");
      $(this).addClass("selected");
    });


    //Navbar Backgrounds
    $(".tiles.primary").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-primary");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.success").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-success");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.warning").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-warning");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.danger").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-danger");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.pink").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-pink");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.info").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-info");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.dark").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-dark");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.default").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
  });
})(jQuery);

(function($) {
  'use strict';
  if($("#sparkline-line-chart").length) {
    $("#sparkline-line-chart").sparkline([5, 6, 7, 9, 9, 5, 3, 2, 2, 4, 6, 7], {
      type: 'line',
      width: '100%',
      height: '100%'
    });
  }

  if($("#sparkline-bar-chart").length) {
    $("#sparkline-bar-chart").sparkline([5, 6, 7, 2, 0, -4, 4], {
      type: 'bar',
      height: '100%',
      barWidth: '58.5%',
      barColor: '#58D8A3',
      negBarColor: '#e56e72',
      zeroColor: 'green'
    });
  }

  if($("#sparkline-pie-chart").length) {
    $("#sparkline-pie-chart").sparkline([1, 1, 2, 4], {
      type: 'pie',
      sliceColors: ['#0CB5F9', '#58d8a3', '#F4767B', '#F9B65F'],
      borderColor: '#',
      width: '100%',
      height: '100%'
    });
  }

  if($("#sparkline-bullet-chart").length) {
    $("#sparkline-bullet-chart").sparkline([10, 12, 12, 9, 7], {
      type: 'bullet',
      height: '238',
      width: '100%',
    });
  }

  if($("#sparkline-composite-chart").length) {
    $("#sparkline-composite-chart").sparkline([5, 6, 7, 2, 0, 3, 6, 8, 1, 2, 2, 0, 3, 6], {
      type: 'line',
      width: '100%',
      height: '100%'
    });
  }

  if($("#sparkline-composite-chart").length) {
    $("#sparkline-composite-chart").sparkline([5, 6, 7, 2, 0, 3, 6, 8, 1, 2, 2, 0, 3, 6], {
      type: 'bar',
      height: '150px',
      width: '100%',
      barWidth: 10,
      barSpacing: 5,
      barColor: '#60a76d',
      negBarColor: '#60a76d',
      composite: true
    });
  }

  if($(".demo-sparkline").length) {
    $(".demo-sparkline").sparkline('html', {
      enableTagOptions: true,
      width: '100%',
      height: '30px',
      fillColor: false
    });
  }

  if($(".top-seelling-dashboard-chart").length) {
    $(".top-seelling-dashboard-chart").sparkline('html', {
      enableTagOptions: true,
      width: '100%',
      barWidth: 30,
      fillColor: false
    });
  }

})(jQuery);

(function($) {
  'use strict';
  $(function() {
    if($('#sortable-table-1').length) {
      $('#sortable-table-1').tablesort();
    }
    if($('#sortable-table-2').length) {
      $('#sortable-table-2').tablesort();
    }
  });
})(jQuery);

(function($) {
    'use strict';
    $(function() {
        if($('.demo-tabs').length) {
          $('.demo-tabs').pwstabs({
              effect: 'none'
          });
        }

        if($('.hello_world').length) {
          $('.hello_world').pwstabs();
        }

        if($('#rtl-tabs-1').length) {
          $('#rtl-tabs-1').pwstabs({
              effect: 'slidedown',
              defaultTab: 2,
              rtl: true
          });
        }

        if($('#vertical-left').length) {
          $('#vertical-left').pwstabs({
              effect: 'slideleft',
              defaultTab: 1,
              containerWidth: '600px',
              tabsPosition: 'vertical',
              verticalPosition: 'left'
          });
        }

        if($('#horizontal-left').length) {
          $('#horizontal-left').pwstabs({
              effect: 'slidedown',
              defaultTab: 2,
              containerWidth: '600px',
              horizontalPosition: 'bottom'
          });
        }

        if($('.tickets-tab').length) {
          $('.tickets-tab').pwstabs({
            effect: 'none'
          });
        }

    });
})(jQuery);

(function($) {
  'use strict';
  if($('.grid').length) {
    var colcade = new Colcade('.grid', {
        columns: '.grid-col',
        items: '.grid-item'
    });
  }
})(jQuery);

(function ($) {
    showSuccessToast = function(){
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Success',
            text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
            showHideTransition: 'slide',
            icon: 'success',
            loaderBg: '#f96868',
            position: 'top-right'
        })
    };
    showInfoToast = function(){
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Info',
            text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
            showHideTransition: 'slide',
            icon: 'info',
            loaderBg: '#46c35f',
            position: 'top-right'
        })
    };
    showWarningToast = function(){
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Warning',
            text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
            showHideTransition: 'slide',
            icon: 'warning',
            loaderBg: '#57c7d4',
            position: 'top-right'
        })
    };
    showDangerToast = function(){
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Danger',
            text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
            showHideTransition: 'slide',
            icon: 'error',
            loaderBg: '#f2a654',
            position: 'top-right'
        })
    };
    showToastPosition = function(position) {
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Positioning',
            text: 'Specify the custom position object or use one of the predefined ones',
            position: String(position),
            icon: 'info',
            stack: false,
            loaderBg: '#f96868'
        })
    }
    showToastInCustomPosition = function() {
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Custom positioning',
            text: 'Specify the custom position object or use one of the predefined ones',
            icon: 'info',
            position: {
                left: 120,
                top: 120
            },
            stack: false,
            loaderBg: '#f96868'
        })
    }
    resetToastPosition = function() {
        $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
        $(".jq-toast-wrap").css({"top": "", "left": "", "bottom":"", "right": ""}); //to remove previous position style
    }
})(jQuery);

(function($) {
  'use strict';
  $(function() {
    var todoListItem = $('.todo-list');
    var todoListInput = $('.todo-list-input');
    $('.todo-list-add-btn').on("click", function (event) {
      event.preventDefault();

      var item = $(this).prevAll('.todo-list-input').val();

      if(item)
      {
        todoListItem.append("<li><div class='form-check'><label class='form-check-label'><input class='checkbox' type='checkbox'/>" + item + "<i class='input-helper'></i></label></div><i class='remove mdi mdi-close-circle-outline'></i></li>");
        todoListInput.val("");
      }

    });

    todoListItem.on('change', '.checkbox', function()
    {
      if($(this).attr('checked'))
      {
        $(this).removeAttr('checked');
      }
      else
      {
        $(this).attr('checked', 'checked');
      }

      $(this).closest( "li" ).toggleClass('completed');

    });

    todoListItem.on('click', '.remove', function()
    {
      $(this).parent().remove();
    });

  });
})(jQuery);

(function($) {
	'use strict';
	
	$(function() {
		/* Code for attribute data-custom-class for adding custom class to tooltip */ 
		if (typeof $.fn.tooltip.Constructor === 'undefined') {
			throw new Error('Bootstrap Tooltip must be included first!');
		}
		
		var Tooltip = $.fn.tooltip.Constructor;
		
		// add customClass option to Bootstrap Tooltip
		$.extend( Tooltip.Default, {
				customClass: ''
		});
		
		var _show = Tooltip.prototype.show;
		
		Tooltip.prototype.show = function () {
			
			// invoke parent method
			_show.apply(this,Array.prototype.slice.apply(arguments));
			
			if ( this.config.customClass ) {
				var tip = this.getTipElement();
				$(tip).addClass(this.config.customClass);
			}
			
		};
		$('[data-toggle="tooltip"]').tooltip();
			
	});
})(jQuery);

(function($) {
    'use strict';
    var tour = new Tour({
        steps: [
          {
            element: "#tourHeadingElement",
            title: "Heading Element",
            content: "This is an example of card heading",
            placement: 'left'
          },
          {
            element: "#tourParagraphElement",
            title: "Paragraph",
            content: "This is an example of paragraph",
            placement: 'left'
          },
          {
            element: "#tourButtonElement",
            title: "Button",
            content: "We provide you with a bunch of buttons in different colors",
            placement: 'right'
          },
          {
            element: "#tourRoundedButtonElement",
            title: "Rounded Button",
            content: "And there are rounded buttons too!",
            placement: 'left'
          },
          {
            element: "#tourTableElement",
            title: "Table",
            content: "This is an example of a bordered table",
            placement: 'left'
          },
          {
            element: "#tourTextStylesElement",
            title: "Text styles",
            content: "Checkout the different text styles provided in our template",
            placement: 'left'
          }
        ],
        template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='p-2'><button class='btn btn-secondary btn-sm mr-1' data-role='prev'>Prev</button><button class='btn btn-secondary btn-sm mr-3' data-role='next'>Next</button><button class='btn btn-danger btn-sm' data-role='end'>End tour</button></div></div>",
        autoscroll: 'true',
        backdrop: 'true',
        onShown: function(tour) {
          var step = tour._options.steps[tour.getCurrentStep()];
          var element=$(step.element);
          $('.container-scroller').animate({ scrollTop: element.offset().top - $('.content-wrapper').offset().top - 70}, 10);
        }
    });
    if (tour.ended()) {
        tour.init();
        tour.restart();
    } else {
        tour.init();
        tour.restart();
    }
})(jQuery);

(function ($) {
    'use strict';
    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            var substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            for(var i=0; i<strs.length;i++) {
              if (substrRegex.test(strs[i])) {
                  matches.push(strs[i]);
              }
            }

            cb(matches);
        };
    };

    var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
        'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
        'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
        'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
        'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
        'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
        'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
        'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
        'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    ];

    $('#the-basics .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'states',
        source: substringMatcher(states)
    });
    // constructs the suggestion engine
    var states = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // `states` is an array of state names defined in "The Basics"
        local: states
    });

    $('#bloodhound .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'states',
        source: states
    });
})(jQuery);

(function ($) {
    'use strict';
    var form = $("#example-form");
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onFinished: function (event, currentIndex) {
            alert("Submitted!");
        }
    });
    var validationForm = $("#example-validation-form");
    validationForm.val({
        errorPlacement: function errorPlacement(error, element) {
            element.before(error);
        },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    validationForm.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex) {
            validationForm.val({
                ignore: [":disabled",":hidden"]
            })
            return validationForm.val();
        },
        onFinishing: function (event, currentIndex) {
            validationForm.val({
                ignore: [':disabled']
            })
            return validationForm.val();
        },
        onFinished: function (event, currentIndex) {
            alert("Submitted!");
        }
    });
    var verticalForm = $("#example-vertical-wizard");
    verticalForm.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical",
        onFinished: function (event, currentIndex) {
            alert("Submitted!");
        }
    });
})(jQuery);

(function($) {
  'use strict';
  $(function() {
    if($('#editable-form').length) {
      $.fn.editable.defaults.mode = 'inline';
      $.fn.editableform.buttons =
          '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
          '<i class="fa fa-fw fa-check"></i>' +
          '</button>' +
          '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
          '<i class="fa fa-fw fa-times"></i>' +
          '</button>';
      $('#username').editable({
          type: 'text',
          pk: 1,
          name: 'username',
          title: 'Enter username'
      });

      $('#firstname').editable({
          validate: function(value) {
              if ($.trim(value) === '') return 'This field is required';
          }
      });

      $('#sex').editable({
          source: [{
                  value: 1,
                  text: 'Male'
              },
              {
                  value: 2,
                  text: 'Female'
              }
          ]
      });

      $('#status').editable();

      $('#group').editable({
          showbuttons: false
      });

      $('#vacation').editable({
          datepicker: {
              todayBtn: 'linked'
          }
      });

      $('#dob').editable();

      $('#event').editable({
          placement: 'right',
          combodate: {
              firstItem: 'name'
          }
      });

      $('#meeting_start').editable({
          format: 'yyyy-mm-dd hh:ii',
          viewformat: 'dd/mm/yyyy hh:ii',
          validate: function(v) {
              if (v && v.getDate() === 10) return 'Day cant be 10!';
          },
          datetimepicker: {
              todayBtn: 'linked',
              weekStart: 1
          }
      });

      $('#comments').editable({
          showbuttons: 'bottom'
      });

      $('#note').editable();
      $('#pencil').on("click", function (e) {
          e.stopPropagation();
          e.preventDefault();
          $('#note').editable('toggle');
      });

      $('#state').editable({
          source: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
      });

      $('#state2').editable({
          value: 'California',
          typeahead: {
              name: 'state',
              local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
          }
      });

      $('#fruits').editable({
          pk: 1,
          limit: 3,
          source: [{
                  value: 1,
                  text: 'banana'
              },
              {
                  value: 2,
                  text: 'peach'
              },
              {
                  value: 3,
                  text: 'apple'
              },
              {
                  value: 4,
                  text: 'watermelon'
              },
              {
                  value: 5,
                  text: 'orange'
              }
          ]
      });

      $('#tags').editable({
          inputclass: 'input-large',
          select2: {
              tags: ['html', 'javascript', 'css', 'ajax'],
              tokenSeparators: [",", " "]
          }
      });

      $('#address').editable({
          url: '/post',
          value: {
              city: "Moscow",
              street: "Lenina",
              building: "12"
          },
          validate: function(value) {
              if (value.city === '') return 'city is required!';
          },
          display: function(value) {
              if (!value) {
                  $(this).empty();
                  return;
              }
              var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $('<div>').text(value.street).html() + ' st., bld. ' + $('<div>').text(value.building).html();
              $(this).html(html);
          }
      });

      $('#user .editable').on('hidden', function(e, reason) {
          if (reason === 'save' || reason === 'nochange') {
              var $next = $(this).closest('tr').next().find('.editable');
              if ($('#autoopen').is(':checked')) {
                  setTimeout(function() {
                      $next.editable('show');
                  }, 300);
              } else {
                  $next.focus();
              }
          }
      });
    }
  });
})(jQuery);



