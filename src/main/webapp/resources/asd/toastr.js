/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    toastr.options = {
        "debug": false,
        "newestOnTop": false,
        "positionClass": "toast-top-right",
        "closeButton": true,
        "toastClass": "animated fadeInDown"
    };
    $.ajax({url: "./UnreadNotification",
        method: "GET",
        success: function (data) {
            if (data.ChildMarriage != 0) {
                toastr.info(data.ChildMarriage+' Child Marriage is not solved yet');
            }
            if (data.Nirvaya != 0) {
                toastr.error(data.Nirvaya+' Nirvaya has newly arrived');
            }

        }});
    $.ajax({url: "./CountNotSolvedGrivence",
        method: "GET",
        success: function (data) {
             $('#side-menu li a .nav-label').each(function() {
                 if($(this).html()=='All Grievance'){
                     $(this).append("&nbsp;&nbsp;<button class=\"btn btn-success btn-circle\" type=\"button\">"+data+"</button>");
                 }
                
            });
        }});
});
