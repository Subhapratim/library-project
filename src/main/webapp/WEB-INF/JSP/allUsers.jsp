<title>All Users</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">All Users Details</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">All Applied Users</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>User Name</th>
                                    <th>Email Id</th>
                                    <th>Details</th>
                                    <th>Status</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>User Name</th>
                                    <th>Email Id</th>
                                    <th>Details</th>
                                    <th>Status</th>

                                </tr>
                            </tfoot>
                            <tbody>

                            </tbody>
                        </table>
                        <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"> User Details</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="masonry-item col-md-12">
                                            <div class="bgc-white p-20 bd">
                                                <fieldset disabled="disabled">
                                                    <input type="hidden" id="id">
                                                    <div class="form-group">
                                                        <label for="disabledTextInput">Username</label>
                                                        <input type="text" class="form-control" id="username1">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="disabledTextInput">Email</label>
                                                        <input type="text" class="form-control" id="email1">
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success" id="approveUser"> Approve </button>
                                        <button type="button" class="btn btn-danger" id="deleteUser"> Delete </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery_Toastr/toastr.min.js"></script>
<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            "ajax": "./getAllUsers",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Scheme', className: 'btn-sm'},
                {extend: 'pdf', title: 'Scheme', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ],
            columns: [

                {data: "id", "visible": false},
                {data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "username"},
                {data: "email"},
                {defaultContent: "<button id='details' class=\"btn btn-primary\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"> Details </button>"},
                {data: "status",
                    render: function (data, type, row) {
                        if (data === 1) {
                            return "<span class=\"badge badge-pill badge-success lh-0 p-15\"> Approved </span>";
                        }if (data === 0) {
                            return "<span class=\"badge badge-pill badge-warning lh-0 p-15\"> Pending </span>";
                        }if (data === -1) {
                            return "<span class=\"badge badge-pill badge-danger lh-0 p-15\"> Not Approved </span>";
                        }
                    }
                }
            ]
        });
        $('#dTable tbody').on('click', 'button#details', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();
            $('#id').val(data.id);
            $('#username1').val(data.username);
            $('#email1').val(data.email);
        });
//        $('#search_btn').click(function () {
//            if ($('#status').val() !== '' && $('#finance_id').val() !== '') {
//                $('#dTable').dataTable().fnDestroy();
//                table = $('#dTable').DataTable({
//                    "ajax": "./ViewUserData?companyStatusId=" + $('#status').val() + "&finance_id=" + $('#finance').val(),
//                    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
//                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
//
//                    columns: [
//
//                        {data: "id", "visible": false},
//                        {data: null,
//                            render: function (data, type, row, meta) {
//                                return (meta.row + meta.settings._iDisplayStart + 1);
//                            }
//                        },
//                        {data: "username"},
//                        {data: "email"},
//                        {defaultContent: "<button id='details' class=\"btn btn-success\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"> Details </button>"}
//                    ]
//                });
//            } else {
//                sweetAlert("Error", "please select date", "error");
//            }
//        });

    });
    $('#approveUser').click(function () {
        var job = {};
        job["id"] = $("#id").val();
        console.log(job);
        $.ajax({
            url: "./userApproval",
            type: 'POST',
            data: JSON.stringify(job),
            dataType: "json",
            contentType: 'application/json',
            complete: function (data) {
                if (data.responseText === "SUCCESS") {
                    swal({
                        title: "Success",
                        text: "User Application is Approved",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeCofirm: true
                    }, function () {
                        window.location.reload();
                    });
                } else {
                    sweetAlert("Sorry", "User Application is not Approved", "error");
                }
            }
        });
    });
    $('#deleteUser').click(function () {
        var job = {};
        job["id"] = $("#id").val();
        console.log(job);
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to not approve this user?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: true,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function () {
            $.ajax({
                url: "./userNonApprove",
                type: 'POST',
                data: JSON.stringify(job),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        swal({
                            title: "Ok",
                            text: " User Application is Rejected",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeCofirm: true
                        }, function () {
                            window.location.reload();
                        });
                    } else {
                        sweetAlert("Sorry", "User Application is not Approved", "error");
                    }
                }
            });
            window.location.reload();
        });
    });


</script>		