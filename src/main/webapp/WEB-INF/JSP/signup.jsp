<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<!DOCTYPE html>
<html>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <link rel="icon" href="${pageContext.request.contextPath}/resources/Images/icon.png" type="image/png" sizes="16x16">
        <title>Sign Up</title>
        <style>
            #loader {
                transition: all .3s ease-in-out;
                opacity: 1;
                visibility: visible;
                position: fixed;
                height: 100vh;
                width: 100%;
                background: #fff;
                z-index: 90000
            }

            #loader.fadeOut {
                opacity: 0;
                visibility: hidden
            }

            .spinner {
                width: 40px;
                height: 40px;
                position: absolute;
                top: calc(50% - 20px);
                left: calc(50% - 20px);
                background-color: #333;
                border-radius: 100%;
                -webkit-animation: sk-scaleout 1s infinite ease-in-out;
                animation: sk-scaleout 1s infinite ease-in-out
            }

            @-webkit-keyframes sk-scaleout {
                0% {
                    -webkit-transform: scale(0)
                }
                100% {
                    -webkit-transform: scale(1);
                    opacity: 0
                }
            }

            @keyframes sk-scaleout {
                0% {
                    -webkit-transform: scale(0);
                    transform: scale(0)
                }
                100% {
                    -webkit-transform: scale(1);
                    transform: scale(1);
                    opacity: 0
                }
            }
        </style>
        <link href="${pageContext.request.contextPath}/resources/CSS/style.css" rel="stylesheet">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/SweetAlert/sweetalert.css">
    </head>
    <body class="app">
        <div id="loader">
            <div class="spinner"></div>
        </div>
        <script type="text/javascript">
            window.addEventListener('load', () => {
                const loader = document.getElementById('loader');
                setTimeout(() => {
                    loader.classList.add('fadeOut');
                }, 300);
            });
        </script>
        <div class="peers ai-s fxw-nw h-100vh">
            <div class="peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv"
                 style="background-image: url(${pageContext.request.contextPath}/resources/Images/Library.JPG)">
                <div class="pos-a centerXY">
                    <%-- <div class="bgc-white bdrs-50p pos-r"
                            style="width: 120px; height: 120px">
                            <img class="pos-a centerXY" src="${pageContext.request.contextPath}/Images/logo.png"
                                    alt="">
                    </div> --%>
                </div>
            </div>
            <div class="col-12 col-md-4 peer pX-40 pY-40 h-100 bgc-white scrollable pos-r" style="min-width: 320px">

                <c:if test="${message != null}">
                    <div class="alert alert-danger alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${message}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Danger!</strong> <b>File Size</b> is more than Specified Size.
                    </div>
                </c:if>

                <div class="col-md-12" style="width: 100%; height: 120px">
                    <img class="pos-a centerXY" src="${pageContext.request.contextPath}/resources/Images/logo_1_1.png" alt="">
                </div>

                <h4 class="fw-300 c-grey-900 mB-40 mT-40">Signup</h4>
                <form action="signup" method="post" data-parsley-validate="" novalidate="" id="needs-validation" name="signupForm"  enctype="multipart/form-data">
                    <input type="hidden" id="status" value="${status}">
                    <div class="form-group">
                        <label for="Username">Username</label>
                        <input id="Username" type="text" name="username" required="" placeholder="Username" class="form-control">
                        <div class="invalid-feedback">Please provide an username.</div>
                    </div>
                    <div class="form-group">
                        <label for="Email">Email</label>
                        <input id="inputEmail" type="email" name="email" required="" placeholder="Email" class="form-control">
                        <div class="invalid-feedback">Please provide a valid Email Id.</div>
                    </div>
                    <div class="form-group">
                        <label for="Password">Password</label>
                        <input id="Password" type="password" name= "password" placeholder="Password" required="" class="form-control">
                        <div class="invalid-feedback">Please provide a Password.</div>
                    </div>
                    <div class="form-group">
                        <label for="image">Upload Image</label>
                        <input id="image" type="file" required="" name="imageUser" placeholder="Upload Image" class="form-control">
                        <div class="invalid-feedback">Please provide the your image.</div>
                    </div>

                    <div class="form-group">
                        <div class="peers ai-c jc-sb fxw-nw">
                            <div class="peer">
                                <!-- <div class="checkbox checkbox-circle checkbox-info peers ai-c"><input type="checkbox"
                                                                                            id="inputCall1"
                                                                                            name="inputCheckboxesCall"
                                                                                            class="peer"><label
                              for="inputCall1" class="peers peer-greed js-sb ai-c"><span class="peer peer-greed">Remember Me</span></label>
                                </div> -->
                                
                                Back to <a href="./signin">Sign in</a>
                            </div>
                            <div class="peer">
                                <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                <button type="reset" class="btn btn-space btn-secondary">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            !function () {
                "use strict";
                window.addEventListener("load", function () {
                    var t = document.getElementById("needs-validation");
                    t.addEventListener("submit", function (e) {
                        !1 === t.checkValidity() && (e.preventDefault(), e.stopPropagation()), t.classList.add("was-validated")
                    }, !1)
                }, !1)
            }()
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                if ($("#status").val() != '') {
                    swal("Oops!", /*"Something went wrong on the page!\n\n"+ */$("#status").val(), "error");
                }
            });
        </script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/vendor.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/bundle.js"></script>
    </body>
</html>
