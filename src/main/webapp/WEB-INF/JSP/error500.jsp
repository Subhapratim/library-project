<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html><html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <title>500</title>
    <link href="${pageContext.request.contextPath}/CSS/style.css" rel="stylesheet">
</head>
<body class="app">
    <div class="pos-a t-0 l-0 bgc-white w-100 h-100 d-f fxd-r fxw-w ai-c jc-c pos-r p-30">
        <div class="mR-60"><img alt="#" src="${pageContext.request.contextPath}/Images/500.png"></div>
        <div class="d-f jc-c fxd-c">
            <h1 class="mB-30 fw-900 lh-1 c-red-500" style="font-size:60px">500</h1>
            <h3 class="mB-10 fsz-lg c-grey-900 tt-c">Internal server error</h3>Something goes wrong with our servers, please try again later.
            <p class="mB-30 fsz-def c-grey-700"></p>
            <div>
                <a href="index" type="primary" class="btn btn-primary">Go to Home</a>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="${pageContext.request.contextPath}/JS/vendor.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/JS/bundle.js"></script>
</body>
</html>
