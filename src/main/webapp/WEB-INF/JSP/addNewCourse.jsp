<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>Add New Course</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">
                
                <c:if test="${status=='SUCCESS'}">
                    <div class="alert alert-success alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${status}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> New Book is Successfully Added.
                    </div>
                </c:if>
                
                <c:if test="${message != null}">
                    <div class="alert alert-danger alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${message}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Danger!</strong> <b>File Size</b> is more than Specified Size.
                    </div>
                </c:if>
                
                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Add New Course</h4>
                    <div class="mT-30">
                        <form method="post" action="./addNewCourse" name="addCourse" enctype="multipart/form-data">
                            <!-- for sweet alert -->
                            <input type="hidden" id="status" value="${status}">
                            
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputCourseType"> Course Type </label>
                                    <select  name="courseType" id="courseType" class="form-control">
                                        <option value="" selected hidden> --Choose Course Type-- </option>
                                        <%String course[] = {"Academic Course",
                                                "Vocational Course",
                                                "Professinal"};
                                            for (int i = 0; i < 3; i++) {
                                        %>
                                        <option><%= course[i]%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputEducationQualification"> Education Qualification </label>
                                    <select  name="education" id="education" class="form-control">
                                        <option value="" selected hidden> --Choose Education Qualification-- </option>
                                        <%String education[] = {"After PostGraduate",
                                                "After Graduate",
                                                "After Higher Secondary"};
                                            for (int i = 0; i < 3; i++) {
                                        %>
                                        <option><%= education[i]%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputStream"> Select Stream </label>
                                    <select  name="stream" id="stream" class="form-control">
                                        <option value="" selected hidden> --Choose Stream Type-- </option>
                                        <%String stream[] = {"Science",
                                                "Arts",
                                                "Comarce",
                                                "Medical",
                                                "Engineering"};
                                            for (int i = 0; i < 5; i++) {
                                        %>
                                        <option><%= stream[i]%></option>
                                        <%}%>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputDescription"> Description of Course </label>
                                    <textarea name="description" id="description" class="form-control" placeholder="Course Details" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputAttachedFile"> Attachment File </label>
                                    <input type="file"  class="form-control" name="attachmentFile" id="attachmentFile" placeholder="Attached the File" accept="image/jpeg,image/gif,image/png,application/pdf">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputUrl"> Enter url </label>
                                    <input type="text" class="form-control" name="url" id="url" value="https://" style="padding: 9px;">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block mT-30">Add Course</button>
                                </div>
                                <div class="form-group col-md-4">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of e-Links</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Course Type</th>
                                    <th>Education Qualification</th>
                                    <th>Select Stream</th>
                                    <th>Description</th>
                                    <th>Attached File</th>
                                    <th>url</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            "ajax": "./getAllCourse",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "scrollX": true,
            columns: [

                {data: "id", "visible": false},
                {data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "courseType"},
                {data: "education"},
                {data: "stream"},
                {data: "description"},
                {data: "attachment",
                    render: function (data, type, row, meta) {
                        return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '">' + data.substr(data.indexOf("r") + 2) + '</a>';
                    }
                },
                {data: "url",
                    render: function (data, type, row, meta) {
                        return '<a href="' + data + '"><button type="button" class="btn btn-info">' + 'view' + '</button></a>';
                    }
                }

            ]
        });
        $('#dTable tbody').on('click', 'a', function (e) {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
    });
</script>
<script src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display','none');
            }, 5000);
        }
    });
</script>