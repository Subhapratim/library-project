<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="Tiles" %>

<html>
    <head>

    </head>
    <body>
        <Tiles:insertAttribute name="userHeader" />
        <Tiles:insertAttribute name="body" />
        <Tiles:insertAttribute name="footer" />
    </body>
</html>