<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="Tiles" %>
<%@page import="com.example.Demo1.Entity.User" %>

<html>
    <head>
        
    </head>
    <body>
        <%  User user = (User)session.getAttribute("UserSession"); %>
        
        <% if(user.getRoleId().roleName.equals("User")) {%>
            <Tiles:insertAttribute name="userHeader" />
            <Tiles:insertAttribute name="body" />
            <Tiles:insertAttribute name="footer" />
        <% } %>
        
        <% if(user.getRoleId().roleName.equals("Admin")) {%>
            <Tiles:insertAttribute name="adminHeader" />
            <Tiles:insertAttribute name="body" />
            <Tiles:insertAttribute name="footer" />
        <% } %>
    </body>
</html>