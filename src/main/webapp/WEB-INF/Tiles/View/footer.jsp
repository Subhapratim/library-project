<footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
    <span>Copyright � 2017 Designed by 
        <a href="http://dos-infotech.com/" target="_blank" title="Dos_Infotech">Dos Infotech</a>. All rights reserved.</span>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
    </script>
</footer>
</div>
</div>

<!--<script src="${pageContext.request.contextPath}/JS/SweetAlert/sweetalert.min.js"></script>-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/vendor.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/bundle.js"></script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
